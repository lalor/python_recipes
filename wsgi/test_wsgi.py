#!/usr/bin/python
#-*- coding: UTF-8 -*-

from eventlet import wsgi
import eventlet


def hello_word(env, start_response):

    print env
    print type(env)
    print start_response
    print type(start_response)
    start_response('200 OK', [('Content-Type', 'text/plain')])
    return ['Hello, World!\r\n']

wsgi.server(eventlet.listen(('', 8090)), hello_word)
