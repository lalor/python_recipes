#!/usr/bin/python


import os
import sys
import time
import traceback
import subprocess
import json
import urllib
import socket

cfg = {
    'rds-meta-url': '127.0.0.1:9023',
    'check-seconds': 1,
    'haproxy-dir': '/root/workspace/topserver/haproxy-conf',
    'start_proxy_port': 10500,
}

def current_tenant_id():
    with open('/etc/vm_monitor/info') as fd:
        data = fd.read()
        return json.loads(data)['ori_user']

#
# Global variables.
#
HAPROXYD_META_JSON = 'haproxyd-meta.json'
CURRENT_TENANT_ID = current_tenant_id()



def default_haproxyd_meta_json():
    return os.path.join(cfg['haproxy-dir'], HAPROXYD_META_JSON)

def exec_cmd(cmd):
    p = subprocess.Popen(cmd,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         shell=True)
    stdout, stderr = p.communicate()
    if p.returncode != 0:
        return p.returncode, stderr
    else:
        return p.returncode, stdout


def check_port_available(port):
    s = socket.socket()
    try:
        s.connect(('127.0.0.1', port))
    except:
        return True
    finally:
        # close socket.
        try:
            s.close()
        except:
            pass
    # some process is using the port, so we can not use this port again.
    return False


def fetch_haproxyd_pid():
    cmd = "ps aux | grep haproxyd | grep -v grep | awk '{print $2}' | head -n 1"
    ret, out = exec_cmd(cmd)
    if ret != 0:
        print 'debug: ', 'execute command failed: ', cmd, 'error: ', out
    slices =  out.strip().split('\n')
    if len(slices) >= 1:
        for it in slices:
            if int(it) != os.getpid():
                return int(it)
    return -1

def request_list_all_dbinstance():
    '''
    DBInstance JSON Format Example:

        {
            "dbInstanceIdentifier": "zy-test-122218-readreplica1:d23f65c8b77345b59b86f82b6c8041fc", 
            "instanceEndpoint": "10.180.156.180", 
            "port": "3306", 
            "userProductId": "d23f65c8b77345b59b86f82b6c8041fc"
        }

    '''

    url = 'http://%s/?action=list_all_dbinstance' % cfg['rds-meta-url']
    try:
        fd = urllib.urlopen(url)
        data = fd.read()
        fd.close()
        dbinstances = json.loads(data)
        return dbinstances
    except Exception as e:
        print traceback.format_exc()
        raise e

def request_to_save_proxy_port(db_meta):
    assert isinstance(db_meta, DBMeta)

    # format key values pairs
    pairs = []
    for key, val in db_meta.to_dict().iteritems():
        pairs.append('%s=%s' % (key, urllib.quote(str(val))))

    # format http request url.
    pairs_str = '&'.join(pairs)
    url = 'http://%s/?action=save_proxy_port&%s' % (
                cfg['rds-meta-url'],  pairs_str)

    try:
        fd = urllib.urlopen(url)
        data = fd.read()
        fd.close()
    except Exception as e:
        print traceback.format_exc()
        return False
    else:
        return True


def request_to_save_proxy_port_with_retries(db_meta):
    tries = 0
    while tries < 3:
        try:
            request_to_save_proxy_port(db_meta)
        except Exception as e:
            print traceback.format_exc()
            tries += 1
            time.sleep(1)
        else:
            return
    print 'debug: ', 'save proxy port failed. retries >= 3 times.', \
          'dbInstanceIdentifier: ', db_meta.get_instance_name()


def check_haproxy_cmd_exists():
    ret, _ = exec_cmd('which haproxy')
    return ret == 0


HAPROXY_CONF = '''
frontend %(dbInstanceIdentifier)s
        bind 0.0.0.0:%(proxyPort)s
        mode tcp
        timeout client 3600s
        backlog 5
        maxconn 5
        default_backend  %(dbInstanceIdentifier)s-backend

backend %(dbInstanceIdentifier)s-backend
        mode  tcp
        server web1 %(instanceEndpoint)s:%(port)s
        timeout connect 1s
        timeout queue 5s
        timeout server 3600s
'''


class DBMeta(object):

    def __init__(self, conf_dir, db_dict):
        # Directory where haproxy-<dbinstanceIdentifer>.conf file save.
        self.conf_dir = conf_dir

        # dbinstance's meta data.
        self.dbInstanceIdentifier = db_dict['dbInstanceIdentifier']
        self.instanceEndpoint = db_dict['instanceEndpoint']
        self.port = db_dict['port']
        self.userProductId = db_dict['userProductId']
        self.proxyPort = None
        self.proxyHost = '127.0.0.1'

    def get_instance_name(self):
        if self.dbInstanceIdentifier is None:
            return ''

        if ':' in self.dbInstanceIdentifier:
            return self.dbInstanceIdentifier.split(':')[0]

        # not : found in dbInstanceIdentifier string.
        return self.dbInstanceIdentifier

    def is_haproxy_conf_exists(self):
        return os.path.isfile(self.haproxy_conf_file_path())

    def save_haproxy_conf(self, proxy_port):
        # update cache first
        self.proxyPort = proxy_port

        content = HAPROXY_CONF % {
            'dbInstanceIdentifier': self.dbInstanceIdentifier,
            'port': str(self.port),
            'proxyPort': self.proxyPort,
            'proxyHost': self.proxyHost,
            'instanceEndpoint': self.instanceEndpoint,
        }
        full_path = self.haproxy_conf_file_path()
        with open(full_path, 'w') as fd:
            fd.write(content)

    def haproxy_conf_file_path(self):
        return os.path.join(self.conf_dir, self.get_instance_name())

    def start_haproxy(self):
        cmd = 'haproxy -D -f %s' % (self.haproxy_conf_file_path(), )
        ret, err_msg = exec_cmd(cmd)
        if ret != 0:
            raise Exception('execute command failed: %s , err_msg: %s' % (cmd, err_msg))

    def to_dict(self):
        return {
            'dbInstanceIdentifier': self.dbInstanceIdentifier,
            'port': str(self.port),
            'proxyPort': self.proxyPort,
            'proxyHost': self.proxyHost,
            'userProductId': self.userProductId,
            'instanceEndpoint': self.instanceEndpoint,
            'haproxyConf': self.haproxy_conf_file_path(),
        }


class HaProxyMeta(object):

    '''
    json format example:
        {
            'dbinstance_identifier': {
                'proxy_port': 10500,
                'dbinstance_port': 3306,
                'dbinstance_host': '127.0.0.1',
                'haproxy_conf': '',
            },
        }
    '''

    def __init__(self, meta_json_file):
        self._meta_dict = {}

        self.meta_json_file = meta_json_file
        self._load_json(meta_json_file)

    def _load_json(self, meta_json_file):
        if os.path.isfile(meta_json_file):
            data = ''
            with open(meta_json_file) as fd:
                data = fd.read()
            try:
                self._meta_dict = json.loads(data)
            except Exception as e:
                print 'not json file: %s' % (meta_json_file, )
        else:
            print 'meta_json_file not exists: %s' % meta_json_file

    def _dumps_json(self):
        with open(self.meta_json_file, 'w+') as fd:
            fd.write(json.dumps(self._meta_dict, indent=4))

    def update(self, db_meta):
        assert isinstance(db_meta, DBMeta)

        name = db_meta.get_instance_name()
        self._meta_dict[name] = db_meta.to_dict()

        self._dumps_json()

    def remove(self, db_name):
        assert isinstance(db_name, basestring)

        if db_name in self._meta_dict:
            self._meta_dict.pop(db_name)

        self._dumps_json()

    def add(self, db_meta):
        assert isinstance(db_meta, DBMeta)
        self.update(db_meta)

    def max_proxy_port(self):
        max_port = int(cfg['start_proxy_port'])
        for name, db_dict in self._meta_dict.iteritems():
            if 'proxyPort' in db_dict and db_dict['proxyPort']:
                max_port = max(max_port, int(db_dict['proxyPort']))
        return max_port

class HaProxyManager(object):

    def __init__(self, work_dir):
        self.work_dir = work_dir

    def start_haproxy(self, db_meta):
        assert isinstance(db_meta, DBMeta)
        db_meta.start_haproxy()

    def shutdown_haproxy(self, db_name):
        assert isinstance(db_name, basestring)

        # if db_meta is not exists in haproxy,  then, we do nothing.
        if not self.check_haproxy_exists(db_name):
            return 

        # fetch the haproxy pid.
        name2pid = self.list_all_haproxy_name()
        if db_name not in name2pid:
            return
        pid = name2pid[db_name]

        # kill fetch pid proccess for shutdown haproxy.
        cmd = 'kill -9 %s' % pid
        ret, err_msg = exec_cmd(cmd)
        if ret !=0 :
            print 'debug: ', 'shutdown haproxy for dbinstance: ', \
                   db_name, 'failed, err_msg', err_msg, 'pid: ', pid

        if self.check_haproxy_exists(db_name):
            print 'debug: ', 'haproxy is still exists after kill.', \
                   'dbinstance: ', db_name, 'pid:', pid
            return


    def check_haproxy_exists(self, db_name):
        assert isinstance(db_name, basestring)

        for name in self.list_all_haproxy_name():
            if name == db_name:
                return True

        return False

    def list_all_haproxy_name(self):
        cmd = "ps aux | grep -i haproxy | grep -v grep | grep -v haproxyd | awk '{print $2,$14}' "
        ret, out = exec_cmd(cmd)
        if ret != 0:
            print 'debug: ', 'execute command failed', 'cmd: ', \
                  cmd, 'reason: ', out
        # dict cache haproxy name mapping to process id (pid)
        name2pid = {}
        if out is not None :
            out = out.strip().strip('\n').strip()
            for line in out.split('\n'):
                if line is not None:
                    # example line:
                    #   12580 dev2hu-no-ha-private4
                    line = line.strip()
                    # filter all empty string.
                    items = [it for it in line.split() if it.strip() != '']
                    # check items length is 2.
                    if len(items) != 2:
                        continue
                    pid, name = items[0], os.path.basename(items[1])
                    name2pid[name] = pid
        return name2pid


class DaemonProcess(object):

    def __init__(self):
        self.conf_dir = cfg['haproxy-dir']
        self.haproxy_meta = HaProxyMeta(default_haproxyd_meta_json())
        self.haproxyd = HaProxyManager(self.conf_dir)

    def start(self):
        while True:
            try:
                self.do_task()
            except Exception, e:
                print traceback.format_exc()
            finally:
                time.sleep(int(cfg['check-seconds']))

    def do_task(self):
        dbinstance_list = request_list_all_dbinstance()
        for db_dict in dbinstance_list:
            try:
                self.handle_dbinstance(db_dict)
            except Exception as e:
                print traceback.format_exc()

        # try to clear haproxy for deleted dbInstance.
        self.try_shutdown_deleted_dbinstance(dbinstance_list)

    def handle_dbinstance(self, db_dict):
        if 'userProductId' not in db_dict:
            return

        #if db_dict['userProductId'] != CURRENT_TENANT_ID:
        #    print 'tenantId is not equals skip.'
        #    return

        db_meta = DBMeta(self.conf_dir, db_dict)
        #db_meta.save_haproxy_conf(DaemonProcess._new_listen_port())
        if self.haproxyd.check_haproxy_exists(db_meta.get_instance_name()):
            return
        else:
            # save haproxy conf.
            db_meta.save_haproxy_conf(self._new_listen_port())

            # start haproxy process
            self.haproxyd.start_haproxy(db_meta)

            # update haproxy meta
            self.haproxy_meta.update(db_meta)

            # request to rds meta http server to update proxy port.
            request_to_save_proxy_port_with_retries(db_meta)

    def try_shutdown_deleted_dbinstance(self, dbinstance_list):
        assert isinstance(dbinstance_list, list)

        db_meta_dict = {}
        for db_dict in dbinstance_list:
            db_meta = DBMeta(self.conf_dir, db_dict)
            db_meta_dict[db_meta.get_instance_name()] = db_meta

        for name in self.haproxyd.list_all_haproxy_name():
            if name not in db_meta_dict:
                print 'shutdown haproxy for dbinstance, dbinstance_name', name

                # shutdown haproxy.
                self.haproxyd.shutdown_haproxy(name)

                # update haproxy meta
                self.haproxy_meta.remove(name)

                # remove haproxy configuration file
                cfg_file = os.path.join(self.conf_dir, name)
                if os.path.isfile(cfg_file):
                    os.unlink(cfg_file)

    def _new_listen_port(self):
        '''
        Generate new haproxy listen port.
        '''
        new_port = self.haproxy_meta.max_proxy_port()
        while True:
            new_port = new_port + 1

            # new listen port is exhausted.
            if new_port >= 65535:
                print 'listen port exhausted. new_port >= 65535'
                sys.exit(1)

            # check port whether is available.
            if check_port_available(new_port):
                return new_port


def validate_env():

    # check haproxy whether be installed.
    if not check_haproxy_cmd_exists():
        print 'debug: ', 'command not found: haproxy', 

    assert os.path.isdir(cfg['haproxy-dir'])
    assert int(cfg['start_proxy_port'])


    # check haproxyd is exits
    pid = fetch_haproxyd_pid()
    if pid != -1:
        print 'debug: ', 'haproxyd is running. pid:', pid
        sys.exit(1)

    #TODO 
    # check all options in configuration file.
    # if error exists, then sys.exit(1)

def main():

    # check configuration option
    validate_env()

    # start daemon process.
    d = DaemonProcess()
    d.start()

if __name__ == '__main__':
    main()
