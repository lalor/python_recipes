"""
    example
"""
#!/usr/bin/python
#-*- coding: UTF-8 -*-
import traceback
import eventlet
from eventlet import wsgi


class HttpServer(object):
    def __init__(self, host, port, backlog=50, thread_size=100):
        self.host = host
        self.port = port

        self.backlog = backlog
        self.thread_size = thread_size

    def start(self, wsgi_app):
        # address
        address = (self.host, self.port)

        # sever socket
        self.srv_sock = eventlet.listen(address, backlog=200)

        wsgi.server(self.srv_sock, wsgi_app, max_size=self.thread_size)

class Application(object):

    def __call__(self, env, start_response):
        resp_header = [('Content-Type', 'application/json')]
        try:
            if not 'QUERY_STRING' in env:
                start_response('400 Bad Request', resp_header)
                return ['Invalid Action']

            qs = env['QUERY_STRING']
            print qs
            print type(qs)

            query_dict = qs

            if 'action' not in query_dict:
                start_response('400 Bad Request', resp_header)
                return ['Must have action param']

            start_response('200 OK', resp_header)
            return [ query_dict ]
        except Exception as e:
            traceback.print_exc()
            start_response('500 InternalError', resp_header)
            return ['Internal Error']

#    def convert_dict_to_key_val(self, qs):
#        return "".join([ "{0}={1}".format(key, val) for key, val in qs] )

def main():
    srv = HttpServer('0.0.0.0', 9023)
    app = Application()
    srv.start(app)

if __name__ == '__main__':
    main()
