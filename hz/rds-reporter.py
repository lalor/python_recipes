# -*- coding: utf8 -*-
#! /usr/local/bin/python

import sys
import os
import re
import time
import datetime

import pymysql
import threading

from smtplib import SMTP_SSL as SMTP       # this invokes the secure SMTP protocol (port 465, uses SSL)
# from smtplib import SMTP                  # use this for standard SMTP protocol   (port 25, no encryption)
from email.MIMEText import MIMEText


cfg = {
    'mysql_user': 'pubcloud_rds',
    'mysql_host': '10.166.3.6',
    'mysql_port': 4331,
    'mysql_database': 'pub_rds',
    'mysql_password': 'QjdI92PsSoz',
    'send_email': 'ddb_hz@corp.netease.com',
    'send_username': 'ddb_hz',
    'send_password': 'wsPps5:WXYK(',
    'stmp_server': 'corp.netease.com',
    'destination': [
        'hzlaimingxing@corp.netease.com',
        'hzhuzheng@corp.netease.com',
        'hzguoyi@corp.netease.com',
        'hzwenzhh@corp.netease.com',
        'hzxuhan@corp.netease.com',
        'hzzhao_yan@corp.netease.com',
        'hzxiejun@corp.netease.com',
        'hzzhaotianyuan@corp.netease.com',
        'rds-dev@corp.netease.com',
    ],
    'create_report_time': [
        '09:00:00',
        '18:00:00',
    ]
}



def get_sysdb_conn():
    conn = pymysql.Connect(user=cfg['mysql_user'],
                           password=cfg['mysql_password'],
                           host=cfg['mysql_host'],
                           port=cfg['mysql_port'],
                           database=cfg['mysql_database'],
                           charset='utf8')
    return conn


def java_unix_time_to_readable_str(java_unix_ts):
    unix_ts = int(java_unix_ts) / 1000
    dt = datetime.datetime.fromtimestamp(int(unix_ts))
    return dt.strftime('%Y-%m-%d %H:%M:%S')


def get_datetime_now_readable_str():
    dt = datetime.datetime.now()
    return dt.strftime('%Y-%m-%d %H:%M:%S')

def get_date_now_readable_str():
    dt = datetime.datetime.now()
    return dt.strftime('%Y-%m-%d')

def get_hour_now_readable_str():
    dt = datetime.datetime.now()
    return dt.strftime('%H:%M:%S')


class DBInstance:

    def __init__(self):
        self.dbInstanceIdentifier = None
        self.userProductId = None
        self.isHA = None
        self.networkType = None
        self.instanceEndpoint = None
        self.port = None
        self.instanceCreateTime = None
        self.instanceStatus = None


INSTANCE_STATUS_DICT = {
    1: 'AVAILABLE',
    2: 'CREATING',
    3: 'RESTORING',
    4: 'BACKUPING',
    5: 'DELETING',
    6: 'DELETED',
    7: 'MODIFYING',
    8: 'REBOOTING',
    9: 'STARTING',
    10: 'RESTORING',
    11: 'REVOCERING_MASTER_BLOCK',
    12: 'HOTSWAPING',
    13: 'MAINTAIN',
    14: 'BACKUPING_AFTER_RECOVER',
    15: 'IMPORTING',
    16: 'IMPORTING_FAILED',
    17: 'PROMOTING_READ_REPLICA',
    18: 'WAIT_RECOVER_READ_REPLICA',
    19: 'RECOVERING_READREPLICA_WITHOUT_MOVE_IP',
    21: 'RECOVERING_WITH_RELAYLOG',
    22: 'PURGE_RELAY_LOG',
    23: 'SCHEMA_CHANGING',
    24: 'UPGRADE_AGENT',
    26: 'SWITCHING_TO_NON_SAFE_MODE',
    27: 'PROXY_MASTER_RECOVERING',
    28: 'PROXY_SLAVE_RECOVERING',
    29: 'SWITCHING_TO_SAFE_MODE',
}

def instance_status_to_string(instance_status_code):
    if isinstance(instance_status_code,int) and \
        instance_status_code in INSTANCE_STATUS_DICT:
        return INSTANCE_STATUS_DICT[instance_status_code].lower()
    else:
        return 'unknown code: %s' % (instance_status_code, )

def parseDBInstance(row):
    db = DBInstance()

    # dbinstanceIdentifier
    db.dbInstanceIdentifier = row[0]
    if row[0] != None and len(row[0].split(':')) == 2:
        db.dbInstanceIdentifier = row[0].split(':')[0]

    # userProductId
    db.userProductId = row[1]

    # isHA
    db.isHA = row[2]
    if db.isHA == 1:
        db.isHA = 'true'
    elif db.isHA == 0:
        db.isHA = 'false'

    # networkType
    db.networkType = row[3]
    if db.networkType == 0:
        db.networkType = 'intranet'
    elif db.networkType == 1:
        db.networkType = 'public'
    elif db.networkType == 2:
        db.networkType = 'private'

    # instanceEndpoint
    db.instanceEndpoint = row[4]

    # port
    db.port = row[5]

    # instanceCreateTime
    db.instanceCreateTime = java_unix_time_to_readable_str(row[6])

    # instanceStatus
    db.instanceStatus = instance_status_to_string(row[7])

    return db


def fetch_all_db_instance():
    dbinstance_list = []
    with get_sysdb_conn() as cursor:
        cursor.execute('select dbInstanceIdentifier, \
                               userProductId, \
                               isHA, \
                               networkType, \
                               instanceEndpoint, \
                               port, \
                               instanceCreateTime,\
                               instanceStatus \
                               from dbinstance')
        for row in cursor:
            dbInstance = parseDBInstance(row)
            if dbInstance.instanceStatus != 'available':
                dbinstance_list.append(dbInstance)
    return dbinstance_list



class EmailManager(object):

    @staticmethod
    def send_email(text_subtype, subject, content):
        # format message.
        msg = MIMEText(content, text_subtype)
        msg['Subject'] = subject
        msg['From'] = cfg['send_email']
        msg.set_charset('utf8')

        # Get email connection.
        conn = SMTP(cfg['stmp_server'])
        conn.set_debuglevel(False)

        # Login smtp server
        conn.login(cfg['send_username'], cfg['send_password'])
        try:
            conn.sendmail(cfg['send_email'],
                          cfg['destination'],
                          msg.as_string())
        finally:
            conn.close()


def email_template(dbinstance_list):
    content = '''
<html>
    <style type="text/css">
    table, td, th
      {
        border:1px solid black;
      }
    td
      {
        padding:10px;
      }
    </style>

    <p>请注意<font color=red>公有云环境</font>以下实例处于<font color=red>异常</font>状态， 报告时间: %s</p>
    <br>
    <br>
    <table border=1 cellspacing=0 cellpadding=0>
        <tr>
            <td>实例名</td>
            <td>实例状态</td>
            <td>实例创建时间</td>
            <td>租户</td>
            <td>网络类型</td>
            <td>是否高可用</td>
        </tr>
        %s
    </table>
</html>
    '''
    buf = []
    for dbinstance in dbinstance_list:
        sub = '''
        <tr>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
        </tr>
        ''' % (dbinstance.dbInstanceIdentifier, 
               dbinstance.instanceStatus,
               dbinstance.instanceCreateTime,
               dbinstance.userProductId,
               dbinstance.networkType,
               dbinstance.isHA,)

        buf.append(sub)

    content = content % (
                   get_datetime_now_readable_str(),
                   ''.join(buf).encode('utf8'),
               )
    return content

def report_task():
    text_subtype = 'html'
    subject = '%s RDS公有云(yun.163.com)健康报告' % get_date_now_readable_str()


    dbinstance_list = fetch_all_db_instance()
    content = email_template(dbinstance_list)

    EmailManager.send_email(text_subtype, subject, content)


REPORT_MAP =  dict([(hour_str, None) for hour_str in cfg['create_report_time'] ])

def should_report():

    hour_str = get_hour_now_readable_str()
    date_str = get_date_now_readable_str()

    if hour_str in cfg['create_report_time'] and date_str != REPORT_MAP[hour_str]:
        REPORT_MAP[hour_str] = date_str
        return True
    return False


def main():
    while True:
        try:
            if should_report():
                report_task()
        except Exception as e:
            pass
        time.sleep(0.1)

if __name__ == '__main__':
    main()
