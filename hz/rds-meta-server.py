#!/usr/bin/python
import eventlet
from eventlet import wsgi
eventlet.monkey_patch(all=True)

import urllib
import urlparse
import json
import pymysql
import traceback

cfg = {
    'mysql_user': 'rds',
    'mysql_password': 'rds3316',
    'mysql_host': '127.0.0.1',
    'mysql_port': 3306,
    'mysql_database': 'rds',
    'mysql_charset': 'utf8',
}

class DBInstance(object):

    def __init__(self):
        self.dbInstanceIdentifier = None
        self.instanceEndpoint = None
        self.userProductId = None
        self.port = None
        self.proxyPort = None
        self.proxyHost = None

    @staticmethod
    def parse_from_dict(cursor):
        self = DBInstance()
        self.dbInstanceIdentifier = cursor['dbInstanceIdentifier']
        self.instanceEndpoint = cursor['instanceEndpoint']
        self.userProductId = cursor['userProductId']
        self.port = cursor['port']

        if 'proxyPort' not in cursor:
            self.proxyPort = 3306
        else:
            self.proxyPort = cursor['proxyPort']

        if 'proxyHost' not in cursor:
            self.proxyHost = '127.0.0.1'
        else:
            self.proxyHost = cursor['proxyHost']

        return self

    def to_dict(self):
        return {
            'dbInstanceIdentifier': self.dbInstanceIdentifier,
            'port': self.port,
            'proxyPort': self.proxyPort,
            'proxyHost': self.proxyHost,
            'instanceEndpoint': self.instanceEndpoint,
            'userProductId': self.userProductId,
        }

def get_mysql_conn():
    return pymysql.Connect(user=cfg['mysql_user'],
                           password=cfg['mysql_password'],
                           host=cfg['mysql_host'],
                           port=cfg['mysql_port'],
                           charset=cfg['mysql_charset'],
                           database=cfg['mysql_database'],
                           cursorclass=pymysql.cursors.DictCursor)


def fetch_dbinstance_list():
    db_instance_list = []
    sql = 'select * from dbinstance'
    with get_mysql_conn() as cursor:
        cursor.execute(sql)
        for row in cursor:
            dbins = DBInstance.parse_from_dict(row)
            db_instance_list.append(dbins)

    # return dbinstance list.
    return db_instance_list


def save_proxy_port(dbInstance):
    assert isinstance(dbInstance, DBInstance)
    sql0 = "select * from haproxyMapping where dbInstanceIdentifier = '%(dbInstanceIdentifier)s'"
    sql1 = "update haproxyMapping set proxyHost = '%(proxyHost)s', proxyPort='%(proxyPort)s' where dbInstanceIdentifier='%(dbInstanceIdentifier)s' "
    sql2 = "insert into haproxyMapping(dbInstanceIdentifier, proxyHost, proxyPort) values('%(dbInstanceIdentifier)s', '%(proxyHost)s', %(proxyPort)s)"
    with get_mysql_conn() as cursor:
        cursor.execute(sql0 % dbInstance.to_dict())
        rows = [row for row in cursor]
        if len(rows) == 0:
            cursor.execute(sql2 % dbInstance.to_dict())
        else:
            cursor.execute(sql1 % dbInstance.to_dict())


class HttpServer(object):
    def __init__(self, host, port, backlog=50, thread_size=100):
        self.host = host
        self.port = port

        self.backlog = backlog
        self.thread_size = thread_size

    def start(self, wsgi_app):
        """
        :param wsgi_app:
             app(environ, start_response)
        :return:
        """
        # address: (host, port) pair
        address = (self.host, self.port)
        # server socket
        self.srv_sock = eventlet.listen(address, backlog=500)

        wsgi.server(self.srv_sock, wsgi_app, max_size=self.thread_size)


def parse_from_query_dict(query_dict):
    qs_dict = {}
    for key, vals in query_dict.iteritems():
        qs_dict[key] = urllib.unquote(vals[0])
    return qs_dict

class Application(object):

    def __call__(self, env, start_response):
        resp_headers = [('Content-Type', 'application/json')]
        try:
            if not 'QUERY_STRING' in env:
                start_response('400 Bad Request', resp_headers)
                return [ 'Invalid Action' ]

            qs = env['QUERY_STRING']
            query_dict = urlparse.parse_qs(qs)

            if 'action' not in query_dict:
                start_response('400 Bad Request', resp_headers)
                return [ 'Invalid Action' ]

            qs_dict = parse_from_query_dict(query_dict)

            response_json_bytes = self.handle_qs_dict(qs_dict)
            start_response('200 OK', resp_headers)
            return [response_json_bytes]
        except Exception as e:
            traceback.print_exc()
            start_response('500 InternalError', resp_headers)
            return [ 'InternalError: %s' % str(e) ]

    def handle_qs_dict(self, qs_dict):
        action = qs_dict['action']
        if action == 'list_all_dbinstance':
            return self.do_list_all_dbinstance(qs_dict)
        elif action == 'save_proxy_port':
            return self.do_save_proxy_port(qs_dict)
        else:
            return 'Invalid Action. %s no supported' % action

    def do_list_all_dbinstance(self, qs_dict):
        db_json_array = []

        db_instance_list = fetch_dbinstance_list()
        for dbins in db_instance_list:
            db_json = dbins.to_dict()
            db_json_array.append(db_json)

        response_json_bytes = json.dumps(db_json_array)
        return response_json_bytes

    def do_save_proxy_port(self, qs_dict):
        dbInstance = DBInstance.parse_from_dict(qs_dict)
        save_proxy_port(dbInstance)
        return 'OK'

def main():
    srv = HttpServer('0.0.0.0', 9024)
    app = Application()
    srv.start(app)

if __name__ == '__main__':
    main()
