import json

meta_json = {
            "vm_info": {
                "server_uuid": None,
                "floating_ip": None,
                "db_instance_identifier": None,
                "monitor_access_key": None,
                "monitor_secret_key": None,
                "monitor_server_ip": None,
                "user_product_id": None,
                "resource_id": None
            },
            "communicate": {
                "version": None,
                "status": None,
                "request_msg_body": None,
                "response_msg_body": None,
            }
}

print json.dumps(meta_json, indent=4)
