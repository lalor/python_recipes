# -*- coding: utf8 -*-

__author__ = 'hzhuzheng'

import os
import re
import threading
import json
import uuid
import subprocess


def generate_uuid_str():
    return str(uuid.uuid4())


def byteify(input):
    """
      当input为str类型时，通过json.loads(input)获取到的dict, dict的key,value全部都会变成unicode类型。
      导致打印日志过程中各种出错。所以我们在这里为了统一处理，在执行json.loads获取dict之后，把所有的unicode转换成str类型。
    """
    if isinstance(input, dict):
        return {byteify(key): byteify(value) for key,value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input


def json_encode(_dict, indent=None):
    return json.dumps(_dict, indent=indent)


def json_decode(_str):
    _dict = json.loads(_str)
    return byteify(_dict)


def execute_cmd(cmd):
    p = subprocess.Popen(cmd,
                         shell=True,
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    if p.returncode != 0:
        return p.returncode, stderr
    return p.returncode, stdout


def lower_case_with_underscores(name):
    """
    link: http://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-camel-case
    :param name:
    :return:
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def is_int_type(x):
    try:
        int(x)
    except:
        return False
    else:
        return True


class ProxyAgentMeta(object):

    MSG_DONE = "DONE"
    MSG_SENDING = "SENDING"
    MSG_DOING = "DOING"

    def __init__(self,
                 meta_file,
                 server_uuid=None,
                 floating_ip=None,
                 db_instance_identifier=None,
                 monitor_access_key=None,
                 monitor_secret_key=None,
                 monitor_server_ip=None,
                 user_product_id=None,
                 resource_id=None,
                 version=0):

        # check file path is exists.
        # if not os.path.isfile(meta_file):
        #     raise Exception('proxy agent meta file is not exits: %s' % meta_file)
        self.meta_file = meta_file

        # initialize threading.Lock()
        self.fd_lock = threading.Lock()
        self.update_lock = threading.Lock()

        # initialize all properties.
        self.server_uuid = server_uuid
        self.floating_ip = floating_ip
        self.db_instance_identifier = db_instance_identifier
        self.monitor_access_key = monitor_access_key
        self.monitor_secret_key = monitor_secret_key
        self.monitor_server_ip = monitor_server_ip
        self.user_product_id = user_product_id
        self.resource_id = None

        self.version = version
        self.status = ProxyAgentMeta.MSG_DONE
        self.request_msg_body = None
        self.response_msg_body = None

        if not os.path.isfile(self.meta_file):
            self.save()
        else:
            self.load()

    def save(self):
        meta_json = {
            "vm_info": {
                "server_uuid": self.server_uuid,
                "floating_ip": self.floating_ip,
                "db_instance_identifier": self.db_instance_identifier,
                "monitor_access_key": self.monitor_access_key,
                "monitor_secret_key": self.monitor_secret_key,
                "monitor_server_ip": self.monitor_server_ip,
                "user_product_id": self.user_product_id,
                "resource_id": self.resource_id
            },
            "communicate": {
                "version": self.version,
                "status": self.status,
                "request_msg_body": self.request_msg_body,
                "response_msg_body": self.response_msg_body,
            }
        }
        meta_json_str = json_encode(meta_json, indent=4)
        try:
            self.fd_lock.acquire()
            with open(self.meta_file, 'w') as fd:
                fd.write(meta_json_str)
        finally:
            self.fd_lock.release()

    def load(self):
        try:
            self.fd_lock.acquire()

            with open(self.meta_file) as fd:
                json_str = fd.read()

            json_dict = util.json_decode(json_str)

            vm_info = json_dict['vm_info']
            communicate = json_dict['communicate']

            self.server_uuid = vm_info['server_uuid']
            self.floating_ip = vm_info['floating_ip']
            self.db_instance_identifier = vm_info['db_instance_identifier']
            self.monitor_access_key = vm_info['monitor_access_key']
            self.monitor_secret_key = vm_info['monitor_secret_key']
            self.monitor_server_ip = vm_info['monitor_server_ip']
            self.user_product_id = vm_info['user_product_id']

            self.version = communicate['version']
            self.status = communicate['status']
            self.request_msg_body = communicate['request_msg_body']
            self.response_msg_body = communicate['response_msg_body']
        finally:
            self.fd_lock.release()

    def update_version(self, version):
        try:
            self.update_lock.acquire()
            self.load()
            self.version = version
            self.save()
        finally:
            self.update_lock.release()

    def update_status(self, status):
        try:
            self.update_lock.acquire()
            self.load()
            self.status = status
            self.save()
        finally:
            self.update_lock.release()

    def update_request_msg_body(self, request_msg_body):
        try:
            self.update_lock.acquire()
            self.load()
            self.request_msg_body = request_msg_body
            self.save()
        finally:
            self.update_lock.release()

    def update_response_msg_body(self, response_msg_body):
        try:
            self.update_lock.acquire()
            self.load()
            self.response_msg_body = response_msg_body
            self.save()
        finally:
            self.update_lock.release()

    def update_msg_request(self, version, status, request_msg_body):
        try:
            self.update_lock.acquire()
            self.load()
            self.version = version
            self.status = status
            self.request_msg_body = request_msg_body
            self.save()
        finally:
            self.update_lock.release()

    def update_msg_response(self, status, response_msg_body):
        try:
            self.update_lock.acquire()
            self.load()
            self.status = status
            self.response_msg_body = response_msg_body
            self.save()
        finally:
            self.update_lock.release()

    def get_floating_ip(self):
        return self.floating_ip

    def get_server_uuid(self):
        return self.server_uuid

    def get_monitor_server_ip(self):
        return self.monitor_server_ip

    def get_monitor_access_key(self):
        return self.monitor_access_key

    def get_monitor_secret_key(self):
        return self.monitor_secret_key

    def get_user_product_id(self):
        return self.user_product_id

    def get_service_type(self):
        # TODO add service type
        return ''

