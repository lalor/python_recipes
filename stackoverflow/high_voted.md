# high voted question

*  [What does the yield keyword do in Python?](http://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do-in-python)

    To master yield, you must understand that when you call the function, the code
you have written in the function body does not run. The function only returns
the generator object, this is a bit tricky :-)

* [Check whether a file exists using Python](http://stackoverflow.com/questions/82831/check-whether-a-file-exists-using-python)
    
        import os.path
        os.path.isfile(fname) 

* [Does Python have a ternary conditional operator?](http://stackoverflow.com/questions/394809/does-python-have-a-ternary-conditional-operator)


        a if test else b

* [What does `if __name__ == “__main__”:` dodo ?](http://stackoverflow.com/questions/419163/what-does-if-name-main-do)

    When the Python interpreter reads a source file, it executes all of the code found in it. Before executing the code, it will define a few special variables. For example, if the python interpreter is running that module (the source file) as the main program, it sets the special __name__ variable to have a value "__main__". If this file is being imported from another module, __name__ will be set to the module's name.
