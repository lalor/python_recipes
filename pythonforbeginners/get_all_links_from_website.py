#!/usr/bin/python
#-*- coding: UTF-8 -*-
import urllib2
import re

# http://www.pythonforbeginners.com/code-snippets-source-code/regular-expression-re-findall/

url = 'http://dblab.xmu.edu.cn'
#connect to a url
website = urllib2.urlopen(url)

#read html code
html = website.read()

#use re.findall to get all the links
links = re.findall('"((http|ftp)s?://.*?)"', html)
for link in links:
    print link[0]
