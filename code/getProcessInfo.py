#!/usr/bin/python
#-*- coding: UTF-8 -*-

import os
def process_list():
    """get process list"""
    pids = []
    for subdir in os.listdir('/proc'):
        if subdir.isdigit() :
            pids.append(subdir)

    return pids


def main():
    """main function"""
    pids = process_list()
    print("total number of running processes:{0}".format(len(pids)))


if __name__ == '__main__':
    main()
