#!/usr/bin/python
#-*- coding: UTF-8 -*-
import smtplib, mimetypes
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase

from email.utils import COMMASPACE, formatdate
from email import encoders
import os, sys


email_list=[]
title=""

def send_email(message, file):
    #email_list=["joy_lmx@163.com"]
    msg = MIMEMultipart()
    msg['From'] = "lmx_test@163.com"
    msg['to'] = ";".join(email_list)
    msg['Subject'] = title
    print ("in send_email:")

    txt = MIMEText(message)
    msg.attach(txt)

    part = MIMEBase('application', 'octet-stream')
    part.set_payload(open(file, 'rb').read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename = "%s"' % os.path.basename(file))
    msg.attach(part)

    smtp = smtplib.SMTP()
    smtp.connect('smtp.163.com:25')
    smtp.login('lmx', 'lmx123')
    smtp.sendmail(msg['From'], email_list, msg.as_string())
    smtp.quit()
    print 'email send succeed!'


def main():
    """docstring for main"""
    if len(sys.argv) != 5 :
    	print ("please input ./sendMail.py [maillist.txt] [Subject] [Message] [Attachment]")
    	return

    f = open(sys.argv[1])
    while True:
    	line = f.readline()
    	if len(line) == 0 : #Zero length indicates EOF
    		break
    	line=line.strip("\n")
    	email_list.append(line)
    f.close()
    print email_list
    global title
    title = sys.argv[2]
    msg = sys.argv[3]
    file = sys.argv[4]
    send_email(msg, file)

if __name__ == '__main__':
    main()

