#!/usr/bin/python
# -*- coding: UTF-8 -*-
import multiprocessing
import random


def compute(n):
    return sum([random.randint(1, 100) for i in range(1000000)])

# Start 8 workers
pool = multiprocessing.Pool(8)
print ("Results:{0}".format(pool.map(compute, range(8))))
# print ("Results:{0}".format(sum(pool.map(compute, range(8)))))
