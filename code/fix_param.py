"""
    fix vm parameter
"""
#!/usr/bin/python
#-*- coding: UTF-8 -*-

import pymysql

def get_conn():
    """ get a connection"""
    kargs={ 'host':'10.180.148.45',
            'user':'rds',
            'password':'rds3316',
            'port':3306,
            'db':'rds'}
    return pymysql.connect(host=kargs['host'], user=kargs['user'],
            password=kargs['password'], port=kargs['port'],
            db=kargs['db'])

def get_vms():
    """ get all vm info from system db"""
    with get_conn() as cursor:
        cursor.execute("select vmId, vmclass, dbParameter from vm")
        vms = cursor.fetchall()
    return vms

def format_boolean(val):
    """
    convert boolean value to ON|OFF
    """
    true_vals = ['1', 1, 'ON', 'on', 'true', 'TRUE']
    false_vals = ['0', 0, 'OFF', 'off', 'false', 'FALSE']

    if true_vals.count(val) == 1:
        return 'ON'
    elif false_vals.count(val) == 1:
        return 'OFF'
    else:
        raise SystemExit('{0} is not a boolean val'.format(val))

def format_parameter(vmclass, parameter):
    """
    format database parameter
    """
    # 1. replase true or false to TRUE or FALSE
    parameter = parameter.replace('true', 'TRUE')
    parameter = parameter.replace('false', 'FALSE')
    parameter = parameter.replace('[mysqld]', '')
    param = parameter.strip('\n').split('\n')

    # convert parameter to dict
    param_dict = {}
    # deal with special param like core-file, skip-slave-start
    single_param = []
    for item in param:
        '''
        innodb_file_per_table
        skip-slave-start
        core-file
        skip_external_locking
        skip-name-resolve
        '''
        if len(item.split('=')) == 1:
            single_param.append(item)
        else:
            key, val = item.split('=')
            param_dict[key] = val


    # 2. formatting boolean parameter
    boolean_args = ['general_log', 'slow_query_log',
            'innodb_print_all_deadlocks', 'sync_binlog',
            'lower_case_table_names']
    for arg in boolean_args:
        param_dict[arg] = format_boolean(param_dict[arg])

    # 3. change innodb_buffer_pool_size to standard size
    # 3. change max_connections to standard size
    if vmclass == 2:
        param_dict['innodb_buffer_pool_size'] = '375390208'
        param_dict['max_connections'] = '89'
#    elif vmclass == 9:
#        param_dict['innodb_buffer_pool_size'] = 536870912 * 2 + ""
#        param_dict['max_connections'] = 'xxxx'

    # final, convert parameter to string
    param_after_format = "[mysqld]\n" + "".join(
            [ item.strip('\n') + '\n' for item in single_param])
    param_after_format += "".join(
            ["{0}={1}\n".format(key, val) for key, val in param_dict.items()])
    return param_after_format.strip('\n')


def fix_param_format(vmid, vmclass, parameter):
    """
    1. get correct parameter
    2. update system database
    """
    parameter = format_parameter(vmclass, parameter)
    with get_conn() as cursor:
        sql = """update vm set dbParameter ="{0}" where vmid = {1}""".format(
                parameter, vmid)
        cursor.execute(sql)

def main():
    """ entry point """
    try:
        vms = get_vms()
        for vm in vms:
            fix_param_format(vm[0], vm[1], vm[2])
    except pymysql.err.ProgrammingError, e:
        raise SystemExit(e)


if __name__ == '__main__':
    main()
