#!/usr/bin/python
#-*- coding: UTF-8 -*-
"""
ConfigParser
"""
from ConfigParser import SafeConfigParser

def main():
    """
    main fucntion
    """

    parser = SafeConfigParser(allow_no_value=True)
    print parser.read('my.cnf')

    #print parser.get('bug_tracker', 'url')

    for section_name in parser.sections():
        print 'Section:', section_name
        print 'Options:', parser.options(section_name)
        for name, value in parser.items(section_name):
            print ' %s = %s' % (name, value)

    print parser.get('mysql', 'user1')
#    parser.add_section('bug_tracker2')
#    parser.set('bug_tracker2', 'url', 'http://localhost:8080/bugs')
#    parser.set('bug_tracker2', 'username', 'dhellmann')
#    parser.set('bug_tracker2', 'password', 'secret')
#
    parser.write(open("my.cnf", "w"))

if __name__ == '__main__':
    main()
