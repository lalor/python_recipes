#!/usr/bin/python
#-*- coding: UTF-8 -*-

from collections import OrderedDict

def Meminfo():
    meminfo=OrderedDict()
    with open('/proc/meminfo') as f:
        for line in f:
            meminfo[line.split(':')[0]] = line.split(':')[1].strip()
    return meminfo


def main():
    """main function"""
    meminfo = Meminfo()
    print('Total memory:{0}'.format(meminfo['MemTotal']))
    print('Free memory:{0}'.format(meminfo['MemFree']))

if __name__ == '__main__':
    main()


