"""
subprocess example
"""
import subprocess
from subprocess import PIPE


def main():
    """
    main function
    """
    p = subprocess.Popen('ls -al | grep root', shell=True,
            stdin=PIPE,
            stdout=PIPE,
            stderr=PIPE)
    stdout, stderr = p.communicate()
    print "returncode: ", p.returncode
    print 'stderr: ', stderr
    print 'stdout: ', stdout


if __name__ == '__main__':
    main()
