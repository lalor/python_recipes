#!/usr/bin/python2.7

import random
import threading
import datetime
import pymysql
from optparse import OptionParser

def _parser_args():
    parser = OptionParser()
    parser.add_option('--host', dest='mysql_host', help='host of mysql server')
    parser.add_option('--user', dest='mysql_user', default='root', help='user to connect mysql')
    parser.add_option('--passwd', dest='mysql_passwd', default='', help='password to connect mysql')
    parser.add_option('--port', dest='mysql_port', default=3306, help='port of mysql server')
    parser.add_option('--rows', dest='rows_count', default=10, help='rows to insert into for each worker')
    parser.add_option('--thread_size', dest='thread_size', default=2, help='number of thread to insert data')
    (options, args) = parser.parse_args()
    if not options.mysql_host:
     	parser.error('--host options not given')
    return options, args



def get_conn(options, db=None):
    kw = dict(host=options.mysql_host,
              port=int(options.mysql_port),
              user=options.mysql_user)
    print kw
    if options.mysql_passwd:
       kw['passwd'] = options.mysql_passwd
    if db:
       kw['db'] = db
    return pymysql.connect(**kw)


def create_db_and_table(options, db, table):
    with get_conn(options) as c:
        try:
           c.execute('create database %s' % db)
           c.execute('use %s' % db)
           c.execute("""create table %s (
                          id int(10) NOT NULL AUTO_INCREMENT,
                          name varchar(255) NOT NULL,
                          datetime datetime NOT NULL,
                          PRIMARY KEY (`id`)
                     )""" % table)
        except pymysql.err.ProgrammingError as e:
            if e.args[0] != 1007:
                raise




def random_string(length=10):
    str_ = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"
    len_ = len(str_)
    buf = []
    for i in range(length):
       idx = random.randint(0, len_ - 1)
       buf.append(str_[idx])
    return ''.join(buf)


def add_row(cursor, table):
    sql = "INSERT INTO "+ table +" set name=%s, datetime=%s"
    name = random_string()
    time_ = datetime.datetime.now()
    cursor.execute(sql, [name, time_])


def insert_data(options, db, table, rows):
    conn = get_conn(options, db)
    with conn as c:
        for i in xrange(rows):
            add_row(c, table)
            conn.commit()



def main():
    options, args =  _parser_args()
    db = 'test_insert_data_db'
    table = 'test_insert_data_table'
    rows = int(options.rows_count)
    # Initial database and table.
    create_db_and_table(options, db, table)
    tg = []
    for i in range(int(options.thread_size)):
        th = threading.Thread(target=insert_data,
                              args=(options, db, table, rows))
        tg.append(th)
        th.start()
    # wait for all the thread join.
    for th in tg:
        th.join()


if __name__ == '__main__':
    main()
