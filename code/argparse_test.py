import argparse

def _argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', action='store', dest='simple_value', help='Store a simple value', type=int)
    parser.add_argument('-t', action='store_true', default=False, dest='boolean_switch', help='Set a switch to true')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    return parser.parse_args()


def main():
    parser = _argparse()
    print 'simple_value     =', parser.simple_value
    pass


if __name__ == '__main__':
    main()

