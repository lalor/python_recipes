# -*- coding:UTF-8 -*-

import time
import os,re
import MySQLdb
import threading
import string
import random
import sys

class MysqlConnection:
  def __init__(self, socket, user, passwd):
    self.connParams = (socket, user, passwd)
    self.conn = self.__create__()

  def __create__(self):
    conn = None
    try:
      conn = MySQLdb.connect(unix_socket = self.connParams[0], user=self.connParams[1], passwd=self.connParams[2])
      conn.autocommit(True)
    except:
      pass
    return conn

  def execSql(self, sql):
    #pdb.set_trace()
    rows = None
    # 连接没建立，重新建立连接
    if self.conn == None:
      self.conn = self.__create__()
    # 连接创建失败，直接返回失败
    if self.conn == None:
      return None
    cursor = self.conn.cursor()
    try:
      # 执行SQL获取结果
      cursor.execute(sql)
      rows = cursor.fetchall()
      cursor.close()
    except:
      cursor.close()
      # SQL执行出错，可能是由于连接断掉了，重新尝试建立连接
      self.conn = self.__create__()
      # 重建成功，执行SQL
      if self.conn != None:
        cursor = self.conn.cursor()
        try:
          cursor.execute(sql)
          rows = cursor.fetchall()
          cursor.close()
        except:
          cursor.close()
    return rows

  def close(self):
    if self.conn != None:
      self.conn.close()

class ConnectionPool:
  def __init__(self):
    self.connList = []
    self.mutex = threading.Lock()

  def put(self, conn):
    self.mutex.acquire()
    self.connList.append(conn)
    self.mutex.release()

  def get(self):
    ret = None
    self.mutex.acquire()
    if len(self.connList) != 0:
      ret = self.connList.pop(0)
    self.mutex.release()
    return ret

  def execSql(self, sql):
    rows = None
    conn = self.get()
    if conn != None:
      rows = conn.execSql(sql)
      self.put(conn)
    return rows

  def destory(self):
    # 连接池连接销毁函数，调用前确保没有连接正在进行操作
    self.mutex.acquire()
    for conn in self.connList:
      conn.close()
    self.mutex.release()


class Mysql():

    def __init__(self, socket, user, passwd):
      self.connectionPool = ConnectionPool()
      for i in range(8):
          conn = MysqlConnection(socket, user, passwd)
          self.connectionPool.put(conn)

    def exec_sql(self, sql):
        return self.connectionPool.execSql(sql)


def main():

    mysql = Mysql('/tmp/mysql.sock', 'rdsadmin', '?lACPAs82IDMs#')

    log = open("out.txt", "w")
    while True:
        # 1. sleep
        time.sleep(random.randint(2, 6))

        # 2. set read only
        mysql.exec_sql('set global read_only=on')

        # 3. show master status
        res = mysql.exec_sql('show master status')
        show_master_status =  res[0]

        # 4. sleep
        time.sleep(random.randint(2, 6))

        # 5. get binlog size
        binlog_index = '/ebs/mysql_data/mysql-bin.index'
        with open(binlog_index) as f:
            res = f.readlines()
        file_name = res[-1].strip()
        size = os.path.getsize('/ebs/mysql_data/{0}'.format(file_name))


        # 6. unset read only
        mysql.exec_sql('set global read_only=off')
        print >>log, '{0}   {1}   {2}'.format(show_master_status[0], show_master_status[1], size)
        sys.stdout.flush()


if __name__ == '__main__':
    main()
