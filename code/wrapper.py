#!/usr/bin/python
#-*- coding: UTF-8 -*-
import functools
import inspect


def check_privilege(username):
    def check_is_admin(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            func_args = inspect.getcallargs(f, *args, **kwargs)
            if func_args.get('username') != 'admin':
                raise Exception("This use is not allowed to get food")
            return f(*args, **kwargs)
        return wrapper


@check_is_admin
def get_food(username, type='chocolate'):
    return type + " non non non!"


def main():
    print get_food('admin')


if __name__ == '__main__':
    main()
