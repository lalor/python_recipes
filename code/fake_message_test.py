#!/usr/bin/python
""
import zmq
import sys
import json
def main():
    fname = "/home/rds-user/log/mylog"
    if len(sys.argv) == 2:
        fname = sys.argv[1]

    context = zmq.Context().instance()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5000")
    try:
        last = None
        with open(fname) as fhandle:
            for line in fhandle:
                if line.find('recv Manager') != -1:
                    last = line

        # 日志文件中没有接收到过任何消息，则返回
        if not last:
            print 'No message found !!!'
            sys.exit(1)

        message = last[last.find('{'):]
        dmsg = json.loads(message)
        #修改版本号
        dmsg['Version'] = str(long(dmsg['Version']) + 1)
        socket.send(json.dumps(dmsg))
        msg = socket.recv()
        print '<---------msg={0}'.format(msg)

    except Exception, err:
        print "{0}".format(err)
    socket.close()
    context.term()

if __name__ == '__main__':
    main()
