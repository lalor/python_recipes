#!/usr/bin/python
#-*- coding: UTF-8 -*-
import pymysql
import traceback
import random
import string


def getConnection():
    return pymysql.connect(user='root', password='root', unix_socket='/tmp/mysql.sock')


def insert_rows(n, n_city, n_fname):
    city =  [  "".join(random.sample(list(string.letters + string.digits), 40)) for i in range(n_city)  ]
    lname = [  "".join(random.sample(list(string.letters + string.digits), 40)) for i in range(n_fname) ]
    fname = [  "".join(random.sample(list(string.letters + string.digits), 40)) for i in range(n_fname) ]

    sql = "insert into test.t1(fname, lname, pno, city) values('%s', '%s', %s, '%s') "
    with getConnection() as cursor:
        for i in range(n):
            cursor.execute( sql % ( random.choice(lname), random.choice(fname), 200, random.choice(city)))


def create_table(table_name):
    DROP_TABLE=""" drop table if exists %s """ % table_name
    CREATE_TABLE = """ create table %s (cno int auto_increment primary key,
    fname varchar(200), lname varchar(200), pno int default 200, city
    varchar(200)); """ % table_name

    with getConnection() as cursor:
        affect_rows = cursor.execute(DROP_TABLE)
        affect_rows = cursor.execute(CREATE_TABLE)


def prepare_data():
    nums = 1000000

    create_table("test.t1")
    insert_rows(nums, 10, 10)


def main():
    try:
        prepare_data()
    except Exception, e:
        traceback.format_exc()



if __name__ == '__main__':
    main()

