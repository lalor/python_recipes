#!/usr/bin/python
#-*- coding: UTF-8 -*-

#!/usr/bin/python
#-*- coding: UTF-8 -*-
import functools
import inspect
import os


def check_is_admin(f):
    """check is admin"""
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        func_args = inspect.getcallargs(f, *args, **kwargs)
        print "check tool"
        if os.path.exists("/usr/local/bin/split_binlog_for_rds"):
            return f(*args, **kwargs)
        else:
            print "split binlog for rds not found"
            return False
    return wrapper

is_alive=True

def check_is_mysql_alive(f):
    def wrapper(*args, **kwargs):
        print "check is mysql alive"
        if is_alive:
            return f(*args, **kwargs)
        else:
            return False, "MySQL not alive"
    return wrapper




@check_is_admin
@check_is_mysql_alive
def flashback():
    print "Ready to flabask dbinstance"
    return True, ""


def main():
    ret = flashback()


if __name__ == '__main__':
    main()
