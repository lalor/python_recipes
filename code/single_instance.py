#!/usr/bin/python
#-*- coding: UTF-8 -*-
'''
single instance
单实例
'''
import fcntl
import sys
import time

def main():
    '''
    main function
    '''
    single = open("single.pid", 'w')
    try:
        fcntl.lockf(single, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError, err:
        print str(err)
        sys.exit(1)

    # do something
    num = 100
    while num != 0:
        print num
        num -= 1
        time.sleep(1)


if __name__ == '__main__':
    main()
