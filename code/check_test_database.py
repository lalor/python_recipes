#!/usr/bin/python

import subprocess
import pymysql
from subprocess import PIPE
import json
import re
import sys


cfg = {
        'mysql_user': 'zty',
        'mysql_host': '10.120.144.7',
        'mysql_pass': 'ztyyy',
        'mysql_port': 4331,
        'mysql_database': 'node_rds'
}

def get_conn():
    return pymysql.Connect(user=cfg['mysql_user'],
                           port=cfg['mysql_port'],
                           password=cfg['mysql_pass'],
                           host=cfg['mysql_host'],
                           database=cfg['mysql_database'],
                           cursorclass=pymysql.cursors.DictCursor)


def is_empty(s):
    return s == None or s == '' or len(s.strip()) == 0

def is_exists_test_database(addr):
    for port in [1046, 22]:
        cmd = '''ssh rds-user@%s -p %s "mysql -urdsadmin -p?lACPAs82IDMs# -S/tmp/mysql.sock -e ' show databases ' | grep -w test" ''' % (addr, port)
        #print cmd
        p = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        stdout, stderr = p.communicate()
        if p.returncode == 0:
            for line in stdout.split('\n'):
                if line.lower() == 'test':
                    return True
        return False

def create_test_database(addr):
    for port in [1046, 22]:
        cmd = '''ssh rds-user@%s -p %s "mysql -urdsadmin -p?lACPAs82IDMs# -S/tmp/mysql.sock -e ' create database  test ' " ''' % (addr, port)
        print cmd
        p = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        stdout, stderr = p.communicate()
        if p.returncode == 0:
            return
        else:
            print stdout, stderr
            #sys.exit(1)


def get_vm_role(vm):
    vm_role = 'unkown'
    if vm['vmRole'] == 1:
        vm_role = 'Master'
    elif vm['vmRole'] == 2:
        vm_role = 'Slave'
    else:
        vm_role = 'ReadReplica'
    return vm_role

def print_vm_info(vm):
    print '###', 'host:', vm['instanceEndpoint'], 'Identifier: ', vm['dbInstanceIdentifier'].split(':')[0] , 'Role:', get_vm_role(vm)

def main():
    with get_conn() as cursor:
        cursor.execute("select vm.*, dbinstance.dbInstanceIdentifier, dbinstance.replicationType, dbinstance.syncStatus, dbinstance.instanceEndpoint from vm, dbinstance where vm.dbInstanceId = dbinstance.dbInstanceId")
        for vm in cursor:
            if vm['vmRole'] == 1:
                host = vm['vmIp'].split(':')[0]
                if not is_exists_test_database(host):
                    #print_vm_info(vm)
                    visit_host = vm['instanceEndpoint']
                    print visit_host
                    create_test_database(host)

if __name__ == '__main__':
    main()
