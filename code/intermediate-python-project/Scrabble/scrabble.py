#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
import operator

scores = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2,
         "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3,
         "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1,
         "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4,
         "x": 8, "z": 10}


def get_word_list(file_name):
    """docstring for get_word_list"""
    words = []
    with open(file_name) as f:
        for line in f:
            if line.strip():
                words.append(line.strip())
    return words


def isValid(rack, word):
    vals = [c for c in rack]
    subvals = [c for c in word]
    vals.sort()
    subvals.sort()

    for item in subvals:
        if vals.count(item):
            vals.remove(item)
        else:
            return False
    return True


def getScore(word):
    score = 0
    for c in word.lower():
        score = score + scores[c]
    return score


def main():
    if len(sys.argv) != 2:
        print "please input rack. Usage: {0} RACK".format(sys.argv[0])
        sys.exit(1)

    file_name = "sowpods.txt"
    words = get_word_list(file_name)
    valid_words = []
    rack = "ZZAAEEI"
    for word in words:
        if isValid(rack, word):
            valid_words.append(word)

    d = {}
    for word in valid_words:
        d[word] = getScore(word)

    sorted_d = sorted(d.items(), key=operator.itemgetter(1), reverse=True)
    for k, v in sorted_d:
        print v, k


if __name__ == '__main__':
    main()
