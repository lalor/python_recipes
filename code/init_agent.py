#!/usr/bin/python
#-*- coding: UTF-8 -*-

"""
initizlize agent, upload to NOS
"""

import os
import shutil
import argparse
import subprocess


BASE_PATH   = '/home/rds-user/'
SOURCE_PATH = os.path.join(BASE_PATH, 'rdsAgent')
CONFIG_PATH = os.path.join(SOURCE_PATH, 'config')
AGENT_PATH  = os.path.join(BASE_PATH, 'agent')
TARGET_PATH = os.path.join(BASE_PATH, 'agent', 'rdsAgent')


def main():
    """main function"""

    args = parser_args()

    if os.getcwd() != os.path.expanduser('~'):
        raise SystemExit('please run the script in {0}'.format(
            os.path.expanduser ('~')))

    dirs = [item for item in os.listdir(CONFIG_PATH)
            if os.path.isdir(os.path.join(CONFIG_PATH, item))]
    if not dirs.count(args.env):
        raise SystemExit("%s is not a correct environment" % args.env)

    if args.op_type in ['all', 'init']:
        init_upgrade_agent(args.env)

    if args.op_type in ['all', 'upload']:
        upload_to_nos(args.env)


def parser_args():
    """
    parser arguments
    """
    parser = argparse.ArgumentParser(description='upload RDS Agent to NOS')
    parser.add_argument('-e', '--env', metavar='environment', required=True,
            dest='env', action='store',
            help='which environment you want to depoly')
    parser.add_argument('--op-type', dest='op_type', action='store',
            choices={'all', 'init', 'upload'},
            default='all', help='do what you want')
    return parser.parse_args()


def init_upgrade_agent(env):
    """
    initizlize agent directory, configure agent directory
    """
    init_direcotry_hierarchy()
    init_rds_agent(env)


def init_direcotry_hierarchy():
    """
    initialize a directory
    $ tree -L 1
    .
    ├── rdsAgent
    ├── tools
    ├── upgrade.cnf
    └── upgrade.sh
    """
    backup_agent = AGENT_PATH + '.bak'
    if os.path.isdir(AGENT_PATH):
        if os.path.isdir(backup_agent):
            shutil.rmtree(backup_agent)
        shutil.move(AGENT_PATH, backup_agent)

    os.mkdir(AGENT_PATH)
    os.mkdir(os.path.join(AGENT_PATH, 'tools'))
    with open(os.path.join(AGENT_PATH, 'upgrade.sh'), 'w') as fptr:
        fptr.write("""date >> /home/rds-user/log/upgrade.log""")

    content = "[fake_tool]\nsource=fake_tool\ntarget=/usr/local/bin/fake_tool"
    with open(os.path.join(AGENT_PATH, 'upgrade.cnf'), 'w') as fptr:
        fptr.write(content)

    with open( os.path.join(AGENT_PATH, 'tools', 'fake_tool'), 'w') as fptr:
        fptr.write("""echo hello, world""")

    shutil.copytree(SOURCE_PATH, TARGET_PATH)


def init_rds_agent(env):
    """
    initizlize rdsAgent

        * rdsAgent/env.cfg
        * rdsAgent/config/RDSAgent.cnf
        * rdsAgent/storage/nos/conf/
    """
    config_env(env)
    config_agent(env)
    config_nos(env)


def config_env(env):
    """
    prepare /home/rds-user/rdsAgent/env.cfg
    """
    with open(os.path.join(TARGET_PATH, 'env.cfg'), 'w') as fptr:
        fptr.write("""[env]\nenvironment = %s """ % env)


def config_nos(env):
    """
    prepare /home/rds-user/rdsAgent/storage/nos/conf direcotry
    """
    target_dir = os.path.join(TARGET_PATH, 'storage', 'nos', 'conf')
    shutil.rmtree(target_dir)
    shutil.copytree(os.path.join(TARGET_PATH, 'config', env, 'conf'),
            target_dir)


def config_agent(env):
    """
    prepare /home/rds-user/rdsAgent/config/RDSAgent.cnf
    """
    with open(os.path.join(TARGET_PATH, 'config', 'RDSAgent.cnf'), 'w') as fptr:
        with open(os.path.join(TARGET_PATH, 'config', env, 'RDSAgent.cnf')) as sptr:
            fptr.write(sptr.read())



def exec_command(cmd):
    """
    execute linux command
    """
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    _, stderr = p.communicate()
    if p.returncode != 0:
        print "exec {0} error".format(cmd)
        raise SystemExit(stderr)
    return True


def clear_file(file_name):
    """
    clear file
    """
    if os.path.isfile(file_name):
        os.remove(file_name)

    if os.path.isdir(file_name):
        shutil.rmtree(file_name)


def upload_to_nos(env):
    """
    upload agent
    """
    nos_dict = {
    'online_b': 'cloudrdsa-publickey$/home/rds-user/xiaoshanb-agent.tar.gz',
    'online_a': 'cloudrdsa-publickey$/home/rds-user/xiaoshana-agent.tar.gz',
    'online_bj_zw' : 'cloudrds-bj-zw$/home/rds-user/zw-agent.tar.gz',
    'online_pub'   : 'cloudrdspub$/home/rds-user/pub-agent.tar.gz',
    'qa'           : 'cloudrdsyanlian$/home/rds-user/qa-agent.tar.gz',
    'self-test'    : 'cloudrdsyanlian$/home/rds-user/self-test-agent.tar.gz',
    'self-test2'   : 'cloudrdsyanlian$/home/rds-user/self-test2-agent.tar.gz',
    'performance'  : 'cloudrdsyanlian$/home/rds-user/performance-agent.tar.gz',
    }

    nos_tool = os.path.join(TARGET_PATH, 'storage', 'nos', 'tool.sh')
    bucket, key = nos_dict[env].split('$')
    tar_file = os.path.basename(key)
    clear_file(tar_file)

    shutil.make_archive(tar_file.split('.')[0], 'gztar', 'agent' )
    exec_command("bash {0} -putfile {1} {2} -key {3} -replace true".format(
        nos_tool, tar_file, bucket, key))

    clear_file(tar_file)
    clear_file(AGENT_PATH)

    print "Successfully... {0}".format(nos_dict[env])


if __name__ == '__main__':
    main()
