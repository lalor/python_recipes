# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-14
@contact: jhx1008@gmail.com
version:  1.0
@todo:    线程间消息传递队列，通过list进行模拟
@modify:
"""
import threading

class MsgQueue:
  def __init__(self):
    self.queueLock = threading.Lock()
    self._msgList = []

  def push(self, msg):
    """
     消息进入队列，排在队列的尾部
    """
    self.queueLock.acquire()
    self._msgList.append(msg)
    self.queueLock.release()

  def pop(self):
    """
     消息出队列，取list[0]位置的消息
    """
    obj = None
    self.queueLock.acquire()
    # 如果队列长度为非空，则返回第一个消息
    if len(self._msgList) > 0:
      obj = self._msgList.pop(0)
    self.queueLock.release()
    return obj
