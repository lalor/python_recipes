# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-21
@contact: jhx1008@gmail.com
version:  1.0
@todo:    监控信息上报线程，获取监控信息，通过http形式上报给云监控
@modify:
"""
import re
import subprocess
from util import Util
from common import Common
from log.BaseLog import BaseLog
from BaseThread import BaseThread
from common import MonitorProperties
from MonitorHttp import MetricData, SendRequest

class MonitorReporter(BaseThread):
  def __init__(self, threadName="MonitorReporter", activeReportInterval=5.0, interval=10, logDir=""):
    BaseThread.__init__(self,threadName, activeReportInterval, interval)
    self.oldSelect = 0
    self.oldInsert = 0
    self.oldUpdate = 0
    self.oldDelete = 0
    self.oldCommit = 0
    self.oldXaCommit = 0
    self.oldQuestions = 0
    self.oldSlowQueries = 0
    self.oldInnodbFsyncs = 0
    self.waitCount = 1
    self.baseLog = BaseLog(3, 4, logDir, "monitorReporter")

  def getIOUtils(self):
    """
     获取盘的io信息
    """
    lineNo = 1
    cmd = "iostat -x 1 2"
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    p.wait()
    lines = p.stdout.readlines()
    for line in lines:
      pattern = re.compile('\s(\d+)(\W*)(\d+)$')
      m = pattern.search(line)
      if m and (lineNo == 15):
        return m.group().strip()
      lineNo = lineNo + 1
    return "null"

  def getIOInfo(self):
    """
     获取磁盘IO相关信息
    """
    util = 0
    reads = 0
    writes = 0
    r_waits = 0
    w_waits = 0
    lineNo = 1
    cmd = "iostat -x 1 2"
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    p.wait()
    lines = p.stdout.readlines()
    for line in lines:
      if lineNo == 15:
        #由于line的每项中间可能有多个空格，如果直接split的结果会出现很多''项，join后在split即可忽略多个连续的空格
        ioInfo = ' '.join(line.split()).split()
        reads   = ioInfo[3]
        writes  = ioInfo[4]
        r_waits = ioInfo[10]
        w_waits = ioInfo[11]
        util    = ioInfo[13]
      lineNo = lineNo + 1
    return reads, writes, r_waits, w_waits, util

  def getAverage(self, newVal, oldVal):
    """
     每秒增加的操作，由于线程是每self.loopInterval获取一次，所以计算平均值时，需除于该值
    """
    # 如果new小于old，说明数据库中间进行了重启操作，old值已经无效，重置为0
    if newVal < oldVal:
      oldVal = 0
    return float(newVal - oldVal) / (self.loopInterval * self.waitCount)

  def getAgDimen(self, metricName):
    agDimenVal = ""
    msg = Util.obj2Json(MonitorProperties.aggregationDimensions)
    object = Util.json2Obj(msg)
    dimenList = object.keys()
    if dimenList == None:
      return agDimenVal
    for dimen in dimenList:
      if agDimenVal != "":
        agDimenVal = agDimenVal + ","
      val = object.get(dimen, "null")
      agDimenVal = agDimenVal + "{0}={1}".format(dimen, val)
    return agDimenVal

  def doWork(self):
    if not MonitorProperties.isValid():
      return
    dimensions = "RDS={0}".format(MonitorProperties.dbIdentifier)
    sessions = Common.databaseManager.getSessions()
    # 获取数据库的操作，并计算每秒平均操作数
    newSelect, newInsert, newUpdate, newDelete, newCommit, newXaCommit, newQuestions, \
    bufferPoolHits, newSlowQueries, threadsRunning, newInnodbFsyncs, groupRate = Common.databaseManager.getGlobalStatusInfo()
    secondBehindMaster = Common.databaseManager.getSecondsBehindMaster()
    binlogSize = Common.databaseManager.getBinlogSize()
    systemDiskRate = Util.getSystemDiskRate()
    # 如果获取失败，记录失败需要等待下一次获取，等待次数递增
    if newSelect == -1:
      self.waitCount = self.waitCount + 1
      Common.monitorLog.logWarn("get global status from database error")
      return
    #ioUtils = self.getIOUtils()
    ioReads, ioWrites, ioRWaits, ioWWaits, ioUtils = self.getIOInfo()
    avgSelect = self.getAverage(newSelect, self.oldSelect)
    avgInsert = self.getAverage(newInsert, self.oldInsert)
    avgUpdate = self.getAverage(newUpdate, self.oldUpdate)
    avgDelete = self.getAverage(newDelete, self.oldDelete)
    avgCommit = self.getAverage(newCommit, self.oldCommit)
    avgXaCommit = self.getAverage(newXaCommit, self.oldXaCommit)
    avgQuestions = self.getAverage(newQuestions, self.oldQuestions)
    avgInnodbFsyncs = self.getAverage(newInnodbFsyncs, self.oldInnodbFsyncs)
    if newSlowQueries < self.oldSlowQueries:
      self.oldSlowQueries = 0
    slowQueriesIncreased = newSlowQueries - self.oldSlowQueries

    logJson = Util.obj2Json({'select':avgSelect, 'insert':avgInsert, 
                             'update':avgUpdate, 'delete':avgDelete, 
                             'commit':avgCommit, 'xaCommit':avgXaCommit, 
                             'qps':avgQuestions, 'bufferPoolHits':bufferPoolHits, 'slowQueries': slowQueriesIncreased,
                             'secondBehindMaster':secondBehindMaster, 'binlogSize':binlogSize, 'systemDiskRate':systemDiskRate,
                             'threadsRunning':threadsRunning, 'inndbFsyncs':avgInnodbFsyncs, 'groupRate':groupRate})
    self.baseLog.append(0, logJson + "\n")
    logJson = Util.obj2Json({'newSelect':newSelect, 'oldSelect':self.oldSelect, 
                             'newInsert':newInsert, 'oldInsert':self.oldInsert, 
                             'newUpdate':newUpdate, 'oldUpdate':self.oldUpdate, 
                             'newDelete':newDelete, 'oldDelete':self.oldDelete, 
                             'newCommit':newCommit, 'oldCommit':self.oldCommit, 
                             'newXaCommit':newXaCommit, 'oldXaCommit':self.oldXaCommit, 
                             'newQuestions':newQuestions, 'oldQuestions':self.oldQuestions,
                             'newSlowQueries':newSlowQueries, 'oldSlowQueries':self.oldSlowQueries,
                             'newInnodbFsyncs':newInnodbFsyncs, 'oldInnodbFsyncs':self.oldInnodbFsyncs})
    self.baseLog.append(0, logJson + "\n")
    # 在对old 变量赋值之前判断是否需要跳过本次推送
    skip = self.isSkipThisTurn()
    self.oldSelect = newSelect
    self.oldInsert = newInsert
    self.oldUpdate = newUpdate
    self.oldDelete = newDelete
    self.oldCommit = newCommit
    self.oldXaCommit = newXaCommit
    self.oldQuestions = newQuestions
    self.oldSlowQueries = newSlowQueries
    self.oldInnodbFsyncs = newInnodbFsyncs
    self.waitCount  = 1

    # 如果需要跳过本次推送，则先将new值赋值给old值，然后再跳过
    if skip:
      return

    md1 = MetricData("session", sessions,  dimensions, self.getAgDimen("session"), "个")
    md2 = MetricData("nSelect", avgSelect, dimensions, self.getAgDimen("nSelect"), "次/秒")
    md3 = MetricData("nInsert", avgInsert, dimensions, self.getAgDimen("nInsert"), "次/秒")
    md4 = MetricData("nUpdate", avgUpdate, dimensions, self.getAgDimen("nUpdate"), "次/秒")
    md5 = MetricData("nDelete", avgDelete, dimensions, self.getAgDimen("nDelete"), "次/秒")
    md6 = MetricData("nCommit", avgCommit, dimensions, self.getAgDimen("nCommit"), "次/秒")
    md7 = MetricData("nXaCommit", avgXaCommit, dimensions, self.getAgDimen("nXaCommit"), "次/秒")
    md8 = MetricData("ioUtils", ioUtils, dimensions, self.getAgDimen("ioUtils"), "百分比")
    md9 = MetricData("qps", avgQuestions, dimensions, self.getAgDimen("qps"), "次/秒")
    md10 = MetricData("sQueries", slowQueriesIncreased, dimensions, self.getAgDimen("sQueries"), "条")
    md11 = MetricData("bpHits", bufferPoolHits, dimensions, self.getAgDimen("bpHits"), "百分比")
    md12 = MetricData("sbm", secondBehindMaster, dimensions, self.getAgDimen("sbm"), "秒")
    md13 = MetricData("binlogSize", binlogSize, dimensions, self.getAgDimen("binlogSize"), "MB")
    md14 = MetricData("sDiskRate", systemDiskRate, dimensions, self.getAgDimen("sDiskRate"), "百分比")
    md15 = MetricData("threadsRunning", threadsRunning, dimensions, self.getAgDimen("threadsRunning"), "个")
    md16 = MetricData("fsyncs", avgInnodbFsyncs, dimensions, self.getAgDimen("fsyncs"), "次/秒")
    md17 = MetricData("groupRate", groupRate, dimensions, self.getAgDimen("groupRate"), "百分比")
    md18 = MetricData("ioReads", ioReads, dimensions, self.getAgDimen("ioReads"), "次/秒")
    md19 = MetricData("ioWrites", ioWrites, dimensions, self.getAgDimen("ioWrites"), "次/秒")
    md20 = MetricData("ioRWaits", ioRWaits, dimensions, self.getAgDimen("ioRWaits"), "毫秒")
    md21 = MetricData("ioWWaits", ioWWaits, dimensions, self.getAgDimen("ioWWaits"), "毫秒")
    mdList = [md1.to_dict(), md2.to_dict(), md3.to_dict(), md4.to_dict(), 
              md5.to_dict(), md6.to_dict(), md7.to_dict(), md8.to_dict(),
              md9.to_dict(), md10.to_dict(), md11.to_dict(), md12.to_dict(), 
              md13.to_dict(), md14.to_dict(), md15.to_dict(), md16.to_dict(), md17.to_dict(),
              md18.to_dict(), md19.to_dict(), md20.to_dict(), md21.to_dict()]

    metricDataDict = {}
    metricDataDict["metricDatas"] = mdList
    # 把收集到的信息构造成json格式发送给云监控服务
    metricDataJson = Util.obj2Json(metricDataDict)
    Common.monitorLog.logDebug("send {0} to {1}".format(metricDataJson, MonitorProperties.monitorWebServerUrl))
    request = SendRequest(metricDataJson, MonitorProperties.monitorWebServerUrl, r"/rest/V1/MetricData",  \
              MonitorProperties.userProductId, MonitorProperties.serviceType, MonitorProperties.monitorAcessKey, MonitorProperties.monitorSecretKey)
    request.sendRequestToServer()

  def clear(self):
    self.baseLog.uninit()

  def isSkipThisTurn(self):
    if self.oldSelect == 0 and self.oldCommit == 0 and self.oldDelete == 0:
      return True
    else:
      return False
