# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-12-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:  工作线程，主要用于处理Manager发送过来的命令
"""

import traceback
from common import *
from util import Util
from BaseThread import BaseThread
from communication.response import Response
from communication.ResponseMsg import ResponseMsg

class WorkerThread(BaseThread):
  def __init__(self,threadName="WorkerThread", activeReportInterval=5.0):
    # work线程，不需要进行等待，所以把interval设置成0
    BaseThread.__init__(self, threadName, activeReportInterval, 0)
    self.response = Response()

  def doWork(self):
    obj = Common.msgQueue.pop()
    if obj == None:
      Util.msSleep(10)
      return
    action = obj[0]
    msg = obj[1]
    
    # 如果Agent在SENDING状态下发生了crash，在重启后扫描状态文件发现是SENDING状态
    # 则需要重新发送消息，在普通命令执行时，不会有SENDING的action
    if action == "SENDING":
      self.response.sendAsync(msg)
      Common.statusManager.updateStatus("DONE", "NULL")

    # 异步命令，先保存当前命令
    # 执行当前命令，获取返回值，如果出现异常，则直接返回错误信息
    Common.statusManager.updateStatus(action, msg)
    Common.rdsLog.logInfo("start to do action {0}".format(action))
    try:
      success, message = Action.handleMap[action](msg)
      if success:
        Common.rdsLog.logInfo("do action {0} succeed".format(action))
      else:
        Common.rdsLog.logError("do action {0} failed, error msg: {1}".format(action, message))
      # 执行完毕，准备发送，更新agent状态为SENDING
      Common.statusManager.updateStatus("SENDING", message)
      Common.rdsLog.logInfo("send async msg: {0}".format(message))
      self.response.sendAsync(message)
    except:
      Common.rdsLog.logError('thread [{0}] error, traceback:{1}'.format(self.getName(), traceback.format_exc()))
      message = ResponseMsg.commonFail(Util.json2Obj(msg), "catch exception when do the action {0}".format(action))
      self.response.sendAsync(message)
    # 发送结束，更改agent状态为DONE
    Common.statusManager.updateStatus("DONE", "NULL")

  def clear(self):
    self.response.uninit()
