# -*- coding:UTF-8 -*-
"""
@author:  hzlaimingxing
@date:    2015-02-03
@contact: me@mingxinglai.com
version:  1.0
@todo:    与工作线程并行的运行，执行同步操作
"""

from common import *
from util import Util
from BaseThread import BaseThread
from communication.response import Response

class SyncTaskThread(BaseThread):
  def __init__(self,threadName="SyncTaskThread", activeReportInterval=5.0):
    BaseThread.__init__(self, threadName, activeReportInterval, 5)
    self.response = Response()

  def doWork(self):
    obj = Common.syncMsgQueue.pop()
    if obj == None:
      Util.msSleep(100)
      return
    action = obj[0]
    msg = obj[1]

    id = obj[2]
    Common.rdsLog.logDebug("start to do action {0}".format(action))
    success, message = Action.handleMap[action](msg)
    if success:
      Common.rdsLog.logDebug("do action {0} succeed".format(action))
    else:
      Common.rdsLog.logError("do action {0} failed, error msg: {1}".format(action, message))
    Common.rdsLog.logDebug("send sync msg: {0}".format(message))
    self.response.sendSync(id, message)

  def clear(self):
    self.response.uninit()
