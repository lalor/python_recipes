# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-20
@contact: jhx1008@gmail.com
version:  1.0
@todo:    状态线程，用于获取数据库部分状态信息
@modify:
"""

from common import Common
from BaseThread import BaseThread

class ShowMasterBlockTimeInVSR(BaseThread):
  """
   采用线程方式获取
  """
  def __init__(self, threadName="ShowMasterBlockTimeInVSR", activeReportInterval=5.0, interval = 1):
    BaseThread.__init__(self, threadName, activeReportInterval, interval)
    self.maxBlockTime = 0

  def doWork(self):
    self.maxBlockTime = Common.databaseManager.getMasterBlockTimeInVSR()


class ShowSlaveStatus(BaseThread):
  """
   获取slave延迟时间，取Seconds_Behind_Master的值
  """
  def __init__(self, threadName="ShowSlaveStatus", activeReportInterval=5.0, interval = 1):
    BaseThread.__init__(self, threadName, activeReportInterval, interval)
    self.replDelayTime = 0
    self.replErrorInfo = ""

  def doWork(self):
    self.replDelayTime, self.replErrorInfo = Common.databaseManager.getSlaveStatus()

class ShowSyncStatus(BaseThread):
  """
   获取当前sync是否为sync状态
  """
  def __init__(self, threadName="ShowSyncStatus", activeReportInterval=5.0, interval = 1):
    BaseThread.__init__(self, threadName, activeReportInterval, interval)
    self.replSyncStatus = "NULL"

  def doWork(self):
    self.replSyncStatus = Common.databaseManager.getSyncStatus()

class ShowSecondsBehindMaster(BaseThread):
  """
   获取当前sql线程落后时间
  """
  def __init__(self, threadName="ShowSecondsBehindMaster", activeReportInterval=5.0, interval = 1):
    BaseThread.__init__(self, threadName, activeReportInterval, interval)
    self.secondsBehindMaster = 0

  def doWork(self):
    self.secondsBehindMaster = Common.databaseManager.getSecondsBehindMaster()
