# -*- coding:UTF-8 -*-
"""
@author:  hzlaimingxing
@date:    2015-04-03
@contact: me@mingxinglai.com
version:  1.0
@todo:    Execute SQL Statement
"""

import socket
from threading import Thread
from common import *
from BaseThread import BaseThread


def handlechild(clientsock):
    """
    处理socket的连接的子线程
    """
    while True:
      # 接受客户端发送过来的SQL语句
      sql = clientsock.recv(1024)

      # 执行SQL语句
      Common.rdsLog.logInfo("input sql is {0}".format(sql))
      success, message = Common.databaseManager.executeSQL(sql)

      # 发送结果
      try:
        clientsock.sendall(str(message))
      except Exception, e:
        Common.rdsLog.logError("send result to client error: {0}".format(e))
        break
    clientsock.close()


class ExecuteSQLTaskThread(BaseThread):
  """
  守护者线程，监听端口，等待客户端连接
  生成子线程处理客户端连接
  """
  def __init__(self,threadName="ExecuteSQLTaskThread", activeReportInterval=5.0):
    BaseThread.__init__(self, threadName, activeReportInterval, 5)

  def doWork(self):
    host = 'localhost'
    port = 5678
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((host, port))
    sock.listen(10)

    # 监听
    while True:
      try:
        clientsock, clientaddr = sock.accept()
      except:
        break
      # 处理socket连接
      t = Thread(target=handlechild, args=[clientsock])
      t.setDaemon(0)
      t.start()

  def clear(self):
    pass
