# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-12-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:  心跳线程，定时向Manager发送心跳包
"""
from util import Util
from common import Common
from log.RDSLog import RDSLog
from log.BaseLog import LogLevel
from BaseThread import BaseThread
from communication.ZMQWrapper import *
from config.RDSConfig import RDSConfig
from communication.ResponseMsg import ResponseMsg

class HeartBeatThread(BaseThread):
  """
   心跳线程类，从线程基础类继承，用于向Manager上报心跳
  """
  def __init__(self, threadName="HeartBeat", activeReportInterval=5.0 ,interval=60, \
               thread1 = None, thread2 = None, thread3 = None):
    """
     心跳类初始化函数，由于心跳线程需要上报数据库的一些监控信息，每项数据库监控都为一个线程完成
    """
    BaseThread.__init__(self, threadName, activeReportInterval, interval)
    self.externalClient = ZMQClient(ZMQProtocolType.TCP, ZMQSocketType.REQ, RDSConfig.managerIP, RDSConfig.managerPort)
    self.externalClient.setsockopt(ZMQSocketType.LINGER, 0)
    self.poller = ZMQPoll()
    self.poller.register(self.externalClient, ZMQSocketType.POLLIN)
    self.masterBlockTimeInVSRThread = thread1
    self.showSlaveStatusThread = thread2
    self.showSyncStatusThread = thread3
    self.rdsLog = RDSLog(RDSConfig.logDir, "heartBeat.log", LogLevel.string2Level(RDSConfig.logLevel), RDSConfig.logExpire)

  def doWork(self):
    # 如果没有初始化过databaseInstanceID, 则心跳线程不上报信息
    if Common.databaseInstanceID == "null":
      return
    status = "1"
    if not Common.databaseManager.isAlive():
      status = "0"

    try:
      #try语句是为了处理强制转换为int出错的情况
      replDelayTime = "{0}".format(int(self.showSlaveStatusThread.replDelayTime))
    except:
      replDelayTime = 0
    replErrorInfo = self.showSlaveStatusThread.replErrorInfo
    if replErrorInfo.strip() != "":
      replDelayTime = -1
    replSyncTime  = "{0}".format(self.masterBlockTimeInVSRThread.maxBlockTime)
    replSyncStatus = self.showSyncStatusThread.replSyncStatus
    msg = ResponseMsg.heartBeatMsg(status, replDelayTime, replErrorInfo, replSyncTime, replSyncStatus)
    try:
      self.externalClient.send(msg)
      self.rdsLog.logInfo("send heart beat message: {0}".format(msg))
      self.poller.poll(2*1000)
      if self.poller.isActive(self.externalClient):
        reply = self.externalClient.recvMulti()
        replyMsg = "".join(reply)
        self.rdsLog.logInfo("recv heart beat reply: {0}".format(replyMsg))
        replyObj = Util.json2Obj(replyMsg)
        if Common.vmUUID == "":
          Common.vmUUID = Common.uuidManager.updateUUID(replyObj.get("UUID", ""))
    except:
      # 心跳线程错误，可能Manager不存在，尝试重新创建后连接
      self.externalClient.close()
      self.rdsLog.logWarn("[{0}] error, can not connect the server, reconnect now!".format(self.getName()))
      self.poller.unregister(self.externalClient)
      self.externalClient = ZMQClient(ZMQProtocolType.TCP, ZMQSocketType.REQ, RDSConfig.managerIP, RDSConfig.managerPort)
      self.externalClient.setsockopt(ZMQSocketType.LINGER, 0)
      self.poller.register(self.externalClient, ZMQSocketType.POLLIN)

  def clear(self):
    self.poller.unregister(self.externalClient)
    self.externalClient.close()
    self.rdsLog.uninit()
