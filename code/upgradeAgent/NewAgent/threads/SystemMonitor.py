# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-12-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:    系统资源监控，监控磁盘IO，内存使用, 系统活动情况等
@modify:
"""

import datetime
import subprocess
from log.BaseLog import BaseLog
from BaseThread import BaseThread

class SystemMonitor(BaseThread):
  def __init__(self, threadName="SystemMonitor", activeReportInterval=5.0, interval=60, logDir=""):
    BaseThread.__init__(self, threadName, activeReportInterval, interval)
    # 采用log的基类来写监控信息，logLevel不涉及，随便设置一个即可
    self.baseLog = BaseLog(3, 4, logDir, "systemMonitor")

  def doWork(self):
    self.baseLog.append(0, "{0}\n".format(str(datetime.datetime.now())))
    commands = ["iostat -kx", "vmstat", "sar -n DEV 1 1"]
    # 循环执行命令，把结果保存到文件
    for cmd in commands:
      p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
      p.wait()
      lines = p.stdout.readlines()
      for line in lines:
        self.baseLog.append(0,line)

  def clear(self):
    self.baseLog.uninit()
