# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-12-26
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:
"""

import os
import subprocess
import time
import json
from subprocess import PIPE

from common import Common
from RDSException import RDSException

class Util:

  @staticmethod
  def findProcess(processName, keys=None):
    """
     静态函数，查找给定名称的进程是否存在，忽略同名进程的可能性
    """
    cmd="ps axuf | grep -v grep | grep {0}".format(processName)
    if keys:
      for key in keys:
        cmd = cmd + ' | grep {0}'.format(key)
    result=os.popen(cmd)
    lines = result.readlines()
    for line in lines:
      line = line.upper()
      if line.find('DEFUNCT') == -1:
        return True
    return False

  @staticmethod
  def killProcess(processName,keys=None):
    """
     静态函数，kill给定名称的进程，忽略同名进程的可能性
    """
    # 获取指定进程的pid
    cmd1=r'ps axuf | grep -v grep | grep {0}'.format(processName)
    if keys:
      for key in keys:
        cmd1 = cmd1 + ' | grep {0}'.format(key)
    cmd1 = cmd1 + " | awk -F' ' '{print $2}'" 
    pids=os.popen(cmd1)
    for pid in pids:
      # 去掉pid后面的\r\n等
      pid = pid.strip(' \t\r\n')
      cmd2="sudo kill -9 {0}".format(pid)
      if not Util.execCommand(cmd2):
        return False
    return True

  @staticmethod
  def execCommand(cmd, fetchOut = True):
    """
     静态函数，执行指定命令，成功返回True，失败返回False
    """
    if fetchOut:
      p = subprocess.Popen(cmd, shell=True,
              stdin=PIPE,
              stdout=PIPE,
              stderr=PIPE)
      # 对于后台执行的进程，不能获取它的输出，否则，主进程会被hang住
      stdout, stderr = p.communicate()
      Common.rdsLog.logInfo("The result (return code, stdout, stderr) of cmd ({0}) is : {1}".format(cmd,(p.returncode, stdout, stderr)))

      if Common.rdsLog != None:
        Common.rdsLog.logDebug("execute command [{0}]".format(cmd))
        if p.returncode != 0:
          Common.rdsLog.logWarn("execute command [{0}] error".format(cmd))
      return p.returncode == 0
    else:
      ret = os.system(cmd)
      if Common.rdsLog != None:
        Common.rdsLog.logDebug("execute command [{0}]".format(cmd))
        if ret != 0:
          Common.rdsLog.logWarn("execute command [{0}] error".format(cmd))
      return ret == 0


  @staticmethod
  def popenCommand(cmd):
    """
     静态函数， 通过popen执行指定命令，返回执行结果
    """
    if Common.rdsLog != None:
      Common.rdsLog.logDebug("execute command [{0}]".format(cmd))
    return os.popen(cmd)

  @staticmethod
  def getIPAddress(ifname):
    """
     静态函数,获取IP地址
    """
    ip = '192.168.0.1'
    try:
      lines = os.popen("ip r | grep src | grep eth0 | awk -F' ' '{print $NF}'").readlines()
      for line in lines:
        ip = line.strip()
        break
    except:
      raise RDSException("get ip address error")
    return ip

  @staticmethod
  def syncOS():
    """
     强制OS进行sync操作
    """
    return Util.execCommand("sudo sync")

  @staticmethod
  def msSleep(ms):
    """
     sleep ms毫秒
    """
    time.sleep(float(ms)/1000)

  @staticmethod
  def sleep(n):
    """
     sleep n秒
    """
    time.sleep(n)

  @staticmethod
  def cutFile(fileName, backupFile, offset):
    """
     裁剪文件，fileName为文件名全路径，offset为文件位置
    """
    Util.syncOS()
    # change file mode
    oldMode = oct(os.stat(fileName).st_mode)[-3:]
    Util.execCommand("sudo chmod 777 {0}".format(fileName))
    # backup file
    Util.execCommand("cp {0} {1}".format(fileName,backupFile))
    # cut file
    fp = file(fileName, "r+b")
    fp.truncate(offset)
    fp.close()
    # revert file mode
    Util.execCommand("sudo chmod {0} {1}".format(oldMode, fileName))
    Util.syncOS()
    return True

  @staticmethod
  def json2Obj(str):
    """
     json格式转化为python相应格式，封装了json.loads方法，如果str为非json格式或者未知错误，抛出异常
    """
    obj = None
    try:
      obj = json.loads(str)
    except ValueError:
      raise RDSException("load string to dict error,{0} not a json format".format(str))
    except Exception,e:
      raise RDSException("unknown error:{0}".format(e))
    return obj
      

  @staticmethod
  def obj2Json(obj):
    """
     python格式转化为json字符串格式，封装了json.dumps方法
    """
    return json.dumps(obj)

  @staticmethod
  def getRealPath(path):
    """
     获取给定路径的绝对路径
    """
    return os.path.realpath(path)

  @staticmethod
  def getFileDir(path):
    """
     获取当前文件的存储文件夹全路径
    """
    return os.path.split(os.path.realpath(path))[0]

  @staticmethod
  def getFirstDir(path):
    """
     在指定路径下获取第一个文件夹，主要用于内部备份时获取备份文件夹用
    """
    if not os.path.isdir(path):
      return ""
    for file in os.listdir(path):
      fullPath = "{0}/{1}".format(path, file)
      if os.path.isdir(fullPath):
        return fullPath
    return ""

  @staticmethod
  def isExists(path):
    """
     判断给定路径是否存在，不区分是文件夹还是文件
    """
    return os.path.exists(path)

  @staticmethod
  def getFileSize(fileName):
    """
      获取指定文件的大小
    """
    return os.path.getsize(fileName)

  @staticmethod
  def getFilesSize(path, key):
    """
      获取指定路径下，包含key字符的文件的总的大小
    """
    path = path.strip()
    if not os.path.isdir(path):
      return 0
    if path[-1] != '/':
      path = path + "/"
    cmd = "ls -l %s | grep %s | grep -v grep | awk -F' ' '{print $5}'" % (path, key)
    lines = Util.popenCommand(cmd).readlines()
    sumSize = 0
    for line in lines:
      sumSize = sumSize + long(line)
    return sumSize

  @staticmethod
  def removeFile(fileName):
    """
     删除指定文件
    """
    os.remove(fileName)

  @staticmethod
  def getSystemDiskRate(fs="rootfs", mountPath="/"):
    """
      获取指定盘大小
    """
    rate=0
    cmd = "df | grep %s | awk '{if($6==\"%s\"){print $5}}'" % (fs, mountPath)
    lines = Util.popenCommand(cmd).readlines()
    for line in lines:
      line = line.strip()
      rate = long(line[0:-1])
      break
    return rate

  @staticmethod
  def pathSplit(fileName):
    """
     给定一个文件(夹)路径，获取路径和和文件(夹)名
    """
    return os.path.split(fileName)


  @staticmethod
  def getFileMode(fileName):
    """
     获取指定文件的mod，如0777,0755等
    """
    return oct(os.stat(fileName).st_mode)[-3:]

  @staticmethod
  def precision(num, pre=2):
    """
     保留pre位精度
    """
    fmt = "%." + "%df" % pre
    return float(fmt % num)

  @staticmethod
  def B2M(bytes, precision=2):
    """
     Bytes转换成MB，保留precision位精确度
    """
    _1M = 1024 * 1024
    fmt = "%." + "%df" % precision
    return float(fmt % (float(bytes) / _1M))
  
  @staticmethod
  def B2G(bytes, precision=2):
    """
     Bytes转换成GB，保留precision位精确度
    """
    _1G = 1024 * 1024 * 1024
    fmt = "%." + "%df" % precision
    return float(fmt % (float(bytes) / _1G))

  @staticmethod
  def wait(seconds):
    Util.sleep(seconds)

  @staticmethod
  def versionCompare(newVersion, oldVersion):
    """
     agent版本比较函数，规定agent版本格式为a.b.c形式，其中x都是整数，版本比较规则：
     a,b,c三项，数值大的为较新版本
     依次比较a,b,c三项，如果a能区分大小，则忽略b,c的比较，如：1.0.1与0.5.2, 1.0.1为较新版本
     如果a不能区分大小，则比较b的大小，数值大者为新版本， 如：1.0.1与1.1.2，1.1.2为较新版本
     如果，a,b不能区分大小，则需比较c的大小，大者为新版本，如：1.0.1与1.0.5, 1.0.5为较新版本
     函数返回：
     如果新版本大于旧版本，则返回True，相反的，新版本小于或者等于旧版本，返回False
    """
    try:
      newList = newVersion.split('.')
      oldList = oldVersion.split('.')
      if len(newList) != 3:
        raise RDSException("new version {0} error".format(newVersion))
      if len(oldList) != 3:
        raise RDSException("old version {0} error".format(oldVersion))
      for index in range(0, len(newList)):
        new = int(newList[index])
        old = int(oldList[index])
        if new > old:
          return True
        elif new == old:
          continue
        else:
          return False
    except:
      raise RDSException("version compare exception, new version: {0}, old version: {1}".format(newVersion, oldVersion))
    return False

  @staticmethod
  def getFreeMem():
    '''
    return the info of /proc/meminfo as a dictionary
    '''
    meminfo = {}
    with open('/proc/meminfo') as f:
      for line in f:
        meminfo[line.split(':')[0]] = line.split(':')[1].strip()
    return long(meminfo['MemFree'][:-2].strip()) * 1024

  @staticmethod
  def getCurrentTime():
    '''
    返回一个日期字符串，用以备份binlog的时候创建目录
    字符串的形式如：201410171643
    '''
    return time.strftime('%y%m%d%H%M')

