# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-15
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:  命令检测类，用于检测管理服务器发送到agent的命令格式是否正确
"""
from util import Util
from common import Common

class Command:
  """
   命令格式检测类，检查管理服务器发送过来的命令格式是否正确
  """
  def __init__(self):
    self.cmdmap = {"Action" : "null"}
    self.errmsg=""


  def reset(self):
    """
     清空命令map
    """
    self.cmdmap = {"Action":"null"}


  def __readValue__(self, key, params):
    """
      从params中读取键为key的数据，保存到cmdmap中，如果读取失败，返回False
    """
    value = params.get(key, 0)
    if value == 0:
      self.errmsg = 'read error, no {0} information'.format(key)
      return False
    self.cmdmap[key] = value
    return True


  def checkUnifyLog(self, params):
    """
     检查命令是否为统一日志格式，如果是则返回True，不是返回False，统一日志格式需要包括：
     id，seq，module，op，object
    """
    return self.__readValue__('id', params) \
           and  self.__readValue__('seq', params) \
           and  self.__readValue__('module',params) \
           and  self.__readValue__('op', params)  \
           and  self.__readValue__('object', params)


  def checkPublic(self, params):
    """
     检查命令的公共部分格式
    """
    if not self.__readValue__('Action', params):
      return False
    if not self.__readValue__('VmId', params):
      return False
    
    if (params.get('Action', 0) == "GetErrorLog"
            or params.get('Action', 0) == "DescribeSqlStats"
            or params.get('Action', 0) == "DescribeTableStats"
            or params.get('Action', 0) == "GetServiceStatus"
            or params.get('Action', 0) == "DescribeSlowQuery"
            or params.get('Action', 0) == "ChangeSync_Sync"
            or params.get('Action', 0) == "ChangeAsync_Sync"			
            or params.get('Action', 0) == "StopImportDB"
            or params.get('Action', 0) == "GetRelayLogSize"
            or params.get('Action', 0) == "CreateRoute"
            or params.get('Action', 0) == "DeleteRoute"
            or params.get('Action', 0) == "DescribeRoute"):
      return True

    if not self.__readValue__('Version', params) or \
       not params['Version'].isdigit():
      return False

    logInfo = params.get('LogInfo', 0)
    if logInfo == 0:
      return False

    if not self.checkUnifyLog(Util.json2Obj(logInfo)):
      return False
    return True


  def checkChangeHA(self, params):
    """
     检测change ha命令格式
    """
    if not self.__readValue__('DBInstanceIdentifier', params):
      return False

    self.__readValue__('VMmoniterInfo', params)
    self.__readValue__('DBParameters', params)
    self.__readValue__('RelayLogSizeThreshold', params)
    self.__readValue__('RelayLogSizeInterval', params)
    self.__readValue__('MasterBinlog', params)
    self.__readValue__('MasterOffset', params)
    return True

  def checkStartRepl(self, params):
    """
      检查启动复制命令
    """
    self.__readValue__('ReplicaUserPassword', params)
    self.__readValue__('DBParameters', params)
    self.__readValue__('DBInstanceIdentifier', params)
    self.__readValue__('MasterIP', params)
    self.__readValue__('MasterPort', params)
    self.__readValue__('Binlog', params)
    self.__readValue__('BinlogOffset', params)
    self.__readValue__('ReadOnly', params)
    self.__readValue__('VMmoniterInfo', params)
    self.__readValue__('SlaveBinlog', params)
    self.__readValue__('SlaveOffset', params)
    self.__readValue__('isUninstallSemiSync', params)
    return True

  def checkStartHeartBeat(self, params):
    return True

  def checkUpdate(self, params):
    """
     agent升级
    """
    return self.__readValue__('AgentProgrammeUrl', params) and  \
           self.__readValue__('AgentVersion', params)

  def checkCreate(self, params):
    """
    """
    self.__readValue__('DBName', params)
    if not (self.__readValue__('MasterUserName',params) \
        and self.__readValue__('DBInstanceIdentifier', params) \
        and self.__readValue__('MasterUserPassword', params)  \
        and self.__readValue__('DBParameters', params)  \
        and self.__readValue__('ReplicaUserPassword', params) \
        and self.__readValue__('Device', params)  \
        and self.__readValue__('UserProductID', params) \
        and self.__readValue__('ServiceType', params) \
        and self.__readValue__('MonitorAcessKey', params) \
        and self.__readValue__('MonitorSecretKey', params)  \
        and self.__readValue__('MonitorServerIp', params) \
        and self.__readValue__('MonitorServerDomain', params) \
        and self.__readValue__('Routes', params) \
        and self.__readValue__('MonitorWebServerUrl', params)):
      return False
    self.__readValue__('DDBProductID', params)
    self.__readValue__('AggregationDimensions', params)
    self.__readValue__('AggregationMetrics', params)
    return True


  def checkBackup(self, params):
    """
     检查备份命令是否正确
    """
    if not self.__readValue__('SnapshotURL', params):
      return False
    if len(params['SnapshotURL'].split('$')) != 2:
      return False
    self.__readValue__('Binlog', params)
    self.__readValue__('BinlogOffset', params)
    self.__readValue__('IsLastSnapshot', params)
    self.__readValue__('StartPosition', params)
    return self.__readValue__('SnapshotIdentifier', params)

  def checkInnerBackup(self, params):
    """
     检查备份命令是否正确
    """
    return self.__readValue__("Device", params) and self.__readValue__('SnapshotIdentifier', params)


  def checkRestore(self, params):
    """
     检查恢复命令是否正确
    """
    if not (self.__readValue__('Device', params)  \
        and self.__readValue__('SnapshotIdentifier', params)  \
        and self.__readValue__('DBParameters', params)  \
        and self.__readValue__('DBInstanceIdentifier', params)  \
        and self.__readValue__('SnapshotURL', params) \
        and self.__readValue__('UserProductID', params) \
        and self.__readValue__('ServiceType', params) \
        and self.__readValue__('MonitorAcessKey', params) \
        and self.__readValue__('MonitorSecretKey', params)  \
        and self.__readValue__('MonitorServerIp', params) \
        and self.__readValue__('MonitorServerDomain', params) \
        and self.__readValue__('Routes', params) \
        and self.__readValue__('MonitorWebServerUrl', params)):
      return False
    urls = params['SnapshotURL'].split(';')
    for url in urls:
      if len(url.split('$')) != 2:
        return False
    self.__readValue__('Binlog', params)
    self.__readValue__('BinlogOffset', params)
    self.__readValue__('StopDatetime', params)
    self.__readValue__('DDBProductID', params)
    self.__readValue__('AggregationDimensions', params)
    self.__readValue__('AggregationMetrics', params)
    return True
  
  def checkInnerRestore(self, params):
    """
     检查内部恢复命令是否正确
    """
    if not (self.__readValue__('Device', params)  \
        and self.__readValue__('SnapshotIdentifier', params)  \
        and self.__readValue__('DBParameters', params) \
        and self.__readValue__('DBInstanceIdentifier', params)  \
        and self.__readValue__('UserProductID', params) \
        and self.__readValue__('ServiceType', params) \
        and self.__readValue__('MonitorAcessKey', params) \
        and self.__readValue__('MonitorSecretKey', params)  \
        and self.__readValue__('MonitorServerIp', params) \
        and self.__readValue__('MonitorServerDomain', params) \
        and self.__readValue__('Routes', params) \
        and self.__readValue__('MonitorWebServerUrl', params)):
      return False
    self.__readValue__('UpgradeScript', params)
    self.__readValue__('DDBProductID', params)
    self.__readValue__('AggregationDimensions', params)
    self.__readValue__('AggregationMetrics', params)
    return True

  def checkRebuildVolume(self, params):
    if not (self.__readValue__('VolumeID', params) \
        and self.__readValue__("Device", params)  \
        and self.__readValue__("NewDevice", params)):
      return False
    return True
  
  def checkLog(self, params):
    """
     检查日志格式
    """
    return self.__readValue__('EndRow', params) and \
           self.__readValue__('Extent', params)


  def checkReboot(self, params):
    """
     检查重启命令是否正确
    """
    self.__readValue__('DBParameters', params)
    self.__readValue__('BinlogOffset', params)
    self.__readValue__('Binlog', params)
    return True

  def checkModify(self, params):
    """
     检查用户信息修改(用户名，密码)命令是否正确
    """
    self.__readValue__('DBParameters', params)
    self.__readValue__('MasterUserPassword', params)
    self.__readValue__('MasterUserName', params)
    self.__readValue__('NeedChangePassword', params)
    self.__readValue__('DBParameters', params)
    return True


  def checkStop(self, params):
    """
     检查stop命令格式是否正确
    """
    self.__readValue__('SnapshotIdentifier', params)
    if not self.__readValue__('SkipFinalSnapshot', params):
      return False

    if params.get('SkipFinalSnapshot', 'null').upper() == 'FALSE':
      if params.get('SnapshotURL', 0) != 0 and len(params['SnapshotURL'].split('$')) == 2:
        self.cmdmap['SnapshotURL'] = params['SnapshotURL']
      else:
        return False
    return True


  def checkStart(self, params):
    """
     检查启动命令格式是否正确
    """
    if not (self.__readValue__('Device', params)  \
        and self.__readValue__('Routes', params) \
        and self.__readValue__('DBParameters', params)):
      return False

    self.__readValue__('AllocatedStorage', params)
    self.__readValue__('UserProductID', params)
    self.__readValue__('DBInstanceIdentifier', params)
    self.__readValue__('MonitorServerIp', params)
    self.__readValue__('ServiceType', params)
    self.__readValue__('MonitorAcessKey', params)
    self.__readValue__('MonitorServerDomain', params)
    self.__readValue__('MonitorSecretKey', params)
    self.__readValue__('MasterUserName', params)
    self.__readValue__('ReplicaUserPassword', params)
    self.__readValue__('MonitorWebServerUrl', params)
    self.__readValue__('UpgradeScript', params)
    return True

  def checkChangeAsync(self, params):
    """
     检查切异步命令是否正常
    """
    return self.__readValue__('DBParameters', params)


  def checkChangeSync(self, params):
    """
     检查切同步命令是否正常
    """
    return self.__readValue__('DBParameters', params)

  def checkDescribeSqlStats(self, params):
    """
     检查获取sql stats命令是否正常
    """
    return self.__readValue__('StartTime', params) and \
           self.__readValue__('EndTime', params)

  def checkDescribeTableStats(self, params):
    """
     检查获取table stats命令是否正常
    """
    return self.__readValue__('StartTime', params) and \
           self.__readValue__('EndTime', params)

  def checkGetServerStatus(self, params):
    """
    检查getServiceStatus 命令的参数是否正确
    """
    return self.__readValue__('Operation', params)

  def checkDescribeSlowQuery(self, params):
    """
     检查获取slow log命令是否正常
    """
    return self.__readValue__('StartTime', params) and \
           self.__readValue__('EndTime', params)


  def checkFlashbackDBInstance(self, params):
    """
     检查获取flash back db instance命令的参数是否正确
    """
    return (self.__readValue__('Binlog', params) and
           self.__readValue__('BinlogOffset', params))


  def checkSetReadOnly(self, params):
    """
     检查获取flash back db instance命令的参数是否正确
    """
    self.__readValue__('isKillConnection', params)
    self.__readValue__('ReadOnly', params)
    return True
  
  def checkDumpExternalDBData(self, params):
    """
     检查外部数据dump命令是否正常
    """
    if not (self.__readValue__('HostIp', params) and
            self.__readValue__('Port', params) and
            self.__readValue__('UserName', params) and
            self.__readValue__('Password', params) and
            self.__readValue__('Databases', params) and
            self.__readValue__('Device', params) and
            self.__readValue__("Threads", params) and
            self.__readValue__("MonitorValues", params) and
            self.__readValue__("RowsPerdump", params) and
            self.__readValue__("MigrateType", params)):
      return False
    self.__readValue__("MigrateGrants", params)
    self.__readValue__("LockTimeout", params)
    # 参数检查
    try:
      params['Port'] = long(params['Port'])
      params['Threads'] = long(params['Threads'])
      params['RowsPerdump'] = long(params['RowsPerdump'])
      params['MigrateType'] = long(params['MigrateType'])
    except Exception, e:
      Common.rdsLog.logInfo("{0}".format(e))
      return False
    return True

  def checkLoadExternalDBData(self, params):
    """
     检查外部数据load命令是否正常
    """
    if not (self.__readValue__('Device', params) and
            self.__readValue__('Threads', params) and
            self.__readValue__('CompressKey', params) and
            self.__readValue__('MigrateGrants', params) and
            self.__readValue__('Databases', params)):
      return False
    self.__readValue__('CompressTables', params)
    # 参数检查
    try:
      params['Threads'] = long(params['Threads'])
      params['CompressKey'] = long(params['CompressKey'])
      params['MigrateGrants'] = long(params['MigrateGrants'])
    except Exception, e:
      Common.rdsLog.logInfo("{0}".format(e))
      return False
    return True

  def checkSyncExternalDB(self, params):
    """
     检查外部实例同步命令是否正常
    """
    if not (self.__readValue__('HostIp', params) and
           self.__readValue__('Port', params) and
           self.__readValue__('UserName', params) and
           self.__readValue__('Password', params) and
           self.__readValue__('Databases', params) and
           self.__readValue__('Binlog', params) and
           self.__readValue__('BinlogOffset', params)):
      return False
    # 参数检查
    try:
      params['Port'] = long(params['Port'])
      params['BinlogOffset'] = long(params['BinlogOffset'])
    except Exception, e:
      Common.rdsLog.logInfo("{0}".format(e))
      return False
    return True

  def checkStopImportDB(self, params):
    """
     检查停止数据导入命令是否正常
    """
    return self.__readValue__('Device', params)

  def checkDumpGrants(self, params):
      """
       检查导出权限的消息是否正确
      """
      return self.__readValue__('NOSFileURL', params)

  def checkLoadGrants(self, params):
      """
       检查导入权限的消息是否正确
      """
      return self.__readValue__('NOSFileURL', params)

  def checkCreateRoute(self, params):
    """
    检查CreateRoute的消息是否正确
    """
    if not self.__readValue__('Routes', params):
      return False
    try:
      routeDicts = params.get('Routes') 
      for routeDict in routeDicts:
        for key in ['Destination', 'Gateway', 'Mask', 'NetworkInterface']:
          if key not in routeDict:
            Common.rdsLog.logInfo("Parse RouteDict error, key %s not found: %s" % (key, routeDicts))
            return False
    except Exception,e:
      Common.rdsLog.logInfo("{0}".format(e))
      return False
    return True

  def checkDeleteRoute(self, params):
    """
    检查DeleteRoute的消息是否正确
    """
    if not self.__readValue__('Routes', params):
      return False
    try:
      routeDicts = params.get('Routes') 
      for routeDict in routeDicts:
        for key in ['Destination', 'Gateway', 'Mask', 'NetworkInterface']:
          if key not in routeDict:
            Common.rdsLog.logInfo("Parse RouteDict error, key %s not found: %s" % (key, routeDicts))
            return False
    except Exception,e:
      Common.rdsLog.logInfo("{0}".format(e))
      return False
    return True

  def checkDescribeRoute(self, params):
    """
    检查DescribeRoute的消息是否正确
    """
    return True