#!/usr/bin/python
#-*- coding: UTF-8 -*-

import socket
import sys
from ast import literal_eval
from common import *


try:
  import readline
  readline.parse_and_bind('tab: complete')
  readline.parse_and_bind('set editing-mode emacs')
except:
  Common.rdsLog.logInfo('Cannot ``import readline`` package.')


class bcolors:
  OKGREEN = '\033[1;32;40m'
  WARNING = '\033[1;31;40m'
  ENDC = '\033[0m'


def main():
    port = 5678
    host = "localhost"
    tcpCliSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ADDR = (host, port)
    tcpCliSock.connect(ADDR)

    #interactive mode
    print """
    Welcome to Mingxing LAI's database client.
    Usage:
        JUST INPUT SQL STATEMENT
        QUIT for exit.    """

    exit = ['QUIT', 'EXIT', 'BYE']
    while True:
      data = raw_input('> ')
      if not data:
        continue
      elif data.strip().upper() in exit:
        print "bye"
        tcpCliSock.close()
        sys.exit(0)
      else:
        tcpCliSock.send(data)
        data = tcpCliSock.recv(10240)
        try:
          lines = literal_eval(data)
          for line in lines:
            # 先将数据转换为字符串
            temp = [ "{0:<12}".format(item) for item in  line ]
            # 将各个列用空格连接起来
            print bcolors.OKGREEN + '  '.join(temp) +  bcolors.ENDC
        except Exception, e:
          print bcolors.WARNING + data + bcolors.ENDC


if __name__ == '__main__':
  main()

