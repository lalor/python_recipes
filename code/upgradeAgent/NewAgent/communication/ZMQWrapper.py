# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-28
@contact: jhx1008@gmail.com
version:  1.0
@todo:    ZMQ功能封装
@modify:
"""

import zmq
from common import Common

class ZMQSocketType:
  """
   ZMQ创建socket的类型
  """

  ROUTER = zmq.ROUTER
  DEALER = zmq.DEALER
  POLLIN = zmq.POLLIN
  SNDMORE = zmq.SNDMORE
  REQ    = zmq.REQ
  LINGER = zmq.LINGER
  
class ZMQProtocolType:
  """
   ZMQ通讯类型
  """

  TCP = 'tcp'
  INPROC = 'inproc'
  IPC = 'ipc'
  MULTICAST = 'multicast'

class ZMQComm:
  """
   ZMQ通讯基础类，初始化通讯模式
  """
  context = None

  def __init__(self, protocolType, socketType, host, port):
    self.connectString = ""
    if protocolType == ZMQProtocolType.TCP:
      self.connectString = '{0}://{1}:{2}'.format(protocolType, host, port)
    elif protocolType == ZMQProtocolType.INPROC:
      self.connectString = '{0}://{1}'.format(protocolType, host)
    elif protocolType == ZMQProtocolType.IPC:
      self.connectString = '{0}://{1}'.format(protocolType, host)
    elif protocolType == ZMQprotocolType.MULTICAST:
      pass
    else:
      pass

    self.socket = ZMQComm.context.socket(socketType)

  # 一个进程只允许有一个context，请在所有zmq调用前调用该函数
  @staticmethod
  def initContext():
    ZMQComm.context = zmq.Context().instance()

  @staticmethod
  def uninitContext():
    if ZMQComm.context != None:
      ZMQComm.context.term()

  def setsockopt(self, option, optval):
    self.socket.setsockopt(option, optval)

  def recv(self, type=0):
    return self.socket.recv(type)

  def send(self,msg,type=0):
    self.socket.send(msg, type)

  def sendMulti(self, msg):
    self.socket.send_multipart(msg)

  def recvMulti(self):
    return self.socket.recv_multipart()

  def close(self):
    self.socket.close()

class ZMQClient(ZMQComm):
  """
   ZMQ客户端
  """
  def __init__(self, protocolType, socketType, host, port):
    ZMQComm.__init__(self, protocolType, socketType, host, port)
    self.socket.connect(self.connectString)
    Common.rdsLog.logInfo('create zmq client, connect string:{0}'.format(self.connectString))

class ZMQServer(ZMQComm):
  """
   ZMQ服务端
  """
  def __init__(self, protocolType, socketType, host, port):
    ZMQComm.__init__(self, protocolType, socketType, host, port)
    self.socket.bind(self.connectString)
    Common.rdsLog.logInfo('create zmq server, connect string:{0}'.format(self.connectString))

class ZMQPoll:
  """
   ZMQ poll操作
  """
  def __init__(self):
    self.poller = zmq.Poller()
    self.sockets = {}

  def register(self, zmqComm, socketType):
    """
     注册zmq通讯端到poller
    """
    self.poller.register(zmqComm.socket, socketType)

  def unregister(self, zmqComm):
    """
     解除zmq通讯端到poller的注册
    """
    self.poller.unregister(zmqComm.socket)

  def poll(self, timeout=None):
    self.sockets = dict(self.poller.poll(timeout))

  def isActive(self, zmqComm):
    """
     判断通讯端是否有读写事件
    """
    return (zmqComm.socket in self.sockets) and self.sockets[zmqComm.socket] == zmq.POLLIN
