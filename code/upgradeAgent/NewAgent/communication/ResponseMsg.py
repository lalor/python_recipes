# -*- coding:UTF-8 -*-
"""
@author:  hzjianghongxiang
@date:    2013-11-29
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:
"""
import json
import time
from common import Common

class ResponseMsg:
  mapCmd = {"CreateDBInstance":"RDSCreateResult",
            "StopDBInstance":"RDSDeleteResult",
            "StopSlave":"RDSStopSlaveResult",
            "StartSlave":"RDSStartSlaveResult",
            "RebootDBInstance":"RDSRebootResult",
            "StartDBInstance":"RDSStartResult",
            "UpgradeAgent":"RDSAgentUpgradeResult",
            "ChangeHA":"ChangeHAResult",
            "StartReplication":"StartReplicationResult",
            "ChangeSync":"ChangeSyncResult",
            "ChangeSync_Sync":"ChangeSyncResult",
            "ModifyDBInstance":"RDSModifyResult",
            "ChangeAsync":"ChangeAsyncResult",
            "ChangeAsync_Sync":"ChangeAsyncResult",
            "Backup":"RDSBackupResult",
            "RestoreDBInstanceFromSnapshot":"RDSRestoreResult",
            "RebuildVolume":"RDSRebuildVolumeResult",
            "InnerBackup":"RDSBackupResult",
            "InnerRestoreDBInstanceFromSnapshot":"RDSRestoreResult",
            "FlashbackDBInstance":"RDSFlashbackDBInstanceResult",
            "SetReadOnly":"RDSSetReadOnlyResult",
            "DumpExternalDBData":"ImportDBDumpResult",
            "LoadExternalDBData":"ImportDBLoadResult",
            "SyncExternalDB":"ImportDBSyncResult",
            "StopImportDB":"ImportDBStopResult",
            "DumpGrants":"RDSDumpGrantsResult",
            "LoadGrants":"RDSLoadGrantsResult"
  }

  @staticmethod
  def commonOK(params):
    # master命令正确执行返回
    saveRouteStatus = 'true'
    if 'saveRouteStatus' in params and params['saveRouteStatus'] is False: 
      saveRouteStatus = 'false'
    return json.dumps({"Action":ResponseMsg.mapCmd[params["Action"]], \
            "IsSucess":"true", "VmId":Common.databaseInstanceID, "Version":Common.msgVersion,
            "SaveRouteStatus": saveRouteStatus})

  @staticmethod
  def commonFail(params,  failReason):
    # master命令执行错误返回
    return json.dumps({"Action":ResponseMsg.mapCmd[params["Action"]], \
            "IsSucess":"false","VmId":Common.databaseInstanceID, "FailedReason":failReason,"Version":Common.msgVersion})

  @staticmethod
  def modifyFail(result,  failReason):
    # master命令执行结果，修改失败返回结果
    result["IsSucess"] = "false"
    result["FailedReason"] = failReason
    result["Version"] = Common.msgVersion
    return json.dumps(result)

  @staticmethod
  def stopImportOK():
    return json.dumps({"IsSucess" : "true", "VmId" : Common.databaseInstanceID})

  @staticmethod
  def stopImportFail(failReason):
    return json.dumps({"IsSucess":"false", "FailedReason":failReason, "VmId": Common.databaseInstanceID})

  @staticmethod
  def getErrorLogMsg(rows, logInfo):
    return json.dumps({"IsAccept" : "true", "NextRow" : "{0}".format(rows), \
              "DBErrorLog" : logInfo, "VmId" : Common.databaseInstanceID})

  @staticmethod
  def getSqlStatsMsg(rows):
    return json.dumps({"IsAccept" : "true", "SqlStats" : {"SqlStats":rows}, "VmId" : Common.databaseInstanceID})

  @staticmethod
  def getTableStatsMsg(rows):
    return json.dumps({"IsAccept" : "true", "TableStats" : {"Records":rows}, "VmId" : Common.databaseInstanceID})

  @staticmethod
  def getSlowLogMsg(rows):
    return json.dumps({"IsAccept" : "true", "SlowQuery" : {"Result":rows}, "VmId" : Common.databaseInstanceID})

  @staticmethod
  def getRelayLogSizeMsg(size):
    return json.dumps({"IsAccept" : "true", "RelayLogSize" : size, "VmId" : Common.databaseInstanceID})

  @staticmethod
  def getServiceStatusMsg(serviceStatus, errInfo):
    return json.dumps({"IsAccept" : "true", "ServiceStatus" : serviceStatus, "VmId" : Common.databaseInstanceID, "FailedReason": errInfo})

  @staticmethod
  def restoreFailMsg(params, reason):
    # master命令执行结果，restore失败返回结果
    return json.dumps({"Action":ResponseMsg.mapCmd[params["Action"]],"IsSucess":"false", \
            "FailedReason":reason, "VmId":Common.databaseInstanceID, "Version":Common.msgVersion, \
            "SnapshotIdentifier":params.get("SnapshotIdentifier","null")})

  @staticmethod
  def restoreOkMsg(params):
    # master命令执行结果，restore成功返回结果
    return json.dumps({"Action":ResponseMsg.mapCmd[params["Action"]], "IsSucess":"true", \
            "VmId":Common.databaseInstanceID, "Version":Common.msgVersion, \
            "SnapshotIdentifier":params.get("SnapshotIdentifier","null")})

  @staticmethod
  def backupFailMsg(params, errInfo):
    # master命令执行结果，backup失败返回
    return json.dumps({"Action":ResponseMsg.mapCmd[params["Action"]], "IsSucess":"false", \
            "VmId":Common.databaseInstanceID, "SnapshotIdentifier":params.get("SnapshotIdentifier","null"), \
            "SnapshotURL":params.get("SnapshotURL","null"), \
            "FailedReason":errInfo, "Version":Common.msgVersion})
            	
  @staticmethod
  def backupOkMsg(params, master, masterOffset, backupSize, slave=None, slaveOffset=None, startDatetime=None, endDatetime=None):
    # master命令执行结果，backup成功返回
    return json.dumps({"Action":ResponseMsg.mapCmd[params["Action"]],
        "IsSucess":"true",
        "SnapshotIdentifier":params.get("SnapshotIdentifier","null"),
        "SnapshotURL":params.get("SnapshotURL","null"),
        "Binlog":master,"BinlogOffset":"{0}".format(masterOffset),
        "BackupSize":"{0}".format(backupSize),
        "VmId":Common.databaseInstanceID,
        "Version":Common.msgVersion,
        "SlaveBinlog":slave,
        "SlaveOffset":slaveOffset,
        "StartDatetime":startDatetime,
        "EndDatetime":endDatetime,
        "IsLastSnapshot":params.get("IsLastSnapshot", "null")
    })
    
  @staticmethod
  def dumpExternalOkMsg(params, fileName, offset, host, port):
    # dump外部实例执行结果，dump成功返回
    return json.dumps({"Action":ResponseMsg.mapCmd[params["Action"]],
                       "IsSucess":"true",
                       "Binlog":fileName,
                       "HostIp":host,
                       "Port":port,
                       "BinlogOffset":"{0}".format(offset),
                       "VmId":Common.databaseInstanceID,
                       "Version":Common.msgVersion})

  @staticmethod
  def changeHAOkMsg(params, masterBinlog, masterOffset, slaveBinlog, slaveOffset):
    # master命令执行结果，changeHA成功返回
    return json.dumps({"Action":ResponseMsg.mapCmd[params["Action"]], \
            "IsSucess":"true", "VmId":Common.databaseInstanceID, "SlaveBinlog":slaveBinlog, \
            "SlaveOffset":"{0}".format(slaveOffset), "MasterBinlog":masterBinlog, \
            "MasterOffset":"{0}".format(masterOffset), "Version":Common.msgVersion})

  @staticmethod
  def heartBeatMsg(status, replDelayTime, replErrorInfo, replSyncTime, replSyncStatus):
    return json.dumps({"Action":"RDSHeatBeat", "VmId":Common.databaseInstanceID, "UUID":Common.vmUUID, "InstanceStatus":status,\
                       "RepliDelayTime":"{0}".format(replDelayTime), "RepliErrInfo":replErrorInfo,\
                       "ReplSyncTime":"{0}".format(replSyncTime), "ReplSyncStatus":replSyncStatus,\
                       "TimeStamp":"%ld" % (time.time() * 1000)})


  # 以下部分为同步返回内容，在解析完master命令后就返回的信息，在执行命令之前返回
  @staticmethod
  def errFormat(param, errMsg):
    return json.dumps({"IsAccept":"false", "VmId":param["VmId"],
                        "RefusedReason":"cmd format is wrong, errMsg: {0}".format(errMsg)})

  @staticmethod
  def errBusy(param, msg):
    return json.dumps({"IsAccept":"false","VmId":param["VmId"],
                        "RefusedReason":"Agent is busying now", "IsBusy":"true"})

  @staticmethod
  def errDatabaseID(param, dbInstanceID):
    return json.dumps({"IsAccept":"false","VmId":param["VmId"],
                      "RefusedReason":"DBIdentifier is wrong, agent DBIdentifier: {0}".format(dbInstanceID)})

  @staticmethod
  def errVersion(param, msgVersion):
    # 版本错误
    return json.dumps({"IsAccept":"false", "VmId":param["VmId"],
                        "RefusedReason":"version is wrong, agent is now at version:{0}".format(msgVersion)})

  @staticmethod
  def sendingNow(param, cmd):
    # agent忙状态
    return json.dumps({"IsAccept":"false", "VmId":param["VmId"],
                        "RefusedReason":"Agent is sending now", "IsBusy":"true"})

  @staticmethod
  def sendOK(param, cmd):
    return json.dumps({"IsAccept":"true", "VmId":param["VmId"]})

  @staticmethod
  def createRouteOK():
    return json.dumps({"IsSucess" : "true", "VmId" : Common.databaseInstanceID, "Action": "CreateRoute"})

  @staticmethod
  def createRouteFail(failReason):
    return json.dumps({"IsSucess":"false", "FailedReason": failReason, "VmId": Common.databaseInstanceID, "Action": "CreateRoute"})

  @staticmethod
  def deleteRouteOK():
    return json.dumps({"IsSucess" : "true", "VmId" : Common.databaseInstanceID, "Action": "DeleteRoute"})

  @staticmethod
  def deleteRouteFail(failReason):
    return json.dumps({"IsSucess":"false", "FailedReason":failReason, "VmId": Common.databaseInstanceID, "Action": "DeleteRoute"})

  @staticmethod
  def describeRoutesOK(routes):
    return json.dumps({"IsSucess":"true", "VmId":Common.databaseInstanceID, "Routes": routes, "Action": "DescribeRoute"})

  @staticmethod
  def describeRoutesFail(failReason):
    return json.dumps({"IsSucess":"false", "FailedReason":failReason, "VmId": Common.databaseInstanceID, "Action": "DescribeRoute"})
