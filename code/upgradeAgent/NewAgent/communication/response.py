# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-12
@contact: jhx1008@gmail.com
version:  1.0
@todo:    信息恢复类，接收Manager命令后，处理命令完成，把结果返回给Manager
@modify:
"""

from util import Util
from ZMQWrapper import *
from common import Common
from config.RDSConfig import RDSConfig

class Response:
  """
   处理结果返回类
  """
  def __init__(self):
    """
     初始化函数，定义两个用于与Manager通讯的zmq对象(其实其中一个是与Agent内部通讯后转发)
    """
    self.internalClient = ZMQClient(ZMQProtocolType.INPROC, ZMQSocketType.DEALER, 'rdsagentinternal', 0)
    self.externalClient = ZMQClient(ZMQProtocolType.TCP, ZMQSocketType.REQ, RDSConfig.managerIP, RDSConfig.managerPort)
    self.externalClient.setsockopt(ZMQSocketType.LINGER, 0)
    self.poller = ZMQPoll()
    self.poller.register(self.externalClient, ZMQSocketType.POLLIN)

  def sendSync(self, id, msg):
    """
     同步发送，Manager哪个端口发送的信息，结果就回送到该端口
    """
    self.internalClient.sendMulti([id, msg])


  def sendAsync(self, msg):
    """
     异步发送，agent把处理结果发送到Manager监听的端口
    """
    while 1:
      try:
        self.externalClient.send(msg)
        self.poller.poll(30*1000)
        if self.poller.isActive(self.externalClient):
          message = self.externalClient.recvMulti()
          Common.rdsLog.logDebug("recv manager aplay: {0}".format("".join(message)))
          break
        else:
          Common.rdsLog.logWarn('async send result to manager timeout')
          Util.sleep(5)
      except:
        # 发送出错，可能Manager还没有启动，需要重新创建后连接
        Common.rdsLog.logWarn("send async error, can not connect the server, reconnect now!")
        self.poller.unregister(self.externalClient)
        self.externalClient.close()
        self.externalClient = ZMQClient(ZMQProtocolType.TCP, ZMQSocketType.REQ, RDSConfig.managerIP, RDSConfig.managerPort)
        self.externalClient.setsockopt(ZMQSocketType.LINGER, 0)
        self.poller.register(self.externalClient, ZMQSocketType.POLLIN)
        Util.sleep(2)

  def uninit(self):
    self.poller.unregister(self.externalClient)
    self.internalClient.close()
    self.externalClient.close()
