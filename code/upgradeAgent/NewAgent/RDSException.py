# -*- coding: utf-8 -*-
"""
@author:  hzjianghongxiang
@date:    2013-11-04
@contact: jhx1008@gmail.com
@version: 1.0
@todo:    定义一个异常类RDSException用于处理在Agent运行过程中产生的异常
@modify:
"""

class RDSException(Exception):
  """
   异常类EDSException继承自Exception，用于处理在Agent运行过程中的异常信息
  """
  pass
