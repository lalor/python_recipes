#!/usr/bin/python
#-*- coding: UTF-8 -*-


import re
import json
import subprocess
from util import Util
from common import Common


ROUTE_DEFAULT = "default\svia\s(\d+\.\d+\.\d+.\d+)\sdev\s(\w+)"
ROUTE_SRC = "(\d+\.\d+\.\d+\.\d+)/(\d+)\sdev\s(\w+)\s+proto\skernel\s+scope\slink\s+src\s(\d+\.\d+\.\d+\.\d+)"
ROUTE_NORMAL = "(\d+\.\d+\.\d+\.\d)/(\d+)\svia\s(\d+\.\d+\.\d+\.\d+)\sdev\s(\w+)"

NIC_IPADDR = "\s*inet\s(\d+\.\d+\.\d+\.\d+)/(\d+)\sbrd\s\d+\.\d+\.\d+\.\d+\sscope\sglobal\s(\w+)\s*"

def execCommand(cmd):
    Common.rdsLog.logInfo("start execute command: %s" % (cmd, ))
    p = subprocess.Popen(cmd,
                         shell=True,
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    if p.returncode != 0:
        Common.rdsLog.logInfo("exec cmd failed: %s \n: reason: %s" % (cmd, stderr))
        return None
    else:
        return stdout

def getIPAddrByInterface(eth0):
    stdout = execCommand("sudo ip a")
    hosts = []
    for line in stdout.split('\n'):
        if re.match(NIC_IPADDR, line):
            m = re.match(NIC_IPADDR, line)
            host = m.group(1)
            mask = m.group(2)
            networkInterface = m.group(3)
            if networkInterface == eth0:
                hosts.append(host)
    return hosts


def filterLocalhost(hosts):
    """
    filter 127.0.0.1 and localhost ipAddress in hosts array list.
    @param hosts: [ip0, ip2, ..., ]
    """
    return [host for host in hosts if host not in ['127.0.0.1', 'localhost']]


def route2Command(routeDict):
    command = None
    if routeDict is None:
        return command
    for key in ['Destination', 'Gateway', 'Mask', 'NetworkInterface']:
        if key not in routeDict:
            return command
    # default route
    if routeDict['Destination'] == '0.0.0.0':
        command = 'default via %s dev %s' % (routeDict['Gateway'], 
                                             routeDict['NetworkInterface'])
        return command.strip()
    # source route
    if routeDict['Gateway'] == '0.0.0.0':
        hosts = getIPAddrByInterface(routeDict['NetworkInterface'])
        hosts = filterLocalhost(hosts)
        if len(hosts) == 0:
            return command
        command = '%s/%s dev %s  proto kernel  scope link  src %s' \
                    % (routeDict['Destination'], 
                       routeDict['Mask'], 
                       routeDict['NetworkInterface'],
                       hosts[0])
        return command.strip()

    # noraml route
    command = '%s/%s via %s dev %s' % (routeDict['Destination'], 
                                       routeDict['Mask'], 
                                       routeDict['Gateway'],
                                       routeDict['NetworkInterface'])
    return command.strip()


class RouteManager:

    def __init__(self, fileDir, fileName, user='rds-user', group='rds-user'):
        self.fileDir = fileDir
        self.fileName = fileName
        self.user = user
        self.group = group

        fullPath = '{0}/{1}'.format(fileDir, fileName)
        self.fullPath = fullPath

        if not Util.isExists(fileDir):
            Util.execCommand("sudo mkdir -p {0}".format(fileDir))
            Util.execCommand("sudo chown -R {0}:{1} {2}".format(user, group, fileDir))

        if not Util.isExists(fullPath):
            with open(fullPath,'w') as fd:
                fd.write(execCommand('sudo ip route'))

    def updateRouteInfo(self):
        with open(self.fullPath, 'w') as fd:
            fd.write(execCommand('sudo ip route'))

    def loadRouteInfo(self):
        data = ''
        with open(self.fullPath) as fd:
            data = fd.read()
        dataLines = data.split('\n')
        routeDicts = self.command2RouteDict(dataLines)
        result = True
        for route in routeDicts:
            if not self.createRoute(route):
                result = False
        return result

    def command2RouteDict(self, lines):
        """
        @param lines: execute ip route command ==> stdout.split('\n')
        """
        routeDicts = []
        for line in lines: 
            route = self.extractDefault(line)
            if route is None:
                route = self.extractSRC(line)
            if route is None:
                route = self.extractNormal(line)
            if route is not None:
                routeDicts.append(route)
        return routeDicts

    def extractDefault(self, line):
        route = None
        if line is not None and re.match(ROUTE_DEFAULT, line):
            m = re.match(ROUTE_DEFAULT, line)
            route = dict(Destination='0.0.0.0',
                         Gateway=m.group(1),
                         Mask=0,
                         NetworkInterface=m.group(2))
        return route

    def extractSRC(self, line):
        route = None
        if line is not None and re.match(ROUTE_SRC, line):
            m = re.match(ROUTE_SRC, line)
            route = dict(Destination=m.group(1),
                         Gateway='0.0.0.0',
                         Mask=m.group(2),
                         NetworkInterface=m.group(3))
        return route

    def extractNormal(self, line):
        route = None
        if line is not None and re.match(ROUTE_NORMAL, line):
            m = re.match(ROUTE_NORMAL, line)
            route = dict(Destination=m.group(1),
                         Gateway=m.group(3),
                         Mask=m.group(2),
                         NetworkInterface=m.group(4))
        return route


    def describeAll(self): 
        stdout = execCommand("sudo ip route")
        if stdout is None:
            return False, "Execute command failed: sudo ip route"

        routes = []
        for line in stdout.split('\n'): 
            route = self.extractDefault(line)
            if route is None:
                route = self.extractSRC(line)
            if route is None:
                route = self.extractNormal(line)
            if route is not None:
                routes.append(route)
        return True, routes

    def describeRouteCommand(self):
        stdout = execCommand('sudo ip route')
        return [line.strip() for line in stdout.split('\n')]

    def createRoute(self, routeDict): 
        """
        @param routeDict:   route dict 
        """
        routeCmd = route2Command(routeDict)
        routeCmds = self.describeRouteCommand()
        if routeCmd in routeCmds:
            Common.rdsLog.logInfo("route already exists:  %s, createRoute skipped." % (routeCmd, )) 
            self.updateRouteInfo()
            return True
        cmd = "sudo ip route add %s" % (routeCmd, )
        if Util.execCommand(cmd) != True:
            Common.rdsLog.logInfo("create route failed: %s" % (cmd, ))
            return False
        self.updateRouteInfo()
        return True

    def deleteRoute(self, routeDict): 
        """
        @param routeDict:   route dict 
        """
        routeCmd = route2Command(routeDict)
        routeCmds = self.describeRouteCommand()
        if routeCmd not in routeCmds:
            Common.rdsLog.logInfo("route not exists: %s, deleteRoute skipped." % (routeCmd, ))
            self.updateRouteInfo()
            return True
        cmd = "sudo ip route delete %s" % (routeCmd, )
        if Util.execCommand(cmd) != True:
            Common.rdsLog.logInfo("delete route failed: %s" % (cmd, ))
            return False
        self.updateRouteInfo()
        return True

    def saveRouteInfo(self, routeDicts):
        ret = True
        for routeDict in routeDicts:
            if not self.createRoute(routeDict):
                ret = False
        return ret
