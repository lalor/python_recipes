# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-10
@contact: jhx1008@gmail.com
version:  1.0
@todo:    公共变量存放文件，定义全局变量等等
@modify:
"""

from threads.MsgQueue import MsgQueue

class MonitorProperties:
  dbIdentifier          = "null"
  userProductId         = "null"
  serviceType           = "null"
  aggregationDimensions = {}
  aggregationMetrics    = {}
  monitorWebServerUrl   = "null"
  monitorAcessKey       = "null"
  monitorSecretKey      = "null"

  @staticmethod
  def loadProperties(params):
    MonitorProperties.dbIdentifier          = params.get("DBInstanceIdentifier", "null")
    MonitorProperties.userProductId         = params.get("UserProductID", "null")
    MonitorProperties.serviceType           = params.get("ServiceType", "null")
    MonitorProperties.aggregationDimensions = params.get("AggregationDimensions",{})
    MonitorProperties.aggregationMetrics    = params.get("AggregationMetrics",{})
    MonitorProperties.monitorWebServerUrl   = params.get("MonitorWebServerUrl","null")
    MonitorProperties.monitorAcessKey       = params.get("MonitorAcessKey","null")
    MonitorProperties.monitorSecretKey      = params.get("MonitorSecretKey","null")

  @staticmethod
  def isValid():
    return MonitorProperties.dbIdentifier != "null" and MonitorProperties.userProductId != "null" \
       and MonitorProperties.serviceType  != "null" and MonitorProperties.monitorWebServerUrl != "null" \
       and MonitorProperties.monitorAcessKey != "null" and MonitorProperties.monitorSecretKey != "null"

class Action:
  GETERRORLOG       = 'geterrorlog'
  CHANGEHA          = 'changeha'
  STOPSLAVE         = 'stopslave'
  STARTSLAVE        = 'startslave'
  STARTREPLICATION  = 'startreplication'
  STARTHEARTBEAT    = 'startheartbeat'
  UPGRADEAGENT      = 'upgradeagent'
  CREATEDBINSTANCE  = 'createdbinstance'
  STOPDBINSTANCE    = 'stopdbinstance'
  STARTDBINSTANCE   = 'startdbinstance'
  REBOOTDBINSTANCE  = 'rebootdbinstance'
  BACKUP            = 'backup'
  RESTOREDBINSTANCEFROMSNAPSHOT = 'restoredbinstancefromsnapshot'
  MODIFYDBINSTANCE  = 'modifydbinstance'
  CHANGEASYNC       = 'changeasync'
  CHANGEASYNC_SYNC  = 'changeasync_sync'
  CHANGESYNC        = 'changesync'
  CHANGESYNC_SYNC   = 'changesync_sync'
  REBUILDVOLUME     = 'rebuildvolume'
  EXITAGENT         = 'exitagent'
  DESCRIBESQLSTATS  = 'describesqlstats'
  DESCRIBETABLESTATS= 'describetablestats'
  DESCRIBESLOWQUERY = 'describeslowquery'
  INNERBACKUP       = 'innerbackup'
  INNERRESTOREDBINSTANCEFROMSNAPSHOT = 'innerrestoredbinstancefromsnapshot'
  FLASHBACKDBINSTANCE = 'flashbackdbinstance'
  SETREADONLY = 'setreadonly'
  GETRELAYLOGSIZE = 'getrelaylogsize'
  DUMPEXTERNALDBDATA  = 'dumpexternaldbdata'
  LOADEXTERNALDBDATA  = 'loadexternaldbdata'
  SYNCEXTERNALDB    = 'syncexternaldb'
  STOPIMPORTDB      = 'stopimportdb'
  GETSERVICESTATUS  = 'getservicestatus'
  DUMPGRANTS        = 'dumpgrants'
  LOADGRANTS        = 'loadgrants'
  CREATE_ROUTE      = 'createroute'
  DELETE_ROUTE      = 'deleteroute'
  DESCRIBE_ROUTE    = 'describeroute'

  # action处理集合，key为action的名称，value为处理函数，通过Common::init进行初始化
  handleMap = {}

  @staticmethod 
  def initAction(instanceManager):
    Action.handleMap[Action.GETERRORLOG]          = instanceManager.getErrorLog
    Action.handleMap[Action.CHANGEHA]             = instanceManager.changeHA
    Action.handleMap[Action.STOPSLAVE]            = instanceManager.stopSlave
    Action.handleMap[Action.STARTSLAVE]           = instanceManager.startSlave
    Action.handleMap[Action.STARTREPLICATION]     = instanceManager.startReplication
    Action.handleMap[Action.STARTHEARTBEAT]       = instanceManager.startHeartBeat
    Action.handleMap[Action.UPGRADEAGENT]         = instanceManager.upgradeAgent
    Action.handleMap[Action.CREATEDBINSTANCE]     = instanceManager.createDBInstance
    Action.handleMap[Action.STOPDBINSTANCE]       = instanceManager.stopDBInstance
    Action.handleMap[Action.STARTDBINSTANCE]      = instanceManager.startDBInstance
    Action.handleMap[Action.REBOOTDBINSTANCE]     = instanceManager.rebootDBInstance
    Action.handleMap[Action.BACKUP]               = instanceManager.backup
    Action.handleMap[Action.RESTOREDBINSTANCEFROMSNAPSHOT] = instanceManager.restore
    Action.handleMap[Action.MODIFYDBINSTANCE]     = instanceManager.modifyDBInstance
    Action.handleMap[Action.CHANGEASYNC]          = instanceManager.changeAsync
    Action.handleMap[Action.CHANGEASYNC_SYNC]     = instanceManager.changeAsync
    Action.handleMap[Action.CHANGESYNC]           = instanceManager.changeSync
    Action.handleMap[Action.CHANGESYNC_SYNC]      = instanceManager.changeSync
    Action.handleMap[Action.REBUILDVOLUME]        = instanceManager.rebuildVolume
    Action.handleMap[Action.DESCRIBESQLSTATS]     = instanceManager.getSqlStats
    Action.handleMap[Action.DESCRIBETABLESTATS]   = instanceManager.getTableStats
    Action.handleMap[Action.DESCRIBESLOWQUERY]    = instanceManager.getSlowLog
    Action.handleMap[Action.INNERBACKUP]          = instanceManager.innerBackup
    Action.handleMap[Action.INNERRESTOREDBINSTANCEFROMSNAPSHOT] = instanceManager.innerRestore
    Action.handleMap[Action.FLASHBACKDBINSTANCE] = instanceManager.flashBackDBInstance
    Action.handleMap[Action.SETREADONLY] = instanceManager.setReadOnly
    Action.handleMap[Action.GETRELAYLOGSIZE] = instanceManager.getRelayLogSize
    Action.handleMap[Action.DUMPEXTERNALDBDATA] = instanceManager.dumpExternalDBData
    Action.handleMap[Action.LOADEXTERNALDBDATA] = instanceManager.loadExternalDBData
    Action.handleMap[Action.SYNCEXTERNALDB] = instanceManager.syncExternalDB
    Action.handleMap[Action.STOPIMPORTDB] = instanceManager.stopImportDB
    Action.handleMap[Action.GETSERVICESTATUS] = instanceManager.getServiceStatus
    Action.handleMap[Action.DUMPGRANTS] = instanceManager.dumpGrants
    Action.handleMap[Action.LOADGRANTS] = instanceManager.loadGrants
    Action.handleMap[Action.CREATE_ROUTE] = instanceManager.createRoute
    Action.handleMap[Action.DELETE_ROUTE] = instanceManager.deleteRoute
    Action.handleMap[Action.DESCRIBE_ROUTE] = instanceManager.describeRoute



class Common:
  rdsLog = None
  unifyLog = None
  monitorLog = None
  threads = []
  isAgentRunning = True

  # manager发送过来消息的版本信息，通过第一次接收到的消息的Version进行初始化
  # 用于消息去重，当agent接收到比当前version小的消息时，该消息会被忽略
  msgVersion = 'null'
  # agent标识，通过第一次接收到的消息的VmId进行初始化，初始化后不改变，
  # 用于区分当前消息是否为当前agent处理消息，如果不是第一次初始化，从
  # 持久化文件中进行读取
  databaseInstanceID = 'null'

  vmUUID = ''
  
  # 工作线程与主线程通讯的队列，主线程在接收到Manager的消息，经过前期处理后，
  # 放入消息队列中，工作线程通过扫描消息队列获取相应消息进行处理
  msgQueue = MsgQueue()

  # 同步消息与主线程通讯的队列，主线程在接收到Manager的消息，经过前期处理后，
  # 放入消息队列中，同步消息处理线程通过扫描消息队列获取相应消息进行处理
  syncMsgQueue = MsgQueue()

  # agent状态管理类，需要通过init初始化
  statusManager = None

  # 数据库管理类，需要通过init初始化
  databaseManager = None

  # 存储管理类，需要通过init初始化，而且必须在databaseManager前初始化，databaseManager需要用到
  storageManager = None

  agentInfoManager = None

  diskManager = None

  # UUID状态管理
  uuidManager = None

  # Network Manager
  routeManager = None
