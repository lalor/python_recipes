# -*- coding: UTF-8 -*-
'''
Created on 2012-12-27

@author: sunlzx
'''

import httplib,urllib
import hashlib, hmac
import json;
import traceback
	
class MetricData:
		
	def __init__(self, metricName, value, dimensions, aggregationDimensions, unit):
		self.metricName = metricName;
		self.value = value;	 
		self.dimensions = dimensions;  
		self.aggregationDimensions = aggregationDimensions;
		self.unit = unit;  
		
	def to_dict(self) :
		data = dict();#		
		data["metricName"] = self.metricName;
		data["value"] = self.value;		
		data["dimensions"] = self.dimensions;
		data["aggregationDimensions"] = self.aggregationDimensions;	   
		data["unit"] = self.unit;
		return data;

class SendRequest(object):
	'''
		Send datas to monitor server by accesskey authorization.
	'''
	def __init__(self, metricDatasJson, url, requestURI, projectId1, namespace, accessKey, accessSecret):
		self.url = url
		self.requestURI = requestURI
		self.headers = {'Content-type': 'application/x-www-form-urlencoded'}
		self.monitorInfo = ''
		self.httpMethod = 'POST'
		self.projectId = projectId1
		self.namespace = namespace
		self.accessKey = accessKey
		self.accessSecret = accessSecret
		self.metricDatasJson = metricDatasJson

	def sendRequestToServer(self):
		'''
			Send monitor datas to collect server by POST request.
		'''
		params = urllib.urlencode({
				'ProjectId': self.projectId,
				'Namespace': self.namespace,
				'MetricDatasJson': self.metricDatasJson,
				'AccessKey': self.accessKey,
				'Signature': self.generateSignature()
		})

		if str(self.url).startswith('http://'):
			self.url = str(self.url).split("http://")[-1]
		conn = httplib.HTTPConnection(self.url)
		try:
			conn.request(self.httpMethod, self.requestURI, params, self.headers)
			
			response = conn.getresponse()
			#print(str(response.status))
			#print(str(response.read()))
		except Exception:
			print(traceback.format_exc())
		conn.close()


	def generateStringToSign(self):
		'''
			Generate stringToSign for signature.
		'''
		CanonicalizedHeaders = ''
		CanonicalizedResources = 'AccessKey=%s&MetricDatasJson=%s&Namespace=%s&ProjectId=%s' % \
								(self.accessKey, self.metricDatasJson, self.namespace, self.projectId)

		StringToSign = '%s\n%s\n%s\n%s\n' % \
					  (self.httpMethod, '/rest/V1/MetricData', CanonicalizedHeaders, CanonicalizedResources)

		return StringToSign

	def generateSignature(self):
		'''
			Generate signature for authorization.
			Use hmac SHA-256 to calculate signature string and encode into base64.
			@return String
		'''
		stringToSign = self.generateStringToSign()
		hashed = hmac.new(str(self.accessSecret), stringToSign, hashlib.sha256)
		s = hashed.digest()
		signature = s.encode('base64').rstrip()
		return signature

