# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-11
@contact: jhx1008@gmail.com
version:  1.0
@todo:    模拟RDS Manager向Agent发送消息
@modify:
"""
import zmq
import time
import random
import threading
import crypto

running = True

def serverThread():
  context = zmq.Context().instance()
  socket = context.socket(zmq.REP)
  socket.bind("tcp://*:6000")
  poller = zmq.Poller()
  poller.register(socket, zmq.POLLIN)
  while running:
    sockets = dict(poller.poll(500))
    if socket in sockets and sockets[socket] == zmq.POLLIN:
      msg = socket.recv()
      socket.send("recv msg OK!")
      print "***** recv async msg:[msg={0}]".format(msg)
  poller.unregister(socket)
  socket.close()

try:
  serverThread()
except KeyboardInterrupt:
 print "interrupt by CTRL + C"
finally:
 pass
