# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-24
@contact: jhx1008@gmail.com
version:  1.0
@todo:    实例管理类，用于实现Agent与Manager通讯的接口
@modify:
"""
import subprocess
import AgentManager
import os

from util import Util
from common import Common
from common import MonitorProperties
from config.RDSConfig import RDSConfig
from disk.DiskManager import DiskType
from communication.ResponseMsg import ResponseMsg

class InstanceManager:
  def __init__(self, databaseManager, storageManager, infoManager, diskManager, routeManager):
    self.databaseManager = databaseManager
    self.infoManager     = infoManager
    self.storageManager  = storageManager
    self.diskManager     = diskManager
    self.routeManager    = routeManager
    Common.rdsLog.logInfo("init instance manager completely")

  def getErrorLog(self, msg):
    """
     从error log中获取指定行数(startRow)到指定行数(endRow)之间的数据
    """
    params = Util.json2Obj(msg)
    extent = long(params["Extent"])
    endRow = long(params["EndRow"])
    # 调用database Manager接口用于获取error log信息
    Common.unifyLog.logInfo("get error log")
    rows, logInfo = self.databaseManager.getErrorLog(extent, endRow)
    Common.unifyLog.logInfo("get error log completely")
    return True, ResponseMsg.getErrorLogMsg(rows, logInfo)

  def changeHA(self, msg):
    """ 
     主从切换, 从机上的操作
     1.从机切异步，由于主从切换时，从机会被切成主机，如果不切成异步状态，如果切换成主机后，
       由于该主机没有从机，VSR会卡住，无法写入数据
     2.停止IO线程，从机IO线程停止后，由于VSR的缘故，主机在这个时候被卡住，不会更新数据
     3.等待原来的从机binlog回放完成，保证数据没有延迟(VSR保证从机的数据写入relay log后就返回，
       由于从机为单线程执行，log event的执行比较慢，日志回放需要一定时间)
     4.relay log回放完成，在从机上执行stop slave，这个时候，主从切换从机切成主机部分已经完成
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("change ha")
    # 检查database instance identifier并且进行保存
    dbIdentifier = params.get("DBInstanceIdentifier",0)
    if dbIdentifier != 0:
      Common.unifyLog.logInfo("start to modify the db instance identifier")
      if not self.infoManager.modifyMsgInfo(dbIdentifier):
        Common.unifyLog.logError("modify db instance identifier failed")
        return False, ResponseMsg.commonFail(params, "modify db instance identifier failed")
      MonitorProperties.dbIdentifier = dbIdentifier
    # 检查数据库参数，保存参数
    if params.get("DBParameters", 0) != 0:
      Common.unifyLog.logInfo("start to save configure file")
      if not self.databaseManager.saveConfigInfo(params["DBParameters"]):
        Common.unifyLog.logError("failed, save configure file error")
        return False, ResponseMsg.commonFail(params,"modify configure file error")
    # 检查数据库是否宕机
    Common.unifyLog.logInfo("check the database server is running")
    if not self.databaseManager.isRunning():
      Common.unifyLog.logError("failed, database server is not running")
      return False, ResponseMsg.commonFail(params,"database is not running")
    # 数据库切异步
    Common.unifyLog.logInfo("change async")
    if not self.databaseManager.changeAsync():
      Common.unifyLog.logError("change async error")
      return False, ResponseMsg.commonFail(params,"change async failed")
    Common.unifyLog.logInfo("save VMmoniterInfo")
    if not self.infoManager.modifyVMMonitorInfo(params["VMmoniterInfo"], params["DBInstanceIdentifier"]):
      Common.unifyLog.logError("save VMmoniterInfo error")

    if params.get("RelayLogSizeThreshold", 0) != 0 and params.get("RelayLogSizeInterval", 0) != 0:
      Common.rdsLog.logInfo("check relay log size is under threshold")
      enabled = True
      while enabled:
        relayLogSize = self.databaseManager.getRelayLogSize()
        if relayLogSize == "NULL":
          break
        relayLogSize = relayLogSize / 1024
        Common.rdsLog.logInfo("Ready to compare relayLogSize({0}) and params[RelayLogSizeThreshold]({1})".format(
          relayLogSize, params["RelayLogSizeThreshold"] ))
        if relayLogSize < long(params["RelayLogSizeThreshold"]):
          Common.rdsLog.logInfo("the result compare relayLogSize({0}) with params[RelayLogSizeThreshold]({1}) is false".format(
            relayLogSize, params["RelayLogSizeThreshold"] ))
          enabled = False
        else:
          Util.sleep(long(params["RelayLogSizeInterval"]))

    MasterBinlog = params.get("MasterBinlog", 0)
    MasterOffset= params.get("MasterOffset", 0)
    #如果只带只读的计划内主从切换，则不要停IO线程，否则，要停IO线程
    if MasterBinlog != 0 and MasterOffset != 0:
      pass
    else:
      # 停止IO线程
      Common.unifyLog.logInfo("stop io thread")
      if not self.databaseManager.stopIOThread():
        Common.unifyLog.logError("stop io thread error")
        return False, ResponseMsg.commonFail(params,"stop io thread failed")
    # 等待relay log回放完
    Common.unifyLog.logInfo("wait all relay log applied")
    if not self.databaseManager.allRelayLogApplied():
      return False, ResponseMsg.commonFail(params, "apply relay log error")
    # 停止replication
    Common.unifyLog.logInfo("stop slave")
    if not self.databaseManager.stopSlave():
      Common.unifyLog.logError("stop slave error")
      return False, ResponseMsg.commonFail(params, "stop replication failed")
    if not self.databaseManager.setReadOnly(False):
      Common.unifyLog.logError("clear readonly error")
      return False, ResponseMsg.commonFail(params, "clear readonly error")

    # 对于带有只读从节点的源实例，在主从切换之前，会将源实例设置为只读，并kill所有连接
    # 因此，主从数据应该是严格一致的（不需要cut binlog，不需要flash back)，如果主从
    # 数据不是严格一致，则会导致数据不一致，一般情况下，数据都是一致的，但是为了保险起见
    # 在这个地方判断一下
    if MasterBinlog != 0 and MasterOffset != 0:
      slaveBinlogFile, slaveBinlogOffset = self.databaseManager.getSlaveBinlogOffset()
      if slaveBinlogFile != MasterBinlog or long(slaveBinlogOffset) != long(MasterOffset):
        Common.rdsLog.logError("Slave's data inconsistent with master, binlog receive from master is {0}:{1},"
            " get from slave is {2}:{3}".format(MasterBinlog, MasterOffset, slaveBinlogFile, slaveBinlogOffset))
        return False, ResponseMsg.commonFail(params, "Slave's data inconsistent with master, binlog receive from "
            "master is {0}:{1}, get from slave is {2}:{3}".format(MasterBinlog, MasterOffset, slaveBinlogFile,
                                                                  slaveBinlogOffset))
      else:
        Common.rdsLog.logInfo("Slave's data consistent with master, binlog receive from master is {0}:{1},"
                               " get from slave is {2}:{3}".format(MasterBinlog, MasterOffset, slaveBinlogFile,
                                                                   slaveBinlogOffset))

    # getSlaveBinlogOffset放在reset slave all之前，不然取不到执行的位置
    slaveBinlogFile, slaveBinlogOffset = self.databaseManager.getSlaveBinlogOffset()
    masterBinlogFile, masterBinlogOffset = self.databaseManager.getMasterBinlogOffset()
    #
    #同步操作过程中，同时需要清除原有可能残余的复制信息，主要是主从切换完成后，原先的从机成为新的主机，
    #而这个主机的复制信息是没有被清除的，如果此时实例降为非高可用，原先从机的固定IP被回收，又重新分配，
    #就可能致使该主机去连接新分配的主机，触发MySQL如果一个主连多个从机，有一个从机指定错误的binlog文件名
    #和位置，会导致主机宕机的问题。所以执行同步的实例，一定是主机。
    if not self.databaseManager.resetSlave():
      return False, ResponseMsg.commonFail(params, "reset slave failed")
    # 获取binlog信息并且返回
    Common.unifyLog.logInfo("change ha completely")
    return True, ResponseMsg.changeHAOkMsg(params, masterBinlogFile, masterBinlogOffset,slaveBinlogFile, slaveBinlogOffset)

  def stopSlave(self, msg):
    """
     执行stop slave停止复制
    """
    Common.unifyLog.logInfo("stop slave")
    params = Util.json2Obj(msg)
    if not self.databaseManager.stopSlave():
      # 停止slave失败
      Common.unifyLog.logError("stop slave error")
      return False, ResponseMsg.commonFail(params, "stop slave failed")
    else:
      Common.unifyLog.logInfo("stop slave completely")
      return True, ResponseMsg.commonOK(params)

  def startSlave(self, msg):
    """
     执行start slave开始复制
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("start slave")
    if not self.databaseManager.startSlave():
      # 启动slave失败
      Common.unifyLog.logError("start slave error")
      return False, ResponseMsg.commonFail(params, "start slave failed")

    if not self.databaseManager.setReadOnly(True):
      # 设置read_only=true
      Common.unifyLog.logError("start slave error")
      return False, ResponseMsg.commonFail(params, "start slave failed")

    Common.unifyLog.logInfo("start slave completely")
    return True, ResponseMsg.commonOK(params)

  def startReplication(self, msg):
    """
     启动复制，需要考虑5种不同的情况：
     1. 有isUninstallSemiSync参数，有该参数且为true，则需要卸载semi_sync插件
     2. 有ReadOnly参数，且为1或True，则将当前实例设置为ReadOnly
     3. 普通的复制，没有master binlog和 master pos，则相当于start slave
     4. 有master binlog和master pos，则先执行change master，再起复制
     5. 不但有master binlog和master pos，还有slave binlog和slave
     pos，则等待relay log回放完成，然后再执行change master
    """
    params = Util.json2Obj(msg)
    if params.get("DBParameters", 0) != 0:
      if not self.databaseManager.saveConfigInfo(params['DBParameters']):
        Common.unifyLog.logError("save configure file error")
        return False, ResponseMsg.commonFail(params, "save database configure file error")
    if not self.databaseManager.changeAsync():
      return False, ResponseMsg.commonFail(params, "change async failed")
    Common.unifyLog.logInfo("start replication")
    dbIdentifier = params.get("DBInstanceIdentifier", 0)
    if dbIdentifier != 0:
      Common.unifyLog.logInfo("start to modify db instance identifier")
      if not self.infoManager.modifyMsgInfo(dbIdentifier):
        Common.unifyLog.logError("modify db instance identifier failed")
        return False, ResponseMsg.commonFail(params, "modify db instance identifier failed")
      MonitorProperties.dbIdentifier = dbIdentifier
      if not self.infoManager.modifyVMMonitorInfo(params["VMmoniterInfo"], params["DBInstanceIdentifier"]):
        Common.rdsLog.logWarn("set vm_monitor info fail in startReplication")
    # MySQL没有运行，直接返回
    Common.unifyLog.logInfo("check database server is running")
    if not self.databaseManager.isRunning():
      Common.unifyLog.logError("database server is not running")
      return False, ResponseMsg.commonFail(params, "start replication failed, database not alived")

    # 卸载同步插件
    isUninstallSemiSync = params.get("isUninstallSemiSync", 'False')
    if isUninstallSemiSync.lower() == 'true':
      Common.unifyLog.logInfo("uninstall sync semi plugin")
      if not self.databaseManager.setupSyncSemiRepl(False):
        Common.unifyLog.logError("uninstall semisync plugin error")
        return False, ResponseMsg.commonFail(params, "uninstall semisync plugin error")

    # 把slave设置成只读
    Common.unifyLog.logInfo("check readonly")
    if  not self.databaseManager.setReadOnly():
      Common.unifyLog.logError("set readonly error")
      return False, ResponseMsg.commonFail(params, "set read only failed")
    SlaveBinlog = params.get("SlaveBinlog", 0)
    SlaveOffset = params.get("SlaveOffset", 0)
    if SlaveBinlog != 0 and SlaveOffset != 0:
      #1. 回放所有Relay Log
      Common.rdsLog.logInfo("wait all relay log applied")
      self.databaseManager.allRelayLogApplied()
      #2. 判断当前实例的slaveBinlogFile与slaveBinlogOffset是否与Manager传来的一致
      SlaveBinlogFromNewMaster, SlaveOffsetFromNewMaster = self.databaseManager.getSlaveBinlogOffset()
      if SlaveBinlog != SlaveBinlogFromNewMaster or long(SlaveOffset) != long(SlaveOffsetFromNewMaster):
        Common.rdsLog.logError("ReadReplica's data inconsistent with new master,"
          " slave binlog receive from master is {0}:{1}, get from readreplica"
          " is {2}:{3}".format(SlaveBinlog, SlaveOffset, SlaveBinlogFromNewMaster, SlaveOffsetFromNewMaster))
        return False, ResponseMsg.commonFail(params, "ReadReplica's data"
          " inconsistent with new master, slave binlog receive from master"
          " is：{0}:{1}, get from readreplica is {2}:{3}".format(SlaveBinlog, SlaveOffset, SlaveBinlogFromNewMaster,
                                                                SlaveOffsetFromNewMaster))
      else:
        Common.rdsLog.logInfo("ReadReplica's data consistent with new master,"
          " slave binlog receive from master is {0}:{1}, get from readreplica"
          " is {2}:{3}".format(SlaveBinlog, SlaveOffset, SlaveBinlogFromNewMaster, SlaveOffsetFromNewMaster))

    #停止当前replication
    Common.unifyLog.logInfo("stop replication")
    if not self.databaseManager.stopReplication():
      Common.unifyLog.logError("stop replication error")
      return False, ResponseMsg.commonFail(params, "change to master failed, can't stop replication")
    # 进行change master操作
    masterUser = "replicaUser"
    masterHost = params.get("MasterIP", "null")
    masterPort = params.get("MasterPort", "null")
    masterPasswd = params.get("ReplicaUserPassword", "null")
    masterLogFile = params.get("Binlog", "null")
    masterLogPos  = params.get("BinlogOffset", "null")
    Common.unifyLog.logInfo("change master")
    if not self.databaseManager.changeMaster(masterHost, masterPort, masterUser, masterPasswd, masterLogFile, masterLogPos):
      Common.unifyLog.logError("change master error")
      return False, ResponseMsg.commonFail(params, "change to master failed, do change master failed")
    Common.unifyLog.logInfo("start slave")

    # 重启复制
    if not self.databaseManager.startReplication():
      Common.unifyLog.logError("start slave error")
      return False, ResponseMsg.commonFail(params, "change to master failed, do start replication failed")
    Common.unifyLog.logInfo("start replication completely")
    return True, ResponseMsg.commonOK(params)

  def startHeartBeat(self, msg):
    params = Util.json2Obj(msg)
    return True, ResponseMsg.commonOK(params)

  def upgradeDBVersion(self, upgradeScript):
    isSuccess = False
    failedReason = ""
    try:
      p = subprocess.Popen(upgradeScript, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      stdout, stderr = p.communicate()
      result = p.returncode
      if result == 0:
        isSuccess = True
      else:
        isSuccess = False
        failedReason = stderr
    except:
      isSuccess = False
      failedReason = "execute upgrade script error"
    finally:
      return isSuccess, failedReason

  def upgradeAgent(self, msg):
    """
     agent程序更新，需要从存储服务器上下载相应的版本解压后替换
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("upgrade agent")
    newVersion = params["AgentVersion"]
    Common.unifyLog.logInfo("check agent version")
    if not Util.versionCompare(newVersion, RDSConfig.agentVersion):
      Common.unifyLog.logInfo("agent version is newest, need not upgrade")
      return False, ResponseMsg.commonFail(params, "upgrade failed, current version {0} is newer than {1}".\
                                          format(RDSConfig.agentVersion, newVersion))
    url = params["AgentProgrammeUrl"].encode("ascii", "ignore")
    nameList = url.split("$")
    fileName = "{0}/rdsAgent-{1}.tar.gz".format(RDSConfig.agentRoot, newVersion)
    fileSize = self.storageManager.getFileSize(nameList[0], nameList[1])
    if (not Util.isExists(fileName) or (Util.getFileSize(fileName) != fileSize)):
      # 获取文件失败
      Common.unifyLog.logInfo("download agent package from storage device")
      if not self.storageManager.getFile(nameList[0], nameList[1], fileName):
        Common.unifyLog.logError("download agent package failed")
        return False, ResponseMsg.commonFail(params, "download agent package {0} failed".format(fileName))
      # 下载文件大小不一致
      elif Util.getFileSize(fileName) != fileSize:
        Common.unifyLog.logError("download agent package error, size error")
        return False, ResponseMsg.commonFail(params, "download agent package {0} error, size error".format(fileName))
      else:
        # 解压缩失败
        if not Util.execCommand("tar -xf {0} -C {1}".format(fileName, RDSConfig.agentRoot)):
          Common.unifyLog.logError("download agent package error, decompress error")
          return False, ResponseMsg.commonFail(params, "decompress agent package {0} error".format(fileName))
        else:
          Common.unifyLog.logInfo("upgrade agent completely")
          return True, ResponseMsg.commonOK(params)
    return False, ResponseMsg.commonFail(params, "upgrade agent failed")

  def createDBInstance(self, msg):
    """
     创建数据库实例，主要步骤
     1.检测当前Agent下数据库是否运行，如果正在运行，直接返回True
     2.格式化存储设备
     3.挂载存储设备到指定路径
     4.如果命令中带有数据库配置信息，保存配置信息到配置文件
     5.初始化数据库实例
     6.启动数据库实例
     以下操作针对MySQL数据库，其他数据库根据自身情况实现相应方法
     7.安装同步复制插件
     8.删除root用户(针对MySQL数据库，RDS版本为了mysqltest测试，没有删除root用户，通过RDS agent控制删除)
     9.创建指定名称的数据库
     10.创建用户，包含普通用户和用于replication的用户
    """
    retMsg = ""
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("create database instance")
    # 如果当前服务已经运行，直接返回相应信息
    if self.databaseManager.isAlive():
      retMsg = ResponseMsg.commonOK(params)
      result = Util.json2Obj(retMsg)
      binlogFile, binlogPos = self.databaseManager.getMasterLogInfo()
      result["Binlog"] = binlogFile
      result["BinlogOffset"] = "{0}".format(binlogPos)
      retMsg = Util.obj2Json(result)
      Common.unifyLog.logInfo("database is alive")
      return True, retMsg
    # 保存命令信息
    Common.unifyLog.logInfo("save msg info")
    if not self.infoManager.saveMsgInfo(params):
      Common.unifyLog.logError("save msg info error")
      return False, ResponseMsg.commonFail(params, "save createDBInstance command error")
    msgInfo = self.infoManager.loadMsgInfo()
    if msgInfo == None:
      Common.unifyLog.logError("load msg info from file error")
      return False, ResponseMsg.commonFail(params, "load msg info from file error")
    MonitorProperties.loadProperties(msgInfo)

    # 保存路由信息
    if len(params.get('Routes', None)) is not None:
      routes = Util.json2Obj(params.get('Routes'))
      Common.rdsLog.logInfo("save routes: %s" % routes)
      if not self.routeManager.saveRouteInfo(routes):
        params['saveRouteStatus'] = False
        Common.rdsLog.logError("save routes: %s failed." % routes)
        
    # 格式化存储设备
    Common.unifyLog.logInfo("format disk")
    if not self.diskManager.format(params.get("Device", "null")):
      Common.unifyLog.logError("format disk error")
      return False, ResponseMsg.commonFail(params, "NBS format error")
    # 挂载存储设备
    Common.unifyLog.logInfo("mount disk")
    if not self.diskManager.mount(params.get("Device", "null")):
      Common.unifyLog.logError("mount disk error")
      return False, ResponseMsg.commonFail(params, "NBS mount error")
    # 保存数据库配置信息到配置文件
    Common.unifyLog.logInfo("save configure file")
    if not self.databaseManager.saveConfigInfo(params['DBParameters']):
      Common.unifyLog.logError("save configure file error")
      return False, ResponseMsg.commonFail(params, "save database configure file error")
    # 初始化数据库
    Common.unifyLog.logInfo("install database")
    if not self.databaseManager.install():
      Common.unifyLog.logError("install database error")
      return False, ResponseMsg.commonFail(params, "install database error")
    # 启动数据库
    Common.unifyLog.logInfo("start database")
    if not self.databaseManager.start(True):
      Common.unifyLog.logError("start database error")
      retMsg = ResponseMsg.commonFail(params, "start database error")
      result = Util.json2Obj(retMsg)
      result["DBParameters"] = params["DBParameters"]
      retMsg = Util.obj2Json(result)
      return False, retMsg
    # 创建rdsadmin用户, InnoSQL与RDS版本统一后，默认没有rdsadmin用户，只有root用户
    Common.unifyLog.logInfo("create rdsadmin user")
    if not self.databaseManager.createRDSUser():
      Common.unifyLog.logError("create rdsadmin user error")
      return False, ResponseMsg.commonFail(params, "create rdsadmin user error")
    # 安装同步复制插件
    Common.unifyLog.logInfo("setup sync semi plugin")
    if not self.databaseManager.setupSyncSemiRepl():
      Common.unifyLog.logError("setup semisync plugin error")
      return False, ResponseMsg.commonFail(params, "setup semisync plugin error")
    # 删除root用户
    Common.unifyLog.logInfo("delete root user")
    if not self.databaseManager.deleteUser():
      Common.unifyLog.logError("delete root user error")
      return False, ResponseMsg.commonFail(params, "delete root user error")
    # 创建数据库
    dbName = params.get("DBName", "null")
    Common.unifyLog.logInfo("create database")
    if dbName != "null" and (not self.databaseManager.createDatabase(dbName)):
      Common.unifyLog.logError("create database error")
      return False, ResponseMsg.commonFail(params, "create database {0} failed".format(dbName))
    masterUser = params["MasterUserName"]
    masterUserPasswd = params["MasterUserPassword"]
    # 创建用户，普通用户和replication用户
    Common.unifyLog.logInfo("create master user")
    if not self.databaseManager.createUser(masterUser, masterUserPasswd, 0):
      Common.unifyLog.logError("create master user error")
      return False, ResponseMsg.commonFail(params, "create user {0} failed".format(masterUser))
    replUser = "replicaUser"
    replPasswd = params["ReplicaUserPassword"]
    Common.unifyLog.logInfo("create replication user")
    if not self.databaseManager.createUser(replUser, replPasswd, 1):
      Common.unifyLog.logError("create replication user error")
      return False, ResponseMsg.commonFail(params, "create replication user {0} failed".format(replUser))
    if not self.databaseManager.deleteUser(""):
      Common.rdsLog.logError("delete NULL user error")
      return False, ResponseMsg.commonFail(params, "delete NULL user error")
    Common.unifyLog.logInfo("sync os")
    if not Util.syncOS():
      Common.unifyLog.logError("sync os error")
      return False, ResponseMsg.commonFail(params, "sync os error")
    Common.unifyLog.logInfo("create database instance completely")
    retMsg = ResponseMsg.commonOK(params)
    result = Util.json2Obj(retMsg)
    # 获取binlog信息
    binlogFile, binlogPos = self.databaseManager.getMasterLogInfo()
    result["Binlog"] = binlogFile
    result["BinlogOffset"] = "{0}".format(binlogPos)
    retMsg = Util.obj2Json(result)
    return True, retMsg

  def rebuildVolume(self, msg):
    """
     重建卷处理流程：
     1.将原有的卷的文件系统重新挂载
     2.初始化新的卷
     3.拷贝数据到新卷
     4.将原先的卷卸载
     5.将新卷挂载到原有目录
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("start to do rebuild volume")
    if not self.diskManager.mount(params.get("Device", "null")):
      Common.unifyLog.logError("mount disk error")
      return False, ResponseMsg.commonFail(params, "mount disk error")
    if not self.diskManager.format(params.get("NewDevice", "null"), DiskType.EBS_TMP):
      Common.unifyLog.logError("format new device disk error")
      return False, ResponseMsg.commonFail(params, "format disk error")
    if not self.diskManager.mount(params.get("NewDevice", "null"), DiskType.EBS_TMP):
      Common.unifyLog.logError("mount temp disk error")
      return False, ResponseMsg.commonFail(params, "mount disk error")
    if not self.diskManager.copyFiles(self.diskManager.getMountDir(DiskType.EBS) + "/*", self.diskManager.getMountDir(DiskType.EBS_TMP)):
      Common.unifyLog.logError("copy files error")
      return False, ResponseMsg.commonFail(params, "copy files error")
    if not self.diskManager.umount():
      Common.unifyLog.logError("umount disk error")
      return False, ResponseMsg.commonFail(params, "umount disk error")
    if not self.diskManager.umount(DiskType.EBS_TMP):
      Common.unifyLog.logError("umount temp disk error")
      return False, ResponseMsg.commonFail(params, "umount temp disk error")
    self.diskManager.clearFiles(self.diskManager.getMountDir(DiskType.EBS_TMP))
    self.diskManager.clearOSCache()
    retMsg = ResponseMsg.commonOK(params)
    result = Util.json2Obj(retMsg)
    result["VolumeID"] = params["VolumeID"]
    result["Device"] = params["NewDevice"]
    retMsg = Util.obj2Json(result)
    return True, retMsg
      
  def stopDBInstance(self, msg):
    """
     关闭Agent数据库实例，步骤如下：
     1.把数据库设为只读状态
     2.关闭数据库
     3.卸载挂载盘(由于目前磁盘扩容接口跟startDBInstance为同一个接口，而每次磁盘扩容后需要重新挂载，所以目前在每次stop的时候，卸载挂载盘)
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("stop database instance")
    # 如果数据库服务为运行状态，设置为只读
    if self.databaseManager.isRunning():
      # 设置为只读
      Common.unifyLog.logInfo("set readonly")
      if not self.databaseManager.setReadOnly():
        Common.unifyLog.logError("set read only error")
        return False, ResponseMsg.commonFail(params, "set read only error")
    if params.get("SkipFinalSnapshot", "null").upper() == 'FALSE':
      retMsg = self.backup(msg)[1]
      result = Util.json2Obj(retMsg)
      result["Action"] = "RDSDeleteResult"
      result["SkipFinalSnapshot"] = "false"
      if result.get("IsSucess", 0) == "false" and self.databaseManager.isRunning():
        self.databaseManager.setReadOnly(False)
        return False, Util.obj2Json(result)
      else:
        if not (self.databaseManager.shutdown() and self.diskManager.umount()):
          result["IsSucess"] = "false"
          result["FailedReason"] = "shutdown database error"
          return False, Util.obj2Json(result)
        return True, Util.obj2Json(result)
    else:
      Common.unifyLog.logInfo("shutdown database and umount disk")
      # 关闭数据库然后卸载磁盘
      if (self.databaseManager.shutdown() and self.diskManager.umount()):
        retMsg = ResponseMsg.commonOK(params)
        result = Util.json2Obj(retMsg)
        result["SkipFinalSnapshot"] = "true"
        Common.unifyLog.logInfo("stop database instance completely")
        return True, Util.obj2Json(result)
      else:
        Common.unifyLog.logError("shutdown database and umount disk error")
        retMsg = ResponseMsg.commonFail(params, "shutdown database and umount disk error")
        result = Util.json2Obj(retMsg)
        result["SkipFinalSnapshot"] = "true"
        return False, Util.obj2Json(result)

  def startDBInstance(self, msg):
    """
     启动数据库实例,主要步骤如下：
     1.挂载磁盘
     2.保存当前命令信息(信息中带有监控相关配置，Agent需要定时向云监控上报数据)
     3.如果需要扩容，则先扩容(扩容有Manager完成，Agent端只需要resize即可)
     4.保存配置信息到指定配置文件
     5.启动数据库
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("start database instance")
    Common.unifyLog.logInfo("mount disk")
    if not self.diskManager.mount(params.get("Device", "null")):
      Common.unifyLog.logError("mount disk error")
      return False, ResponseMsg.commonFail(params, "mount disk error")
    if params.get("DBInstanceIdentifier", "null") != "null":
      Common.unifyLog.logInfo("save msg info")
      # 保存信息
      if not self.infoManager.saveMsgInfo(params):
        Common.unifyLog.logError("save msg info error")
        return False, ResponseMsg.commonFail(params, "save startDBInstance command error")
      Common.unifyLog.logInfo("load msg info from file")
      msgInfo = self.infoManager.loadMsgInfo()
      if msgInfo == None:
        Common.unifyLog.logError("load msg info from file error")
        return False, ResponseMsg.commonFail(params, "load msg info from file error")
      Common.unifyLog.logInfo("load msg info from file")
      MonitorProperties.loadProperties(msgInfo)
    
    #保存路由信息
    if len(params.get('Routes', None)) is not None:
      routes = Util.json2Obj(params.get('Routes'))
      Common.rdsLog.logInfo("save routes: %s" % routes)
      if not self.routeManager.saveRouteInfo(routes):
        params['saveRouteStatus'] = False
        Common.rdsLog.logError("save routes: %s failed." % routes)

    if self.databaseManager.isAlive():
      retMsg = ResponseMsg.commonOK(params)
      result = Util.json2Obj(retMsg)
      result["DBParameters"] = params["DBParameters"]
      Common.unifyLog.logInfo("start database instance compleltely")
      return True, Util.obj2Json(result)
    if params.get("AllocatedStorage", "null") != "null":
      Common.unifyLog.logInfo("resize disk size")
      # 存储设备扩容，扩容已经由Manager完成，Agent只需要resize即可
      if not self.diskManager.resize(params["Device"], params["AllocatedStorage"]):
        Common.unifyLog.logError("resize disk size error")
        return False, ResponseMsg.commonFail(params, "resize disk size error")
    Common.unifyLog.logInfo("save database configure file")
    # 保存配置文件
    if not self.databaseManager.saveConfigInfo(params["DBParameters"]):
      Common.unifyLog.logError("save database configure file error")
      return False, ResponseMsg.commonFail(params, "save database configure file error")
    Common.unifyLog.logInfo("start database")
    # 启动数据库
    if not self.databaseManager.start():
      Common.unifyLog.logError("start database error")
      retMsg = ResponseMsg.commonFail(params, "start database error")
      result = Util.json2Obj(retMsg)
      result["DBParameters"] = params["DBParameters"]
      return False, Util.obj2Json(result)
    if params.get("UpgradeScript", "null") != "null":
      if not self.databaseManager.upgradeDBVersion():
        return False, ResponseMsg.commonFail(params, "execute upgrade script error")
    Common.unifyLog.logInfo("sync os")
    if not Util.syncOS():
      Common.unifyLog.logError("sync os error")
      return False, ResponseMsg.commonFail(params, "sync os error")
    retMsg = ResponseMsg.commonOK(params)
    result = Util.json2Obj(retMsg)
    result["DBParameters"] = params["DBParameters"]
    Common.unifyLog.logInfo("stop database instance completely")
    return True, Util.obj2Json(result)


  def rebootDBInstance(self, msg):
    """
     重启数据库实例，可能由于配置发生改变需要重启后生效，具体步骤如下：
     1.如果信息中带有配置信息，说明配置发生改变，保存新的配置到配置文件
     2.关闭数据库服务
     3.如果是异常关闭，异常发生后会进行主从切换，这个时候原来的主机被降成从机，
       重新启动后，可能当前实例的binlog会被主机上多出一部分，需要截掉多余的binlog
     4.启动数据库实例
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("reboot database instance")
    # 如果Manager发送的信息中带有配置信息，则保存该配置信息
    if params.get("DBParameters", 0) != 0:
      self.databaseManager.saveConfigInfo(params["DBParameters"])
    # 重启mysql服务
    Common.unifyLog.logInfo("shutdown database")
    if not self.databaseManager.shutdown():
      Common.unifyLog.logError("shutdown database error")
      return False, ResponseMsg.commonFail(params, "shutdown database error")
    # Manager发送的消息中带有binlog信息，可能需要对binlog进行裁剪
    if params.get("Binlog", "null") != "null":
      if params.get("BinlogOffset", "null") != "null":
        binlogOffset = long(params["BinlogOffset"])
        Common.unifyLog.logInfo("cut binlog")
        if binlogOffset > 0 and not self.databaseManager.cutBinlog(params["Binlog"], binlogOffset):
          Common.unifyLog.logError("cut binlog error")
          return False, ResponseMsg.commonFail(params, "cut binlog error")
      else:
        Common.unifyLog.logError("lost offset field")
        return False, ResponseMsg.commonFail(params, "lost offset field")
    Common.unifyLog.logInfo("start database")
    # 启动数据库实例
    if not self.databaseManager.start():
      Common.unifyLog.logInfo("start database error")
      return False, ResponseMsg.commonFail(params, "start database error")
    Common.unifyLog.logInfo("reboot database instance completely")
    return True, ResponseMsg.commonOK(params)


  def backup(self, msg):
    """
     实例数据备份
     1.在存储设备上查找备份文件是否存在
     2.如果不存在备份文件，则开始进行备份
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("backup data")
    url = params['SnapshotURL'].encode('ascii', 'ignore')
    nameList = url.split('$')
    # 在存储设备上查找文件是否存在
    Common.unifyLog.logInfo("lookup the backup file on storage device")
    if self.storageManager.lookupFile(nameList[0], "{0}-info".format(nameList[1])):
      backupInfo = self.storageManager.getString(nameList[0], "{0}-info".format(nameList[1]))
      if backupInfo == "null":
        return False, ResponseMsg.backupFailMsg(params, "storage invoke error.")
      infos = Util.json2Obj(backupInfo)
      Common.unifyLog.logInfo("backup file exist, backup data completely")
      return True, ResponseMsg.backupOkMsg(params, infos['filename'], infos['offset'], infos['filesize'])

    # 准备工作
    Common.unifyLog.logInfo("backup file not exist, do the backup first")
    # 解析Binlog与Next参数，根据Binlog与Next的取值不同，可以分为三种情况
    # 1. Binlog为空，为全量备份
    # 2. Binlog不为空，为增量备份
    #   2.1 Next 为False，从管理服务器传过来的Binlog开始增量备份
    #   2.2 Next 为True，从管理服务器传过来的Binlog的下一个Binlog文件开始备份
    startBinlog = params.get("Binlog","")
    startPosition = params.get("StartPosition","")

    

    Binlog = params.get("Binlog","")
    Next = params.get("IsNext","")
    isNext = False if Next.lower() != "true" else True

    if Binlog == "":
      startBinlog = Binlog
    elif Binlog != "" and isNext == False:
      startBinlog = Binlog
    else:
      prefix, index = Binlog.split('.')
      startBinlog = "%s.%.6d" % (prefix, long(index) + 1)

    if startBinlog:
      Common.unifyLog.logInfo("Start do incremental backup start binlog {0}".format(startBinlog))
    else:
      Common.unifyLog.logInfo("Start do Fully backup")

    # 进行backup操作
    ret = self.databaseManager.backup(nameList[0], nameList[1], startBinlog, startPosition)
    isSuccess, masterBinlog, masterOffset, backupSize, errInfo, slaveBinlog, slaveOffset, startDatetime, endDatetime= ret

    if not isSuccess:
      Common.unifyLog.logError("do backup failed, backup data error")
      return False, ResponseMsg.backupFailMsg(params, errInfo)

    if not startBinlog:
      Common.unifyLog.logInfo("do fully backup succeed, backup data completely")
      return True, ResponseMsg.backupOkMsg(params, masterBinlog,
                                           masterOffset, backupSize, slaveBinlog, slaveOffset)
    else:
      Common.unifyLog.logInfo("do incremental backup succeed, backup data completely")
      return True, ResponseMsg.backupOkMsg(params, masterBinlog, masterOffset, backupSize,None, None, startDatetime, endDatetime)


  def restore(self, msg):
    """
     数据库实例数据恢复, 数据恢复主要通过把数据恢复到一块新的盘，然后重新挂载该盘实现。
     主要步骤如下：
     1.在存储设备上查找备份文件是否存在
     2.格式化磁盘用于保存数据
     3.挂载磁盘到指定路径
     4.关闭数据库实例
     5.开始数据恢复
     6.恢复成功后启动数据库实例
    """
    params = Util.json2Obj(msg)
    Common.rdsLog.logInfo("Ready to restore data")

    Common.unifyLog.logInfo("save msg info")
    if not self.infoManager.saveMsgInfo(params):
      Common.unifyLog.logError("save msg info error")
      return False, ResponseMsg.commonFail(params, "save startDBInstance command error")
    Common.unifyLog.logInfo("load msg info from file")
    msgInfo = self.infoManager.loadMsgInfo()
    if msgInfo == None:
      Common.unifyLog.logError("load msg info from file error")
      return False, ResponseMsg.commonFail(params, "load msg info from file error")
    MonitorProperties.loadProperties(msgInfo)
    Common.databaseInstanceID = params.get("VmId", "null")
    MonitorProperties.dbIdentifier = params.get("DBInstanceIdentifier", "null")
    stopDatetime = params.get("StopDatetime",0)

    #保存路由信息
    if len(params.get('Routes', None)) is not None:
      routes = Util.json2Obj(params.get('Routes'))
      Common.rdsLog.logInfo("save routes: %s" % routes)
      if not self.routeManager.saveRouteInfo(routes):
        params['saveRouteStatus'] = False
        Common.rdsLog.logError("save routes: %s failed." % routes)
    
    # 恢复分为两种情况：
    # 1. 只有一个URL，则通过innobackupex进行恢复，恢复完成以后启动MySQL即可
    # 2. 有多个URL，则首先通过innobackupex进行恢复，恢复完成以后启动MySQL，
    # 然后再依次回放所有的Binlog
    # 注意1：需要保证在恢复过程中，如果Agent挂了，重启以后能够顺利恢复
    # 注意2：有多个URL时，需要严格按照URL顺序恢复数据，且不能一次下载所有的
    # Binlog文件解压到一个目录进行回放
    # 注意3：对同一个目录中的Binlog文件进行回放时，需要严格按照文件名顺序依次回放
    urls = params["SnapshotURL"].encode("ascii", "ignore")
    nameList = []
    urlList = urls.split(';')
    for url in urlList:
      try:
        nosurl, binlog, offset = url.split('&')
        bucket, key = nosurl.split('$')

        #convert string to long for offset
        try:
          offset = long(offset)
        except:
          offset = 0
          Common.rdsLog.logWarn('long(offset) operator error when convert"'
                                '" {0} to long in {1}'.format(offset, url))

        nameList.append((bucket, key, binlog, offset))

      except Exception, e:
        Common.rdsLog.logError("Parse url error: {0}, error cause is :".format(url, e))
        return False, "Parse url error " + url

    # 查找备份文件是否存在
    Common.unifyLog.logInfo("lookup backup file")
    for elem in nameList:
      if not self.storageManager.lookupFile(elem[0], elem[1]):
        Common.unifyLog.logError("lookup backup file error")
        return False, ResponseMsg.restoreFailMsg(params, "lookup backup file error")
    Common.unifyLog.logInfo("format device")
    # 关闭数据库：一般情况下，恢复前，数据库实例是不存在的，之所以要关闭数据库
    # 是为了处理在恢复过程中Agent挂了重做的情况
    Common.unifyLog.logInfo("shutdown database")
    if not self.databaseManager.shutdown():
      Common.unifyLog.logError("shutdown database error")
      return False, ResponseMsg.restoreFailMsg(params, "shutdown database error")
    self.databaseManager.saveConfigInfo(params["DBParameters"])

    # 格式化磁盘
    if not self.diskManager.format(params.get("Device","null")):
      Common.unifyLog.logError("format device error")
      return False, ResponseMsg.restoreFailMsg(params, "format device error")
    Common.unifyLog.logInfo("mount device")

    # 挂载磁盘
    if not self.diskManager.mount(params.get("Device","null")):
      Common.unifyLog.logError("mount device error")
      return False, ResponseMsg.restoreFailMsg(params, "mount device error")

    self.databaseManager.saveConfigInfo(params['DBParameters'])
    # 开始恢复
    Common.unifyLog.logInfo("do restore")
    isSuccess, errInfo = self.databaseManager.restore(nameList, stopDatetime)

    # 恢复成功，开始启动MySQL服务
    if isSuccess:
      retMsg = ResponseMsg.restoreOkMsg(params)
      fileName, position = self.databaseManager.getMasterLogInfo()
      result = Util.json2Obj(retMsg)
      result["Binlog"] = fileName
      result["BinlogOffset"] = "{0}".format(position)
      Common.unifyLog.logInfo("restore data completely")
      return True, Util.obj2Json(result)
    else:
      Common.unifyLog.logError("do restore error")
      return False, ResponseMsg.restoreFailMsg(params, errInfo)


  def innerBackup(self, msg):
    """
     内部备份，通过给定一个盘，把数据备份到指定盘上实现
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("inner backup")
    # 格式化磁盘
    if not self.diskManager.format(params.get("Device", "null"), DiskType.EBS_INNER):
      Common.unifyLog.logError("format device error")
      return False, ResponseMsg.backupFailMsg(params, "format device error")
    Common.unifyLog.logInfo("mount device")
    # 挂载磁盘
    if not self.diskManager.mount(params.get("Device","null"), DiskType.EBS_INNER):
      Common.unifyLog.logError("mount device error")
      return False, ResponseMsg.backupFailMsg(params, "mount device error")

    ret = self.databaseManager.innerBackup(self.diskManager.getMountDir(DiskType.EBS_INNER))
    isSuccess, masterBinlog, masterOffset, backupSize, errInfo, slave, slaveOffset = ret

    Common.rdsLog.logInfo("slave is {0}, slaveOffset is {1}".format(slave, slaveOffset))
    # 数据刷盘
    if not Util.syncOS():
      Common.unifyLog.logError("sync os error")
      return False, ResponseMsg.commonFail(params, "sync os error")
    # 卸载磁盘
    self.diskManager.umount(DiskType.EBS_INNER)
    if isSuccess:
      Common.unifyLog.logInfo("do backup succeed, backup data completely")
      return True, ResponseMsg.backupOkMsg(params, masterBinlog, masterOffset, backupSize, slave, slaveOffset)
    else:
      Common.unifyLog.logError("do backup failed, backup data error")
      return False, ResponseMsg.backupFailMsg(params, errInfo)

  def innerRestore(self, msg):
    """
     内部恢复，内部恢复通过给定包含备份数据的盘，然后在盘上进行恢复
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("inner restore data")
    if not self.infoManager.saveMsgInfo(params):
      Common.unifyLog.logError("save msg info error")
      return False, ResponseMsg.commonFail(params, "save inner restore command error")
    msgInfo = self.infoManager.loadMsgInfo()
    if msgInfo == None:
      Common.unifyLog.logError("load msg info error")
      return False, ResponseMsg.commonFail(params, "load msg info error")
    MonitorProperties.loadProperties(msgInfo)
    Common.databaseInstanceID = params.get("VmId", "null")
    if self.databaseManager.isAlive():
      return True, ResponseMsg.restoreOkMsg(params)

    #保存路由信息
    if len(params.get('Routes', None)) is not None:
      routes = Util.json2Obj(params.get('Routes'))
      Common.rdsLog.logInfo("save routes: %s" % routes)
      if not self.routeManager.saveRouteInfo(routes):
        params['saveRouteStatus'] = False
        Common.rdsLog.logError("save routes: %s failed." % routes)

    # 挂载磁盘，内部备份不格式化磁盘，数据都保存在盘上
    Common.unifyLog.logInfo("mount device")
    if not self.diskManager.mount(params.get("Device", "null")):
      Common.unifyLog.logError("mount device error")
      return False, ResponseMsg.restoreFailMsg(params, "mount device error")
    Common.unifyLog.logInfo("shutdown database")
    if not self.databaseManager.shutdown():
      Common.unifyLog.logError("shutdown database error")
      return False, ResponseMsg.restoreFailMsg(params, "shutdown database error")
    self.databaseManager.saveConfigInfo(params['DBParameters'])
    Common.unifyLog.logInfo("do restore")
    isSuccess, errInfo = self.databaseManager.innerRestore(self.diskManager.getMountDir())
    if isSuccess:
      Common.unifyLog.logInfo("start database")
      if not self.databaseManager.start():
        return False, ResponseMsg.commonFail(params, "start database failed.")
      if params.get("UpgradeScript", "null") != "null":
        if not self.databaseManager.upgradeDBVersion():
          return False, ResponseMsg.commonFail(params, "execute upgrade script error")
      retMsg = ResponseMsg.restoreOkMsg(params)
      fileName, position = self.databaseManager.getMasterLogInfo()
      result = Util.json2Obj(retMsg)
      result["Binlog"] = fileName
      result["BinlogOffset"] = "{0}".format(position)
      Common.unifyLog.logInfo("restore data completely")
      return True, Util.obj2Json(result)
    else:
      Common.unifyLog.logInfo("do inner restore error")
      return False, ResponseMsg.restoreFailMsg(params, errInfo)

  def modifyDBInstance(self, msg):
    """
     1. 修改用户名、密码
     2. 修改动态MySQL变量
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("modify database instance")
    result = {"Action":"RDSModifyResult", "VmId":Common.databaseInstanceID}
    result["IsSucess"] = "true"
    result["Version"] = Common.msgVersion
    needChangePassword = str(params.get("NeedChangePassword"))

    # 需要修改用户名和密码
    if needChangePassword.lower() == "true":
      if params.get("MasterUserPassword", 0) == 0 or params.get("MasterUserName", 0) == 0:
        return False, ResponseMsg.modifyFail(result, "read user or password error")
      else:
        # 修改用户信息
        Common.unifyLog.logInfo("change user and password")
        if self.databaseManager.changeUserPasswd(params["MasterUserName"], params["MasterUserPassword"]):
          result["MasterUserPassword"] = params["MasterUserPassword"]
        else:
          Common.unifyLog.logError("change user and password error")
          return False, ResponseMsg.modifyFail(result, "change user and password error")
    Common.unifyLog.logInfo("modify database username or password completely")
    dbparameters = params.get("DBParameters", 0)
    if dbparameters == 0:
      Common.rdsLog.logInfo("DBParameters is null, just skip")
    else:
      Common.rdsLog.logInfo("DBParameters is " + dbparameters)
      IsSuccess, errInfo = self.databaseManager.modifyDBParameters(dbparameters)
      if not IsSuccess:
        return False, ResponseMsg.modifyFail(result, errInfo)
    return True, Util.obj2Json(result)

  def changeAsync(self, msg):
    """
     切换到异步状态
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("change async")
    if params.get("DBParameters", 0) != 0:
      Common.unifyLog.logInfo("save configure file")
      # 保存配置信息到相应的配置文件
      if not self.databaseManager.saveConfigInfo(params["DBParameters"]):
        Common.unifyLog.logError("save configure file error")
        return False, ResponseMsg.commonFail(params, "save configure file error")
    # 只会在Master上切异步，如果在Master上切异步，对于普通流程，在Master上执行"set global read_only=off" 是没有问题的
    # 对于带只读的计划内主从切换，如果主从切换失败，则将主专异步，顺便解除主上的read_only
    if not self.databaseManager.setReadOnly(False):
      return False, ResponseMsg.commonFail(params, "set global read_only=off failed")
    # 切换到异步复制
    if not self.databaseManager.changeAsync():
      Common.unifyLog.logError("change async failed")
      return False, ResponseMsg.commonFail(params, "change async failed")
    else:
      Common.unifyLog.logInfo("change async completely")
      return True, ResponseMsg.commonOK(params)

  def changeSync(self, msg):
    """
     切换到同步状态
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("change sync")
    if params.get("DBParameters", 0) != 0:
      Common.unifyLog.logInfo("save configure file")
      #保存相应配置信息到配置文件
      if not self.databaseManager.saveConfigInfo(params["DBParameters"]):
        Common.unifyLog.logError("save configure file error")
        return False, ResponseMsg.commonFail(params, "save configure file error")
    # 切换到同步复制
    if not self.databaseManager.changeSync():
      Common.unifyLog.logError("change sync failed")
      return False, ResponseMsg.commonFail(params, "change sync failed")
    else:
      Common.unifyLog.logInfo("change sync completely")
      return True, ResponseMsg.commonOK(params)

  def getSqlStats(self, msg):
    """
     从sql stats信息中获取指定时间内[StartTime,EndTime]的数据,
     如果StartTime为-1，则取EndTime以前的所有数据，
     如果EndTime为-1，则取StartTime以后的所有数据(包括内存中)
     如果StartTime和EndTime都为-1，则只取内存中的数据
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("get sql stats")
    startTime = 0
    endTime = 0xFFFFFFFF
    if params["StartTime"] != "-1":
      startTime = long(params["StartTime"])
    if params["EndTime"] != "-1":
      endTime = long(params["EndTime"])
    rows = self.databaseManager.getSqlStats(startTime, endTime)
    content = []
    for row in rows:
      info = {"SqlText":"%s"%row[0],"Index":"%s"%row[1],"MemoryTempTables":"%s"%row[2],"DiskTempTables":"%s"%row[3], \
              "RowReads":"%s"%row[4],"ByteReads":"%s"%row[5],"MaxExecTimes":"%s"%row[6],"MinExecTimes":"%s"%row[7], \
              "ExecTimes":"%s"%row[8],"ExecCount":"%s"%row[9]}
      content.append(info)
    Common.unifyLog.logInfo("get sql stats completely")
    return True, ResponseMsg.getSqlStatsMsg(content)

  def getTableStats(self, msg):
    """
     从sql stats信息中获取指定时间内[StartTime,EndTime]的数据,
     如果StartTime为-1，则取EndTime以前的所有数据，
     如果EndTime为-1，则取StartTime以后的所有数据(包括内存中)
     如果StartTime和EndTime都为-1，则只取内存中的数据
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("get table stats")
    startTime = 0
    endTime = 0xFFFFFFFF
    if params["StartTime"] != "-1":
      startTime = long(params["StartTime"])
    if params["EndTime"] != "-1":
      endTime = long(params["EndTime"])
    rows = self.databaseManager.getTableStats(startTime, endTime)
    content = []
    for row in rows:
      info = {"DBName":"%s"%row[0],"TableName":"%s"%row[1],"SelectCount":"%s"%row[2],"UpdateCount":"%s"%row[3],"InsertCount":"%s"%row[4],"DeleteCount":"%s"%row[5]}
      content.append(info)
    Common.unifyLog.logInfo("get table stats completely")
    return True, ResponseMsg.getTableStatsMsg(content)

  def getSlowLog(self, msg):
    """
     获取slow log信息
    """
    params = Util.json2Obj(msg)
    Common.unifyLog.logInfo("get slow log")
    startTime = 0
    endTime = 0xFFFFFFFF
    if params["StartTime"] != "-1":
      startTime = long(params["StartTime"])
    if params["EndTime"] != "-1":
      endTime = long(params["EndTime"])
    rows =self.databaseManager.getSlowLog(startTime, endTime)
    content = []
    for row in rows:
      info = {"StartTime"     : "%s"%row[0],
              "SqlText"       : "%s"%row[1],
              "UserHost"      : "%s"%row[2],
              "QueryTime"     : "%s"%row[3],
              "LockTime"      : "%s"%row[4],
              "RowsSent"      : "%s"%row[5],
              "RowsExamined"  : "%s"%row[6],
              "DBName"        : "%s"%row[7],
              "LogicalReads"  : "%s"%row[8],
              "PhysicalReads" : "%s"%row[9]}
      content.append(info)
    Common.unifyLog.logInfo("get slow log completely")
    return True, ResponseMsg.getSlowLogMsg(content)


  def flashBackDBInstance(self, msg):
    """
        flash backup db instance
    """
    params = Util.json2Obj(msg)
    binlog = params["Binlog"]
    offset = long(params["BinlogOffset"])
    if not self.databaseManager.isAlive():
      Common.rdsLog.logError("flash back db instance error, MySQL is not alive")
      return False, ResponseMsg.commonFail(params, "MySQL is not alive")

    if not self.databaseManager.flashBackDBInstance(binlog, offset):
      Common.rdsLog.logError("flash back db instance error")
      return False, ResponseMsg.commonFail(params, "flash back db instance error")
    else:
      return True, ResponseMsg.commonOK(params)

  def setReadOnly(self, msg):
    """
    将只读实例或高可用的Master实例设置为只读:
    """
    params = Util.json2Obj(msg)
    Common.rdsLog.logInfo('{0}'.format(params))
    isKillConn = params.get('isKillConnection', 'False')
    readOnly = params.get('ReadOnly', 'true')

    if readOnly.lower() == 'true':
      readOnly = True
    elif readOnly.lower() == 'false':
      readOnly = False

    if not self.databaseManager.setReadOnly(readOnly):
      Common.unifyLog.logError("set readonly error")
      return False, ResponseMsg.commonFail(params, "set read only failed")

    if isKillConn.lower() == 'true':
      Common.rdsLog.logInfo('Ready to kill all connection')
      self.databaseManager.killConnection()

    # 为了保险起见，将Master设置为read_only以后，kill数据库连接，kill数据库连接以后，等待1s再去show master status
    Util.sleep(1)
    
    result = {"Action":"RDSSetReadOnlyResult", "VmId":Common.databaseInstanceID}
    result["IsSucess"] = "true"
    result["Version"] = Common.msgVersion
    masterBinlog, masterOffset= self.databaseManager.getMasterBinlogOffset()
    result["MasterBinlog"] = masterBinlog
    result["MasterOffset"] = masterOffset

    return True, Util.obj2Json(result)


  def dumpExternalDBData(self, msg):
    """
     导出外部实例数据到本地磁盘
    """
    Common.unifyLog.logInfo("dump external db data")
    params = Util.json2Obj(msg)
    remoteHost = params["HostIp"]
    remotePort = long(params["Port"])
    userName   = params["UserName"]
    password   = params["Password"]
    databases  = params["Databases"]
    device     = params["Device"]
    threads    = long(params.get("Threads", "1"))
    # 格式化存储设备
    Common.unifyLog.logInfo("format disk")
    if not self.diskManager.format(device,DiskType.EBS_MIGRATE):
      Common.unifyLog.logError("dump external db data failed, format disk error")
      return False, ResponseMsg.commonFail(params, "NBS format error")
    # 挂载存储设备
    Common.unifyLog.logInfo("mount disk")
    if not self.diskManager.mount(device, DiskType.EBS_MIGRATE):
      Common.unifyLog.logerror("dump external db data failed, mount disk error")
      return False, ResponseMsg.commonFail(params, "NBS mount error")
    # 开始导出数据到本地盘
    Common.unifyLog.logInfo("start to dump databases")
    isSuccess, fileName, offset, retHost, retPort, errInfo = self.databaseManager.dumpExternalDatabases(params,
                                                               self.diskManager.getMountDir(DiskType.EBS_MIGRATE))
    if not isSuccess:
      Common.unifyLog.logError("dump external db data failed, error msg: {0}".format(errInfo))
      # dump失败，卸载NBS盘
      self.diskManager.umount(DiskType.EBS_MIGRATE)
      return False, ResponseMsg.commonFail(params, errInfo)
    Common.unifyLog.logInfo("dump external db data completely")
    return True, ResponseMsg.dumpExternalOkMsg(params, fileName, offset, retHost, retPort)

  def loadExternalDBData(self, msg):
    """
     导入外部实例数据到RDS实例
    """
    Common.unifyLog.logInfo("load external db data")
    params = Util.json2Obj(msg)
    Common.rdsLog.logInfo("params recv from manage is {0}".format(params))
    compressKey = long(params.get("CompressKey"))
    compressTables = params.get("CompressTables")
    migrateGrants = long(params.get("MigrateGrants", 0))
    dbs = params.get("Databases")
    threads = long(params.get("Threads"))
    backupDir = self.diskManager.getMountDir(DiskType.EBS_MIGRATE)
    if not self.databaseManager.isAlive():
      Common.unifyLog.logError("load external db data failed, error msg: mysql is not running")
      return False, ResponseMsg.commonFail(params, "mysql is not running")
    Common.unifyLog.logInfo("start to load databases")
    # 开始load数据到RDS实例
    isSuccess, errInfo = self.databaseManager.loadExternalDatabases(threads, dbs, compressKey, compressTables, migrateGrants, backupDir)
    if not isSuccess:
      Common.unifyLog.logError("load external db data failed, error msg: {0}".format(errInfo))
      # load数据失败，卸载NBS盘
      self.diskManager.umount(DiskType.EBS_MIGRATE)
      return False, ResponseMsg.commonFail(params, errInfo)
    Common.unifyLog.logInfo("load external db data completely")
    return True, ResponseMsg.commonOK(params)


  def syncExternalDB(self, msg):
    """
     同步外部节点新数据到RDS节点
    """
    Common.unifyLog.logInfo("sync external db")
    params = Util.json2Obj(msg)
    remoteHost = params["HostIp"]
    remotePort = long(params["Port"])
    userName   = params["UserName"]
    password   = params["Password"]
    databases  = params["Databases"]
    binlog     = params["Binlog"]
    binlogOffset = params["BinlogOffset"]
    # 开始建立RDS实例到外部实例的复制
    if not self.databaseManager.changeMaster(remoteHost, remotePort, userName, password, binlog, binlogOffset):
      Common.unifyLog.logError("sync external db error, change master failed")
      # 建立到外部实例复制失败，卸载NBS盘
      self.diskManager.umount(DiskType.EBS_MIGRATE)
      return False, ResponseMsg.commonFail(params, "sync external db failed, change master failed")
    Common.unifyLog.logInfo("start slave")
    if not self.databaseManager.startReplication():
      Common.unifyLog.logError("start slave error")
      self.diskManager.umount(DiskType.EBS_MIGRATE)
      return False, ResponseMsg.commonFail(params, "sync external db failed, do start replication failed")
    Common.unifyLog.logInfo("sync external db completely")
    return True, ResponseMsg.commonOK(params)

  def stopImportDB(self, msg):
    """
     数据迁移完毕，停止外部复制
    """
    Common.unifyLog.logInfo("stop import db")
    params = Util.json2Obj(msg)
    # 检查是否存在迁移过程中的进程，如果存在，kill这些进程
    Util.killProcess("mysqldump")
    Util.killProcess("mydumper")
    Util.killProcess("myloader")
    Util.killProcess("pt-show-grants")
    Util.killProcess("myloader")

    f1 = '{0}/load.log'.format(RDSConfig.logDir)
    f2 = '{0}/load1.log'.format(RDSConfig.logDir)

    try:
      if os.path.exists(f1):
        if os.path.exists(f2):
          os.remove(f2)
        os.rename(f1, f2)
    except:
      return False, ResponseMsg.stopImportFail("rename load log {0} to {1} error".format(f1, f2))

    if not self.databaseManager.isAlive():
      return False, ResponseMsg.stopImportFail("MySQL is not Alive")

    # 停止复制
    if not self.databaseManager.stopSlave():
      Common.unifyLog.logError("stop import db failed, stop slave error")
      return False, ResponseMsg.stopImportFail("stop replication failed")

    #获取当前的复制位置
    slaveBinlog, slaveOffset = self.databaseManager.getSlaveBinlogOffset()
    Common.rdsLog.logInfo(" Master_Log_File : {0} Exec_Master_Log_Pos = {1}".format(slaveBinlog, slaveOffset))

    # 清理复制关系
    if not self.databaseManager.resetSlave():
      Common.unifyLog.logError("stop import db failed, reset slave failed")
      return False, ResponseMsg.stopImportFail("reset slave failed")

    if not self.diskManager.umount(DiskType.EBS_MIGRATE):
      return False, ResponseMsg.stopImportFail("umount disk failed")

    Common.unifyLog.logInfo("stop import db completely")
    return True, ResponseMsg.stopImportOK()


  def getRelayLogSize(self, msg):
    """
    返回当前RelayLog的大小
    """
    size = self.databaseManager.getRelayLogSize()
    return True, ResponseMsg.getRelayLogSizeMsg(0 if size == "NULL" else size)


  def getServiceStatus(self, msg):
    params = Util.json2Obj(msg)
    op = params['Operation']
    isSuccess, errInfo = self.databaseManager.getServiceStatus(op)
    if isSuccess:
      return True, ResponseMsg.getServiceStatusMsg(1, errInfo)
    else:
      return True, ResponseMsg.getServiceStatusMsg(0, errInfo)


  def dumpGrants(self, msg):
    params = Util.json2Obj(msg)
    url = params['NOSFileURL'].encode('ascii', 'ignore')
    nameList = url.split('$')
    isSuccess, errInfo = self.databaseManager.dumpGrants(nameList[0], nameList[1])

    if not isSuccess:
      Common.rdsLog.logError("Dump Grants failed")
      return False, ResponseMsg.commonFail(params, errInfo)
    else:
      Common.rdsLog.logInfo("Dump Grants success")
      return True, ResponseMsg.commonOK(params)


  def loadGrants(self, msg):
    params = Util.json2Obj(msg)
    url = params['NOSFileURL'].encode('ascii', 'ignore')
    nameList = url.split('$')
    isSuccess, errInfo = self.databaseManager.loadGrants(nameList[0], nameList[1])

    if not isSuccess:
      Common.rdsLog.logError("load Grants failed")
      return False, ResponseMsg.commonFail(params, errInfo)
    else:
      Common.rdsLog.logInfo("load Grants success")
      return True, ResponseMsg.commonOK(params)

  def createRoute(self, msg):
    params = Util.json2Obj(msg)
    routeDicts = params.get('Routes')
    for routeDict in routeDicts:
      ret = self.routeManager.createRoute(routeDict)
      if ret == False:
        errInfo = 'Create Route failed: %s' %  (routeDict, )
        return False, ResponseMsg.createRouteFail(errInfo)
    Common.rdsLog.logInfo('Create Route sucess')
    return True, ResponseMsg.createRouteOK()

  def deleteRoute(self, msg):
    params = Util.json2Obj(msg)
    routeDicts = params.get('Routes')
    for routeDict in routeDicts:
      ret = self.routeManager.deleteRoute(routeDict)
      if ret == False:
        errInfo = 'Delete Route failed: %s' %  (routeDict, )
        return False, ResponseMsg.deleteRouteFail(errInfo)
    Common.rdsLog.logInfo('Delete Route sucess')
    return True, ResponseMsg.deleteRouteOK()

  def describeRoute(self, msg):
    params = Util.json2Obj(msg)
    ret, routes = self.routeManager.describeAll()
    if ret == False:
      errInfo = routes
      return False, ResponseMsg.describeRoutesFail("Describe Route failed.")
    Common.rdsLog.logInfo('describeRoute sucess')
    return True, ResponseMsg.describeRoutesOK(routes)