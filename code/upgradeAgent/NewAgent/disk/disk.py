# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-14
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:  磁盘基础类，用于继承
"""

class Disk:
  
  def mount(self, device):
    return True

  def umount(self):
    return True

  def format(self, device):
    return True

  def resize(self, device, newSize):
    return True

