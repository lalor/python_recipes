# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-14
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:  NBS磁盘管理类，从disk类继承
"""

from util import Util
from disk.disk import Disk

class NBS(Disk):

  def __init__(self, mountDir=r'/ebs'):
    self.mountDir = mountDir
    self.tempDir  = Util.getFileDir(__file__)

  def __addIntoFstab__(self, device):
    """
     把device信息写入fstab文件，写入前，先备份当前fstab，然后写入fstab，
     如果写入成功，则用备份文件覆盖修改过的文件，如果写入成功，删除备份文件。
    """
    if not Util.isExists("/etc/fstab"):
      return False
    # 备份fstab文件
    if not Util.execCommand("cp -f /etc/fstab {0}/fstab.tmp".format(self.tempDir)):
      return False
    # 把ebs信息写入fstab文件
    if Util.execCommand("echo '{0} {1} xfs defaults 0 0' >> {2}/fstab.tmp".format(device, self.mountDir, self.tempDir)):
      # 写入成功，用备份临时文件覆盖原文件
      Util.execCommand("sudo cp -f {0}/fstab.tmp /etc/fstab".format(self.tempDir))
      return True
    else:
      # 写入失败，删除临时文件
      Util.execCommand("rm -rf {0}/fstab.tmp".format(self.tempDir))
    return False

  def __clearFromFstab__(self):
    """
     从fstab文件中删除ebs信息，通过awk把删除了ebs的信息定向到一个临时文件，然后用临时文件覆盖/etc/fstab文件
    """
    if not Util.isExists("/etc/fstab"):
      return False
    # 删除ebs信息，把删除了ebs信息的内容写入临时文件
    if not Util.execCommand("awk '!{0}/' /etc/fstab >> {1}/fstab.tmp".format(self.mountDir, self.tempDir)):
      return False
    # 用临时文件覆盖/etc/fstab文件
    if not Util.execCommand("sudo mv {0}/fstab.tmp /etc/fstab".format(self.tempDir)):
      return False
    return True

  def __mountedAlready__(self):
    """
     检查磁盘是否已经挂载
    """
    cmd = "df | awk '{if($6==\"%s\"){print $6}}'" % self.mountDir
    lines = Util.popenCommand(cmd).readlines()
    if len(lines) == 0:
      return False
    else:
      return True
  
  def mount(self, device):
    """
     磁盘挂载
    """
    if self.__mountedAlready__():
      return True
    if device == "null":
      return False

    tryTimes = 0
    while not Util.isExists(device):
      Util.sleep(1)
      tryTimes = tryTimes + 1
      if tryTimes == 50:
        return False
    # 检查挂载路径是否存在，不存在则创建路径
    if not Util.isExists(self.mountDir):
      Util.execCommand("sudo mkdir {0}".format(self.mountDir))
    if not self.__addIntoFstab__(device):
      return False
    # 挂载磁盘到指定目录
    if not Util.execCommand("sudo mount -t xfs {0} {1}".format(device, self.mountDir)):
      return False
    return True


  def umount(self):
    """
     卸载挂载的磁盘
    """
    if not self.__clearFromFstab__():
      return False
    tryTimes = 0
    while True:
      tryTimes = tryTimes + 1
      if tryTimes == 50:
        return False
      Util.sleep(1)
      # 如果没有挂载，直接返回True
      if not self.__mountedAlready__():
        return True
      # 断开所有该盘上的连接
      Util.execCommand("sudo fuser -mk {0}".format(self.mountDir))
      # 卸载当前盘
      if Util.execCommand("sudo umount {0}".format(self.mountDir)):
        return True


  def format(self, device):
    """
     格式化磁盘，调用mkfs.xfs命令
    """
    # 如果盘已经挂载，则直接返回True，主要可能出现在agent在进行到一半的时候宕机，
    # 重启后重做未完成操作，而上次操作宕机前已经挂载了磁盘
    if self.__mountedAlready__():
      return True
    assert("null" != device)
    tryTimes = 0
    while not Util.isExists(device):
      Util.sleep(1)
      tryTimes = tryTimes + 1
      if tryTimes == 50:
        return False
    if not Util.execCommand("sudo mkfs.xfs -f {0}".format(device)):
      return False
    return True

  def getMountDir(self):
    return self.mountDir

  def resize(self, device, size):
    """
     重新调整磁盘大小，调用xfs_growfs命令，NBS只支持增加磁盘容量，不支持磁盘容量缩小
    """
    assert("null" != device)
    assert("null" != size)
    if not Util.isExists(device):
      return False
    if not Util.execCommand("sudo xfs_growfs {0} -d".format(self.mountDir)):
      return False
    return True

