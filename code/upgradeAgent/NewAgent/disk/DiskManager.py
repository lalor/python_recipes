# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-24
@contact: jhx1008@gmail.com
version:  1.0
@todo:    磁盘管理类
@modify:
"""
from disk import Disk
from util import Util
from nbs.nbs import NBS

class DiskType:
  EBS = 0
  EBS_TMP = 1
  EBS_INNER = 2
  EBS_MIGRATE = 3

class DiskManager:
  def __init__(self):
    self.diskDict = {}
    self.diskDict[DiskType.EBS] = NBS()
    self.diskDict[DiskType.EBS_TMP] = NBS('/ebs_tmp')
    self.diskDict[DiskType.EBS_INNER] = NBS('/ebs_inner')
    self.diskDict[DiskType.EBS_MIGRATE] = NBS('/ebs_migrate')

  def mount(self, device, dtype=DiskType.EBS):
    return self.diskDict[dtype].mount(device)

  def umount(self, dtype=DiskType.EBS):
    return self.diskDict[dtype].umount()

  def format(self, device, dtype=DiskType.EBS):
    return self.diskDict[dtype].format(device)

  def getMountDir(self, dtype=DiskType.EBS):
    return self.diskDict[dtype].getMountDir()

  def resize(self, device, size, dtype=DiskType.EBS):
    return self.diskDict[dtype].resize(device, size)

  def copyFiles(self, src, dst):
    return Util.execCommand("sudo cp -R -p -d -f {0} {1}".format(src, dst))

  def clearFiles(self, files):
    return Util.execCommand("sudo rm -rf {0}".format(files))

  def clearOSCache(self):
    return Util.execCommand("sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'")
