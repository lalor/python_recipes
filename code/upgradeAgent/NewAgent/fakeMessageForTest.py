#!/usr/bin/python
#-*- coding: UTF-8 -*-
"""
@author: Mingxing LAI
@email : me@mignxinglai.com
@util  : 这是一个测试程序，并不是Agent的一部分。该文件的作用是，读取Manager发送给
         Agent的最后一条消息，修改版本号以后，再将消息发送给Agent。这样做的目的是
         快速地调试Agent程序。即，可以临时修改Agent以后，再次发送消息进行测试
"""
import zmq
import sys
import json

def main():

    fname="/home/rds-user/log/mylog"
    if len(sys.argv) == 2 :
        fname = sys.argv[1]

    context = zmq.Context().instance()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5000")
    try:
        last = None
        with open(fname) as fhandle:
            for line in fhandle:
                if line.find('recv Manager') != -1:
                    last = line

        # 日志文件中没有接收到过任何消息，则返回
        if not last:
            print 'No message found !!!'
            sys.exit(1)

        message = last[last.find('{'):]
        d = json.loads(message)
        #修改版本号
        d['Version'] = str( long(d['Version']) + 1 )
        socket.send(json.dumps(d))
        msg = socket.recv()
        print '<---------msg={0}'.format(msg)

    except Exception, e:
        print "{0}".format(e)
    socket.close()
    context.term()


if __name__ == '__main__':
    main()
