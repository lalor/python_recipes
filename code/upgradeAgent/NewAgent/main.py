# -*- coding:UTF-8 -*-
"""
@author:  hzjianghongxiang
@date:    2014-01-20
@contact: jhx1008@gmail.com
version:  1.0
@todo:    agent启动入口
@modify:
"""
import sys
import os
import traceback
from common import *
from AgentManager import *
from command import Command
from config.RDSConfig import *
from log.RDSLog import RDSLog
from log.BaseLog import LogLevel
from log.UnifyLog import UnifyLog
from threads.StatusThread import *
from communication.ZMQWrapper import *
from RDSAppInstance import ApplicationInstance
from communication.ResponseMsg import ResponseMsg
from config.RDSOptionParser import RDSOptionParser
from config.RDSConfigParser import RDSConfigParser
from threads.HeartBeatThread import HeartBeatThread
from threads.SystemMonitor import SystemMonitor
from threads.WorkerThread import WorkerThread
from threads.SyncTaskThread import SyncTaskThread
from threads.ExecuteSQLTaskThread import ExecuteSQLTaskThread
from threads.MonitorReporter import MonitorReporter
from database.DatabaseManager import DatabaseManager
from storage.StorageManager import StorageManager
from disk.DiskManager import DiskManager
from InstanceManager import InstanceManager
from network import route
from RDSException import RDSException

def init():
  optParser = RDSOptionParser()
  # 配置文件默认为RDSAgent.cnf,也可以通过defaults-file进行指定
  configFile = Util.getRealPath(optParser.configFile)
  console = optParser.console

  cfgParser = RDSConfigParser(configFile)
  # 通过配置文件初始化配置
  RDSConfigManager.initByConfigParser(cfgParser)
  # 通过命令行参数初始化配置，必须放在配置文件初始化之后，因为命令行参数
  # 可以覆盖配置文件中同名参数
  RDSConfigManager.initByOptionParser(optParser)
  # 初始化全局变量，例如log句柄等等

  if not console:
    daemonize()

  # 由于ZMQ调用的特殊性，一个进程只允许有一个context，如果实现了daemon，需要在daemon后进行context初始化
  ZMQComm.initContext()
  Common.rdsLog = RDSLog(RDSConfig.logDir, RDSConfig.logFile, \
                  LogLevel.string2Level(RDSConfig.logLevel),RDSConfig.logExpire)
  Common.unifyLog = UnifyLog(RDSConfig.logDir, RDSConfig.unifyLog, Util.getIPAddress("eth0"), expire=RDSConfig.logExpire)
  Common.monitorLog = RDSLog(RDSConfig.logDir, "monitor.log", LogLevel.string2Level(RDSConfig.logLevel),RDSConfig.logExpire)

  Common.statusManager = StatusManager(RDSConfig.agentDataDir, "status.info")
  Common.agentInfoManager = InfoManager(RDSConfig.agentDataDir, "agent.info")
  Common.uuidManager = UUIDManager(RDSConfig.agentDataDir, "uuid.info")
  Common.routeManager = route.RouteManager(RDSConfig.agentDataDir, "route.info") 
  # 检查route.info里面的路由是否都全部load到linux路由表中
  Common.routeManager.loadRouteInfo()
  # 初始化数据库ID和当前消息版本
  Common.databaseInstanceID = Common.agentInfoManager.getDatabaseInstanceID()
  Common.rdsLog.logInfo("get database instance id from file, id: {0}".format(Common.databaseInstanceID))
  Common.unifyLog.logInfo("get database instance id {0} from file".format(Common.databaseInstanceID))
  Common.msgVersion = Common.agentInfoManager.getMsgVersion()
  Common.rdsLog.logInfo("get version from file, version: {0}".format(Common.msgVersion))
  Common.unifyLog.logInfo("get version from file, version: {0}".format(Common.msgVersion))


  # 从文件中初始化监控属性
  msgInfo = Common.agentInfoManager.loadMsgInfo()
  if msgInfo != None:
    MonitorProperties.loadProperties(msgInfo)
  # 初始化存储管理
  Common.storageManager = StorageManager()
  # 初始化数据库管理
  Common.databaseManager = DatabaseManager(RDSConfig.mysqlHome,
          "{0}/my.cnf".format(RDSConfig.mysqlConfigDir),
          RDSConfig.mysqlUser, RDSConfig.mysqlPasswd,
          RDSConfig.backupDir, Common.storageManager)
  Common.diskManager = DiskManager()
  instanceManager = InstanceManager(Common.databaseManager,
          Common.storageManager,
          Common.agentInfoManager,
          Common.diskManager,
          Common.routeManager)
  Action.initAction(instanceManager)

def uninit():
  # 等待所有线程退出
  for thread in Common.threads:
    thread.stop()
  # 关闭状态管理
  if Common.statusManager != None:
    Common.statusManager.uninit()
  # 关闭信息管理
  if Common.agentInfoManager != None:
    Common.agentInfoManager.uninit()
  # 关闭日志管理
  if Common.rdsLog != None:
    Common.rdsLog.uninit()

  if Common.unifyLog != None:
    Common.unifyLog.uninit()

  if Common.uuidManager != None:
    Common.uuidManager.uninit()

def redo():
  """
   重做未完成的命令，读取命令持久化文件，检查当前执行命令状态，
   如果为非DONE状态，则需要重做该条命令
  """
  status = Common.statusManager.getStatus()
  params = Common.statusManager.getParam()
  # 如果为某个action状态，则需要重做，由于在action状态的情况下，一定是经过检查后才发生宕机的，
  # 所以启动后省略了对命令进行检查的操作
  if (status == "SENDING"
    or status == Action.CREATEDBINSTANCE
    or status == Action.UPGRADEAGENT
    or status == Action.STARTDBINSTANCE
    or status == Action.STOPDBINSTANCE
    or status == Action.BACKUP
    or status == Action.RESTOREDBINSTANCEFROMSNAPSHOT
    or status == Action.REBOOTDBINSTANCE
    or status == Action.MODIFYDBINSTANCE
    or status == Action.STARTREPLICATION
    or status == Action.REBUILDVOLUME
    or status == Action.CHANGEHA
    or status == Action.CHANGESYNC
    or status == Action.CHANGEASYNC
    or status == Action.INNERBACKUP
    or status == Action.INNERRESTOREDBINSTANCEFROMSNAPSHOT
    or status == Action.SETREADONLY
    or status == Action.FLASHBACKDBINSTANCE
    or status == Action.SYNCEXTERNALDB
    or status == Action.DUMPEXTERNALDBDATA
    or status == Action.LOADEXTERNALDBDATA
    or status == Action.LOADGRANTS
    or status == Action.DUMPGRANTS):
    Common.unifyLog.initUnifyInfo(Util.json2Obj(params))
    Common.rdsLog.logInfo("redo the action: {0}, params: {1}".format(status, params))
    Common.msgQueue.push((status, params))

def analyze(externalSvr, id, msg):
  """
   分析Manager发送过来的消息，对格式正确的消息进行处理，同时给予应答
  """
  cmd = Command()

  # 把json转换为dictionary形式，如果msg为非json格式，则忽略该条命令
  params = {}

  try:
    params = Util.json2Obj(msg)
  except Exception,e:
    Common.rdsLog.logError("check command [{0}] error, errmsg: {1}".format(msg,e))
    return

  # 检查命令的公共部分
  if not cmd.checkPublic(params):
    Common.rdsLog.logError("check public header error, errmsg: {0}".format(cmd.errmsg))
    externalSvr.sendMulti([id,'',ResponseMsg.errFormat(params, cmd.errmsg)])
    return
  Common.unifyLog.initUnifyInfo(params)

  action = params.get("Action","null").lower()
  # 查询错误日志信息
  if action == Action.GETERRORLOG:
    if not cmd.checkLog(params):
      Common.rdsLog.logError("get error log command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
      Common.syncMsgQueue.push((action, msg, id))
    return

  # 获取sql stats信息
  elif action == Action.DESCRIBESQLSTATS:
    if not cmd.checkDescribeSqlStats(params):
      Common.rdsLog.logError('describe sql stats command [{0}] error, errmsg: {1}'.format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, "", ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
      Common.syncMsgQueue.push((action, msg, id))
    return

  # 获取table stats信息
  elif action == Action.DESCRIBETABLESTATS:
    if not cmd.checkDescribeTableStats(params):
      Common.rdsLog.logError('describe table stats command [{0}] error, errmsg: {1}'.format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, "", ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
      Common.syncMsgQueue.push((action, msg, id))
    return
  elif action == Action.GETSERVICESTATUS:
    if not cmd.checkGetServerStatus(params):
      Common.rdsLog.logError('get server status commnd [ {0} ] error, errmsg: {1}'.format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, "", ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
      Common.syncMsgQueue.push((action, msg, id))
    return

  # 获取slow log信息
  elif action == Action.DESCRIBESLOWQUERY:
    if not cmd.checkDescribeSlowQuery(params):
      Common.rdsLog.logError('describe slow query command [{0}] error, errmsg: {1}'.format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, "", ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
      Common.syncMsgQueue.push((action, msg, id))
    return

  # 获取Relay Log Size
  elif action == Action.GETRELAYLOGSIZE:
    Common.syncMsgQueue.push((action, msg, id))
    return

  # 结束外部实例迁移
  elif action == Action.STOPIMPORTDB:
    if not cmd.checkStopImportDB(params):
      Common.rdsLog.logError("check stop import db command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
       Common.syncMsgQueue.push((action, msg, id))
    return

  # 将同步实例切换到异步状态，主要在将实例设置为维护前使用
  elif action == Action.CHANGEASYNC_SYNC:
    if not cmd.checkChangeAsync(params):
      Common.rdsLog.logError("change async (sync mode) command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
       Common.syncMsgQueue.push((action, msg, id))
    return

  # 将同步实例切换到同步状态，主要在将实例从维护中设置为可用时使用
  elif action == Action.CHANGESYNC_SYNC:
    if not cmd.checkChangeSync(params):
      Common.rdsLog.logError("change sync (sync mode) command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
       Common.syncMsgQueue.push((action, msg, id))
    return	
	
  # agent退出
  elif action == Action.EXITAGENT:
    Common.isAgentRunning = False
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    return

  # 创建路由规则
  elif action == Action.CREATE_ROUTE:
    if not cmd.checkCreateRoute(params):
      Common.rdsLog.logError("check CreateRoute command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
      Common.syncMsgQueue.push((action, msg, id))
      return

  # 删除路由规则
  elif action == Action.DELETE_ROUTE:
    if not cmd.checkDeleteRoute(params):
      Common.rdsLog.logError("check DeleteRoute command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
      Common.syncMsgQueue.push((action, msg, id))
    return

  # 描述路由规则
  elif action == Action.DESCRIBE_ROUTE:
    if not cmd.checkDescribeRoute(params):
      Common.rdsLog.logError("check DescribeRoute command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
    else:
      Common.syncMsgQueue.push((action, msg, id))
      return


  # 检查当前agent的状态, 如果agent正在发送error log，忽略当前命令，直接返回信息
  agentStatus = Common.statusManager.getStatus()
  if agentStatus == "SENDING":
    Common.rdsLog.logWarn("agent is sending, skip msg: {0}".format(msg))
    externalSvr.sendMulti([id, '', ResponseMsg.sendingNow(params, msg)])
    return

  # 当前agent的状态为非DONE时，说明有任务还没执行完毕，忽略当前命令，直接返回信息
  if agentStatus != 'DONE':
    Common.rdsLog.logWarn("agent is busy, skip msg: {0}".format(msg))
    externalSvr.sendMulti([id, '', ResponseMsg.errBusy(params, msg)])
    return
  """
  agent可以处理任务，后面会分析manager传入的命令格式，然后进行相应的操作
  """
  # 先check database instance id与保存的是否一致，如果不一致，则不是该agent的命令，
  # 忽略, VmId在checkPublic中已经验证过，这里可以不需要再验证
  if Common.databaseInstanceID == 'null':
    Common.databaseInstanceID = params['VmId']
  elif params['VmId'] != Common.databaseInstanceID:
    Common.rdsLog.logWarn("check VmId error, current VmId is {0}, skip msg: {1}".format(Common.databaseInstanceID, msg))
    externalSvr.sendMulti([id, '', ResponseMsg.errDatabaseID(params, Common.databaseInstanceID)])
    return

  # 检查消息版本信息，如果版本信息小于当前版本，则会被忽略, version在checkPublic中已经验证过，这里不需要再验证
  if Common.msgVersion == 'null' or long(params['Version']) > long(Common.msgVersion):
    Common.msgVersion = params['Version']
    Common.agentInfoManager.updateInfo(Common.databaseInstanceID, Common.msgVersion)
  else:
    # 如果消息版本相等，返回确认信息
    if long(params['Version']) == long(Common.msgVersion):
      Common.rdsLog.logInfo("the command with version {0} is done already!".format(params['Version']))
      externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    else:
      # 新消息版本小于当前版本，发送错误信息
      Common.rdsLog.logWarn("check version error, current version is {0}, skip msg: {1}".format(Common.msgVersion, msg))
      externalSvr.sendMulti([id, '', ResponseMsg.errVersion(params, Common.msgVersion)])
    return

  # 主从切换命令
  if action == Action.CHANGEHA:
    if not cmd.checkChangeHA(params):
      Common.rdsLog.logError("change ha command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 从节点执行stop slave操作
  elif action == Action.STOPSLAVE:
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 从节点执行start slave操作
  elif action == Action.STARTSLAVE:
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 启动复制命令
  elif action == Action.STARTREPLICATION:
    if not cmd.checkStartRepl(params):
      Common.rdsLog.logError("start replication command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 启动心跳
  elif action == Action.STARTHEARTBEAT:
    if not cmd.checkStartHeartBeat(params):
      Common.rdsLog.logError("start heartbeat command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
  # 升级系统
  elif action == Action.UPGRADEAGENT:
    if not cmd.checkUpdate(params):
      Common.rdsLog.logError("upgrade agent command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 创建数据库
  elif action == Action.CREATEDBINSTANCE:
    if not cmd.checkCreate(params):
      Common.rdsLog.logError("create db instance command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 关闭数据库
  elif action == Action.STOPDBINSTANCE:
    if not cmd.checkStop(params):
      Common.rdsLog.logError("stop db instance command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 启动数据库
  elif action == Action.STARTDBINSTANCE:
    if not cmd.checkStart(params):
      Common.rdsLog.logError("start db instance command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 重启数据库
  elif action == Action.REBOOTDBINSTANCE:
    if not cmd.checkReboot(params):
      Common.rdsLog.logError("reboot db instance command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 数据库备份
  elif action == Action.BACKUP:
    if not cmd.checkBackup(params):
      Common.rdsLog.logError("backup command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 数据库恢复
  elif action == Action.RESTOREDBINSTANCEFROMSNAPSHOT:
    if not cmd.checkRestore(params):
      Common.rdsLog.logError("restore db instance from snapshot command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 修改数据库实例
  elif action == Action.MODIFYDBINSTANCE:
    if not cmd.checkModify(params):
      Common.rdsLog.logError("modify db instance command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 同步转异步
  elif action == Action.CHANGEASYNC:
    if not cmd.checkChangeAsync(params):
      Common.rdsLog.logError("change async command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 异步转同步
  elif action == Action.CHANGESYNC:
    if not cmd.checkChangeSync(params):
      Common.rdsLog.logError("change sync command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  elif action == Action.REBUILDVOLUME:
    if not cmd.checkRebuildVolume(params):
      Common.rdsLog.logError('rebuild volume command [{0}] error, errmsg: {1}'.format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  elif action == Action.INNERBACKUP:
    if not cmd.checkInnerBackup(params):
      Common.rdsLog.logError("inner backup command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 数据库恢复
  elif action == Action.INNERRESTOREDBINSTANCEFROMSNAPSHOT:
    if not cmd.checkInnerRestore(params):
      Common.rdsLog.logError("inner restore db instance from snapshot command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  elif action == Action.FLASHBACKDBINSTANCE:
    if not cmd.checkFlashbackDBInstance(params):
      Common.rdsLog.logError("flash back db instance command[{0}] error,  errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  elif action == Action.SETREADONLY:
    cmd.checkSetReadOnly(params)
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 导出外部实例数据到本地盘
  elif action == Action.DUMPEXTERNALDBDATA:
    if not cmd.checkDumpExternalDBData(params):
      Common.rdsLog.logError("dump external db data command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 导入外部实例数据到RDS实例
  elif action == Action.LOADEXTERNALDBDATA:
    if not cmd.checkLoadExternalDBData(params):
      Common.rdsLog.logError("load external db data command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))
  # 开启外部实例到RDS实例的异步复制
  elif action == Action.SYNCEXTERNALDB:
    if not cmd.checkSyncExternalDB(params):
      Common.rdsLog.logError("sync external db command [{0}] error, errmsg: {1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))

  elif action == Action.DUMPGRANTS:
    if not cmd.checkDumpGrants(params):
      Common.rdsLog.logError("check dump grants command [{0}] error, errmsg :{1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))

  elif action == Action.LOADGRANTS:
    if not cmd.checkLoadGrants(params):
      Common.rdsLog.logError("check load grants command [{0}] error, errmsg :{1}".format(msg, cmd.errmsg))
      externalSvr.sendMulti([id, '', ResponseMsg.errFormat(params, cmd.errmsg)])
      return
    cmdInfo = Util.obj2Json(cmd.cmdmap)
    externalSvr.sendMulti([id, '', ResponseMsg.sendOK(params, msg)])
    Common.statusManager.updateStatus(action, cmdInfo)
    Common.msgQueue.push((action, cmdInfo))


# 守护进程函数
def daemonize():
  try:
    if os.fork() > 0:
      sys.exit(0)
  except OSError, e:
    raise RDSException("fork #1 failed: %d (%s)" % (e.errno, e.strerror))
  os.chdir("/")
  os.setsid()
  os.umask(0)

  try:
    if os.fork() > 0:
      sys.exit(0)
  except OSError, e:
    raise RDSException("fork #2 failed: %d (%s)" % (e.errno, e.strerror))

  sys.stdout.flush()
  sys.stderr.flush()

  si = file('/dev/null', 'r')
  so = file('/dev/null', 'a+')
  se = file('/dev/null', 'a+', 0)
  os.dup2(si.fileno(), sys.stdin.fileno())
  os.dup2(so.fileno(), sys.stdout.fileno())
  os.dup2(se.fileno(), sys.stderr.fileno())

def main():
  init()
  pidFile = '{0}/agent.pid'.format(RDSConfig.logDir)
  fileName = Util.pathSplit(__file__)[1]
  AppInstance = ApplicationInstance(pidFile, fileName)
  # 获取Block时间线程，用于HeartBeat线程上报用
  showMasterBlockTimeThread = ShowMasterBlockTimeInVSR(activeReportInterval=RDSConfig.threadInterval)
  Common.threads.append(showMasterBlockTimeThread)
  showMasterBlockTimeThread.start()

  # slave状态获取线程，获取slave上的延迟，如果发生错误，同时也获取错误信息，用于HeartBeat上报
  showSlaveStatusThread = ShowSlaveStatus(activeReportInterval=RDSConfig.threadInterval)
  Common.threads.append(showSlaveStatusThread)
  showSlaveStatusThread.start()

  # sync状态获取线程，用于获取当前slave状态时sync还是async，用于HeartBeat上报
  showSyncStatusThread = ShowSyncStatus(activeReportInterval=RDSConfig.threadInterval)
  Common.threads.append(showSyncStatusThread)
  showSyncStatusThread.start()

  # 监控信息上报线程，向云监控上报本地监控信息
  monitorReporterThread = MonitorReporter(activeReportInterval=RDSConfig.threadInterval,
         logDir=RDSConfig.logDir)
  Common.threads.append(monitorReporterThread)
  monitorReporterThread.start()

  # 心跳线程，与Manager之间心跳连接，定时上报心跳
  heartBeatThread = HeartBeatThread(activeReportInterval=RDSConfig.threadInterval,
          interval=RDSConfig.heartBeatInterval,
          thread1=showMasterBlockTimeThread,
          thread2=showSlaveStatusThread,
          thread3=showSyncStatusThread)

  Common.threads.append(heartBeatThread)
  heartBeatThread.start()
  # 系统监控线程，监控系统资源等信息
  systemMonitor = SystemMonitor(activeReportInterval=RDSConfig.threadInterval, logDir=RDSConfig.logDir)
  Common.threads.append(systemMonitor)
  systemMonitor.start()

  # 通讯处理
  externalServer = ZMQServer(ZMQProtocolType.TCP, ZMQSocketType.ROUTER, '*', RDSConfig.agentPort)
  internalServer = ZMQServer(ZMQProtocolType.INPROC, ZMQSocketType.DEALER, 'rdsagentinternal', 0)

  # 工作线程，用于处理Manager发送过来的任务，由于工作线程中部分消息需要同步发送到Manager端口，
  # 通过rdsagentinternal进行转发，所以在通讯模块后面进行初始化
  workerThread = WorkerThread(activeReportInterval= RDSConfig.threadInterval)
  Common.threads.append(workerThread)
  workerThread.start()

  # 同步消息处理线程
  syncTaskThread = SyncTaskThread(activeReportInterval= RDSConfig.threadInterval)
  Common.threads.append(syncTaskThread)
  syncTaskThread.start()

  # 监听线程，用以处理MySQL连接爆满的时候，DBA通过Agent持有的长连接查看数据库信息
  #executeSQLTaskThread = ExecuteSQLTaskThread(activeReportInterval= RDSConfig.threadInterval)
  #Common.threads.append(executeSQLTaskThread)
  #executeSQLTaskThread.start()

  # sleep 100ms 确保线程都启动
  Util.msSleep(100)
  redo()

  zmqPoll = ZMQPoll()
  zmqPoll.register(externalServer, ZMQSocketType.POLLIN)
  zmqPoll.register(internalServer, ZMQSocketType.POLLIN)
  while Common.isAgentRunning:
    # 外部通讯接口，接收manager端的消息然后进行处理
    zmqPoll.poll()
    if zmqPoll.isActive(externalServer):
      # 接收Manager发送过来的消息
      result = externalServer.recvMulti()
      id = result[0]
      # 接收id和内容中间有个空格，所以从2开始
      msg = "".join(result[2:])
      Common.rdsLog.logInfo("recv Manager's msg: {0}".format(msg))
      try:
        analyze(externalServer, id, msg)
      except Exception,e:
        Common.rdsLog.logError("analyze exception: {0}".format(e))
        externalServer.sendMulti([id, '', ResponseMsg.errFormat(Util.json2Obj(msg), "unknown exception")])

    # 内部通讯接口，接收内部处理结果，然后通过外部接口发送给manager
    if zmqPoll.isActive(internalServer):
      # 接收内部处理结果
      id, msg = internalServer.recvMulti()
      # 把处理结果发送给manager端
      externalServer.sendMulti([id, '', msg])
      Common.rdsLog.logDebug("send msg to Manager: [id: {0}, msg: {1}]".format(id, msg))

  #关闭Agent与Manager的通讯
  externalServer.close()
  internalServer.close()
  Common.rdsLog.logInfo("agent normal exit")
  Common.unifyLog.logInfo("agent normal exit")
  AppInstance.exitApplication()

if __name__ == '__main__':
  try:
    main()
  except SystemExit:
    pass
  except Exception,e:
    if Common.rdsLog != None:
      Common.rdsLog.logError("start agent error, {0}".format(e))
    else:
      print traceback.format_exc()
  except KeyboardInterrupt:
    if Common.rdsLog != None:
      Common.rdsLog.logError("agent is exit by CTRL + C")
    else:
      print traceback.format_exc()
  except:
    if Common.rdsLog != None:
      Common.rdsLog.logError('start agent error, traceback:{0}'.format(traceback.format_exc()))
    else:
      print traceback.format_exc()
  finally:
    uninit()
