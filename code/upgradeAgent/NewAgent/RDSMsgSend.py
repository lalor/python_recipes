# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-02-11
@contact: jhx1008@gmail.com
version:  1.0
@todo:    模拟RDS Manager向Agent发送消息
@modify:
"""
import sys
import zmq
import time
import random
import threading
import crypto


if __name__ == '__main__':
  context = zmq.Context().instance()
  socket = context.socket(zmq.REQ)
  socket.connect("tcp://localhost:5000")
  publicPart = '"LogInfo":"{\\"timestamp\\":1396939532812,\\"id\\":\\"94c90742-2044-4f39-a707-355429908d7c\\",\\"module\\":\\"RDS\\",\\"level\\":\\"info\\",\\"op\\":\\"CreateDBInstance\\", \\"description\\":\\"Finish Create  NBS!\\",\\"seq\\":\\"2.44\\",\\"object\\":{\\"DBInstanceIdentifier\\":\\"DBInstance@rds-003:32fb34d2ff1e48079990abe9cbcc59a4\\"}, \\"identifier\\":\\"32fb34d2ff1e48079990abe9cbcc59a4\\",\\"ip\\":\\"10.120.41.85\\"}"'
  dbParameters = "\\n[mysqld]\\nserver-id=10153\\nuser=mysql\\nport=3306\\nskip-name-resolve\\nskip_external_locking\\nbasedir=/usr/local/mysql\\ndatadir=/ebs/mysql_data\\nslow_query_log_file=mysql-slow.log\\ntmpdir=/ebs/tmp_dir\\nlog_error=mysql-err.log\\nlog_bin=mysql-bin.log\\npid_file=mysql.pid\\nsocket=/tmp/mysql.sock\\nlog_output=TABLE\\ncharacter_set_server=utf8\\nquery_cache_type=0\\ntable_cache=512\\nlong_query_time=10\\nmax_connections=1000\\nmax_connect_errors=1000\\nmax_allowed_packet=16M\\ngeneral_log=OFF\\nslow_query_log=ON\\nsync_binlog=1\\nrelay-log=mysqld-relay-bin\\nbinlog_format=Row\\nexpire_logs_days=7\\ninnodb_io_capacity=500\\ninnodb_flush_method=O_DIRECT\\ninnodb_file_format=Barracuda\\ninnodb_file_format_max=Barracuda\\ninnodb_log_file_size=1G\\ninnodb_file_per_table\\ninnodb_change_buffering=inserts\\ninnodb_lock_wait_timeout=5\\ninnodb_buffer_pool_size=128M\\ninnodb_print_all_deadlocks=ON\\nskip-slave-start\\ninnodb_flush_log_at_trx_commit=1\\nrelay_log_recovery=ON\\nenable_table_relay_info=1\\ninnodb_additional_mem_pool_size=2M\\ninnodb_data_file_path=ibdata1:512M:autoextend\\ninnodb_autoextend_increment=64\\n\\n\\n"

  actionMsgMap = {
    1   : '"Action":"GetErrorLog","EndRow":"110","Extent":"100"', 
    2   : '"Action":"ChangeHA","DBInstanceIdentifier":"dbid","VMmoniterInfo":"minfo","DBParameters":"{0}"'.format(dbParameters),
    3   : '"Action":"StartReplication","DBInstanceIdentifier":"modifyinstance2$slave","VMmoniterInfo":"/etc/vm_monitor/info","BinlogOffset":"1331","ReplicaUserPassword":"{0}","Binlog":"mysql-bin.000003","MasterIP":"10.160.50.87","MasterPort":"3306"'.format(crypto.encrypt("passwd","NeteaseR")),
    4   : '"Action":"StartDBInstance","DBParameters":"{0}","Device":"/dev/nbs/xdjo","AllocatedStorage":"22"'.format(dbParameters),  
    5   : '"Action":"StopDBInstance","SkipFinalSnapshot":"true"', 
    6   : '"Action":"StartHeartBeat"',
    7   : '"Action":"UpgradeAgent","AgentProgrammeUrl":"http://www.163.com","AgentVersion":"0.2.1"',
    8   : '"Action":"CreateDBInstance","UserProductID":"32fb34d2ff1e48079990abe9cbcc59a4","DBInstanceIdentifier":"rds-003$slave","Device":"/dev/nbs/xdjo","ServiceType":"RDS","MonitorAcessKey":"246d5f3952144a4c9ef5a99fc60dc0bc","MonitorServerIp":"115.236.124.217","MonitorServerDomain":"openfire0.photo.163.org","MasterUserPassword":"{0}","MonitorSecretKey":"bb99beeeb12a423e83a8518f10aa605c","Action":"CreateDBInstance","MasterUserName":"rds","ReplicaUserPassword":"{0}","MonitorWebServerUrl":"http://115.236.124.217:8184","DBName":"rds","DBParameters":"{1}"'.format(crypto.encrypt("passwd", "NeteaseR"), dbParameters),
    9   : '"Action":"StopSlave"', 
    10  : '"Action":"StartSlave"',  
    11  : '"Action":"RebootDBInstance"', 
    12  : '"Action":"Backup","Binlog":"mysql-bin.000012","SnapshotIdentifier":"mysnapshot-blockmaster:32fb34d2ff1e48079990abe9cbcc59a4","SnapshotURL":"cloud-agent20$32fb34d2ff1e48079990abe9cbcc59a4/mydbinstance-blockmaster:32fb34d2ff1e48079990abe9cbcc59a4/mysnapshot-blockmaster-incremental-000012:32fb34d2ff1e48079990abe9cbcc59a4"', 
    13  : '"Action":"RestoreDBInstanceFromSnapshot","Binlog":"mysql-bin.000010","BinlogOffset":"228","DBParameters":"{0}","UserProductID":"32fb34d2ff1e48079990abe9cbcc59a4","DBInstanceIdentifier":"myrestored-blockmaster","Device":"/dev/nbs/xdjo","SnapshotIdentifier":"mysnapshot-blockmaster:32fb34d2ff1e48079990abe9cbcc59a4","ServiceType":"RDS","MonitorAcessKey":"246d5f3952144a4c9ef5a99fc60dc0bc","MonitorServerIp":"115.236.124.217","SnapshotURL":"cloud-agent20$32fb34d2ff1e48079990abe9cbcc59a4/mydbinstance-blockmaster:32fb34d2ff1e48079990abe9cbcc59a4/mysnapshot-blockmaster-incremental:32fb34d2ff1e48079990abe9cbcc59a4;cloud-agent20$32fb34d2ff1e48079990abe9cbcc59a4/mydbinstance-blockmaster:32fb34d2ff1e48079990abe9cbcc59a4/mysnapshot-blockmaster-incremental-000010:32fb34d2ff1e48079990abe9cbcc59a4;cloud-agent20$32fb34d2ff1e48079990abe9cbcc59a4/mydbinstance-blockmaster:32fb34d2ff1e48079990abe9cbcc59a4/mysnapshot-blockmaster-incremental-000012:32fb34d2ff1e48079990abe9cbcc59a4","MonitorServerDomain":"openfire0.photo.163.org","MonitorSecretKey":"bb99beeeb12a423e83a8518f10aa605c","MonitorWebServerUrl":"http://115.236.124.217:8184"'.format(dbParameters),  
    14  : '"Action":"ModifyDBInstance","DBParameters":"{0}","MasterUserPassword":"{1}","MasterUserName":"rds"'.format(dbParameters, crypto.encrypt("passwd","NeteaseR")), 
    15  : '"Action":"ChangeAsync","DBParameters":"{0}"'.format(dbParameters),
    16  : '"Action":"ChangeSync","DBParameters":"{0}"'.format(dbParameters),
    17  : '"Action":"ExitAgent"',
    18  : '"Action":"DescribeSqlStats","StartTime":"-1","EndTime":"-1"',
    19  : '"Action":"DescribeTableStats","StartTime":"-1","EndTime":"-1"',
    20  : '"Action":"DescribeSlowQuery","StartTime":"-1","EndTime":"-1"',
    21  : '"Action":"SyncExternalDB","HostIp":"10.184.17.54","Port":"3306","UserName":"myadmin","Password":"{0}","Databases":"myadmin","Binlog":"mysql-bin.000003","BinlogOffset":"2551"'.format(crypto.encrypt("myadmin","NeteaseR")),
    22  : '"Action":"StopImportDB","Device":"/device/abcd"',
    23  : '"Action":"DumpExternalDBData","HostIp":"10.184.17.54","Port":"3306","UserName":"myadmin","Password":"{0}","Databases":"myadmin","Device":"/dev/abcd", "Threads":"2", "MigrateGrants":"1", "MonitorValues":"Threads_running=25", "RowsPerdump":"1000","LockTimeout":"20","MigrateType":"2"'.format(crypto.encrypt("myadmin","NeteaseR")),
    24  : '"Action":"LoadExternalDBData","Device":"/dev/abcd", "Threads":"3", "CompressKey":"8", "MigrateGrants":"1","CompressTables":"myadmin.person"'
  }
  versionId = 1
  if len(sys.argv) != 1:
    versionId = long(sys.argv[1])
  vmId = 15031
  key = random.randint(22,22)
  actionMsg = actionMsgMap[key]
  msg = '"Version":"{0}","VmId":"{1}",{2},{3}'.format(versionId, vmId, publicPart, actionMsg)
  msg = "{" + msg + "}"
  try:
    socket.send(msg)
    print 'send msg:[msg={0}]'.format(msg)
    msg = socket.recv()
    print 'recv replay:[msg={0}]'.format(msg)
  except:
    pass
  socket.close()
  context.term()
