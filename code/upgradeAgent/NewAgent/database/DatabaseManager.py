# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-12-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:    数据库管理基础类，用于实现agent与数据库之间的接口
@modify:
"""

from mysql import Mysql

class DatabaseManager:
  def __init__(self, installPath, configFile, user, passwd, backupDir, storageManager):
    self.database = Mysql(installPath, configFile, user, passwd, backupDir, storageManager)

  def getErrorLog(self, startRow, endRow):
    return self.database.getErrorLog(startRow, endRow)

  def saveConfigInfo(self, content):
    return self.database.saveConfigInfo(content)

  def isRunning(self):
    return self.database.isRunning()

  def shutdown(self, type=0):
    return self.database.shutdown(type)

  def install(self):
    return self.database.install()

  def start(self, first = False):
    return self.database.start(first)

  def isAlive(self):
    return self.database.isAlive()

  def stopIOThread(self):
    return self.database.stopIOThread()

  def getMasterLogInfo(self):
    return self.database.getMasterLogInfo()

  def allRelayLogApplied(self):
    return self.database.allRelayLogApplied()

  def stopSlave(self):
    return self.database.stopSlave()

  def startSlave(self):
    return self.database.startSlave()

  def setupSyncSemiRepl(self, install=True):
    return self.database.setupSyncSemiRepl(install)

  def startReplication(self):
    return self.database.startReplication()

  def stopReplication(self):
    return self.database.stopReplication()

  def getSlaveBinlogOffset(self):
    return self.database.getSlaveBinlogOffset()

  def getMasterBinlogOffset(self):
    return self.database.getMasterBinlogOffset()

  def backup(self, bucketName, keyName, startBinlog,startPosition):
    return self.database.backup(bucketName, keyName, startBinlog, startPosition)

  def innerBackup(self, backupPath):
    return self.database.innerBackup(backupPath)
  
  def restore(self, nameList, stopDatetime):
    return self.database.restore(nameList, stopDatetime)
  
  def innerRestore(self, restorePath):
    return self.database.innerRestore(restorePath)
  
  def deleteUser(self, user='root'):
    return self.database.deleteUser(user)

  def createUser(self, user, password, type = 0):
    return self.database.createUser(user, password, type)

  def createRDSUser(self):
    return self.database.createRDSUser()

  def createDatabase(self, dbName):
    return self.database.createDatabase(dbName)

  def changeAsync(self):
    return self.database.changeAsync()

  def changeSync(self):
    return self.database.changeSync()

  def cutBinlog(self, binlog, offset):
    return self.database.cutBinlog(binlog, offset)

  def changeMaster(self, masterHost, masterPort, masterUser, masterPasswd, masterLogFile, masterLogPos):
    return self.database.changeMaster(masterHost, masterPort, masterUser, masterPasswd, masterLogFile, masterLogPos)

  def setReadOnly(self, readonly = True):
    return self.database.setReadOnly(readonly)

  def changeUserPasswd(self, user, passwd):
    return self.database.changeUserPasswd(user, passwd)

  def getSessions(self):
    return self.database.getSessions()

  def getGlobalStatusInfo(self):
    return self.database.getGlobalStatusInfo()

  def getMasterBlockTimeInVSR(self):
    return self.database.getMasterBlockTimeInVSR()

  def getSlaveStatus(self):
    return self.database.getSlaveStatus()

  def getSyncStatus(self):
    return self.database.getSyncStatus()

  def resetSlave(self):
    return self.database.resetSlave()

  def getSqlStats(self, startTime, endTime):
    return self.database.getSqlStats(startTime, endTime)

  def getTableStats(self, startTime, endTime):
    return self.database.getTableStats(startTime, endTime)

  def getSlowLog(self, startTime, endTime):
    return self.database.getSlowLog(startTime, endTime)

  def getBinlogSize(self):
    return self.database.getBinlogSize()

  def getSecondsBehindMaster(self):
    return self.database.getSecondsBehindMaster()

  def getCombinedExistingRelayLogSize(self):
    return self.database.getCombinedExistingRelayLogSize()

  def upgradeDBVersion(self):
    return self.database.upgradeDBVersion()

  def flashBackDBInstance(self, binlog, offset):
    return self.database.flashBackDBInstance(binlog, offset)

  def killConnection(self):
    return self.database.killConnection()

  def getRelayLogSize(self):
    return self.database.getRelayLogSize()
    
  def backupBinlogForFlashback(self, binlog, offset):
    return False

  def dumpExternalDatabases(self, params, storeDir):
    return self.database.dumpExternalDatabases(params, storeDir)

  def loadExternalDatabases(self, threads, dbs, compressKey, compressTables, migrateGrants, storeDir):
    return self.database.loadExternalDatabases(threads, dbs, compressKey, compressTables, migrateGrants, storeDir)
 
  def getServiceStatus(self, op):
    return self.database.getServiceStatus(op)

  def modifyDBParameters(self, params):
    return self.database.modifyDBParameters(params)

  def executeSQL(self, sql):
    return self.database.executeSQL(sql)

  def dumpGrants(self, bucketName, keyName):
    return self.database.dumpGrants(bucketName, keyName)

  def loadGrants(self, bucketName, keyName):
    return self.database.loadGrants(bucketName, keyName)
