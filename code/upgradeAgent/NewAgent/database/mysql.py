# -*- coding:UTF-8 -*-
"""
@author:  hzjianghongxiang
@date:    2013-12-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:    数据库管理基础类，用于定义agent与manager之间的接口
@modify:
"""

import time
import os,re
import crypto
import MySQLdb
import threading
import string
from ConfigParser import SafeConfigParser
from util import Util
from database import Database
from common import Common
from config.RDSConfig import *
from RDSException import RDSException

class MysqlConnection:
  def __init__(self, socket, user, passwd):
    self.connParams = (socket, user, passwd)
    self.conn = self.__create__()

  def __create__(self):
    conn = None
    try:
      conn = MySQLdb.connect(unix_socket = self.connParams[0], user=self.connParams[1], passwd=self.connParams[2])
      conn.autocommit(True)
    except:
      pass
    return conn

  def execSql(self, sql):
    rows = None
    # 连接没建立，重新建立连接
    if self.conn == None:
      self.conn = self.__create__()
    # 连接创建失败，直接返回失败
    if self.conn == None:
      return None
    cursor = self.conn.cursor()
    try:
      # 执行SQL获取结果
      cursor.execute(sql)
      rows = cursor.fetchall()
      cursor.close()
    except:
      cursor.close()
      # SQL执行出错，可能是由于连接断掉了，重新尝试建立连接
      self.conn = self.__create__()
      # 重建成功，执行SQL
      if self.conn != None:
        cursor = self.conn.cursor()
        try:
          cursor.execute(sql)
          rows = cursor.fetchall()
          cursor.close()
        except:
          cursor.close()
    return rows

  def close(self):
    if self.conn != None:
      self.conn.close()

class ConnectionPool:
  def __init__(self):
    self.connList = []
    self.mutex = threading.Lock()

  def put(self, conn):
    self.mutex.acquire()
    self.connList.append(conn)
    self.mutex.release()

  def get(self):
    ret = None
    self.mutex.acquire()
    if len(self.connList) != 0:
      ret = self.connList.pop(0)
    self.mutex.release()
    return ret

  def execSql(self, sql):
    rows = None
    conn = self.get()
    if conn != None:
      rows = conn.execSql(sql)
      self.put(conn)
    return rows

  def destory(self):
    # 连接池连接销毁函数，调用前确保没有连接正在进行操作
    self.mutex.acquire()
    for conn in self.connList:
      conn.close()
    self.mutex.release()

class Mysql(Database):
  PRIVILEGES = "SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, RELOAD, PROCESS, REFERENCES, INDEX,\
                ALTER, SHOW DATABASES, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, REPLICATION CLIENT,\
                CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, CREATE USER, EVENT, TRIGGER"
  REPLICATION_PRIV = "REPLICATION SLAVE, REPLICATION CLIENT"
  DUMP_SCHEMA = "schema.sql"
  DUMP_DIRECTORY = "data"
  DUMP_GRANTS = "grants.sql"


  def __init__(self, installPath, configFile, user, passwd, backupDir, storageManager):
    Database.__init__(self, installPath, configFile, backupDir, storageManager)
    self.dataDir = self.getDataDir()
    self.tmpDir = self.getTmpDir()
    self.user = user
    self.passwd = passwd
    self.sudo = True
    self.connectionPool = ConnectionPool()
    self.socket = self.getSocket()
    for i in range(8):
      conn = MysqlConnection(self.socket, self.user, self.passwd)
      self.connectionPool.put(conn)

  def getTmpDir(self):
    tmpDir = self.getValue(self.configFile, "tmpdir")
    if tmpDir == "":
      tmpDir = "/tmp"
    return tmpDir

  def getSocket(self):
    socket = self.getValue(self.configFile, "socket")
    if socket == "":
      socket='/tmp/mysql.sock'
    return socket

  def getDataDir(self):
    """
     从配置文件获取数据存储路径
    """
    dataDir = self.getValue(self.configFile, "innodb_data_home_dir")
    if dataDir == "":
      dataDir = self.getValue(self.configFile, "datadir")
    return dataDir

  def getValue(self, configFile, key):
    """
     根据给定的key值在配置文件中获取相应的value
    """
    if not Util.isExists(configFile):
      return ""
    try:
      f = open(configFile, 'r')
      lines = f.readlines()
      f.close()
      for line in lines:
        if line.find(key) != -1:
          key=(line.split('=')[0]).strip()
          # 首字母为#时，为注释
          if key[0] != '#':
            value=(line.split('=')[1]).strip()
            return value
    except:
      pass
    return ""

  def createDir(self, dirName):
    """
     创建指定文件夹，如果文件夹存在，先删除
    """
    # 删除数据文件夹
    cmd = "rm -rf {0}".format(dirName)
    if self.sudo:
      cmd = "sudo " + cmd
    Util.execCommand(cmd)

    # 创建数据文件夹
    cmd = "mkdir -p {0}".format(dirName)
    if self.sudo:
      cmd = "sudo " + cmd
    Util.execCommand(cmd)

    # 更改文件夹owner
    cmd = "chown -R mysql:mysql {0}".format(dirName)
    if self.sudo:
      cmd = "sudo " + cmd
    Util.execCommand(cmd)

  def install(self):
    """
     MySQL初始化，调用mysql_install_db生成mysql系统库
    """

    # 创建数据文件夹和临时文件夹
    self.createDir(self.dataDir)
    self.createDir(self.tmpDir)

    # 运行mysql_install_db初始化数据库
    cmd = "{0}/scripts/mysql_install_db --defaults-file={1} \
          --basedir={0} > /dev/null 2>&1".format(self.installPath, self.configFile)
    if self.sudo:
      cmd = "sudo " + cmd
    return Util.execCommand(cmd)

  def isAlive(self):
    """
     检查MySQL服务是否正常，通过使用mysqladmin ping检测, 如果返回'mysqld is alive'则说明MySQL服务正常运行
    """
    # 如果通过ps没有找到MySQL进程，说明MySQL没有启动，当然为Flase状态
    if not self.isRunning():
      return False
    # 如果ps存在MySQL进程，MySQL也不一定是服务状态，所以还需要通过连接确认
    rows = self.connectionPool.execSql('select 1;')
    if rows != None:
      return True
    return False

  def __isAlive__(self):
    """
     采用root用户，通过mysqladmin ping命令来检测mysql是否为alive状态，该函数只在启动时调用，启动后root用户会被删除
    """
    cmd = "{0}/bin/mysqladmin --defaults-file={1} -uroot ping".format(self.installPath, self.configFile)
    lines = Util.popenCommand(cmd).readlines()
    for line in lines:
      if line.find("mysqld is alive") != -1:
        return True
    return False

  def isRunning(self):
    """
     MySQL进程是否存在
    """
    keys=[self.installPath,self.configFile]
    return Util.findProcess("mysqld",keys)

  def start(self, first = False):
    """
     以mysql用户启动MySQL服务，RDS需要在启动时设置部分semisync参数，如果是第一次启动，
     由于semi插件都没有安装，不进行参数设置，在安装好semi插件后进行设置，以后每次启动，
     都需要设置semi参数
    """
    if not os.path.exists(self.configFile):
      return False
    # 如果MySQL进程已经存在并且服务运行正常，则直接返回，否则通过mysqld启动MySQL服务
    if not self.isRunning():
      cmd = "{0}/bin/mysqld --defaults-file={1} --basedir={0} --user=mysql &".format(self.installPath, self.configFile)
      if self.sudo:
        cmd = "sudo " + cmd
      if not Util.execCommand(cmd, fetchOut=False):
        return False
    # 如果第一次启动，rdsadmin用户还没有创建，需要通过root用户来判断MySQL服务是否存活
    if first and self.__isAlive__():
      return True
    elif self.isAlive():
      return True
    # 启动mysqld后，mysql需要进行初始化或者recover等操作，所以提供服务需要等待一段时间，最多等待600s
    for i in range(1,120):
      if first and self.__isAlive__():
        return True
      elif self.isAlive():
        return True
      Util.sleep(5)
    return False

  def shutdown(self, type=0):
    """
     通过kill -9方式关闭MySQL服务,type!=0采用mysqladmin方式关闭
    """
    if not self.isRunning():
      return True

    self.connectionPool.destory()

    for i in range(1,10):
      if self.isRunning():
        if type == 0:
          keys=[self.installPath,self.configFile]
          Util.killProcess("mysqld",keys)
        else:
          cmd="{0}/bin/mysqladmin --defaults-file={1} -u{2} -p{3} shutdown".format(
                self.installPath, self.configFile, self.user, self.passwd)
          if self.sudo:
            cmd = "sudo " + cmd
          Util.execCommand(cmd)
        Util.sleep(1)
      else:
        return True
    return False

  def getErrorLog(self, extent, end):
    """
     MySQL错误日志获取函数，返回获取的错误日志的行数和错误日志的内容
     end表示到文件的end行结束，如果end大于文件总的行数，则取文件总行数，如果end为负数，则取文件总行数
     extent为end向前取多少行，如果extent大于文件总行数，则取整个文件大小
    """
    errLog = self.getValue(self.configFile, "log_error")
    path, name = Util.pathSplit(errLog)
    # 调用os.path.split()分割出路径和文件(夹)名，如果路径为空，则为相对路径
    if path == "":
      #相对路径，在dataDir目录下，完全路径为dataDir + "/" + errorLog
      errLog = "{0}/{1}".format(self.dataDir, errLog)
    if not Util.isExists(errLog):
      return 0,""
    content = ""
    cmd = "sed -n '$=' %s" % errLog
    if self.sudo:
      cmd = "sudo " + cmd
    #获取errlog文件行数
    rows = long(Util.popenCommand(cmd).readlines()[0])
    # 如果end为-1，或者结束函数大于文件总行数，则把结束行数定位文件末尾
    if end == -1 or rows < end:
      end = rows
    # 计算需要获取的文件行数
    start = end - extent
    # 如果开始位置大于结束位置，则设置获取的行数为0
    if start < 0:
      start = 0
    cmd = "sed -n '%d,%d'p %s" % (start+1, end, errLog)
    if self.sudo:
      cmd = "sudo " + cmd
    lines = Util.popenCommand(cmd).readlines()
    content = "".join(lines)
    return start, content

  def setReadOnly(self, readonly = True):
    """
     切换MySQL状态为read only
    """
    sql = "set global read_only = true"
    if not readonly:
      sql = "set global read_only = false"
    return self.execSql(sql)

  def stopSlave(self):
    """
     通过stop slave停止slave运行
    """
    sql="stop slave"
    return self.execSql(sql)


  def startSlave(self):
    """
     通过start slave启动slave运行
    """
    # 如果MySQL没有启动，则返回False
    if not self.isRunning():
      return False

    # 如果执行失败，则返回False
    sql="start slave"
    if not self.execSql(sql):
      return False

    # 等待3s，然后判断复制是否正常启动
    Util.sleep(3)
    try:
      sql = 'show slave status'
      rows = self.connectionPool.execSql(sql)
      Common.rdsLog.logInfo("rows : {0}".format(rows))
      if rows == None:
        return False

      slaveIOState = "NULL"
      slaveSQLState = "NULL"
      for row in rows:
        # Slave_IO_Running
        slaveIOState = row[10]
        # Slave_SQL_Running
        slaveSQLState = row[11]
      Common.rdsLog.logInfo("Slave_IO_Running:{0} Slave_SQL_Running:{1}".format(slaveIOState, slaveSQLState))
      if slaveIOState.strip() != "Yes" or slaveSQLState.strip() != "Yes":
        return False
    except Exception, e:
      Common.rdsLog.logInfo("Check start salve error: {}".format(e))
      return False
    return True

  def setupSyncSemiRepl(self, install=True ):
    """
     semi插件安装
    """
    if install:
      sql1="INSTALL PLUGIN rpl_semi_sync_master SONAME \'semisync_master.so\'"
      sql2="INSTALL PLUGIN rpl_semi_sync_slave SONAME \'semisync_slave.so\'"
    else:
      sql1="set global rpl_semi_sync_slave_enabled=off"
      sql2="set global rpl_semi_sync_master_enabled=off"
    return self.execSql(sql1) and self.execSql(sql2)

  def startReplication(self):
    """
    在执行主从切换的时候，如果原来的主有处于Prepare状态的xa事务，那么，在mysqld重启
    以后，这些xa事务依然会持有锁。需要将xa事务回滚掉，以此来释放它持有的锁。不然，
    它可能阻塞SQL线程，导致复制失败。
    """
    if not self.isRunning():
      return False

    sql = "xa recover"
    transactions = self.connectionPool.execSql(sql)
    for trx in transactions:
      formatid = trx[0]
      gtrid_len = trx[1]
      data = trx[3]
      sql = "xa rollback '{0}', '{1}', {2};".format(data[:gtrid_len],
              data[gtrid_len:], formatid)
      try:
        self.connectionPool.execSql(sql)
      except Exception, e:
        raise RDSException("恢复xa事务异常：{0}".format(e))

    return self.startSlave()

  def stopReplication(self):
    """
     停止replication
    """
    sql="stop slave"
    return self.execSql(sql)

  def stopIOThread(self):
    """
     停止IO线程运行
    """
    sql="stop slave io_thread"
    return self.execSql(sql)

  def startIOThread(self):
    """
    启动IO线程
    """
    sql="start slave io_thread"
    return self.execSql(sql)

  def getMasterLogInfo(self):
    """
     获取master上写入binlog的当前信息，包括当前binlog文件名和写入位置
     show master status\G后显示的信息如下：
     File: mysql-bin.xxxx
     Position: xxx
    """
    binlogFile = ""
    position = -1
    if self.isRunning():
      # 执行show master status获取master上binlog信息
      sql = "show master status"
      rows = self.connectionPool.execSql(sql)
      if rows != None:
        for row in rows:
          binlogFile = row[0]
          position = long(row[1])
    return binlogFile, position

  def changeMaster(self, masterHost, masterPort, masterUser,
                      masterPasswd, masterLogFile, masterLogPos):
    """
     执行change master操作
    """
    first = True
    cmd = ""
    sql = "change master to"
    if masterHost != "null":
      cmd = " master_host='{0}'".format(masterHost)
      if first:
        first = False
      else:
        cmd = "," + cmd
      sql = sql + cmd
    if masterPort != "null":
      cmd = " master_port={0}".format(masterPort)
      if first:
        first = False
      else:
        cmd = "," + cmd
      sql = sql + cmd
    if masterUser != "null":
      cmd = " master_user='{0}'".format(masterUser)
      if first:
        first = False
      else:
        cmd = "," + cmd
      sql = sql + cmd
    if masterPasswd != "null":
      cmd = " master_password='{0}'".format(crypto.decrypt(masterPasswd, "NeteaseR"))
      if first:
        first = False
      else:
        cmd = "," + cmd
      sql = sql + cmd
    if masterLogFile != "null":
      cmd = " master_log_file='{0}'".format(masterLogFile)
      if first:
        first = False
      else:
        cmd = "," + cmd
      sql = sql + cmd
    if masterLogPos != "null":
      cmd = " master_log_pos={0}".format(masterLogPos)
      if first:
        first = False
      else:
        cmd = "," + cmd
      sql = sql + cmd
    return self.execSql(sql)


  def deleteUser(self, userName='root'):
    """
     删除MySQL账户
    """
    sql="delete from mysql.user where user='{0}'".format(userName)
    return self.execSql(sql)

  def createDatabase(self, dbName):
    """
     创建数据库，如果库已经存在，则先drop然后创建
    """
    sql = "drop database if exists {0}".format(dbName)
    if not self.execSql(sql):
      return False
    sql = "create database {0}".format(dbName)
    return self.execSql(sql)

  def createRDSUser(self):
    """
     创建rdsadmin用户，默认只有root用户
    """
    sql="GRANT ALL ON *.* TO \'{0}\'@'LOCALHOST' IDENTIFIED BY \'{1}\' WITH GRANT OPTION; FLUSH PRIVILEGES".format(self.user, self.passwd)
    cmd = "{0}/bin/mysql --defaults-file={1} -uroot -e \"{2}\"".format(self.installPath, self.configFile, sql)
    return Util.execCommand(cmd)

  def createUser(self, user, password, type=0):
    """
     创建RDS用户,type为用户类型，分为普通用户和replication用户两种，
     type为0表示普通用户，1表示replication用户, 2表示管理员用户
    """
    privileges = Mysql.PRIVILEGES
    if type == 1:
      privileges = Mysql.REPLICATION_PRIV
    elif type == 2:
      privileges = 'all'
    sql = "GRANT {0} ON *.* TO \'{1}\'@\'%\' IDENTIFIED BY \'{2}\' WITH GRANT OPTION".format(\
              privileges, user, crypto.decrypt(password, "NeteaseR"))
    return self.execSql(sql)

  def parseXtrabackBackupLog(self):

    masterBinlog = None
    masterOffset = None
    slaveBinlog = None
    slaveOffset = None
    with open(self.backupLog) as f:
      for line in f:
        if line.find("MySQL slave binlog position") != -1:
          slaveBinlog = line.split(',')[1].strip('" filename ').strip('\'')
          slaveOffset = line.split(',')[2].strip('\n').strip(' position ')
        elif line.find("innobackupex: MySQL binlog position:") != -1:
          masterBinlog = line.split('\'')[1]
          masterOffset = line.split()[7]
    return  masterBinlog, masterOffset, slaveBinlog, slaveOffset


  def getDatetimeByBinlogOffset(self, binlog, binlogOffset):
    #1. 文件不需要切割
    fileSize = Util.getFileSize(binlog)
    freeMemSize = Util.getFreeMem()
    if fileSize * 5 < freeMemSize:
      Common.rdsLog.logInfo("Ready to Parse Datetime without split")
      return self.getDatetimeInSegments(binlog, None, binlogOffset)
    else:
      #判断flash back这个工具是否存在
      flashback='/usr/local/bin/split_binlog_for_rds'
      if not Util.execCommand(flashback + " --help"):
        Common.rdsLog.logError(flashback + ' is not found')
        return None
      #大文件需要切割成小文件
      cmd = 'sudo {0} --split-size-interval={1} {2} | grep start'.format(flashback, RDSConfig.flashBackSplitSize, binlog)
      Common.rdsLog.logInfo("Ready to split binlog for parse Datetime: {0}".format(cmd))
      lines = Util.popenCommand(cmd).readlines()
      Common.rdsLog.logInfo("lines is :{0}".format(lines))
      segments = []
      for line in lines:
        if line.find('start') != -1:
          segments.append(line.split()[5])
      segments.append(fileSize)
      return self.getDatetimeInSegments(binlog, segments, binlogOffset)

  def getDatetimeInSegments(self, binlog, segments, binlogOffset):
    """
     解析binlog文件，根据binlogoffset获取对应的时间戳
    """
    if not segments:
      cmd = "{0}/bin/mysqlbinlog -vv  {1}".format(self.installPath, binlog) 
      if self.sudo:
        cmd = "sudo " + cmd
      lines = os.popen(cmd).readlines()
      for line in lines:
        if ('end_log_pos ' + str(binlogOffset) in line):
          pattern = re.compile(r'\d{6} \d{2}:\d{2}:\d{2}|\d{6}  \d{1}:\d{2}:\d{2}')
          mt = pattern.search(line)
          if mt:
            datetime = mt.group()
            datetime = "20" + datetime;
            timeArray = time.strptime(datetime, "%Y%m%d %H:%M:%S")
            return int(time.mktime(timeArray))
      return None
    else:
      start = -1
      for stop in segments:
        if start == -1:
          cmd = '{0}/bin/mysqlbinlog -vv --stop-position={1} {2}'.format(self.installPath, stop, binlog)
        else:
          cmd = '{0}/bin/mysqlbinlog -vv --start-position={1} --stop-position={2} {3}'.format(self.installPath, start, stop, binlog)
        start = stop

        if self.sudo:
          cmd = "sudo " + cmd
        lines = os.popen(cmd).readlines()
        for line in lines:
          if ('end_log_pos ' + str(binlogOffset) in line):
            pattern = re.compile(r'\d{6} \d{2}:\d{2}:\d{2}|\d{6}  \d{1}:\d{2}:\d{2}')
            mt = pattern.search(line)
            if mt:
               datetime = mt.group()
               datetime = "20" + datetime;
               timeArray = time.strptime(datetime, "%Y%m%d %H:%M:%S")
               return int(time.mktime(timeArray))
      return None
            

  def getStartDatetime(self, binlog, startBinlogOffset):
    if int(startBinlogOffset) == 0:
      return 0
    else:
      return self.getDatetimeByBinlogOffset(binlog,startBinlogOffset)


  def getBinlogOffsetByDatetime(self, binlog, datetime):
    #将大binlog进行切分
    fileSize = Util.getFileSize(binlog)
    freeMemSize = Util.getFreeMem()
    if fileSize * 5 < freeMemSize:
      Common.rdsLog.logInfo("Ready to Parse Datetime without split")
      return self.getBinlogOffsetInSegments(binlog,None,datetime)
    else:
      #判断flash back这个工具是否存在
      flashback='/usr/local/bin/split_binlog_for_rds'
      if not Util.execCommand(flashback + " --help"):
        Common.rdsLog.logError(flashback + ' is not found')
        return None
      cmd = 'sudo {0} --split-size-interval={1} {2} | grep start'.format(flashback, RDSConfig.flashBackSplitSize, binlog)
      Common.rdsLog.logInfo("Ready to split binlog for parse Datetime: {0}".format(cmd))
      lines = Util.popenCommand(cmd).readlines()
      Common.rdsLog.logInfo("lines is :{0}".format(lines))
      segments = []
      for line in lines:
        if line.find('start') != -1:
          segments.append(line.split()[5])
      segments.append(fileSize)
      return self.getBinlogOffsetInSegments(binlog,segments,datetime)

  def getBinlogOffsetInSegments(self, binlog, segments, datetime):
    if not segments:
      cmd = "{0}/bin/mysqlbinlog -vv  {1}".format(self.installPath, binlog) 
      binlogOffset = self.getBinlogOffsetCmd(cmd,datetime)
      if binlogOffset:
        return binlogOffset
      return None
    else:
      start = -1
      for stop in segments:
        if start == -1:
          cmd = '{0}/bin/mysqlbinlog -vv --stop-position={1} {2}'.format(self.installPath, stop, binlog)
        else:
          cmd = '{0}/bin/mysqlbinlog -vv --start-position={1} --stop-position={2} {3}'.format(self.installPath, start, stop, binlog)
        start = stop
        binlogOffset = self.getBinlogOffsetCmd(cmd,datetime)
        if binlogOffset:
          return binlogOffset
      return None


  def getBinlogOffsetCmd(self, command, datetime):

    targettimeArray = time.strptime(datetime, "%Y%m%d %H:%M:%S")
    targetTimestamp = int(time.mktime(targettimeArray))
    stopPos = None
    datetime = datetime[2:]
    unix_timestamp= 0;
    unix_timestampOlder = 0;

    if self.sudo:
      command = "sudo " + command
    lines = os.popen(command).readlines()
    for line in lines:
      if ('end_log_pos' in line):
        pattern = re.compile(r'\d{6} \d{2}:\d{2}:\d{2}|\d{6}  \d{1}:\d{2}:\d{2}')
        mt = pattern.search(line)
        if mt:
          datestamp = mt.group()
          datestamp = "20" + datestamp;
          unix_timestampOlder = unix_timestamp
          unix_timestamp = int(time.mktime(time.strptime(datestamp, "%Y%m%d %H:%M:%S")))
          if (unix_timestampOlder <= targetTimestamp and targetTimestamp <= unix_timestamp):
            pattern = re.compile(r'end_log_pos \d+')
            stopPos = pattern.search(line).group()
            if stopPos:
              return re.search(r'\d+',stopPos).group()
    return None

  def fullyBackup(self, bucketName, keyName):

    isSuccess = False
    masterBinlog =""
    masterOffset = 0
    errInfo = ""
    slaveBinlog = None
    slaveOffset = None

    # 如果innobackupex正在运行中，直接返回错误
    if Util.findProcess("innobackupex"):
      isSuccess = False
      errInfo = "innobackupex is running now, backup failed"
      return isSuccess, masterBinlog, masterOffset, errInfo, slaveBinlog, slaveOffset

    # 如果innobackupex没有运行，则启动innobackupex
    cmd = "innobackupex --slave-info --lock-wait-timeout=3600 --lock-wait-threshold=5  --lock-wait-query-type=all " \
          "--defaults-file={0} --socket={1} --stream=xbstream   --user={2} --password={3} {4}/backup 2>{5} " \
          "| qpress -io dir ".format( self.configFile, self.socket, self.user, self.passwd, self.backupDir, self.backupLog)

    if self.sudo:
      cmd = "sudo " + cmd
    if not self.storageManager.putStringByPipe(cmd, bucketName, keyName):
      isSuccess = False
      errInfo = "execute innobackupex failed"
      return isSuccess, masterBinlog, masterOffset, errInfo, slaveBinlog, slaveOffset

    # 等待innobackupex运行结束，每隔10s检查一次
    while Util.findProcess('innobackupex'):
      Util.sleep(10)
    isSuccess, masterBinlog, masterOffset, errInfo = self.isBackupOK()
    if isSuccess:
      masterBinlog, masterOffset, slaveBinlog, slaveOffset = self.parseXtrabackBackupLog()
    return isSuccess, masterBinlog, masterOffset, errInfo, slaveBinlog, slaveOffset


  def incrementalBackup(self, bucketName, keyName, startBinlog, startPosition):
    # 切换binlog文件
    self.execSql("flush binary logs")

    # 获取切换后正在写入的binlog文件
    binlog = self.getMasterLogInfo()[0]
    prefix, start = startBinlog.split(".")
    prefix, end   = binlog.split(".")
    start = long(start)
    end = long(end) - 1

    #处理Manager传了一个错误的Binlog文件的情况
    if end < start:
      return False, "", 0, "Is start binlog({0}) incorrect ?".format(startBinlog),None, None

    # 备份的binlog从startBinlog开始到flush binary logs前一个log文件，备份时需要打包所有的binlog
    tmpFile = "{0}/{1}-{2:0>6}.tar.gz".format(self.dataDir, startBinlog, end)
    cmd = "tar czf {0} -C {1}".format(tmpFile, self.dataDir)
    # [start, end]
    for index in range(start, end + 1):
      binlog = " {0}.{1:0>6}".format(prefix, index)
      cmd = cmd + binlog

    if not Util.execCommand(cmd if not self.sudo else "sudo " + cmd):
      return False, None, None, "run command {0} failed!".format(cmd), None, None

    if self.storageManager.putFile(bucketName, keyName, tmpFile):
      masterBinlog = "{0}.{1:0>6}".format(prefix, end)
      startDatetime = self.getStartDatetime("{0}/{1}".format(self.dataDir, startBinlog),startPosition)
      masterOffset = Util.getFileSize("{0}/{1}".format(self.dataDir, masterBinlog))
      endDatetime = self.getDatetimeByBinlogOffset("{0}/{1}".format(self.dataDir, masterBinlog),masterOffset)
      Common.rdsLog.logInfo("The StartBinlog and StartPosition of Incremetal Backup are {0} {1}".format(startBinlog,startPosition))
      Common.rdsLog.logInfo("The EndBinlog and EndPosition of Incremetal Backup are {0} {1}".format(masterBinlog,masterOffset))
      Common.rdsLog.logInfo("The startDatetime and endDatetime of Incremetal Backup are {0} {1}".format(startDatetime,endDatetime))
      if startDatetime is not None and endDatetime is not None:
        isSuccess = True
        errInfo  = ""
      else:
        isSuccess = False
        errInfo = "get datetime by position error"
    else:
      isSuccess = False
      errInfo = "do incremental backup error"
      masterBinlog = ""

    Util.execCommand("sudo rm -rf {0}".format(tmpFile))
    masterOffset = Util.getFileSize("{0}/{1}".format(self.dataDir, masterBinlog))
    return isSuccess, masterBinlog, masterOffset, errInfo, startDatetime, endDatetime


  def backup(self, bucketName, keyName, startBinlog, startPostion):
    """
     数据库备份，通过innobackupex工具进行备份，备份数据以流的形式输出，
     输出流通过qpress进行压缩后通过管道输出到NOS,如果startBinlog为空，
     则为全量备份，startBinlog不为空，则为增量备份，增量备份时，需要
     通过flush binary logs命令切换新的binlog，打包上次备份的binlog
     到新binlog之间的binlog文件，上传到NOS
    """

    masterBinlog = ""
    masterOffset = 0
    backupSize = 0
    slaveBinlog = ""
    slaveOffset = 0
    startDatetime = 0
    endDatetime = 0
    # 如果mysql没有运行，直接返回
    if not self.isRunning():
      isSuccess=False
      errInfo="database is not running"
      return isSuccess, masterBinlog, masterOffset, backupSize, errInfo, slaveBinlog, slaveOffset, startDatetime, endDatetime

    # 创建桶失败，则直接返回错误
    if self.storageManager.makeBucket(bucketName) == False:
      isSuccess = False
      errInfo   = "can not create bucket {0}".format(bucketName)
      return isSuccess, masterBinlog, masterOffset, backupSize, errInfo, slaveBinlog, slaveOffset, startDatetime, endDatetime

    #备份
    if startBinlog == "":
      ret = self.fullyBackup(bucketName, keyName)
      isSuccess, masterBinlog, masterOffset, errInfo, slaveBinlog, slaveOffset = ret
    else:
      ret = self.incrementalBackup(bucketName, keyName, startBinlog, startPostion)
      isSuccess, masterBinlog, masterOffset,errInfo, startDatetime, endDatetime= ret



    # 检查备份文件是否存在于NOS上
    if self.storageManager.lookupFile(bucketName, keyName):
      backupSize = self.storageManager.getFileSize(bucketName, keyName)
    else:
      isSuccess = False
      errInfo = "backup data upload failed"

    # 上传备份文件信息
    if isSuccess and not self.storageManager.putString(Util.obj2Json({"filename":masterBinlog,
      "filesize":"{0}".format(backupSize), "offset":"{0}".format(masterOffset)}), bucketName, keyName + "-info"):
      isSuccess = False
      errInfo = "backup information file:[{0}] upload failed".format(masterBinlog)

    return isSuccess, masterBinlog, masterOffset, backupSize, errInfo, slaveBinlog, slaveOffset, startDatetime, endDatetime


  def innerBackup(self, backupPath):
    """
     备份数据到本地磁盘, 通过innobackupex工具进行备份
    """
    isSuccess = True
    masterBinlog = errInfo = ""
    masterOffset= backupSize = -1
    slaveBinlog = None
    slaveOffset = None
    # 如果mysql没有运行， 直接返回
    if not self.isRunning():
      isSuccess = False
      errInfo = "database is not running"
      return isSuccess, masterBinlog, masterOffset, backupSize, errInfo, slaveBinlog, slaveOffset

    # 如果innobackupex正在运行中，直接返回错误
    if Util.findProcess("innobackupex"):
      isSuccess = False
      errInfo = "innobackupex is running now, backup failed"
      return isSuccess, masterBinlog, masterOffset, backupSize, errInfo, slaveBinlog, slaveOffset

    # 如果innobackupex没有运行，则启动innobackupex
    cmd = "innobackupex --slave-info --lock-wait-timeout=3600 --lock-wait-threshold=5 \
            --lock-wait-query-type=all --defaults-file={0} --socket={1} --user={2}  \
            --password={3} {4} 2>{5}".format(self.configFile, self.socket, self.user,
            self.passwd, backupPath, self.backupLog)
    if self.sudo:
      cmd = "sudo " + cmd
    Util.execCommand(cmd)

    # 等待innobackupex运行结束，每隔10s检查一次
    while Util.findProcess('innobackupex'):
      Util.sleep(10)
    isSuccess, masterBinlog, masterOffset, errInfo = self.isBackupOK()
    # 备份size参数没什么用，而内部备份需要通过遍历文件夹中所有文件来获得size大小，
    # 这个比较麻烦，直接返回一个值，不能为负数，Manager设计size为unsigned
    backupSize = 0
    if isSuccess:
      masterBinlog, masterOffset, slaveBinlog, slaveOffset = self.parseXtrabackBackupLog()
    return isSuccess, masterBinlog, masterOffset, backupSize, errInfo, slaveBinlog, slaveOffset


  def restoreFullyPart(self, nameTuple):
    """
    使用innobackupex恢复增量备份中的全量部分
    :param nameTuple: (bucket, key, binlog, offset)
    :return:(True/Flase, error info)
    """
    isSuccess = False
    bucketName, keyName, _, _ = nameTuple
    cmd = "qpress -dio | sudo xbstream -x -C {0}".format(self.dataDir)

    # 从存储平台上下载数据，并且通过qpress解压后通过xbstream保存到数据目录
    if not self.storageManager.getStringByPipe(cmd, bucketName, keyName):
      errInfo = "xbstream execute failed"
      return isSuccess, errInfo

    # 等待xbstream运行结束
    while Util.findProcess("xbstream"):
      Util.sleep(10)

    # 通过innobackupex恢复数据
    cmd = "innobackupex --apply-log --user={0} --password={1} {2} 2>{3}".format(
      self.user,
      self.passwd,
      self.dataDir,
      self.restoreLog)

    if not Util.execCommand(cmd if not self.sudo else "sudo " + cmd):
      errInfo = "apply log failed"
      return isSuccess, errInfo

    # 等待innobackupex运行结束
    while Util.findProcess("innobackupex"):
      Util.sleep(10)

    # 修改数据文件的拥有者(owner)
    cmd = "chown -R mysql:mysql {0}".format(self.dataDir)
    Util.execCommand(cmd if not self.sudo else "sudo " + cmd)

    cmd = "chown -R mysql:mysql {0}".format(self.tmpDir)
    Util.execCommand(cmd if not self.sudo else "sudo " + cmd)

    # 检查恢复日志查看恢复是否成功
    return self.isRestoreOK()


  def restoreIncrementalPart(self, nameList, stopDatetime):
    """
    通过mysqlbinlog解析，使用mysql回放的方式，
    完成增量备份恢复的增量备份
    """
    tempDir = self.tmpDir + "/binlog"

    # 保存sync_binlog和innodb_flush_log_at_trx_commit的值
    ovalBinlog = self.connectionPool.execSql("show variables like 'sync_binlog'")[0][1]
    ovalTrxCommit = self.connectionPool.execSql("show variables like 'innodb_flush_log_at_trx_commit'")[0][1]

    # 将sync_binlog 与 innodb_flush_log_at_trx_commit都设置为0，以加快回放速度
    self.connectionPool.execSql('set global sync_binlog=0')
    self.connectionPool.execSql('set global innodb_flush_log_at_trx_commit=0')
    try:
      for (bucket, key, binlog, offset) in nameList:
        Util.execCommand("sudo mkdir {0}".format(tempDir))
        Util.execCommand("sudo chown rds-user:rds-user {0}".format(tempDir))

        # 从NOS上下载所有的增量binlog包并解压到临时目录
        binlogPacket = "{0}/binlog_package.tar.gz".format(tempDir)
        if not self.storageManager.getFile(bucket, key, binlogPacket):
          return False, "cant get incremental binlog from nos error {0}:{1}".format(
            bucket, key )
        cmd = "tar xzf {0} -C {1}".format(binlogPacket, tempDir)
        Util.execCommand(cmd)

        # 处理Manager传递了错误的binlog的情况
        binlogFiles = self.getBinlogFiles(binlog, tempDir)
        if not binlogFiles:
          return False, 'do apply binlog error, Is the start binlog ({0}) incorrect?'.format(binlog)

        #如果stopDatetime不为None，则该操作是Point-in-Time恢复，需要获取stopDatetime对应的position;否则为普通增量备份恢复
        if stopDatetime != 0 and (bucket, key, binlog, offset) == nameList[-1]:
          Common.rdsLog.logInfo("Ready to apply binlogs for point-in-time restore :{0}".format(binlogFiles))
          binlogsForPointInTimes = []
          for binlogFile in binlogFiles:
            binlogsForPointInTimes.append(binlogFile)
            stopPos = self.getBinlogOffsetByDatetime(binlogFile,stopDatetime)
            if stopPos:
              break
            elif binlogFile == binlogFiles[-1]:
              return False, 'do apply binlog error, cannt get stoppos for stopDatetime : {0}'.format(stopDatetime) 
          cmd = "sudo {0}/bin/mysqlbinlog --start-pos={1} --stop-pos={2} ".format(self.installPath, offset, stopPos)
          cmd += " ".join(binlogsForPointInTimes)
          cmd += " | sudo {0}/bin/mysql --defaults-file={1} -u{2} -p{3}".format(
            self.installPath, self.configFile, self.user, self.passwd)

          Common.rdsLog.logInfo("Ready to apply mysql binlog: {0}".format(cmd))
          isSuccess = Util.execCommand(cmd)
        else:
          # 拼接Binlog文件，通过mysqlbinlog解析一次备份的所有binlog文件，
          # 通过管道传递给mysql进行回放,  多个binlog文件中间.以空格隔开即可
          Common.rdsLog.logInfo("Ready to apply binlogs :{0}".format(binlogFiles))
          cmd = "sudo {0}/bin/mysqlbinlog --start-pos={1} ".format(self.installPath, offset)
          cmd += " ".join(binlogFiles)
          cmd += " | sudo {0}/bin/mysql --defaults-file={1} -u{2} -p{3}".format(
            self.installPath, self.configFile, self.user, self.passwd)

          Common.rdsLog.logInfo("Ready to apply mysql binlog: {0}".format(cmd))
          isSuccess = Util.execCommand(cmd)

        if not isSuccess:
          Common.rdsLog.logInfo("apply mysql binlog failed : {0}".format(cmd))
          return False, "do apply binlog error, restore failed"

        Util.execCommand("sudo rm -rf {0}".format(tempDir))
    finally:
      self.connectionPool.execSql('set global sync_binlog={0}'.format(ovalBinlog))
      self.connectionPool.execSql('set global innodb_flush_log_at_trx_commit={0}'.format(ovalTrxCommit))
    return True, ""


  def restore(self, nameList, stopDatetime):
    """
     数据恢复:从存储平台上下载备份数据，通过innobackupex恢复全量部分，
     通过mysql回放恢复增量部分
    """
    isSuccess = False

    # xbstream & innobackupex都没有运行，则启动restore
    if Util.findProcess("xbstream") or Util.findProcess("innobackupex"):
      errInfo = "xbstream or innobackupex is running now, restore failed"
      return isSuccess, errInfo

    #新建恢复目录
    Util.execCommand("sudo rm -rf {0}".format(self.dataDir))
    Util.execCommand("sudo mkdir {0}".format(self.dataDir))
    Util.execCommand("sudo rm -rf {0}".format(self.tmpDir))
    Util.execCommand("sudo mkdir {0}".format(self.tmpDir))

    isSuccess, errInfo = self.restoreFullyPart(nameList.pop(0))
    # 如果innobackupex恢复过程没有成功，则直接返回恢复失败
    if not isSuccess:
      return isSuccess, errInfo

    # 启动数据库实例
    Common.unifyLog.logInfo("start database")
    if not self.start():
      return False, "start database error"

    if len(nameList) == 0:
      # 如果管理服务器只传了一个URL过来，则说明，不需要回放Binlog，直接返回即可
      return isSuccess, errInfo
    else:
      # 如果管理服务器传递了多个URL，则第一个URL中的数据是全量备份数据，其他URL中
      # 的数据是增量备份数据，增量备份部分，通过mysqlbinlog回放，任何一个binlog文件
      # 回放错误，都会返回失败
      Common.rdsLog.logInfo("Ready to apply mysql binlog, url is {0}".format(nameList))
      return self.restoreIncrementalPart(nameList, stopDatetime)




  def innerRestore(self, restorePath):
    """
     数据恢复，从给定路径下恢复数据到指定目录
    """
    if not Util.findProcess("innobackupex"):
      #self.createDir(self.dataDir)
      self.createDir(self.tmpDir)
      restoreDir = Util.getFirstDir(restorePath)
      if restoreDir == "":
        return False, "get backup dir error, restore failed"

      cmd = "innobackupex --apply-log --defaults-file={0} --user={1} --password={2} \
            {3} 2>{4}".format(self.configFile, self.user, self.passwd, restoreDir, self.restoreLog)
      if self.sudo:
        cmd = "sudo " + cmd
      if not Util.execCommand(cmd):
        return False, "apply log error, restore failed"

      isSuccess, errInfo = self.isRestoreOK()
      if not isSuccess:
        return False, errInfo

      # 重命名文件夹后change文件夹拥有者
      cmd = "mv {0} {1}".format(restoreDir, self.dataDir)
      Util.execCommand( cmd if not self.sudo else "sudo " + cmd )

      cmd = "chown -R mysql:mysql {0}".format(self.dataDir)
      Util.execCommand( cmd if not self.sudo else "sudo " + cmd )

      cmd = "chown -R mysql:mysql {0}".format(self.tmpDir)
      Util.execCommand( cmd if not self.sudo else "sudo " + cmd )
      return isSuccess, errInfo
    else:
      return False, "innobackupex is running, restore failed"


  def saveConfigInfo(self, content):
    """
     保存Manager发送过来的配置信息到配置文件，该操作完成后，更新了配置文件中的信息
    """
    configDir = Util.getFileDir(self.configFile)
    if not Util.isExists(configDir):
      cmd = "mkdir -p {0}".format(configDir)
      if self.sudo:
        cmd = "sudo " + cmd
      Util.execCommand(cmd)
      cmd = "chown -R rds-user:rds-user {0}".format(configDir)
      if self.sudo:
        cmd = "sudo " + cmd
      Util.execCommand(cmd)

    handle = open(self.configFile, "wb")
    handle.write(content)
    handle.close()
    cmd = "chmod 0644 {0}".format(self.configFile)
    if self.sudo:
      cmd = "sudo " + cmd
    Util.execCommand(cmd)
    self.dataDir = self.getDataDir()
    self.tmpDir  = self.getTmpDir()
    self.socket = self.getSocket()
    return True


  def isBackupOK(self):
    """
     检查备份日志，确认备份是否成功, 查找日志文件末尾是否有备份成功标志
     innobackupex: completed OK!
    """
    isSuccess = False
    fileName = errInfo = ""
    offset = -1
    # 备份日志不存在，无法确认备份是否成功
    if not Util.isExists(self.backupLog):
      errInfo = "backup log file not exist"
      return isSuccess, fileName, offset, errInfo

    rePosition = re.compile("position[\s]+([0-9]+)")
    reFileName = re.compile("filename[\s]+\'([0-9a-z-.]+)\'", re.IGNORECASE)
    reErrInfo  = re.compile("Error:([\s\S]+)", re.IGNORECASE)
    lines = Util.popenCommand("tail -10 {0}".format(self.backupLog)).readlines()
    for line in lines:
      info = reErrInfo.search(line)
      # 找到Error信息，backup时出现错误，直接跳出循环
      if info != None:
        errInfo = info.group(1)
        break
      # 找到备份成功信息'innobackupex: completed OK!'，说明备份成功
      if line.find("innobackupex: completed OK!") != -1:
        isSuccess = True
        break
      # 获取备份文件名
      info = reFileName.search(line)
      if info != None:
        fileName = info.group(1)
      # 获取position位置
      info = rePosition.search(line)
      if info != None:
        try:
          offset = long(info.group(1))
        except:
          offset = -1
    return isSuccess, fileName, offset, errInfo

  def isRestoreOK(self):
    isSuccess = False
    errInfo = ""
    # 检查恢复日志文件是否存在
    if not Util.isExists(self.restoreLog):
      errInfo = "restore log file not exist"
      return isSuccess, errInfo

    lines = Util.popenCommand("tail -10 {0}".format(self.restoreLog)).readlines()
    for line in lines:
      # restore成功
      if line.find("innobackupex: completed OK!") != -1:
        isSuccess = True
        break
      # restore错误
      if line.upper().find("ERROR:") != -1:
        isSuccess = True
        errInfo = line
        break
    return isSuccess, errInfo

  def getStatusValue(self, variable):
    """
     通过show status like 'variable\G'获取变量variable的状态，返回状态值
    """
    result = "NULL"
    if not self.isRunning():
      return result
    sql = "show status like \'{0}\'".format(variable)
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        result = row[1]
    return result.strip()

  def getGlobalStatusInfo(self):
    """
     获取当前server的select，insert，update等的次数
    """
    comSelect   = -1
    comInsert   = -1
    comUpdate   = -1
    comDelete   = -1
    comCommit   = -1
    comXaCommit = -1
    questions   = -1
    slowQueries = -1
    bufferPoolHits = -1
    threadsRunning = -1
    innodbFsyncs = -1
    groupRate = 1.
    if not self.isRunning():
      return comSelect, comInsert, comUpdate, comDelete, comCommit, comXaCommit, \
             questions, bufferPoolHits, slowQueries, threadsRunning, innodbFsyncs, groupRate
    sql = 'show global status'
    rows = self.connectionPool.execSql(sql)
    bufferPoolReads = 0
    bufferPoolRequests = 0
    innodbDataFsyncs = 0
    innodbLogFsyncs = 0
    commitNum = 0
    commitGrpNum = 0
    if rows != None:
      for row in rows:
        if row[0] == "Com_select":
          comSelect = long(row[1])
        elif row[0] == "Com_insert":
          comInsert = long(row[1])
        elif row[0] == "Com_update":
          comUpdate = long(row[1])
        elif row[0] == "Com_delete":
          comDelete = long(row[1])
        elif row[0] == "Com_commit":
          comCommit = long(row[1])
        elif row[0] == "Com_xa_commit":
          comXaCommit = long(row[1])
        elif row[0] == "Questions":
          questions = long(row[1])
        elif row[0] == "Slow_queries":
          slowQueries = long(row[1])
        elif row[0] == "innodb_buffer_pool_reads":
          bufferPoolReads = long(row[1])
        elif row[0] == "innodb_buffer_pool_read_requests":
          bufferPoolRequests = long(row[1])
        elif row[0] == "Threads_running":
          threadsRunning = long(row[1])
        elif row[0] == "Innodb_data_fsyncs":
          innodbDataFsyncs = long(row[1])
        elif row[0] == "Innodb_os_log_fsyncs":
          innodbLogFsyncs = long(row[1])
        elif row[0] == "Commit_num":
          commitNum = long(row[1])
        elif row[0] == "Commit_group_num":
          commitGrpNum = long(row[1])
      # 为了防止bufferPoolRequests为0导致除数为0，加0.0001
      bufferPoolHits =(1 - float(bufferPoolReads)/(bufferPoolRequests + 0.0001)) * 100
      groupRate = Util.precision(float(commitNum)/(commitGrpNum + 0.0001))
      innodbFsyncs = innodbDataFsyncs + innodbLogFsyncs
    return comSelect, comInsert, comUpdate, comDelete, comCommit, comXaCommit, questions, \
            Util.precision(bufferPoolHits), slowQueries, threadsRunning, innodbFsyncs, groupRate

  def getSecondsBehindMaster(self):
    """
     通过show slave status获取当前slave sql线程落后时间，如果为master则显示为0
    """
    seconds = 0
    if not self.isRunning():
      return -1
    sql = "show slave status"
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        if row[32] != None:
          seconds = long(row[32])
    return seconds

  def getMasterBlockTimeInVSR(self):
    """
     通过查询processlist表，获取等待时间
    """
    blockTime = 0
    if not self.isRunning():
      return -1
    sql = "select max(ACK_WAIT_TIME) from information_schema.PROCESSLIST;"
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        blockTime = long(row[0]) / 1000
    return blockTime


  def getSlaveStatus(self):
    """
     获取replication状态，返回延时时间和error信息
     1. 首先执行show slave status，如果返回为空，则当前主机可能是Master，否则，为slave
     2. 如果是Master，则修改test数据库下的heartbeat表，插入最新时间
     3. 如果是Slave，则先获取SecondBehindMaster的值, 然后尝试获取test数据库下的heartbeat
    """
    replDelayTime = 0
    replErrorInfo = " "

    if not self.isRunning():
      return replDelayTime, replErrorInfo
    sql = "show slave status;"
    rows = self.connectionPool.execSql(sql)
    # if this MySQL instance is slave
    if rows:
      for row in rows:
        # Seconds_Behind_Master
        if row[32] == None:
          replDelayTime = -1
        else:
          replDelayTime = long(row[32])
        # Last_IO_Error
        if row[35] != "":
          replErrorInfo = row[35]
        # Last_SQL_Error
        if row[37] != "":
          replErrorInfo = row[37]
      # 获取heartbeat表中的时间戳
      try:
        sql = "select time from test.heartbeat"
        rows = self.connectionPool.execSql(sql)
        if rows:
          replDelayTime = abs(time.time() - float(rows[0][0]))
      except:
        Common.rdsLog.logError('exec sql ({0}) error'.format(sql))
    else:
      # if this MySQL instance is master
      sql = "Select Host from information_schema.processlist where command like 'Binlog Dump'"
      # 如果当前没有slave连上master做复制，则不能确定当前MySQL是Master，有可能只是刚启动的slave
      # 因此，我们什么都不做，返回空即可，如果这是刚启动的slave，那么，这时候show slave status不会有任何结果，
      # 这时候向test数据库插入heartbeat表，将导致复制出错
      if not self.connectionPool.execSql(sql):
        return replDelayTime, replErrorInfo
      sql1 = "update test.heartbeat set time = {0}".format(time.time())
      sql1 += "where 'OFF' in (select VARIABLE_VALUE from information_schema.GLOBAL_VARIABLES where VARIABLE_NAME = 'READ_ONLY')"

      sql2 = "use test;CREATE TABLE `heartbeat` ( `time` decimal(20,2) DEFAULT NULL ) ENGINE=Innodb DEFAULT CHARSET=utf8;" + \
      " insert into test.heartbeat select 1; "
      try:
        # 尝试写test.heartbeat表，如果失败，有可能是该表不存在，则创建该表
        ret = self.connectionPool.execSql(sql1)
        if ret == None:
          Common.rdsLog.logInfo("Ready to run SQL {0}".format(sql2))
          self.connectionPool.execSql(sql2)
      except:
        Common.rdsLog.logError("exec sql ({0} / {1}) error".format(sql1, sql2))
    return replDelayTime, replErrorInfo

  def getSyncStatus(self):
    """
      获取master sync状态
    """
    syncStatus = self.getStatusValue("Rpl_semi_sync_master_status")
    return syncStatus


  def getLastExecPos(self):
    """
     获取当前slave执行的Relay log文件和执行位置
    """
    readMasterLogFile = ""
    readMasterLogPos  = -1
    execMasterLogFile = ""
    execMasterLogPos  = -1

    if not self.isRunning():
      return execMasterLogFile, execMasterLogPos

    sql = 'show slave status'
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        # Master_Log_File
        readMasterLogFile = row[5]
        # Read_Master_log_Pos
        readMasterLogPos = long(row[6])
        # Relay_Master_Log_File
        execMasterLogFile = row[9]
        # Exec_Master_Log_Pos
        execMasterLogPos = long(row[21])

    if readMasterLogFile != execMasterLogFile or readMasterLogPos < 0 or execMasterLogPos < 0:
      return execMasterLogFile, long(-1)
    return execMasterLogFile, execMasterLogPos

  def getSlaveBinlogOffset(self):
    """
     获取slave读取binlog的位置
    """
    readMasterLogFile = ""
    readMasterLogPos  = -1
    if not self.isRunning():
      return readMasterLogFile, readMasterLogPos
    sql = 'show slave status'
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        # Master_Log_File
        readMasterLogFile = row[5]
        # Exec_Master_Log_Pos
        readMasterLogPos = long(row[21])
    return readMasterLogFile, readMasterLogPos

  def getMasterBinlogOffset(self):
    """
     获取Master binlog信息
    """
    masterLogFile = ""
    masterLogPos  = -1
    if not self.isRunning():
      return masterLogFile, masterLogPos
    sql = "show master status"
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        masterLogFile = row[0]
        masterLogPos = long(row[1])
    return masterLogFile, masterLogPos

  def allRelayLogApplied(self):
    """
     所有的relay log全部应用完成，在主从切换时，要求slave上apply所有的relay log后
     才能切成master，返回True表示所有的relay log apply完成，False则没有完成
     检查方法：
     1. SQL线程是单线程执行的，如果并行复制没有开启，SQL线程负责读取Relay Log，然后执行每一条Event，如果读到Relay Log的末尾，
        则将状态置为："Slave has read all relay log; waiting for the slave I/O thread"。
	      如果开启并行复制，SQL Thread负责读取Relay Log，然后下发给对应的子线程执行，如果读到Relay Log的末尾，
        则状态也为"Slave has read all relay log; waiting for the slave I/O thread"。
      2.在并行复制关闭条件下，因为SQL是单线程执行的，如果SQL Thread状态为"Slave has read all relay log; waiting for the slave I/O thread"，
        则表示一定回放完毕。在并行复制开启条件下，SQL Thread为该状态，只能保证SQL Thread已经下发给对应的子线程队列中，
        不能保证这些Event已经执行。所以除了检查SQL Thread状态，还需要检查子线程队列的长度。子线程的执行过程是先从队列中全部获取Event，
        然后执行，执行结束，统一对队列进行减法计数，这样，如果队列全部为0，表示所有Event已经执行完毕。
    """
    Common.unifyLog.logInfo("start check sql thread status...")
    needWait = True
    while needWait:
      # 在判断所有的binlog已经回放完成之前，需要判断复制是否出错，如果复制出错，则
      # Relay Log 永远也回放不完，且这时候已经卡主，将导致服务不可用
      try:
        sql = 'show slave status'
        lastError = ""
        rows = self.connectionPool.execSql(sql)
        for row in rows:
          # Last_Error
          lastError = row[19]
        if lastError.strip() != "":
          Common.rdsLog.logInfo("Last_Error:{0}".format(lastError))
          # 如果检查到复制出错，还需要启动IO线程，避免卡主
          self.startIOThread()
          return False
      except Exception, e:
        Common.rdsLog.logInfo("Check replication status error in allRelayLogApplied: {0}".format(e))
        self.startIOThread()
        return False

      # 检查所有binlog写入relay log是否完成
      key="Slave has read all relay log; waiting for the slave I/O thread"
      sql="select State from information_schema.PROCESSLIST where State != ''"
      rows = self.connectionPool.execSql(sql)
      if rows != None:
        for row in rows:
          if row[0].find(key) != -1:
            needWait = False
            break
      Util.sleep(1)
    #判断MySQL版本
    Common.unifyLog.logInfo("execute select information_schema.processlist command and find sql thread has finish the task!start to check mysql version...")
    version = self.getVersion()
    Common.unifyLog.logInfo("get mysql version information %s"%version)
    #InnoSQL 5.5.20-v2-rds-log不支持并行复制功能
    if cmp(version,"5.5.20-v2-rds-log") == 0:
      return True
    else:
      while 1:
        #获取并行复制线程个数
        sql="show variables like \"%slave_parallel_threads%\""
        lines = self.connectionPool.execSql(sql)
        threadNum = "0"
        if lines != None:
          for line in lines:
            threadNum = string.strip(line[1])
            break
        #判断是否所有的SQL 子线程的队列长度全部为0
        sql="show slave sql_thread"
        lines = self.connectionPool.execSql(sql)
        i = 0
        if lines != None:
          for line in lines:
            if line[1] == 0:
              i=i+1
        #如果队列长度为0的SQL子线程个数与子线程总数相等，表明所有的子线程队列都是0，表现已完全回放完毕。
        Common.unifyLog.logInfo("find sub sql thread zero task queue  num is %d, sub sql thread num is %s"%(i,threadNum))
        if cmp(str(i),threadNum) == 0:
          return True
        #单位1秒
        Util.sleep(1)

  def changeAsync(self):
    """
     同步转异步
    """
    sql="SET GLOBAL rpl_semi_sync_master_keepsyncrepl=0;SET GLOBAL rpl_semi_sync_master_trysyncrepl=0;"
    return self.execSql(sql)

  def changeSync(self):
    """
     异步转同步，相对于同步转异步，需要等到切换到同步后才能返回
    """
    sql="SET GLOBAL rpl_semi_sync_master_keepsyncrepl=1;SET GLOBAL rpl_semi_sync_master_trysyncrepl=1;"
    if not self.execSql(sql):
      return False

    # 检查是否已经转成同步，转成同步后退出
    while "ON" != self.getStatusValue("rpl_semi_sync_master_status"):
      self.execSql("flush logs")
      Util.sleep(60)
    return True

  def cutBinlog(self, binlog, offset):
    """
     在进行master，slave切换时，由于可能出现master比slave上多出一段binlog的现象，切换时需要切除该段binlog
    """
    binlogFile = "{0}/{1}".format(self.dataDir, binlog)
    Util.syncOS()
    try:
      oldMode = Util.getFileMode(binlogFile)
      # 修改文件的属性
      cmd = "chmod 777 {0}".format(binlogFile)
      if self.sudo:
        cmd = "sudo " + cmd
      Util.execCommand(cmd)
      # 备份binlog文件
      Util.execCommand("sudo cp -f {0} {0}.bak".format(binlogFile))
      # truncate binlog文件
      f = open(binlogFile, "r+b")
      f.truncate(offset)
      f.close()
      # 把binlog文件的属性该回来
      cmd = "chmod {0} {1}".format(oldMode, binlogFile)
      if self.sudo:
        cmd = "sudo " + cmd
      Util.execCommand(cmd)
      Util.syncOS()
    except Exception,e:
      raise RDSException("截取binlog文件{0}异常:{1}".format(binlogFile, e))
    return True

  def getCombinedExistingRelayLogSize(self):
    relayLogSpace = "NULL"
    sql = "show slave status"
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        relayLogSpace = row[22]
    return relayLogSpace

  def resetSlave(self):
    sql = "reset slave all"
    return self.execSql(sql)

  def getSessions(self):
    """
     获取当前的连接数
    """
    if not self.isRunning():
      return 0
    rows = self.connectionPool.execSql('show processlist')
    if rows == None:
      return 0
    return len(rows) - 1

  def changeUserPasswd(self, user, passwd):
    sql = "SET PASSWORD FOR {0}=PASSWORD('{1}');FLUSH PRIVILEGES;".format(user, crypto.decrypt(passwd, "NeteaseR"))
    return self.execSql(sql)

  def execSql(self, sql):
    """
     执行SQL语句，返回True或者False
     注意：执行SQL语句，执行失败，返回None，执行成功，则返回执行的结果
     对于没有结果的SQL语句，返回的是[]
    """
    # 如果MySQL没有运行，返回False
    if not self.isRunning():
      return False

    return self.connectionPool.execSql(sql) != None

  def enableSemisync(self):
    sql = "set global rpl_semi_sync_master_enabled=on"
    ret = self.execSql(sql)
    sql = "set global rpl_semi_sync_slave_enabled=on"
    ret = ret and self.execSql(sql)
    sql = "set global rpl_semi_sync_master_commit_after_ack=on"
    return ret and self.execSql(sql)

  def getStatisticsOutputCycle(self):
    """
     获取topSQL输出间隔
    """
    output_cycle = 1
    sql = "show variables like 'statistics_output_cycle';"
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        output_cycle = long(row[1])
    return output_cycle

  def getSqlStats(self, startTime, endTime):
    """
     获取指定时间间隔内的sql stats
    """
    sqlstats = ()
    if endTime == 0xFFFFFFFF:
      sql = "show sql stats;"
      rows = self.connectionPool.execSql(sql)
      if rows != None:
        sqlstats = sqlstats + rows
    cycle = self.getStatisticsOutputCycle()
    startTime = startTime - cycle * 3600
    endTime   = endTime + cycle * 3600
    sql = "select sql_text,index_info,memory_temp_tables,disk_temp_tables,row_reads,byte_reads,max_exec_times,min_exec_times,exec_times,exec_count from mysql.sql_stats where start_time > {0} and end_time < {1}".format(startTime, endTime)
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      sqlstats = sqlstats + rows
    return sqlstats

  def getTableStats(self, startTime, endTime):
    """
     获取指定时间间隔内的table stats
    """
    tablestats = ()
    if endTime == 0xFFFFFFFF:
      sql = "show table stats;"
      rows = self.connectionPool.execSql(sql)
      if rows != None:
        tablestats = tablestats + rows
    cycle = self.getStatisticsOutputCycle()
    startTime = startTime - cycle * 3600
    endTime = endTime + cycle * 3600
    sql = "select dbname,table_name,select_count,update_count,insert_count,delete_count from mysql.table_stats where start_time > {0} and end_time < {1}".format(startTime, endTime)
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      tablestats = tablestats + rows
    return tablestats

  def getSlowLog(self, startTime, endTime):
    """
     获取指定时间间隔内的slow log
    """
    condition = "where unix_timestamp(start_time) between {0} and {1}".format(startTime, endTime)
    if startTime == 0 and endTime == 0xFFFFFFFF:
      condition = "limit 10"

    version = self.getVersion()
    if version.find('5.5.20') != -1:
      sql="select start_time, sql_text,user_host,query_time,lock_time,rows_sent,rows_examined,db,0,0 from mysql.slow_log {0}".format(condition)
    else:
      sql="select start_time, sql_text,user_host,query_time,lock_time,rows_sent,rows_examined,db,logical_reads,physical_reads from mysql.slow_log {0}".format(condition)
    return self.connectionPool.execSql(sql)

  def getVersion(self):
    """
     获取数据库的版本信息
    """
    version = ""
    sql="select version();"
    rows = self.connectionPool.execSql(sql)
    if rows != None:
      for row in rows:
        version = row[0]
        break
    return version

  def getBinlogSize(self):
    """
     获取MySQL数据库的Binlog大小, 单位为MB，保留2为小数
    """
    _1M = 1024 * 1024
    size = Util.getFilesSize(self.dataDir, "mysql-bin")
    return Util.B2M(size)

  def upgradeDBVersion(self):
    """
     数据库升级，对于MySQL来说，执行mysql_upgrade脚本
    """
    if self.isRunning():
      cmd = "{0}/bin/mysql_upgrade -f --socket={1} -u{2} -p{3}".format(self.installPath, self.socket, self.user, self.passwd)
      if self.sudo:
        cmd = "sudo " + cmd
      return Util.execCommand(cmd)
    else:
      return False


  def getBinlogFiles(self, binlog, directory=None):
    """
    返回比当前binlog文件更新的所有binlog文件
    """
    if not directory:
      directory = self.dataDir
    Common.rdsLog.logInfo('Ready to get binlog files list')

    binlogFile = "{0}/{1}".format(directory, binlog)
    if not os.path.isfile(binlogFile):
      Common.rdsLog.logError("binlog file({0}) does not exists".format(binlogFile))
      return []

    files = [ binlogFile ]
    nextBinlogFileSuffix = long(binlogFile.split('.')[1]) + 1
    nextBinlogFile = binlogFile.split('.')[0] + '.{0:0>6}'.format(
            nextBinlogFileSuffix)

    while os.path.isfile(nextBinlogFile):
      files.append(nextBinlogFile)
      binlogFile = nextBinlogFile
      nextBinlogFileSuffix = long(binlogFile.split('.')[1]) + 1
      nextBinlogFile = binlogFile.split('.')[0] + '.{0:0>6}'.format(
              nextBinlogFileSuffix)
    return files


  def backupBinlogForFlashback(self, binlog, offset):
    '''
    备份所有比当前binlog更新的binlog文件名和offset到指定文件夹下
    '''
    # 获取所有需要blashback的binlog文件
    Common.rdsLog.logInfo('Ready to backup binlog files')
    files = self.getBinlogFiles(binlog)
    if not files:
      Common.unifyLog.logError("fail to backup binlog for flash back")
      return False

    #备份offset
    flashbackFile="{0}/flashback{1}.txt".format(self.dataDir, Util.getCurrentTime())
    Util.execCommand("echo {0} | sudo tee {1} >/dev/null".format(offset,
                flashbackFile))

    #备份binlog的文件名
    for f in files:
      if not Util.execCommand("echo {0} | sudo tee -a {1} >/dev/null".format(f, flashbackFile)):
        return False
    Util.syncOS()
    return True



  def flashBackDBInstance(self, binlog, offset):
    """
    flash back db instance
    """
    #判断flash back这个工具是否存在
    flashback='/usr/local/bin/split_binlog_for_rds'
    tmpSql='{0}/flashback.sql'.format(self.dataDir)
    if not Util.execCommand(flashback + " --help"):
      Common.rdsLog.logError(flashback + ' is not found')
      return False

    #备份所有的binlog，保存offset
    if not self.backupBinlogForFlashback(binlog, offset):
      return False

    # 获取所有需要blashback的binlog文件
    files = self.getBinlogFiles(binlog)

    #从后向前Flash
    files.reverse()

    cmd = "show variables like '%log_bin%'"
    rows = self.connectionPool.execSql(cmd)
    Common.rdsLog.logInfo("the result of {0} is :{1}".format(cmd, rows))

    ovalBinlog = self.connectionPool.execSql("show variables like 'sync_binlog'")[0][1]
    ovalTrxCommit = self.connectionPool.execSql("show variables like 'innodb_flush_log_at_trx_commit'")[0][1]
    Common.rdsLog.logInfo('[before flashback]sync_binlog = {0}'.format(ovalBinlog))
    Common.rdsLog.logInfo('innodb_flush_log_at_trx_commit = {0}'.format(ovalTrxCommit))

    self.connectionPool.execSql('set global sync_binlog=0')
    self.connectionPool.execSql('set global innodb_flush_log_at_trx_commit=0')

    try:
      currOffset = 0
      for f in files:
        Common.rdsLog.logInfo("Flash back for {0}".format(f))
        if Util.pathSplit(f)[1] == binlog:
          currOffset = offset
        size = Util.getFileSize(f) - offset
        freeMemSize = Util.getFreeMem()

        #1. 不用split
        if size * 5 < freeMemSize:
          Common.rdsLog.logInfo("Ready to falshback without split")
          cmd = 'sudo {0} -B --start-position={1} {2}'.format( flashback, currOffset, f)

          #首先将解析出的sql语句保存到一个临时文件中
          cmd = cmd + ' | sudo tee {0} >/dev/null'.format(tmpSql)
          Common.rdsLog.logDebug("Ready to run {0}".format(cmd))
          if not Util.execCommand(cmd):
            Common.rdsLog.logError("Run {0} error".format(cmd))
            return False

          Util.execCommand('sudo chmod 777 {0}'.format(tmpSql))
          cmd = "{0}/bin/mysql -u{1} -p{2} -S{3} < {4}".format(self.installPath, self.user, self.passwd, self.socket, tmpSql)
          Common.rdsLog.logInfo("Ready to run {0}".format(cmd))
          if not Util.execCommand(cmd):
            Common.rdsLog.logError("(without split)flash back error when"
                    " exec sql, Is there has DDL statument")
            return False
          continue

        #2. binlog文件比较大，需要进行split
        cmd = 'sudo {0} --split-size-interval={1} --start-position={2} {3} | grep start'.format(flashback, RDSConfig.flashBackSplitSize, currOffset, f)
        Common.rdsLog.logInfo("Ready to split binlog for flash back: {0}".format(cmd))

        lines = Util.popenCommand(cmd).readlines()
        Common.rdsLog.logInfo("lines is :{0}".format(lines))
        segments = []

        for line in lines:
          if line.find('start') != -1:
            segments.append(line.split()[5])

        #从后向前flash back
        segments.reverse()
        # deal with special condition for first binlog
        if segments and currOffset == offset and long(segments[-1]) > currOffset:
          segments.append(currOffset)

        Common.rdsLog.logInfo("reverse segments :{0}".format(segments))
        stop = -1
        for start in segments:
          if stop == -1:
            cmd = 'sudo {0} -B --start-position={1} {2}'.format(flashback, start, f)
          else:
            cmd = 'sudo {0} -B --start-position={1} --stop-position={2} {3}'.format(flashback, start, stop, f)
          stop = start

          #首先将解析出的sql语句保存到一个临时文件中
          cmd = cmd + ' | sudo tee {0} >/dev/null'.format(tmpSql)
          if not Util.execCommand(cmd):
            Common.rdsLog.logError( cmd + "error")
            return False
          Util.execCommand('sudo chmod 777 {0}'.format(tmpSql))

          cmd = "{0}/bin/mysql -u{1} -p{2} -S{3} < {4}".format(self.installPath, self.user, self.passwd, self.socket, tmpSql)
          Common.rdsLog.logInfo("Ready to run {0}".format(cmd))
          if not Util.execCommand(cmd):
            Common.rdsLog.logError("(with split)flash back error when exec sql,"
                    " Is there has DDL statument")
            Common.rdsLog.logError("binlogfile: {0}, start pos: {1}, stop pos:"
                    " {2}".format(f, start, stop ))
            return False
      return True
    finally:
      self.connectionPool.execSql('set global sync_binlog={0}'.format(ovalBinlog))
      self.connectionPool.execSql('set global innodb_flush_log_at_trx_commit={0}'.format(ovalTrxCommit))
      Util.syncOS()

      valBinlog = self.connectionPool.execSql("show variables like 'sync_binlog'")[0][1]
      valTrxCommit = self.connectionPool.execSql("show variables like 'innodb_flush_log_at_trx_commit'")[0][1]
      Common.rdsLog.logInfo('[after flashback]sync_binlog = {0}'.format(valBinlog))
      Common.rdsLog.logInfo('innodb_flush_log_at_trx_commit = {0}'.format(valTrxCommit))



  def killConnection(self):
    """
    用于只读从节点计划内的主从切换，本函数的功能是kill掉所有的连接
    kill连接的shell语句如下所示：

    mysqladmin -uuser -ppassword processlist -S/tmp/mysql.sock | awk -F'|' '$3 !~ "replicaUser" && $3 !~ "User"{ print $2}' | sed '/^$/d' | xargs -n 1 mysqladmin -uuser -ppassword -S/tmp/mysql.sock kill
    """
    statement = "{0}/bin/mysqladmin --defaults-file={1} -u{2} -p{3} ".format(
          self.installPath, self.configFile, self.user, self.passwd)

    cmd = statement + " processlist"
    cmd += "| awk -F'|'  '$3 !~ \"replicaUser\" && $3 !~ \"User\"{ print $2}'"
    cmd += " | sed '/^$/d' | xargs -n 1 "
    cmd += statement + ' kill'

    Common.rdsLog.logInfo("Ready to kill mysql connection: {0}".format(cmd))
    Util.execCommand(cmd)



  def getRelayLogSize(self):
    """
     获取未被执行的RelayLog Size
    """
    readMasterLogPos  = -1
    execMasterLogPos  = -1
    MasterLogFile = ""
    RelayMasterLogFile = ""


    if not self.isRunning():
      return 0

    sql = 'show slave status'
    rows = self.connectionPool.execSql(sql)

    if rows == None:
        return 'NULL'

    for row in rows:
      # Master_Log_File
      MasterLogFile = row[5]
      # Read_Master_log_Pos
      readMasterLogPos = long(row[6])
      # Relay_Master_Log_File
      RelayMasterLogFile = row[9]
      # Exec_Master_Log_Pos
      execMasterLogPos = long(row[21])

    if MasterLogFile != RelayMasterLogFile:
      ret = RDSConfig.maxRelayLogSize# max int value
    else:
      ret = readMasterLogPos - execMasterLogPos
    Common.rdsLog.logInfo("Master_Log_File: {0}  Exec_Master_log_Pos:{1}".format(MasterLogFile, execMasterLogPos))
    Common.rdsLog.logInfo("Relay_Master_Log_File: {0}  Read_Master_Log_Pos:{1}".format(RelayMasterLogFile, readMasterLogPos))
    return ret
    
  def dumpExternalDatabases(self, params, storeDir):
    """
     导出外部实例数据到指定路径
    """
    binlog = ""
    offset = -1
    retHost = None
    retPort = None
    errInfo = ""

    host = params["HostIp"]
    port = long(params["Port"])
    user = params["UserName"]
    passwd = params["Password"]
    databases = params["Databases"]
    threads = long(params.get("Threads", "4"))
    migrateGrants = long(params.get("MigrateGrants", 0))
    monitorValue = params.get("MonitorValues")
    rowsPerdump = long(params.get("RowsPerdump"))
    lockTimeout = long(params.get("LockTimeout", 0))
    migrateType = long(params.get("MigrateType"))

    Common.rdsLog.logInfo("params which recv from manager is {0}".format(params))
    Util.execCommand("sudo chmod -R 777 {0}".format(storeDir))
    #准备mysqldump 命令
    dblist = databases.split(';')
    # 注意:末尾有个空格
    mysqldumpCmd = "{0}/bin/mysqldump -d -u{1} -p{2} -h{3} -P{4} -R --single-transaction --databases ".format(
              self.installPath, user, crypto.decrypt(passwd, "NeteaseR"), host, port)

    mysqldumpCmd = mysqldumpCmd + " ".join(dblist)
    mysqldumpCmd = mysqldumpCmd + " > {0}/{1}".format(storeDir, Mysql.DUMP_SCHEMA)

    # 准备pt-show-grants命令
    ptShowGrantsCmd = "/usr/local/bin/pt-show-grants -h {0} -u {1} -p {2} -P {3}".format(host,
                          user, crypto.decrypt(passwd, "NeteaseR"), port)
    ptShowGrantsCmd = ptShowGrantsCmd + " > {0}/{1}".format(storeDir, Mysql.DUMP_GRANTS)

    # 准备mydumper命令
    dumpLog = "{0}/dump.log".format(RDSConfig.logDir)

    mydumperCmd  = "sudo mydumper -u {0} -p {1} -h {2} -m -P {3} -L {4} -v 3 -r {5} -t {6}".format(
                    user, crypto.decrypt(passwd, "NeteaseR"), host, port, dumpLog, rowsPerdump, threads)
    if lockTimeout != 0:
      mydumperCmd = mydumperCmd + " -W {0}".format(lockTimeout)

    if monitorValue != 0 and str(monitorValue).strip().lower() != 'null':
      mydumperCmd = mydumperCmd + " -M '{0}'".format(monitorValue)

    mydumperCmd = mydumperCmd + " -d '{0}' -o {1}/{2}".format(";".join(dblist), storeDir, Mysql.DUMP_DIRECTORY)

    # 查找mysqldump进程是否存在
    if Util.findProcess("mysqldump"):
      Common.rdsLog.logInfo("process mysqldump is exist, kill it first")
      Util.killProcess("mysqldump")


    # 开始dump数据库结构
    mysqldumpCmd = "sudo sh -c '{0}'".format(mysqldumpCmd)
    Common.rdsLog.logInfo("ready to run {0} to dump schema".format(mysqldumpCmd))
    if not Util.execCommand(mysqldumpCmd):
      errInfo = "execute cmd [{0}] error".format(mysqldumpCmd)
      return False, binlog, offset, retHost, retPort, errInfo

    if migrateGrants:
      # 查找pt-show-grant进程是否存在
      if Util.findProcess("pt-show-grants"):
        Common.rdsLog.logInfo("process pt-show-grants is exist, kill it first")
        return False, binlog, offset, retHost, retPort, errInfo

      # 开始dump权限信息
      Common.rdsLog.logInfo("ready to run {0} to dump grants".format(ptShowGrantsCmd))
      if not Util.execCommand(ptShowGrantsCmd):
        errInfo = "execute cmd [{0}] error".format(ptShowGrantsCmd)
        return False, binlog, offset, retHost, retPort, errInfo

    # 查找mydumper进程是否存在
    if Util.findProcess("mydumper"):
      Common.rdsLog.logInfo("process mydumper is exist, kill it first")
      Util.killProcess("mydumper")


    cmd = "{0}/bin/mysql -u{1} -p{2} -h {3} -P {4} -e \"show variables like 'innodb_old_blocks_time'\"".format(
      self.installPath, user, crypto.decrypt(passwd, "NeteaseR"), host, port)

    # 执行这条语句返回的结果如：['Variable_name\tValue\n', 'innodb_old_blocks_time\t0\n']
    oldInnodbOldBlocksTime = Util.popenCommand(cmd).readlines()
    Common.rdsLog.logInfo("the result for cmd : {0} is : {1}".format(cmd, oldInnodbOldBlocksTime))

    try:
      oldInnodbOldBlocksTime = oldInnodbOldBlocksTime[1].strip().split('\t')[1]
      cmd = "{0}/bin/mysql -u{1} -p{2} -h {3} -P {4} -e \"set global innodb_old_blocks_time=1000\"".format(
      self.installPath, user, crypto.decrypt(passwd, "NeteaseR"), host, port)
      Util.execCommand(cmd)
      # 开始dump数据
      Common.rdsLog.logInfo("ready to run {0} to dump data".format(mydumperCmd))
      if not Util.execCommand(mydumperCmd):
        errInfo = "execute cmd [{0}] error".format(mydumperCmd)
        return False, binlog, offset, retHost, retPort, errInfo

      return self.isDumpExternalOK(dumpLog, storeDir, migrateType)
    except Exception, e:
      Common.rdsLog.logError('error : {0}'.format(e))
      return False, binlog, offset, retHost, retPort, 'error : {0}'.format(e)
    finally:
      cmd = "{0}/bin/mysql -u{1} -p{2} -h {3} -P {4} -e 'set global innodb_old_blocks_time = {5}'".format(
        self.installPath, user, crypto.decrypt(passwd, "NeteaseR"), host, port, oldInnodbOldBlocksTime)
      if not Util.execCommand(cmd):
        Common.rdsLog.logError("exec cmd {0} error".format(cmd))

  def isDumpExternalOK(self, dumpLog, storeDir, migrateType):
    """
     检查dump是否成功
    """
    binlog  = ""
    offset  = 0
    retHost = None
    retPort = None
    errInfo = ""
    # 检查日志文件，是否有'CRITICAL **:'标识的错误产生
    with open("{0}".format(dumpLog)) as f:
      for lines in f:
        if lines.find("CRITICAL **:") != -1:
          errInfo = lines
          break
    if errInfo != "":
      return False, binlog, offset, retHost, retPort, errInfo

    if migrateType == 1:
      return True, binlog, offset, retHost, retPort, errInfo

    # 从metadata文件获取binlog和offset信息

    metadata = "{0}/{1}/metadata".format(storeDir, Mysql.DUMP_DIRECTORY)
    Util.execCommand("sudo chmod -R 777 {0}".format(storeDir))
    # the content of metadata is:
    #   SHOW MASTER STATUS:
    #     Log: mysql-bin.000040
    #     Pos: 408
    #
    #   SHOW SLAVE STATUS:
    #     Host:10.184.145.50
    #     Log: mysql-bin.00025
    #     Pos:16808
    #     Port:3306
    context = []

    isBegin = False
    with open("{0}".format(metadata)) as f:
      for line in f:
        if migrateType == 2 and line.find('SHOW MASTER STATUS:') != -1:
          isBegin = True
        elif migrateType == 3 and line.find('SHOW SLAVE STATUS:') != -1:
          isBegin = True

        if isBegin:
          context.append(line)
          if migrateType == 2 and len(context) == 3:
            break
          elif migrateType == 3 and len(context) == 5:
            break

    for line in context:
      if line.find("Log:") != -1:
        binlog = line.split(':')[1].strip()
      elif line.find("Pos:") != -1:
        offset = line.split(':')[1].strip()
      elif line.find("Host:") != -1:
        retHost = line.split(':')[1].strip()
      elif line.find("Port:") != -1:
        retPort = line.split(':')[1].strip()
    return True, binlog, offset, retHost, retPort, errInfo


  def loadExternalDatabases(self, threads, dbs, compressKey, compressTables, migrateGrants, storeDir):
    """
     导入数据到RDS实例
    """
    # 导入数据时，为了加快导入速度，把sync_binlog和innodb_flush_log_at_trx_commit都设置成0
    oldSyncBinlog = self.connectionPool.execSql("show variables like 'sync_binlog'")[0][1]
    oldTrxCommit  = self.connectionPool.execSql("show variables like 'innodb_flush_log_at_trx_commit'")[0][1]
    oldLongQueryTime = self.connectionPool.execSql("show variables like 'log_slow_queries'")[0][1]

    Common.rdsLog.logInfo("set sync_binlog = 0 and innodb_flush_log_at_trx_commit = 0")
    self.execSql("set global sync_binlog = 0")
    self.execSql("set global innodb_flush_log_at_trx_commit = 0")
    self.execSql("set global log_slow_queries = off")

    try:
      # 查看mysql source操作是否存在，如果存在直接返回
      if Util.findProcess("mysql",["source"]):
         Common.rdsLog.logInfo("mysql--source--process is exist, Ready to kill it.")
         Util.killProcess("mysql",["source"])

      # 执行mysql source导入数据库结构
      cmd = "sudo {0}/bin/mysql -u{1} -p{2} --socket={3} -e \"source {4}/{5}\"".format(self.installPath,
          self.user, self.passwd, self.socket, storeDir, Mysql.DUMP_SCHEMA)
      if not Util.execCommand(cmd):
        return False, "load schema error"

      # 准备导入权限
      if migrateGrants:
        cmd = "sudo {0}/bin/mysql -u{1} -p{2} --socket={3} -e \"source {4}/{5}\"".format(self.installPath,
            self.user, self.passwd, self.socket, storeDir, Mysql.DUMP_GRANTS)
        if not Util.execCommand(cmd):
          return False, "load grants error"

      # 准备loader命令
      loadLog = "{0}/load.log".format(RDSConfig.logDir)
      myloader = "sudo sh -c 'myloader -u {0} -p {1} --socket={2} -t {3} -v 3 -d {4}/{5} > {6} 2>&1'".format(self.user,
                  self.passwd, self.socket, threads, storeDir, Mysql.DUMP_DIRECTORY, loadLog)

      # 查找myloader进程是否存在，如果存在则有可能是Agent在load的过程中宕机重启
      # 先kill进程，然后truncate已load的表，再开始load
      if Util.findProcess("myloader", [Mysql.DUMP_DIRECTORY]):
        Common.rdsLog.logInfo("myloader is running, kill it first")
        cmd = "ps axuf | grep -v grep | grep myloader | awk '{print $2}' | xargs sudo kill -9 "
        if not Util.execCommand(cmd):
          return False, "kill myloader failed"


      # 如果load.log存在，则有可能是在load的过程中，Agent宕机重做的情况
      # 为了顺利重做，需要先将已经load成功的表truncate掉
      #load.log中的日志如下所示：
      #** Message: 1 threads created
      #** Message: Thread 1 restoring `myadmin`.`person` part 0
      #** Message: Thread 1 restoring `myadmin`.`sbtest` part 0
      #** Message: Thread 1 restoring `myadmin`.`sbtest` part 1
      #** Message: Thread 1 shutting down
      if Util.isExists(loadLog):
        try:
          with open(loadLog) as f:
            old = ""
            for line in f:
              if line.find('restoring') != -1 and line.find('** Message: Thread') != -1:
                Common.rdsLog.logDebug("restore load, parse message for {0}".format(line))
                start = line.find('`')
                end = line.rfind('`') + 1
                sql = 'truncate {0}'.format(line[start:end])
                if old != sql:
                  old = sql
                  Common.rdsLog.logInfo("Ready to exec sql {0}".format(sql))
                  ret = self.connectionPool.execSql(sql)
                  if ret == None:
                    return False, "exec sql {0} error".format(sql)
                  Common.rdsLog.logInfo("exec sql {0} success".format(sql))
        except Exception,e:
          Common.rdsLog.logError('restore load error: {0}'.format(e))
          return False, 'restore load error'

      # 将表修改为压缩表
      if compressKey:
        if len(compressTables):
          for tableName in compressTables.split(';'):
            sql = 'ALTER TABLE {0} ENGINE=INNODB ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE={1};'.format(tableName, compressKey)
            Common.rdsLog.logInfo("Ready to exec sql : {0}".format(sql))
            ret = self.connectionPool.execSql(sql)
            if ret == None:
              return False, "exec sql {0} error".format(sql)
        else:
          for schema in dbs.split(';'):
            sql = 'SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = "{0}" and ENGINE = "InnoDB"'.format(schema)
            rows = self.connectionPool.execSql(sql)
            # 执行SQL语句获取的结果示例：(('person',), ('person1',), ('person2',))
            for row in rows:
              tableName = row[0]
              sql = 'use {0}; ALTER TABLE {1} ENGINE=INNODB ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE={2};'.format(schema, tableName, compressKey)
              Common.rdsLog.logInfo("Ready to exec sql : {0}".format(sql))
              ret = self.connectionPool.execSql(sql)
              if ret == None:
                return False, "exec sql {0} error".format(sql)

      # 开始loader
      if not Util.execCommand(myloader):
        return False, "loader error"

    finally:
      self.execSql("set global sync_binlog = {0}".format(oldSyncBinlog))
      self.execSql("set global innodb_flush_log_at_trx_commit = {0}".format(oldTrxCommit))
      self.execSql("set global log_slow_queries = {0}".format(oldLongQueryTime))
    return True, ""

  def getServiceStatus(self, op):
    """
    通过执行SQL语句的方式，来判断实例的服务状态
    """
    op = str(op)
    # for master
    if op == 'Update':
      sql = "update test.heartbeat set time = {0} ".format(time.time())
      sql += "where 'OFF' in (select VARIABLE_VALUE from information_schema.GLOBAL_VARIABLES where VARIABLE_NAME = 'READ_ONLY')"
    # for slave
    elif op == 'Select':
      sql = "select time from test.heartbeat"
    # for readreplica
    else:
      sql = "select 1"

    if not self.isRunning():
      return False, "MySQL is not running"

    try:
      Common.rdsLog.logDebug("Ready to run SQL {0}".format(sql))
      ret = self.connectionPool.execSql(sql)
      if ret == None:
        return False, "exec sql {0} error".format(sql)
    except:
        Common.rdsLog.logError("exec sql {0} error".format(sql))
        return False, "catch exception when exec sql {0} ".format(sql)
    return True, ""

  def modifyDBParameters(self, params):
    try:
      # 修改配置文件的权限
      cmd = "sudo chmod 666 {0}".format(self.configFile)
      Util.execCommand(cmd)
	  
      # 读取my.cnf
      parser = SafeConfigParser(allow_no_value=True)
      parser.read(self.configFile)
      # 读取管理服务器要修改的值
      parameters = Util.json2Obj(params)
      for key, val in parameters.items():
        sql = " set global {0} = {1}".format(key, val)
        ret = self.connectionPool.execSql(sql)
        if ret == None:
          # 为了避免val中有MySQL内置关键字，导致失败， 需要加``号再重新设置一遍
          sql1 = " set global {0} = `{1}`".format(key, val)
          ret1 = self.connectionPool.execSql(sql1)
          if ret1 == None:
            return False, "got error when exec sql {0}".format(sql1)
        parser.set("mysqld", key, val)

      # 持久化修改
      parser.write(open(self.configFile, 'w'))

      cmd = "sudo chmod 644 {0}".format(self.configFile)
      Util.execCommand(cmd)
    except Exception, e:
      return False, str(e)
    return True, ""

  def executeSQL(self, sql):
    rows = self.connectionPool.execSql(sql)
    if rows is None:
      Common.rdsLog.logError("exec sql [{0}] error".format(sql))
      return False, "exec sql [{0}] error".format(sql)
    else:
      Common.rdsLog.logInfo("exec sql [{0}] success".format(sql))
      return True, str(rows)

  def dumpGrants(self, bucketName, keyName):

    isSuccess = False
    errInfo = ""
    # 如果mysql没有运行，直接返回
    if not self.isRunning():
      errInfo="database is not running"
      return isSuccess, errInfo

    # 创建桶失败，则直接返回错误
    if self.storageManager.makeBucket(bucketName) == False:
      errInfo   = "can not create bucket {0}".format(bucketName)
      return isSuccess, errInfo

    # 准备pt-show-grants命令
    tmpFile = "{0}/{1}".format(RDSConfig.logDir, Mysql.DUMP_GRANTS)
    ptShowGrantsCmd = "/usr/local/bin/pt-show-grants -u {0} -p {1} -S{2} > {3}".format(self.user, self.passwd, self.socket, tmpFile)

    Common.rdsLog.logInfo("ready to run {0} to dump grants".format(ptShowGrantsCmd))
    if not Util.execCommand(ptShowGrantsCmd):
      errInfo = "execute cmd [{0}] error".format(ptShowGrantsCmd)
      return isSuccess, errInfo

    if not self.storageManager.putFile(bucketName, keyName, tmpFile):
      errInfo = "upload file({0}) fail".format(tmpFile)
      return isSuccess, errInfo

    Util.execCommand("sudo rm -rf {0}".format(tmpFile))

    # 检查备份文件是否存在于NOS上
    if not self.storageManager.lookupFile(bucketName, keyName):
      errInfo = "backup data upload failed"
      return isSuccess, errInfo

    return True, errInfo


  def loadGrants(self, bucketName, keyName):

    isSuccess = False
    errInfo = ""

    # 如果mysql没有运行，直接返回
    if not self.isRunning():
      errInfo="database is not running"
      return isSuccess, errInfo

    tmpFile = "{0}/{1}".format(RDSConfig.logDir, Mysql.DUMP_GRANTS)

    # 从NOS上下载权限信息
    if not self.storageManager.getFile(bucketName, keyName, tmpFile):
      return False, "cant get grants from nos error {0}:{1}".format(bucketName, keyName)

    # 在导入权限信息之前，需要对权限进行一些特殊处理，如repliUser
    if not Util.execCommand("sudo sed -i '/replicaUser/d' {0}".format(tmpFile)):
      Common.rdsLog.logInfo("delete replicaUser in grants.sql error")
      return False, "delete replicaUser in grants.sql error"

    # 由于InnoSQL的bug，删除权限信息中的PROFILE_ACL，否则，会导致20升级30的实例，迁移权限出错
    if ( not Util.execCommand("sudo sed -i '/PROFILE_ACL ON/d' {0}".format(tmpFile))
        or not Util.execCommand("sudo sed -i 's/PROFILE_ACL,/ /g' {0}".format(tmpFile))):
      return False, "delete PROFILE_ACL in grants.sql error"

    # 对于从20升级到30的实例，如果覆盖rdsadmin用户也会报错，因此，统一不覆盖rdsadmin
    if not Util.execCommand("sudo sed -i '/rdsadmin/d' {0}".format(tmpFile)):
      return False, "delete rdsadmin in grants.sql error"

    # 导入权限信息
    cmd = "sudo {0}/bin/mysql -u{1} -p{2} --socket={3} -e \"source {4}\"".format(self.installPath,
            self.user, self.passwd, self.socket, tmpFile)
    if not Util.execCommand(cmd):
      errInfo = "load grants error"
      return isSuccess, errInfo

    # 删除临时文件
    Util.execCommand("sudo rm -rf {0}".format(tmpFile))
    return True, errInfo
