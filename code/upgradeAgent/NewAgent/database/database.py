# -*- coding:UTF-8 -*-
"""
@author:  hzjianghongxiang
@date:    2013-12-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:    数据库基础类，用于定义数据库的接口
@modify:
"""

from util import Util

class Database:
  def __init__(self, installPath, configFile, backupDir, storageManager):
    self.installPath = installPath
    self.configFile  = configFile
    self.backupDir   = backupDir
    if not Util.isExists(backupDir):
      Util.execCommand("mkdir -p {0}".format(backupDir))
    self.backupLog   = "{0}/backup.log".format(backupDir)
    self.restoreLog  = "{0}/restore.log".format(backupDir)
    self.storageManager = storageManager

  def install(self):
    return True

  def isRunning(self):
    return True

  def isAlive(self):
    return True

  def start(self, first = False):
    return True

  def shutdown(self, type=0):
    """
     关闭数据库，type=0则采用kill -9方式关闭，type=1采用正常方式关闭
    """
    return True

  def saveConfigInfo(self, content):
    return True

  def getErrorLog(self, startRow, endRow):
    return 0,""

  def setReadOnly(self, readonly = True):
    return True

  def stopSlave(self):
    return True

  def startSlave(self):
    return True

  def setupSyncSemiRepl(self, install=True):
    return True

  def startReplication(self):
    return True

  def stopReplication(self):
    return True

  def stopIOThread(self):
    return True

  def getMasterLogInfo(self):
    return "log-bin.000001", 100

  def allRelayLogApplied(self):
    return True

  def getSlaveBinlogOffset(self):
    return "log-bin.000001",100

  def getMasterBinlogOffset(self):
    return "log-bin.000001",100

  def changeMaster(self, host, port, user, passwd, logFile, logPos):
    return True

  def deleteUser(self, userName='root'):
    return True

  def createDatabase(self, dbName):
    return True

  def createUser(self, user, password, type=0):
    return True

  def createRDSUser(self):
    return True

  def backup(self, bucketName, keyName, startBinlog, startPosition):
    return True, "backupFile", 100, 10000, "","",0,0,0

  def innerBackup(self, backupPath):
    return True, "backupFile", 100, 10000, ""
  
  def restore(self, nameList):
    return True, ""
  
  def innerRestore(self, restorePath):
    return True, ""
  
  def getMasterBlockTimeInVSR(self):
    return 0

  def getSlaveStatus(self):
    return 0, ""

  def getSyncStatus(self):
    return "ON"

  def changeAsync(self):
    return True

  def changeSync(self):
    return True

  def cutBinlog(self, binlog, offset):
    pass

  def getSessions(self):
    return 1

  def changeUserPasswd(self, user, passwd):
    return True

  def getGlobalStatusInfo(self):
    return 1,1,1,1,1,1

  def getSqlStats(self, startTime, endTime):
    return []

  def getTableStats(self, startTime, endTime):
    return []

  def getSlowLog(self, startTime, endTime):
    return []

  def getVersion(self):
    return ""

  def getSecondsBehindMaster(self):
    return 0
  
  def getCombinedExistingRelayLogSize(self):
    return 0
  
  def getBinlogSize(self):
    return ""
  
  def upgradeDBVersion(self):
    return True

  def falshBackDBInstance(self, binlog, offset):
    return True

  def getBinlogFiles(self, binlog):
    return False

  def backupBinlogForFlashback(self, binlog, offset):
    return False

  def killConnection(self):
    return False

  def getRelayLogSize(self):
    return False

  def dumpExternalDatabases(self, params, storeDir):
    return True, "", "", ""

  def getServiceStatus(self, op):
    return True, ""

  def loadExternalDatabases(self, threads, dbs, compressKey, compressTables, migrateGrants, storeDir):
    return True, ""

  def modifyDBParameters(self, params):
    return True, ""

  def executeSQL(self, sql):
    return True, ""

  def dumpGrants(self, bucketName, keyName):
    return True, ""

  def loadGrants(self, bucketName, keyName):
    return True, ""
