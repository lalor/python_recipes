# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-03
@contact: jhx1008@gmail.com
version:  1.0
@todo:    存储定义基类，用于继承
@modify:
"""

from util import Util

class Storage:
  """
   存储基类
  """
  def __init__(self, file):
    self.fileDir = Util.getFileDir(file)
  
  def lookup(self, bucketName):
    return 1

  def deleteBucket(self, bucketName):
    return True

  def makeBucket(self, buckName):
    return True

  def putString(self, str, bucketName, keyName):
    return True

  def putStringByPipe(self, cmd, bucketName, keyName):
    return True

  def getString(self, bucketName, keyName):
    return "abcd"

  def getStringByPipe(self, cmd, bucketName, keyName):
    return True

  def deleteFile(self, bucketName, keyName):
    return True
  
  def putFile(self, bucketName, keyName, fileName):
    return True

  def getFile(self, bucketName, keyName, fileName):
    return True

  def lookupFile(self, bucketName, keyName):
    return True

  def getFileSize(self, bucketName, keyName):
    return 0
