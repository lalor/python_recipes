# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-03
@contact: jhx1008@gmail.com
version:  1.0
@todo:    nos存储管理类，定义了nos的相关操作
@modify:
"""

from util import Util
from storage.storage import Storage

class Nos(Storage):
  """
   NOS存储类
  """
  
  NOS_RETURN_POS_1 = "1"
  NOS_RETURN_NEG_1 = "-1"
  NOS_RETURN_NEG_2 = "-2"

  RETURN_POS_1 = 1
  RETURN_NEG_1 = -1
  RETURN_NEG_2 = -2
  RETURN_ZERO  = 0
  
  def __init__(self):
    Storage.__init__(self, __file__)
    self.nosTool = "bash {0}/tool.sh".format(self.fileDir)
  
  def lookup(self, bucketName):
    """
     查找桶名为bucketName的桶是否存在，存在返回True，不存在返回False
    """
    cmd="{0} -lookup {1}".format(self.nosTool, bucketName)
    lines = Util.popenCommand(cmd).readlines()
    for line in lines:
      line = line.strip()
      if line == Nos.NOS_RETURN_NEG_1:
        return Nos.RETURN_NEG_1
      elif line == Nos.NOS_RETURN_NEG_2:
        return Nos.RETURN_NEG_2
      elif line == Nos.NOS_RETURN_POS_1:
        return Nos.RETURN_POS_1
    return Nos.RETURN_ZERO

  def deleteBucket(self, bucketName):
    """
     通过NOS工具tool.sh删除名称为bucketName的桶，删除之前先查找桶是否存在，
     如果不存在，直接返回True,如果存在，执行删除操作，操作失败返回False，
     操作成功返回True
    """
    lookupRet = self.lookup(bucketName)
    # NOS返回1,说明找到名为bucketName的桶
    if Nos.RETURN_POS_1 == lookupRet:
      lines = Util.popenCommand("{0} -deletebucket {1}".format(self.nosTool, bucketName)).readlines()
      # 处理NOS做桶删除操作返回的结果
      for line in lines:
        line = line.strip()
        if line == Nos.NOS_RETURN_NEG_1:
          return False
        elif line == Nos.NOS_RETURN_POS_1:
          return True
      return False
    elif Nos.RETURN_NEG_2 == lookupRet:
      return False
    else:
      # 如果桶本身不存在，也返回True
      return True

  def makeBucket(self, bucketName):
    """
     通过调用NOS工具tool.sh创建桶，先查找桶是否存在，如果不存在，则执行创建桶操作，
     如果创建桶错误或者查询桶是否存在时出错，则返回False，创建桶成功或者桶本存在，返回True
     +---------------+-------------------+
     |   桶状态      |      返回         |
     +---------------+-------------------+
     |    查询失败   |       False       |
     +---------------+-------------------+
     |    创建失败   |       False       |
     +---------------+-------------------+
     |    创建成功   |       True        |
     +---------------+-------------------+
     |  查询不存在   |       True        |
     +---------------+-------------------+
    """
    lookupRet = self.lookup(bucketName)
    if Nos.RETURN_NEG_1 == lookupRet:
      lines = Util.popenCommand("{0} -create {1}".format(self.nosTool, bucketName)).readlines()
      for line in lines:
        line = line.strip()
        # 返回-1，则说明创建桶不成功
        if Nos.NOS_RETURN_NEG_1 == line:
          return False
        # 返回1，说明创建桶成功
        elif Nos.NOS_RETURN_POS_1 == line:
          return True
      return False
    elif Nos.RETURN_NEG_2 == lookupRet:
      return False
    else:
      return True

  def putString(self, str, bucketName, keyName):
    """
     把str上传到NOS指定桶中，创建桶失败直接返回False
    """

    # 如果桶不存在或者创建桶失败，直接返回False
    if not self.makeBucket(bucketName):
      return False
    lines = Util.popenCommand("echo '{0}' | {1} -putstream {2} {3}".format(str, self.nosTool, bucketName, keyName)).readlines()
    for line in lines:
      line  = line.strip()
      if Nos.NOS_RETURN_NEG_1 == line:
        return False
      elif Nos.NOS_RETURN_POS_1 == line:
        return True
    return False

  def putStringByPipe(self, cmd, bucketName, keyName):
    """
     通过管道的形式上传数据，cmd为shell命令，输出数据流，NOS需要导入
     cmd输出的数据流，把数据上传到(bucketName, keyName)中
    """
    cmd = "{0} | {1} -putstream {2} {3}".format(cmd, self.nosTool, bucketName, keyName)
    lines = Util.popenCommand(cmd)
    for line in lines:
      line = line.strip()
      if Nos.NOS_RETURN_NEG_1 == line:
        return False
      if Nos.NOS_RETURN_POS_1 == line:
        return True
    return False

  def getString(self, bucketName, keyName):
    """
     从NOS上指定的桶上获取key值为keyName的数据
    """
    lookupRet = self.lookup(bucketName)
    if Nos.RETURN_POS_1 != lookupRet:
      # 查找桶名为bucketName的桶不存在，直接返回
      return "null"
    lines = Util.popenCommand('{0} -getobject {1} {2}'.format(self.nosTool, bucketName, keyName))
    str = ""
    for line in lines:
      line = line.strip()
      if Nos.NOS_RETURN_NEG_1 == line:
        return "null"
      if Nos.NOS_RETURN_POS_1 == line:
        continue
      str = str + line
    return str

  def getStringByPipe(self, cmd, bucketName, keyName):
    """
     通过管道的形式把获取的数据传给xbstream进行解压，然后保存到目录
    """
    cmd = "{0} -getobject {1} {2} | {3}".format(self.nosTool, bucketName, keyName, cmd)
    return Util.execCommand(cmd)

  def deleteFile(self, bucketName, keyName):
    """
     在NOS上删除桶名为bucketName，key名为keyName的数据
    """
    # 在NOS上查看(bucketName，keyName)是否存在
    if self.lookupFile(bucketName, keyName):
      # 数据存在，则调用NOS工具进行删除操作
      lines = Util.popenCommand("{0} -deleteobject {1} {2}".format(self.nosTool, bucketName, keyName)).readlines()
      for line in lines:
        line = line.strip()
        if Nos.NOS_RETURN_NEG_1 == line:
          return False
        elif Nos.NOS_RETURN_POS_1 == line:
          return True
      return False
    else:
      # 数据不存在，直接返回True
      return True

  def getFile(self, bucketName, keyName, fileName):
    """
     从NOS上获取指定数据(bucketName, keyName)到文件(fileName)
    """
    # 如果指定资源(bucketName，keyName)不存在，直接返回False
    if not self.lookupFile(bucketName, keyName):
      return False
    return Util.execCommand("{0} -getobject {1} {2} > {3}".format(self.nosTool, bucketName, keyName, fileName))

  def putFile(self, bucketName, keyName, fileName):
    """
     上传指定文件fileName到NOS上指定位置(bucketName, keyName)
    """
    if self.lookupFile(bucketName, keyName):
      return True
    return Util.execCommand("{0} -putfile {1} {2} -key {3}".format(self.nosTool, fileName, bucketName, keyName))

  def lookupFile(self, bucketName, keyName):
    """
     查找指定的资源(bucketName,keyName)是否存在，如果存在返回True， 不存在返回False
    """
    lines = Util.popenCommand("{0} -lookup {1} {2}".format(self.nosTool, bucketName, keyName)).readlines()
    for line in lines:
      line = line.strip()
      if Nos.NOS_RETURN_NEG_1 == line or Nos.NOS_RETURN_NEG_2 == line:
        return False
      elif line.isdigit():
        return True
    return False

  def getFileSize(self, bucketName, keyName):
    """
     在NOS上获取指定资源(bucketName, keyName)的大小
    """
    lines = Util.popenCommand("{0} -lookup {1} {2}".format(self.nosTool, bucketName, keyName)).readlines()
    for line in lines:
      line = line.strip()
      if Nos.NOS_RETURN_NEG_1 == line or Nos.NOS_RETURN_NEG_2 == line:
        return long(-1)
      elif line.isdigit():
        return long(line)
    return long(-1)
