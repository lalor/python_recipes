# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-03
@contact: jhx1008@gmail.com
version:  1.0
@todo:    存储管理类，存储相关操作控制类
@modify:
"""

from util import Util
from nos.nos import Nos

class StorageManager:
  """
   存储管理类
  """
  def __init__(self):
    self.storage = Nos()

  def lookup(self, bucketName):
    return self.storage.lookup(bucketName)

  def deleteBucket(self, bucketName):
    return self.storage.deleteBucket(bucketName)

  def makeBucket(self, bucketName):
    return self.storage.makeBucket(bucketName)

  def putString(self, str, bucketName, keyName):
    return self.storage.putString(str, bucketName, keyName)

  def putStringByPipe(self, cmd, bucketName, keyName):
    return self.storage.putStringByPipe(cmd, bucketName, keyName)

  def getString(self, bucketName, keyName):
    return self.storage.getString(bucketName, keyName)

  def getStringByPipe(self, cmd, bucketName, keyName):
    return self.storage.getStringByPipe(cmd, bucketName, keyName)

  def deleteFile(self, bucketName, keyName):
    return self.storage.deleteFile(bucketName, keyName)

  def putFile(self, bucketName, keyName, fileName):
    return self.storage.putFile(bucketName, keyName, fileName)

  def getFile(self, bucketName, keyName, fileName):
    return self.storage.getFile(bucketName, keyName, fileName)

  def lookupFile(self, bucketName, keyName):
    return self.storage.lookupFile(bucketName, keyName)

  def getFileSize(self, bucketName, keyName):
    return self.storage.getFileSize(bucketName, keyName)
