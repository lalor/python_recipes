# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-17
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:  agent状态管理类，Agent在接收Manager信息后，需要持久化当前命令处理状态，这样处理的主要目的是
          为了让Agent在重启后，可以处理上次未完成的任务
"""

import threading
from util import Util
from common import Common


class UUIDManager:
  """
  管理UUID，方法如下：
  1. 如果没有UUID的持久化文件，则新建一个UUID持久化文件，并初始化内容为空
  2. 每次Manager发送过来一个消息都调用updateUUID方法
    2.1 如果是第一次获取UUID，则持久化UUID
    2.2 如果不是第一次获取，且Manager下发的UUID与当前持久化的UUID相同，则忽略
    2.3 如果不是第一次获取，且Manager下发的UUID与当前持久化的UUID不同，则忽略
  """

  def __init__(self, fileDir, fileName, user='rds-user', group='rds-user'):
    """
     fileDir为状态文件保存目录
    """
    self.fileHandle = None
    fullPath = '{0}/{1}'.format(fileDir, fileName)

    if not Util.isExists(fileDir):
      Util.execCommand("sudo mkdir -p {0}".format(fileDir))
      Util.execCommand("sudo chown -R {0}:{1} {2}".format(user, group, fileDir))

    if not Util.isExists(fullPath):
      self.fileHandle = open(fullPath, "w")
      self.fileHandle.write('')
      self.fileHandle.close()

    self.fileHandle = open(fullPath,"r+")
    self.uuid = self.fileHandle.readline()


  def uninit(self):
    """
     关闭UUID文件
    """
    if not self.fileHandle.closed:
      self.fileHandle.close()


  def updateUUID(self, uuid):
    """
     更新agent的UUID
    """
    if self.fileHandle.closed:
      return self.uuid

    if not uuid:
      return self.uuid

    if not self.uuid:
      # 如果是第一次获取UUID，则持久化该UUID
      self.fileHandle.truncate(0)
      self.fileHandle.write(uuid)
      self.fileHandle.flush()
      self.uuid  = uuid
      return self.uuid

    if self.uuid == uuid:
      # 一般情况下，Manager每次传递下来的UUID都是一样的，即与
      # 持久化了的UUID相等
      return self.uuid
    else:
      # 如果已经持久化了一个UUID，Manager传递了一个不一样的UUID，
      # 则忽略这个UUID
      return self.uuid


class StatusManager:
  """
   Agent状态管理类，用于记录Agent当前处理命令状态
  """
  def __init__(self, fileDir, fileName="status.info",user='rds-user', group='rds-user', type=0):
    """
     fileDir为状态文件保存目录
    """
    self.fileHandle = None
    self.type = type
    fullPath = '{0}/{1}'.format(fileDir, fileName)
    self.lock = threading.Lock()
    # 状态文件保存文件夹不存在，创建文件夹
    if not Util.isExists(fileDir):
      Util.execCommand("sudo mkdir -p {0}".format(fileDir))
      Util.execCommand("sudo chown -R {0}:{1} {2}".format(user, group, fileDir))
    # 状态文件不存在，创建文件
    if not Util.isExists(fullPath):
      self.fileHandle = open(fullPath, "w")
      self.fileHandle.write(Util.obj2Json(('DONE','NULL')))
      self.fileHandle.flush()
      self.fileHandle.close()
    self.fileHandle = open(fullPath,"r+")
    if self.type == 0:
      Common.rdsLog.logInfo("init status manager succeed, status file: {0}".format(fullPath))

  def uninit(self):
    """
     状态管理类清理函数
    """
    if not self.fileHandle.closed:
      self.fileHandle.close()
    if self.type == 0:
      Common.rdsLog.logInfo("uninit status manager succeed")


  def updateStatus(self, status, param):
    """
     更新agent的状态
    """
    if self.fileHandle.closed:
      return False
    msg = Util.obj2Json((status, param))
    self.lock.acquire()
    self.fileHandle.seek(0)
    self.fileHandle.write(msg)
    # 截断文件长度为当前字符串长度
    self.fileHandle.truncate(len(msg))
    self.fileHandle.flush()
    self.lock.release()
    if type == 0:
      Common.rdsLog.logInfo("update agent status to {0}".format(status))
    return True


  def getItem(self):
    """
     获取保存的状态信息
    """
    if self.fileHandle.closed:
      return ["DONE", "NULL"]
    self.lock.acquire()
    self.fileHandle.seek(0)
    msg = self.fileHandle.read()
    self.lock.release()
    if msg.find('[') != -1:
      return Util.json2Obj(msg)
    else:
      return ["DONE", "NULL"]


  def getStatus(self):
    """
     获取状态
    """
    return self.getItem()[0]


  def getParam(self):
    """
     获取执行参数
    """
    return self.getItem()[1]

class InfoManager:
  """
   info manager主要用于持久化一些信息，包括VmId和消息的version，
   VmId用于区分agent，每个agent对应一个VmId，VmId在接收第一条命令时被初始化后写入文件，以后不会被改变，
   在接收消息时，检查消息的VmId与保存的是否一致，如果不一致，则忽略该条消息
   version主要用于消息去重，manager发送过来的消息都带有version，version为递增形式，如果manager没有收到
   agent的回复会进行重发，这时，如果agent已经处理过该version的消息，则会忽略该条消息
  """
  def __init__(self, fileDir, verFile="agent.info", msgFile="mydb.info", user='rds-user', group='rds-user'):
    self.statusManager = StatusManager(fileDir, verFile, user, group, 1)
    self.reportMsgFile = "{0}/{1}".format(fileDir, msgFile)

    if not Util.isExists(fileDir):
      Util.execCommand("sudo mkdir -p {0}".format(fileDir))
      Util.execCommand("sudo chown -R {0}:{1} {2}".format(user, group, fileDir))
    Common.rdsLog.logInfo("init information manager succeed, verFile: {0}, msgFile: {1}".format(verFile, msgFile))

  def updateInfo(self, databaseInstanceID, msgVersion):
    return self.statusManager.updateStatus(databaseInstanceID, msgVersion)

  def getDatabaseInstanceID(self):
    id = self.statusManager.getStatus()
    if id == "DONE":
      return "null"
    return id

  def getMsgVersion(self):
    """
     获取Manager发过来的消息的版本信息
    """
    version = self.statusManager.getParam()
    if version == "NULL":
      return "null"
    return version

  def saveMsgInfo(self, params):
    """
     保存命令信息到指定文件
    """
    try:
      handle = open(self.reportMsgFile, "wb")
      handle.write(Util.obj2Json(params))
      handle.close()
      return True
    except:
      return False

  def loadMsgInfo(self):
    """
     从文件中读取Msg信息，然后检查是否为json格式，返回从json load的结构信息
    """
    if Util.isExists(self.reportMsgFile):
      handle = open(self.reportMsgFile, "rb")
      msg = handle.read()
      handle.close()
      return Util.json2Obj(msg)
    else:
      return None

  def modifyMsgInfo(self, databaseInstanceID):
    """
     更新database instance identifier信息
    """
    if not Util.isExists(self.reportMsgFile):
      return False
    try:
      handle = open(self.reportMsgFile, "rb")
      msg = handle.read()
      handle.close()
      if msg.find("{") == -1 or msg.find("}") == -1:
        return False
      params = Util.json2Obj(msg)
      if not params.has_key('DBInstanceIdentifier'):
        return False
      params['DBInstanceIdentifier'] = databaseInstanceID
      handle = open(self.reportMsgFile, "wb")
      handle.write(Util.obj2Json(params))
      handle.close()
      return True
    except:
      return False

  def modifyVMMonitorInfo(self, vmInfoFile, value):
    """
    """
    if not Util.isExists(vmInfoFile):
      return False
    handle = open(vmInfoFile, "r")
    content = handle.read()
    handle.close()
    params = Util.json2Obj(content)
    params["resource_id"] = value
    content = Util.obj2Json(params)
    oldMode = Util.getFileMode(vmInfoFile)
    Util.execCommand("sudo chmod 777 {0}".format(vmInfoFile))
    handle = open(vmInfoFile, "w")
    handle.write(content)
    Util.execCommand("sudo chmod {0} {1}".format(oldMode, vmInfoFile))
    return True

  def uninit(self):
    self.statusManager.uninit()
    Common.rdsLog.logInfo("uninit information manager succeed")



