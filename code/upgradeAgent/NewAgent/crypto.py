# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-20
@contact: jhx1008@gmail.com
version:  1.0
@todo:    编解码函数定义文件
@modify:
"""

from Crypto.Cipher import DES
from base64 import b64decode as decode
from base64 import b64encode as encode
from RDSException import RDSException

def pad(s):
  """
   把字符串填充为8的倍数长，如果s长度为20，则在s后面填充4个直接为8的倍数24
   填充时按照1,2,3,4,5,6,7,8进行填充，这样在解码时，直接可以读取最后一个字符就可以计算出
   填充的字节数。
  """
  ll = ['\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08']
  length = len(s)
  needPad = 8 - length % 8 
  for i in range(0, needPad):
    s = s + ll[needPad]
  return s

def depad(s):
  """
   解析字符串，忽略pad的字节
  """
  length = len(s)
  try:
    count = ord(s[length - 1])
    return s[0:length - count]
  except:
    raise RDSException('depad字符串{0}出错'.format(s))

def encrypt(s , key):
  """
   对字符串s进行编码
  """
  obj = DES.new(key, DES.MODE_ECB)
  return encode(obj.encrypt(pad(s)))

def decrypt(s, key):
  """
   对字符串s进行解码
  """
  obj = DES.new(key.encode('ascii'), DES.MODE_ECB)
  return depad(obj.decrypt(decode(s)))

