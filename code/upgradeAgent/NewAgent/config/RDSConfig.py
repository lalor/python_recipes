# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-18
@contact: jhx1008@gmail.com
version:  1.0
@todo:    RDS Agent配置类，该类集成了所有RDS Agent的配置变量
@modify:
"""
import os
from RDSException import RDSException

class RDSConfig:
  """
   RDS Agent配置类，该类主要包含了所有Agent配置参数，通过静态变量的形式定义
  """

  # [manager]
  # manager配置参数
  managerIP = r'10.162.161.127'
  managerPort = 6000

  # [agent]
  # agent配置，包括Agent版本信息，监听端口，安装目录等等
  agentPort = 5000
  agentVersion = r'2.3'
  agentRoot = r'/home/rds-user'
  threadInterval = 5.0
  heartBeatInterval = 1.0
  agentDataDir = r'/home/rds-user/log'

  # [log]
  # 日志信息配置，包括默认日志级别，日志保存目录，过期时长等等
  logLevel = r'Error'
  logDir = r'/home/rds-user/log'
  logExpire = 3
  logFile = r'mylog'
  unifyLog = r'log.info'
  dumpMetaDir = r'/home/rds-user/log'

  maxRelayLogSize = 2147483647

  # [mysql]
  # 数据库信息配置
  mysqlHome = r''
  mysqlConfigDir = r''
  mysqlUser = r'rdsadmin'
  mysqlPasswd = r'?lACPAs82IDMs#'
  #mysqlErrorLog = r'/ebs/mysql_data/mysql-err.log'
  #mysqlBinlog = r'/ebs/mysql_data'
  backupDir = r'/rdsAgent/backup'
  backupLog = r'backup.log'
  restoreLog = r'restore.log'
  xtrabackup_slave_info = r'xtrabackup_slave_info'

  flashBackSplitSize=0

  @staticmethod
  def setManagerIP(ip):
    """
     设置管理服务器的IP地址，检查ip地址，如果地址格式不对，则忽略该配置项
    """
    errMsg="Manager IP Address error:{0}".format(ip)
    l = ip.split('.')
    if len(l) != 4:
      raise RDSException(errMsg)
    for elem in l:
      try:
        n = int(elem)
        # ip地址的每个字节为0 ~ 255之间
        if n < 0 or n > 255:
          raise RDSException(errMsg)
      except ValueError:
        raise RDSException(errMsg)
    RDSConfig.managerIP = ip
    return True

  @staticmethod
  def setManagerPort(port):
    """
     设置管理服务器的监听端口
    """
    errMsg="Manager Port error:{0}".format(port)
    try:
      p = int(port)
      # 端口值必须为0 ~ 65535之间的数值
      if p < 0  or p > 65535:
        raise RDSException(errMsg)
    except ValueError:
      raise RDSException(errMsg)
    RDSConfig.managerPort = p
    return True

  @staticmethod
  def setAgentPort(port):
    """
     设置agent监听的端口
    """
    p = int(port)
    # 端口必须为0~255之间的数值
    if p < 0 or p > 65535:
      print "warning port {0} error".format(port)
      return False
    RDSConfig.agentPort = p
    return True

  @staticmethod
  def setAgentVersion(version):
    """
     设置agent版本信息
    """
    RDSConfig.agentVersion = version
    return True

  @staticmethod
  def setAgentRoot(path):
    """
     设置agent的根目录
    """
    if not os.path.isdir(path):
      raise RDSException("agent root path:{0} error".format(path))
    RDSConfig.agentRoot = path
    return True

  @staticmethod
  def setThreadInterval(interval):
    """
     设置线程打印alive信息的间隔
    """
    RDSConfig.threadInterval = int(interval)
    return True

  @staticmethod
  def setHeartBeatInterval(interval):
    """
     设置心跳上报间隔时间
    """
    RDSConfig.heartBeatInterval = int(interval)
    return True

  @staticmethod
  def setAgentDataDir(path):
    """
     设置agent运行过程中产生数据保存目录
    """
    if not os.path.isdir(path):
      raise RDSException("agent data dir [{0}] not exist".format(path))
    RDSConfig.agentDataDir = path
    return True

  @staticmethod
  def setLogLevel(level):
    """
     设置log级别，DEBUG为最小，ERROR最大，相应的关系如下表
     +-------------+------------------------------------+
     |  设置级别   |              打印级别              |
     +-------------+------------------------------------+
     |    DEBUG    |    DEBUG, INFO, WARNING, ERROR     |
     +-------------+------------------------------------+
     |    INFO     |        INFO, WARNING, ERROR        |
     +-------------+------------------------------------+
     |    WARNING  |          WARNING, ERROR            |
     +-------------+------------------------------------+
     |    ERROR    |              ERROR                 |
     +-------------+------------------------------------+
    """
    lev = level.upper()
    if lev != 'DEBUG' and lev != 'INFO' and lev != 'WARNING' and lev != 'ERROR':
      raise RDSException( "log level:{0} error".format(level))
    RDSConfig.logLevel = lev
    return True

  @staticmethod
  def setLogDir(path):
    """
     设置agent日志保存目录
    """
    if not os.path.isdir(path):
      raise RDSException("error log dir:{0} not exist".format(path))
    RDSConfig.logDir = path
    return True

  @staticmethod
  def setLogExpire(expire):
    """
     设置agent日志过期时间，agent日志保存方式为每天产生一个日志文件，例如：当前正在写入的日志文件为agent.log，
     则过一天后，上一天的日志文件为agent.log.1，然后是agent.log.2，直到agent.log.expire，如果这个时候，又过了
     一天，则需要把最早的文件删除后继续写入
    """
    expr = int(expire)
    # 过期时间不能小于1天
    if expr < 1:
      raise RDSException("warning log expire:{0} error".format(expire))
    # 过期时间设置过长，给出warning提醒，但不返回False
    if expr > 30:
      print "warning log expire:{0} to long".format(expire)
    RDSConfig.logExpire = expr
    return True

  @staticmethod
  def setLogFile(name):
    """
     设置普通日志文件名
    """
    RDSConfig.logFile = name

  @staticmethod
  def setUnifyLog(name):
    """
     设置统一日志文件名
    """
    RDSConfig.unifyLog = name

  @staticmethod
  def setMysqlUser(name):
    """
     MySQL用户名配置
    """
    RDSConfig.mysqlUser = name

  @staticmethod
  def setMysqlPasswd(passwd):
    """
     MySQL用户对应的密码配置
    """
    RDSConfig.mysqlPasswd = passwd
  
  @staticmethod
  def setMysqlHome(path):
    """
     设置MySQL安装目录
    """
    RDSConfig.mysqlHome = path

  @staticmethod
  def setMysqlConfigDir(dirName):
    """
     设置Mysql配置文件路径
    """
    RDSConfig.mysqlConfigDir = dirName

  @staticmethod
  def setBackupDir(path):
    """
     备份保存路径
    """
    if not os.path.isdir(path):
      raise RDSException("mysql backup dir {0} not exist".format(path))
    RDSConfig.backupDir = path

  @staticmethod
  def setBackupLog(name):
    """
      备份日志
    """
    if not os.path.isdir(RDSConfig.backupDir):
      raise RDSException("mysql backup dir {0} not exist, can't create backup log file".format(RDSConfig.backupDir))
    RDSConfig.backupLog = "{0}/{1}".format(RDSConfig.backupDir, name)

  @staticmethod
  def setRestoreLog(name):
    """
     数据恢复日志
    """
    if not os.path.isdir(RDSConfig.backupDir):
      raise RDSException("mysql backup dir {0} not exist, can't create restore log file".format(RDSConfig.backupDir))
    RDSConfig.restoreLog = "{0}/{1}".format(RDSConfig.backupDir, name)

  @staticmethod
  def setFlashBackSplitSize(name):
    """
    flash back的切分大小
    """
    try:
      size = long(name)
    except:
      print "warning flash_back_split_size({0}) is not a number".format(name)
      return False
    if size < 0:
      print "warning flash_back_split_size({0}) is a negative number".format(name)
      return False
    RDSConfig.flashBackSplitSize = size
    return True

class RDSConfigManager:
  """
   RDSConfig操作类，用于管理RDSConfig中的相关变量
  """
  configDict = {
      "manager_ip"      : RDSConfig.setManagerIP,
      "manager_port"    : RDSConfig.setManagerPort,
      "agent_port"      : RDSConfig.setAgentPort,
      "agent_version"   : RDSConfig.setAgentVersion,
      "agent_root"      : RDSConfig.setAgentRoot,
      "thread_interval" : RDSConfig.setThreadInterval,
      "heartbeat_interval" : RDSConfig.setHeartBeatInterval,
      "agent_data_dir"  : RDSConfig.setAgentDataDir,
      "log_level"       : RDSConfig.setLogLevel,
      "log_dir"         : RDSConfig.setLogDir,
      "log_expire"      : RDSConfig.setLogExpire,
      "log_file"        : RDSConfig.setLogFile,
      "unify_log"       : RDSConfig.setUnifyLog,
      "mysql_user"      : RDSConfig.setMysqlUser,
      "mysql_passwd"    : RDSConfig.setMysqlPasswd,
      "mysql_home"      : RDSConfig.setMysqlHome,
      "mysql_config"    : RDSConfig.setMysqlConfigDir,
      "backup_dir"      : RDSConfig.setBackupDir,
      "backup_log"      : RDSConfig.setBackupLog,
      "restore_log"     : RDSConfig.setRestoreLog,
      "flash_back_split_size" : RDSConfig.setFlashBackSplitSize
  }

  @staticmethod
  def initByConfigParser(cfgParser):
    """
     通过配置文件初始化RDSConfig中的参数
    """
    sections = cfgParser.getSections()
    for section in sections:
      # agent的配置文件中只有agent一个section，其他section忽略
      if section.upper() != "AGENT":
        raise RDSException("unknown section find:{0}".format(section))
      items = cfgParser.getItems(section)
      for item in items:
        try:
          # 通过item获取回调函数，运行回调函数设置参数的值
          callback=RDSConfigManager.configDict[item[0]]
          callback(item[1])
        except KeyError:
          # 配置有误，配置的变量不存在
          print "section:{0}, item:{1}".format(section, item[0])
          raise RDSException("configure {0}={1} error".format(item[0], item[1]))
        except Exception,e:
          # 其他未知错误
          raise RDSException("unknown exception occur when read the config file, error: {0}".format(e))

  @staticmethod
  def initByOptionParser(optParser):
    """
     通过命令行参数初始化RDSConfig中的参数，该函数应该在initByConfigParser后面调用，
     如果存在相同的配置，则覆盖配置文件的配置
    """
    keys = optParser.getKeys()
    for key in keys:
      try:
        callback = RDSConfigManager.configDict[key]
        callback(optParser.getValue(key))
      except KeyError:
        raise RDSException("option {0}={1} error".format(key, optParser.getValue(key)))
      except:
        raise RDSException("unknown exception occur when parse the option")
