# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-05
@contact: jhx1008@gmail.com
version:  1.0
@todo:    该类用于解析Agent中的命令行参数，命令行参数解析放在配置文件解析之后，可以覆盖配置文件的配置
@modify:
"""

from optparse import OptionParser
from RDSException import RDSException

class RDSOptionParser(OptionParser):
  """
    命令行参数解析类
  """
  def __init__(self):
    """
     命令行解析类，命令行参数优先于配置文件：如果相同一个参数，既在配置文件中配置了，
     又在命令行参数中进行了设置，则命令行参数的设置将覆盖配置文件中的配置
    """
    usage = "usage: %prog [-option]"
    parser = OptionParser(usage)
    parser.add_option("-m","--manager-ip",dest="managerIP",help="管理服务器的IP地址")
    parser.add_option("-p","--manager-port",dest="managerPort",help="管理服务器的监听端口")
    parser.add_option("-a","--agent-root",dest="agentRoot",help="agent的保存位置")
    parser.add_option("-l","--log-dir",dest="logDir",help="agent日志保存文件夹")
    parser.add_option("-e","--log-expire",dest="logExpire",help="agent日志过期时间")
    parser.add_option("-c","--console",action="store_true", dest="console", help="前台运行")
    # 由于option的参数需要覆盖configure文件的配置，而configure文件需要通过default-files进行指定，
    # 所以需要先解析default-files参数，default-files参数解析直接通过sys.argv进行，不通过optionParser
    # 进行解析，所以在OptionParser解析时，可以忽略default-files的解析
    parser.add_option("-f","--defaults-file",dest="configFile",help="指定agent的配置文件")
    self.configDict={}
    self.configFile = "RDSAgent.cnf"
    self.console = False

    try: 
      (options, args) = parser.parse_args()

      if options.managerIP:
        self.configDict['manager_ip'] = options.managerIP

      if options.managerPort:
        self.configDict['manager_port'] = options.managerPort

      if options.agentRoot:
        self.configDict['agent_root'] = options.agentRoot

      if options.logDir:
        self.configDict['log_dir'] = options.logDir

      if options.logExpire:
        self.configDict['log_expire'] = options.logExpire

      if options.console:
        self.console = options.console

      if options.configFile:
        self.configFile = options.configFile

    except SystemExit,e:
      raise RDSException(e)

  def getValue(self, name, default=None):
    return self.configDict.get(name, default)

  def getKeys(self):
    return self.configDict.keys()
