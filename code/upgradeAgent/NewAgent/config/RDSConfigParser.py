# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-05
@contact: jhx1008@gmail.com
version:  1.0
@todo:    配置文件解析类，用于解析Agent的配置文件
@modify:
"""

import os
from RDSException import RDSException
from ConfigParser import ConfigParser

class RDSConfigParser(ConfigParser):
  """
   用于解析Agent的配置文件
  """
  
  def __init__(self, configFileName):
    """
     配置文件解析类初始化函数，configFileName为配置文件全路径，cmdDict为配置参数字典
    """
    if not os.path.isfile(configFileName):
      raise RDSException("config file {0} not exists".format(configFileName))

    try:
      self.cfgParser = ConfigParser()
      self.cfgParser.read(configFileName)
    except Exception,e:
      raise RDSException(e)

  def getSections(self):
    return self.cfgParser.sections()

  def getItems(self,section):
    return self.cfgParser.items(section)

  def getValue(self,section,option):
    try:
      return self.cfgParser.get(section, option)
    except:
      return None
