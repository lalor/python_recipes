# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:    RDSLog,继承自BaseLog，用于记录RDS Agent自身的log信息到本地文件
@modify:
"""

import os
import time
from BaseLog import *

class RDSLog(BaseLog):
  """
   RDS Agent日志类，用于记录Agent运行过程中的日志信息
  """
  
  def __init__(self, logDir, logFile, level = LogLevel.INFO, expire = 3):
    """
     RDSLog初始化函数
     @level: 日志打印级别，分为DEBUG，INFO，WARN，ERROR 4种不同类型
     @expire:日志过期时间，单位为天，超过该时间的日志将被删除
    """
    BaseLog.__init__(self, expire, level, logDir, logFile)

  def __getTimeStamp__(self):
    """
     获取日志记录的时间戳
    """
    return time.strftime('%Y-%m-%d %H:%M:%S')


  def __writeLog__(self, level, info):
    """
     写入日志函数，该函数从BaseLog中继承，通过BaseLog::append进行调用
     @level: 写入日志的类型, 如果该类型小于__level__，则该信息不会被写入日志文件
     @info:  写入的日志信息
    """
    if level < self.__level__:
      return

    msg = '{0} [{1}] {2}\n'.format(self.__getTimeStamp__(), LogLevel.level2String(level), info)
    self.__fileHandle__.write(msg)
    self.__fileHandle__.flush()

    


