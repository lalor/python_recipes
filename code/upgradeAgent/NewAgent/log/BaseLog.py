# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:    日志基础类，用于继承
@modify:
"""

import os
import time
import threading
from RDSException import RDSException

class LogLevel:
  DEBUG = 0
  INFO  = 1
  WARN  = 2
  ERROR = 3
  UNKNOWN = 4

  @staticmethod
  def string2Level(level):
    level = level.upper()
    ret = LogLevel.DEBUG
    if level == 'DEBUG':
      ret = LogLevel.DEBUG
    elif level == 'INFO':
      ret = LogLevel.INFO
    elif level == 'WARN':
      ret = LogLevel.WARN
    elif level == 'ERROR':
      ret = LogLevel.ERROR
    else:
      ret = LogLevel.UNKNOWN
    return ret

  @staticmethod
  def level2String(level):
    ret = 'DEBUG'
    if level == LogLevel.DEBUG:
      pass
    elif level == LogLevel.INFO:
      ret = 'INFO'
    elif level == LogLevel.WARN:
      ret = 'WARN'
    elif level == LogLevel.ERROR:
      ret = 'ERROR'
    else:
      ret = 'UNKNOWN'
    return ret


class BaseLog:
  """
   RDS Agent日志基础类，RDS Agent中的日志分为普通日志和统一日志两种，该类用于继承
  """

  def __init__(self, expire, logLevel, logDir, logFile):
    """
     基础类初始化函数
     @logDir：日志文件保存的目录
     @logFile:日志文件名
    """

    self.__mutex__ = threading.Lock()
    self.__expire__ = expire
    self.__level__  = logLevel
    self.__logDir__ = logDir
    self.__logFile__ = logFile
    self.__yDay__ = time.localtime().tm_yday
    self.__fileHandle__ = None

    try:
      if not os.path.exists(logDir):
        os.mkdir(logDir)
    except:
      raise RDSException("make log dir %s failed" % logDir)

    try:
      self.__fileHandle__ = open('{0}/{1}'.format(logDir,logFile),"a+")
    except:
      raise RDSException("create or open log file %s/%s failed" % (logDir, logFile))


  def append(self, level, info):
    """
     日志写入函数，该函数会调用writeLog进行日志写入
    """
    self.__mutex__.acquire()
    self.__changeFile__()
    self.__writeLog__(level, info)
    self.__mutex__.release()

  def logDebug(self, info):
    self.append(LogLevel.DEBUG, info)

  def logInfo(self, info):
    self.append(LogLevel.INFO, info)

  def logWarn(self, info):
    self.append(LogLevel.WARN, info)

  def logError(self, info):
    self.append(LogLevel.ERROR, info)

  def __changeFile__(self):
    """
     该函数用于负责文件切换，日志记录时按天生成一个日志文件，所以如果日期发生变换，文件需要被保存后重新生成一个文件,
     该函数需要被__mutex__保护
    """
    yday = time.localtime().tm_yday
    # 如果日期没有发生改变，则不需要切换日志文件
    if yday == self.__yDay__:
      return

    # 获取当前归档日志中最大的日志文件编号
    maxID = self.__expire__ - 1
    for i in range(0, self.__expire__):
      id = maxID - i
      backupLog = '{0}/{1}.{2}'.format(self.__logDir__, self.__logFile__, id)
      if not os.path.exists(backupLog):
        continue

      # 找到当前最大编号的归档日志文件，与允许的最大日志文件编号比较，如果相等，则说明当前归档日志文件
      # 已经达到最大数量，需要删除最老的文件后才能继续，如果没有达到最大数量，则把归档日志的编号+1
      if id == maxID:
        os.remove(backupLog)
      else:
        newLog = '{0}/{1}.{2}'.format(self.__logDir__, self.__logFile__, id + 1)
        os.rename(backupLog, newLog)
    
    # 把当前写的日志文件归档，关闭文件句柄，重命名，然后重新创建一个文件
    self.__fileHandle__.close()
    fileName = '{0}/{1}'.format(self.__logDir__, self.__logFile__)
    newName = '{0}.0'.format(fileName)
    os.rename(fileName, newName)
    self.__fileHandle__ = open(fileName,"a+")
    self.__yDay__ = yday


  def __writeLog__(self, level ,info):
    """
     日志写入方法，各个类自己实现
    """
    self.__fileHandle__.write(info)
    self.__fileHandle__.flush()
    os.fsync(self.__fileHandle__.fileno())

  def uninit(self):
    """
     结束时调用，关闭日志文件
    """
    if not self.__fileHandle__.closed:
      self.__fileHandle__.close()
