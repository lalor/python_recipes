# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-06
@contact: jhx1008@gmail.com
version:  1.0
@todo:    RDS Agent统一日志处理类
@modify:
"""
import os
import time
import json
from BaseLog import *

class UnifyLog(BaseLog):
  """
   统一日志处理类
  """
  level2String = {LogLevel.DEBUG : "debug", LogLevel.INFO : "info", LogLevel.WARN : "warn", LogLevel.ERROR : "error"}
  
  def __init__(self, logDir, logFile, localIP, level = LogLevel.INFO, expire = 3):
    BaseLog.__init__(self, expire, level, logDir, logFile)
    self.localIP = localIP
    self.sequence = 1
    self.unifyInfo = {}

  def __writeLog__(self, level, info):
    """
     通过params获取统一日志字段，对日志进行格式化
     @logSeq为日志的seq编号
    """
    formatInfo = self.formatLog(level, info)
    if level < self.__level__  or formatInfo == "":
      return
    self.__fileHandle__.write("{0}\n".format(formatInfo))
    self.__fileHandle__.flush()
    os.fsync(self.__fileHandle__.fileno())

  def initUnifyInfo(self, params):
    """
     初始化unifyInfo中的信息，在每次接收到命令的checkPublic后进行调用
    """
    self.unifyInfo['seq'] = params.get('seq', "0")
    self.unifyInfo['object'] = params.get('object', "0")
    self.unifyInfo['id'] = params.get('id', "0")
    self.unifyInfo['module'] = params.get('module', "0")
    self.unifyInfo['op'] = params.get('op', "0")
    self.sequence = 1
    
  def formatLog(self, level, info):
    """
     格式化统一日志格式
    """
    if info == "" or 0 == self.unifyInfo.get("seq", 0):
      return ""

    logDict = {}
    logDict["seq"] = self.unifyInfo.get("seq", "0") + ".%d" % self.sequence
    logDict["timestamp"] = "%d" % time.time()
    logDict["object"] = self.unifyInfo.get("object","0")
    logDict["description"] = info
    logDict["level"] = UnifyLog.level2String.get(level, "INFO")
    logDict["ip"] = self.localIP
    logDict["id"] = self.unifyInfo.get("id","0")
    logDict["module"] = self.unifyInfo.get("module","0") + ".Agent"
    logDict["identifier"] = self.unifyInfo.get("identifier", "0")
    logDict["op"] = self.unifyInfo.get("op", "0")
    self.sequence = self.sequence + 1
    return json.dumps(logDict)
    
