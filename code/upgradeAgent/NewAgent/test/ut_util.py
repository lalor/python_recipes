# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-12-25
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify:
"""

import os
import sys
import time
import unittest
sys.path.append(os.path.split(os.getcwd())[0])
from util import Util

class UtilTestCase(unittest.TestCase):
  def setUp(self):
    pass

  def tearDown(self):
    pass

  def testUtil(self):
    cmd="while [ 1 ]; do :; done"
    Util.execCommand("echo \"{0}\" > findProcessTest.sh".format(cmd))
    Util.execCommand("bash findProcessTest.sh &")
    time.sleep(3)
    self.assertTrue(Util.findProcess("findProcessTest"))
    Util.killProcess("findProcessTest")
    self.assertFalse(Util.findProcess("findprocessTest"))
    Util.execCommand("rm -rf findProcessTest.sh")

  def testGetIPAddress(self):
    ip1 = Util.popenCommand("/sbin/ifconfig eth0 | grep 'inet addr' | awk -F' ' '{print $2}' | awk -F':' '{print $2}'").readlines()[0]
    ip2 = Util.getIPAddress("eth0")
    ip1=ip1.strip(" \r\n")
    ip2=ip2.strip(" \r\n")
    self.assertTrue(ip1 == ip2)

  def testObj2Json(self):
    d={'a':1,'b':2,'c':3}
    ret = Util.obj2Json(d)
    self.assertTrue(ret == '{"a": 1, "c": 3, "b": 2}')

  def testJson2Obj(self):
    s='{"a":1,"b":2,"c":3}'
    ret = Util.json2Obj(s)
    self.assertTrue(ret == {'a':1,'b':2,'c':3})

if __name__ == "__main__":
  unittest.main()
