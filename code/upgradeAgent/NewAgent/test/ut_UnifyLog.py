# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-07
@contact: jhx1008@gmail.com
version:  1.0
@todo:    UnityLog的单元测试
@modify:
"""
import os
import sys
import unittest

sys.path.append(os.path.split(os.getcwd())[0])
from RDSException import RDSException
from log.UnifyLog import *

class UnifyLogTestCase(unittest.TestCase):
  def setUp(self):
    self.path = "{0}/{1}".format(os.getcwd(), "unify.log")
    self.assertTrue(not os.path.exists(self.path))
    self.unifyLog = UnifyLog(os.getcwd(), "unify.log", "192.168.0.1")
    params = {"seq":"1","object":"class", "id":1234,"module":"RDS","identifier":"330501","op":"set"}
    self.unifyLog.initUnifyInfo(params)

  def tearDown(self):
    self.unifyLog.uninit()
    if os.path.exists(self.path):
      os.remove(self.path)

  def testAppend(self):
    try:
      self.unifyLog.append(LogLevel.ERROR, "hello world")
      fh = open(self.path, 'r')
      msg = fh.readline()
      self.assertTrue(msg.find("hello world") != -1)
      fh.close()

    except RDSException,e:
      print e

  def testFormatLog(self):
    key = "\"description\": \"hello world\", \"seq\": \"1.1\", \"level\": \"error\", \"ip\": \"192.168.0.1\", \"object\": \"class\", \"module\": \"RDS.Agent\""
    msg = self.unifyLog.formatLog(LogLevel.ERROR, "hello world")
    self.assertTrue(msg.find(key) != -1)

if __name__ == "__main__":
  unittest.main()
