# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-22
@contact: jhx1008@gmail.com
version:  1.0
@todo:    command类单元测试
@modify:
"""

import os
import sys
import unittest

sys.path.append(os.path.split(os.getcwd())[0])
from command import Command

class CommandTestCase(unittest.TestCase):
  def setUp(self):
    pass

  def tearDown(self):
    pass

  def testCheckUnifyLog(self):
    cmd = Command()
    params={'id':1,'seq':2,'module':3,'op':4,'object':5}
    self.assertTrue(cmd.checkUnifyLog(params))
    keys = params.keys()
    # 依次缺少id,seq,module...，检测checkUnifyLog函数是否返回False
    for key in keys:
      temp = params.copy()
      temp.pop(key)
      self.assertFalse(cmd.checkUnifyLog(temp))

  def testCheckPublic(self):
    cmd = Command()
    params={'Action':1,'VmId':2,'Version':'1','LogInfo':'{"id":1,"seq":2,"module":3,"op":4,"object":5}'}
    # 正常返回True
    self.assertTrue(cmd.checkPublic(params))
    cmd.reset()
    # 依次缺少Action，VmId，Version等，异常返回False
    keys = params.keys()
    for key in keys:
      temp = params.copy()
      temp.pop(key)
      self.assertFalse(cmd.checkPublic(temp))
      cmd.reset()
    # 检测Action为GetErrorLog时的返回
    temp = params.copy()
    temp['Action']='GetErrorLog'
    temp['Version']='error'
    self.assertTrue(cmd.checkPublic(temp))
    cmd.reset()
    # 检测Version为非数字时，是否返回False
    temp = params.copy()
    temp['Version']='s.a'
    self.assertFalse(cmd.checkPublic(temp))
    cmd.reset()

  def testCheckChangeHA(self):
    cmd = Command()
    params = {'DBInstanceIdentifier':1, 'VMoniterInfo':2, 'DBParameters':3}
    self.assertTrue(cmd.checkChangeHA(params))
    cmd.reset()
    params.pop('DBInstanceIdentifier')
    self.assertFalse(cmd.checkChangeHA(params))
    cmd.reset()

  def testcheckStartRepl(self):
    cmd = Command()
    params = {'ReplicaUserPassword':1, 'DBInstanceIdentifier':2, 'MasterIP':3, 'MasterPort':4, \
              'Binlog':5, 'BinlogOffset':6, 'ReadOnly':7, 'VMmoniterInfo':8}
    cmd.checkStartRepl(params)
    self.assertTrue(cmd.errmsg=='')

  def testCheckCreate(self):
    cmd = Command()
    params={'DBName':1, 'MasterUserName':2, 'DBInstanceIdentifier':3, 'MasterUserPassword':4, \
            'DBParameters':5, 'ReplicaUserPassword':6, 'Device':7, 'UserProductID':8, \
            'ServiceType':9, 'MonitorAcessKey':10, 'MonitorSecretKey':11, 'MonitorServerIp':12, \
            'MonitorServerDomain':13, 'MonitorWebServerUrl':14, 'DDBProductID':15, \
            'AggregationDimensions':16, 'AggregationMetrics':17}
    self.assertTrue(cmd.checkCreate(params))
    params.pop('DBName')
    params.pop('DDBProductID')
    params.pop('AggregationDimensions')
    params.pop('AggregationMetrics')
    cmd.reset()
    self.assertTrue(cmd.checkCreate(params))
    keys = params.keys()
    for key in keys:
      params.pop(key)
      cmd.reset()
      self.assertFalse(cmd.checkCreate(params))

  def testCheckBackup(self):
    cmd = Command()
    params = {'SnapshotURL':'a$b', 'SnapshotIdentifier':1}
    self.assertTrue(cmd.checkBackup(params))
    params['SnapshotURL'] = 'a$b$c'
    cmd.reset()
    self.assertFalse(cmd.checkBackup(params))
    params.pop('SnapshotURL')
    cmd.reset()
    self.assertFalse(cmd.checkBackup(params))

  def testCheckRestore(self):
    cmd = Command()
    params={'Device':1, 'SnapshotIdentifier':2, 'DBParameters':3, 'DBInstanceIdentifier':4, \
            'SnapshotURL':'a$b', 'UserProductID':5, 'ServiceType':6, 'MonitorAcessKey':7, \
            'MonitorSecretKey':8, 'MonitorServerIp':9, 'MonitorServerDomain':10,'MonitorWebServerUrl':11}
    self.assertTrue(cmd.checkRestore(params))
    keys = params.keys()
    for key in keys:
      cmd.reset()
      params.pop(key)
      self.assertFalse(cmd.checkRestore(params))

  def testCheckLog(self):
    cmd = Command()
    params = {'EndRow':1,'Extent':2}
    self.assertTrue(cmd.checkLog(params))
    keys = params.keys()
    for key in keys:
      params.pop(key)
      cmd.reset()
      self.assertFalse(cmd.checkLog(params))

  def testCheckReboot(self):
    cmd = Command()
    params = {'DBParameters':1, 'BinlogOffset':2, 'Binlog':3}
    self.assertTrue(cmd.checkReboot(params))
    params.pop('DBParameters')
    cmd.reset()
    cmd.checkReboot(params)
    self.assertTrue(cmd.errmsg == 'read error, no DBParameters information')

  def testCheckModify(self):
    cmd = Command()
    params = {'DBParameters':1, 'MasterUserPassword':2, 'MasterUserName':3}
    self.assertTrue(cmd.checkModify(params))
    keys = params.keys()
    for key in keys:
      temp = params.copy()
      temp.pop(key)
      cmd.reset()
      cmd.checkModify(temp)
      self.assertTrue(cmd.errmsg == 'read error, no {0} information'.format(key))

  def testCheckStop(self):
    cmd = Command()
    params = {'SnapshotIdentifier':1,'SkipFinalSnapshot':'2', 'SnapshotURL':'a$b'}
    temp = params.copy()
    self.assertTrue(cmd.checkStop(temp))
    
    cmd.reset()
    temp.pop('SkipFinalSnapshot')
    self.assertFalse(cmd.checkStop(temp))

    temp = params.copy()
    temp['SkipFinalSnapshot']='False'
    temp['SnapshotURL'] = 'a$b$c'
    cmd.reset()
    self.assertFalse(cmd.checkStop(temp))

    temp.pop('SnapshotURL')
    cmd.reset()
    self.assertFalse(cmd.checkStop(temp))

  def testCheckStart(self):
    cmd = Command()
    params={'Device':1, 'DBParameters':2, 'AllocatedStorage':3, 'UserProductID':4,  \
            'DBInstanceIdentifier':5, 'MonitorServerIp':6, 'ServiceType':7,'MonitorAcessKey':8, \
            'MonitorServerDomain':9, 'MonitorSecretKey':10, 'MasterUserName':11, 'ReplicaUserPassword':12,  \
            'MonitorWebServerUrl':13}
    self.assertTrue(cmd.checkStart(params))
    temp = params.copy()
    temp.pop('Device')
    cmd.reset()
    self.assertFalse(cmd.checkStart(temp))

    temp = params.copy()
    temp.pop('DBParameters')
    cmd.reset()
    self.assertFalse(cmd.checkStart(temp))

    keys = temp.keys()
    for key in keys:
      s = temp.copy()
      s['Device'] = 1
      s['DBParameters'] = 2
      s.pop(key)
      cmd.reset()
      cmd.checkStart(s)
      self.assertTrue(cmd.errmsg == 'read error, no {0} information'.format(key))

  def testCheckChangeAsync(self):
    cmd = Command()
    params = {'DBParameters':1}
    self.assertTrue(cmd.checkChangeAsync(params))
    cmd.reset()
    params.pop('DBParameters')
    self.assertFalse(cmd.checkChangeAsync(params))
  
  def testCheckChangeSync(self):
    cmd = Command()
    params = {'DBParameters':1}
    self.assertTrue(cmd.checkChangeSync(params))
    cmd.reset()
    params.pop('DBParameters')
    self.assertFalse(cmd.checkChangeSync(params))

if __name__ == '__main__':
  unittest.main()


