# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-23
@contact: jhx1008@gmail.com
version:  1.0
@todo:    Agent状态管理单元测试
@modify:
"""

import os
import sys
import unittest

sys.path.append(os.path.split(os.getcwd())[0])

from AgentManager import StatusManager

class StatusManagerTestCase(unittest.TestCase):
  def setUp(self):
    self.fileDir = os.getcwd()
    user=os.getlogin()
    self.statusManager = StatusManager(self.fileDir, "status.info", user, 'netease',type=1)

  def tearDown(self):
    self.statusManager.uninit()
    fileName = '{0}/status.info'.format(self.fileDir)
    if os.path.exists(fileName):
      os.remove(fileName)

  def testInit(self):
    self.assertTrue(os.path.exists('{0}/status.info'.format(self.fileDir)))

  def testUpdateStatus(self):
    status='DOING'
    param='DOING THE START COMMAND'
    self.assertTrue(self.statusManager.updateStatus(status, param))

  def testGetStatus(self):
    status='DOING'
    param='NULL'
    self.statusManager.updateStatus(status, param)
    self.assertTrue('DOING' == self.statusManager.getStatus())

  def testGetParam(self):
    status='DOING'
    param='DOING THE START COMMAND'
    self.statusManager.updateStatus(status, param)
    self.assertTrue('DOING THE START COMMAND' == self.statusManager.getParam())


if __name__ == "__main__":
  unittest.main()
