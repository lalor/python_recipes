import unittest
import sys
import os

sys.path.append('/home/rds-user/rdsAgent')

from network import route
from network.route import RouteManager
from common import Common
from util import Util



class FakeLog:

    def logInfo(self, msg):
        print 'Info', msg

    def logDebug(self, msg):
        print 'DEBUG', msg

    def logWarn(self, msg):
        print 'Warn', msg

class RouteManagerTestCase(unittest.TestCase):

    def setUp(self):
        Common.rdsLog = FakeLog()
        self.fileDir = '/home/rds-user/rdsAgent/log'

    def testExcCommand(self):
        stdout = route.execCommand("ls -al")
        self.assertTrue(stdout != None)

        stdout = route.execCommand("ls -2l")
        self.assertTrue(stdout == None)

    def testRoute2Command(self):

        routeDict  = None
        cmd = route.route2Command(routeDict)
        self.assertTrue(cmd is None)

        routeDict = dict()
        cmd = route.route2Command(routeDict)
        self.assertTrue(cmd is None)

        routeDict = dict(Destination='0.0.0.0', Gateway='192.168.0.1')
        cmd = route.route2Command(routeDict)
        self.assertTrue(cmd is None)

        routeDict = dict(Destination='0.0.0.0',
                         Gateway='192.168.0.1',
                         Mask='24',
                         NetworkInterface='eth0')
        cmd = route.route2Command(routeDict)
        self.assertTrue(cmd is not None)
        self.assertTrue(cmd == 'default via 192.168.0.1 dev eth0')

        routeDict = dict(Destination='192.168.0.22',
                         Gateway='192.168.0.1',
                         Mask='24',
                         NetworkInterface='eth0')
        cmd = route.route2Command(routeDict)
        self.assertTrue(cmd == '192.168.0.22/24 via 192.168.0.1 dev eth0')


        routeDict = dict(Destination='192.168.0.22',
                         Gateway='0.0.0.0',
                         Mask='24', 
                         NetworkInterface='eth0')
        cmd = route.route2Command(routeDict)
        self.assertEqual(cmd, '192.168.0.22/24 dev eth0  proto kernel  scope link  src %s' % (route.getIPAddrByInterface('eth0')[0], ))

    def testRouteManager(self):
        routeManager = RouteManager(self.fileDir, "route.info")
        self.assertTrue(Util.isExists(routeManager.fullPath))


        def readRouteInfo():
            data = ''
            with open(routeManager.fullPath) as fd:
                data = fd.read()
            return data

        routeManager.updateRouteInfo()
        self.assertTrue(readRouteInfo() == route.execCommand('sudo ip route'))

        routeManager.loadRouteInfo()
        self.assertTrue(readRouteInfo() == route.execCommand('sudo ip route'))


        ret, routes = routeManager.describeAll()
        self.assertTrue(ret == True)
        lines = [line.strip() for line in route.execCommand('sudo ip route').split('\n')]
        for routeDict in routes:
            command = route.route2Command(routeDict)
            self.assertTrue(command in lines)

    def testCreateRoute(self):
        routeManager = RouteManager(self.fileDir, "route.info")
        self.assertTrue(Util.isExists(routeManager.fullPath))

        routeDict = dict(Destination='192.168.0.22', 
                         Mask=24,
                         Gateway='192.168.0.1',
                         NetworkInterface='eth0')
        ret = routeManager.createRoute(routeDict)
        self.assertTrue(ret == False)

    def  testDeleteRoute(self):
        routeManager = RouteManager(self.fileDir, "route.info")
        self.assertTrue(Util.isExists(routeManager.fullPath))

        routeDict = dict(Destination='192.168.0.22',
                         Mask=24,
                         Gateway='102.168.0.1',
                         NetworkInterface='eth0')
        ret = routeManager.deleteRoute(routeDict)
        self.assertTrue(ret == True)

if __name__ == '__main__':
    unittest.main()