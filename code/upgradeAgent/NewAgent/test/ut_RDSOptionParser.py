# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-02
@contact: jhx1008@gmail.com
version:  1.0
@todo:    运行参数类OptionParser，单元测试
@modify:
"""

import os
import sys
import unittest

sys.path.append(os.path.split(os.getcwd())[0])
from config.RDSOptionParser import RDSOptionParser

class RDSOptionParserTestCase(unittest.TestCase):

  def setUp(self):
    sys.argv.append('--manager-ip=192.168.0.1')
    sys.argv.append('--manager-port=6666')
    self.optParser = RDSOptionParser()

  def tearDown(self):
    pass

  def testGetValue(self):
    self.assertTrue(self.optParser.getValue('manager_ip') == '192.168.0.1')
    self.assertTrue(self.optParser.getValue('abcd') == None)

if __name__ == "__main__":
  unittest.main()
