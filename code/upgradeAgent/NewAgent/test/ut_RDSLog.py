# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-07
@contact: jhx1008@gmail.com
version:  1.0
@todo:    RDSLog的单元测试
@modify:
"""
import os
import sys
import unittest

sys.path.append(os.path.split(os.getcwd())[0])
from RDSException import RDSException
from log.RDSLog import *

class RDSLogTestCase(unittest.TestCase):
  def setUp(self):
    pass

  def tearDown(self):
    pass

  def testAppend(self):
    try:
      path='{0}/{1}'.format(os.getcwd(),"uttest.log")
      self.assertTrue(not os.path.exists(path))

      rdsLog = RDSLog(os.getcwd(), "uttest.log")
      rdsLog.append(LogLevel.ERROR, "hello world")
      rdsLog.append(LogLevel.DEBUG, "welcome to china")
      rdsLog.uninit()

      fh = open(path, 'r')
      msg = fh.readline()
      self.assertTrue(msg.find("hello world") != -1 )
      msg = fh.readline()
      self.assertTrue(msg.find("welcome to china") == -1)
      fh.close()
      os.remove(path)
    except RDSException:
      pass

if __name__ == "__main__":
  unittest.main()
