# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-02
@contact: jhx1008@gmail.com
version:  1.0
@todo:    RDSConfigParser单元测试
@modify:
"""

import os,sys
import unittest
sys.path.append(os.path.split(os.getcwd())[0])
from RDSException import RDSException
from config.RDSConfigParser import RDSConfigParser

class RDSConfigParserTestCase(unittest.TestCase):

  def setUp(self):
    self.configFile='{0}/testRDSConfigParser.cnf'.format(os.getcwd())
    l=["[mysqld]\n","socket=abcd\n","[mysql]\n","port=123\n","host=192.168.0.1"]
    self.createFile(l)
    self.configParser = RDSConfigParser(self.configFile)

  def tearDown(self):
    self.deleteFile()

  def createFile(self, l):
    f = open(self.configFile,'w+')
    f.writelines(l)
    f.close()

  def deleteFile(self):
    print self.configFile
    if os.path.isfile(self.configFile):
      os.remove(self.configFile)

  def testGetSessions(self):
    sections=['mysqld','mysql']
    self.assertTrue(self.configParser.getSections() == sections)

  def testGetItems(self):
    items=[('port','123'),('host','192.168.0.1')]
    self.assertTrue(self.configParser.getItems('mysql') == items)

  def testGetValue(self):
    d = {'port':'123','host':'192.168.0.1'}
    keys=d.keys()
    for key in keys:
      self.assertTrue(self.configParser.getValue('mysql',key) == d[key])


if __name__ == "__main__":
  unittest.main()
