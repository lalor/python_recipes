# !/usr/bin/bash

function lookup()
{
  if [ "$1" = "bucketName" ] || [ "$1" = "bucketName1" ] 
  then
    echo "1"
  else
    echo "-1"
  fi
}

function delete_bucket()
{
  if [ "$1" = "bucketName" ]
  then
    echo "1"
  else
    echo "-1"
  fi
}

function create()
{
  if [ "$1" = "bucketName" ]
  then
    echo "1"
  elif [ "$1" = "bucketName2" ]
  then
    echo "1"
  elif [ "$1" = "bucketName3" ]
  then
    echo "-1"
  else
    echo "-1"
  fi
}

function put_stream()
{
  if [ "$1" = "bucketName" ] && [ "$2" = "keyName" ]
  then
    echo "1"
  else
    echo "0"
  fi
}

function get_object()
{
  if [ "$1" = "bucketName" ] && [ "$2" = "keyName" ]
  then
    echo "1"
  else
    echo "-1"
  fi
}

function delete_object()
{
  if [ "$1" = "bucketName" ] && [ "$2" = "keyName" ]
  then
    echo "1"
  else
    echo "0"
  fi
}

case $1 in
  -lookup)
  lookup $2
  ;;
  -deletebucket)
  delete_bucket $2
  ;;
  -create)
  create $2
  ;;
  -putstream)
  put_stream $2 $3
  ;;
  -getobject)
  get_object $2 $3
  ;;
  -deleteobject)
  delete_object $2 $3
esac
