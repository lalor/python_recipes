# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-11-07
@contact: jhx1008@gmail.com
version:  1.0
@todo:    zmq wrapper测试
@modify:
"""
import os
import sys
import time
import unittest
import threading

sys.path.append(os.path.split(os.getcwd())[0])
from RDSException import RDSException
from communication.ZMQWrapper import *
from common import Common
from log.RDSLog import RDSLog

out1 = ''
out2 = ''

def serverThread():
  global out1
  server = ZMQServer(ZMQProtocolType.TCP, ZMQSocketType.DEALER, '*', 5000)
  out1 = server.recv()
  server.send('bbb')
  server.close()

def clientThread():
  global out2
  client = ZMQClient(ZMQProtocolType.TCP, ZMQSocketType.DEALER,'127.0.0.1',5000)
  client.send('aaa')
  out2 = client.recv()
  client.close()

class ZMQWrapperTestCase(unittest.TestCase):
  def setUp(self):
    pass

  def tearDown(self):
    log_file = "{0}/rds.log".format(os.getcwd())
    if os.path.exists(log_file):
      os.remove(log_file)

  def testZMQWrapper(self):
    st = threading.Thread(target=serverThread, args=())
    ct = threading.Thread(target=clientThread, args=())
    st.start()
    ct.start()
    st.join()
    ct.join()
    global out1, out2
    self.assertTrue(out1 == 'aaa' and out2 == 'bbb')

if __name__ == "__main__":
  ZMQComm.initContext()
  Common.rdsLog = RDSLog(os.getcwd(),"rds.log",1,3)
  unittest.main()
