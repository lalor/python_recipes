# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-24
@contact: jhx1008@gmail.com
version:  1.0
@todo:    MySQLManager单元测试
@modify:
"""

import os
import sys
import time
import unittest

sys.path.append(os.path.split(os.getcwd())[0])
from database.mysql import Mysql
from storage.nos.nos import Nos

class MysqlTestCase(unittest.TestCase):
  def setUp(self):
    nos = Nos()
    nos.nosTool = "bash {0}/nos/tool.sh".format(os.getcwd())
    self.mysql = Mysql(r'/styx/home/hzjianghongxiang/jianghx/mysql',\
                                  r'/styx/home/hzjianghongxiang/jianghx/mysql/conf/my.cnf', \
                                  'rdsadmin','?lACPAs82IDMs#',r'/styx/home/hzjianghongxiang/jianghx/mysql/backup',nos)
    self.mysql.sudo = False

  def tearDown(self):
    pass

  def redirectOUT(self):
    so = open('/dev/null','a+')
    os.dup2(so.fileno(), sys.stdout.fileno())
  
  def redirectERR(self):
    se = open('/dev/null','a+')
    os.dup2(se.fileno(), sys.stderr.fileno())

  def testInit(self):
    self.assertTrue(self.mysql.user=='rdsadmin')
    self.assertTrue(self.mysql.passwd=='?lACPAs82IDMs#')

  def testGetValue(self):
    configFile = self.mysql.configFile
    f = open(configFile, 'r')
    lines = f.readlines()
    f.close()
    for line in lines:
      # 忽略不符合格式的配置
      if line.find('=') == -1:
        continue
      key=line.split('=')[0].strip()
      if key[0] == '#':
        continue
      value=line.split('=')[1].strip()
      self.assertTrue(self.mysql.getValue(configFile,key) == value)

  def testInstall(self):
    # install测试
    tm = int(time.time())
    backup = "{0}.{1}".format(self.mysql.dataDir, tm)
    
    if os.path.exists(self.mysql.dataDir):
      os.system('mv {0} {1}'.format(self.mysql.dataDir,backup))
      os.system('mkdir {0}'.format(self.mysql.dataDir))

    self.assertTrue(self.mysql.install())
    # 检测mysql_install_db命令执行是否成功
    binlogIndex = "{0}/mysql-bin.index".format(self.mysql.dataDir)
    self.assertTrue(os.path.exists(binlogIndex))
    # 清除运行mysql_install_db产生的文件
    cmd="rm -rf {0}/*".format(self.mysql.dataDir)
    os.system(cmd)
    
    if os.path.exists(backup):
      os.system('rm -rf {0}'.format(self.mysql.dataDir))
      os.system('mv {0} {1}'.format(backup,self.mysql.dataDir))

  def testIsAlive(self):
    self.assertFalse(self.mysql.isAlive())

  def testIsRunning(self):
    self.assertFalse(self.mysql.isRunning())

  def testStart(self):
    self.mysql.install()
    self.assertTrue(self.mysql.start())
      
if __name__ == '__main__':
  unittest.main()
