# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2014-01-24
@contact: jhx1008@gmail.com
version:  1.0
@todo:    nos单元测试，由于单元测试时无法连接nos，需要模拟nos工具进行
@modify:
"""

import os
import sys
import unittest

sys.path.append(os.path.split(os.getcwd())[0])
from storage.nos.nos import Nos

class NosTestCase(unittest.TestCase):
  def setUp(self):
    self.nos = Nos()
    self.nos.nosTool = "bash {0}/nos/tool.sh".format(os.getcwd())

  def tearDown(self):
    pass

  def testLookup(self):
    self.assertTrue(self.nos.lookup('bucketName') == 1)
    self.assertFalse(self.nos.lookup('name') == 1)

  def testDeleteBucket(self):
    # 存在的bucketName
    self.assertTrue(self.nos.deleteBucket('bucketName'))
    # 存在bucketName1，但是删除失败
    self.assertFalse(self.nos.deleteBucket('bucketName1'))
    # 桶不存在，返回True
    self.assertTrue(self.nos.deleteBucket('name'))

  def testMakeBucket(self):
    #桶已经存在
    self.assertTrue(self.nos.makeBucket('bucketName'))
    #桶不存在，创建成功
    self.assertTrue(self.nos.makeBucket('bucketName2'))
    #桶不存在，创建失败
    self.assertFalse(self.nos.makeBucket('bucketName3'))

  def testPutString(self):
    self.assertTrue(self.nos.putString('', 'bucketName', 'keyName'))
    self.assertFalse(self.nos.putString('', 'bucket', 'key'))

  def testGetString(self):
    self.assertTrue(self.nos.getString('name','key') == 'null')
    self.assertTrue(self.nos.getString('bucketName', 'keyName') == '1' )

  def testLookupFile(self):
    self.assertTrue(self.nos.lookupFile('bucketName','key'))
    self.assertFalse(self.nos.lookupFile('bucket', 'key'))

  def getFileSize(self):
    self.assertTrue(self.nos.lookupFile('bucketName','key') == 1)
    self.assertTrue(self.nos.lookupFile('bucket','key') == -1)

if __name__ == '__main__':
  unittest.main()
