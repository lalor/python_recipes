# -*- coding:UTF-8 -*- 
"""
@author:  hzjianghongxiang
@date:    2013-12-31
@contact: jhx1008@gmail.com
version:  1.0
@todo:
@modify: RDSConfig单元测试
"""

import os,sys
import unittest
sys.path.append(os.path.split(os.getcwd())[0])
from RDSException import RDSException
from config.RDSConfig import *
from config.RDSConfigParser import RDSConfigParser
from config.RDSOptionParser import RDSOptionParser

class RDSConfigTestCase(unittest.TestCase):
  def setUp(self):
    pass

  def tearDown(self):
    pass

  def testSetManagerIP(self):
    self.assertRaises(RDSException,RDSConfig.setManagerIP,'258.1.1.0')
    self.assertRaises(RDSException,RDSConfig.setManagerIP,'192.168.0')
    self.assertRaises(RDSException,RDSConfig.setManagerIP,'a.b.c')
    self.assertTrue(RDSConfig.setManagerIP('192.168.0.1'))
    self.assertTrue(RDSConfig.managerIP == '192.168.0.1')

  def testSetManagerPort(self):
    self.assertRaises(RDSException, RDSConfig.setManagerPort, '555555')
    self.assertRaises(RDSException, RDSConfig.setManagerPort, 'aaa')
    self.assertTrue(RDSConfig.setManagerPort('3333'))
    self.assertTrue(RDSConfig.managerPort == 3333)

  def testSetAgentVersion(self):
    version='1.1'
    RDSConfig.setAgentVersion(version)
    self.assertTrue(RDSConfig.agentVersion == version)

  def testSetAgentRoot(self):
    pwd=os.getcwd()
    self.assertRaises(RDSException, RDSConfig.setAgentRoot, '{0}/abcd'.format(pwd))
    self.assertTrue(RDSConfig.setAgentRoot(pwd))
    self.assertTrue(RDSConfig.agentRoot == pwd)

  def testSetLogLevel(self):
    levels={'wrong','debug','info','warning','error','DEBUG','INFO','WARNING','ERROR'}
    for level in levels:
      if level == 'wrong':
        self.assertRaises(RDSException, RDSConfig.setLogLevel, level)
      else:
        self.assertTrue(RDSConfig.setLogLevel(level))
        self.assertTrue(RDSConfig.logLevel == level.upper())

  def testSetLogDir(self):
    pwd=os.getcwd()
    self.assertRaises(RDSException, RDSConfig.setLogDir, '{0}/abcd'.format(pwd))
    self.assertTrue(RDSConfig.setLogDir(pwd))
    self.assertTrue(RDSConfig.logDir == pwd)

  def testSetLogExpire(self):
    self.assertRaises(RDSException, RDSConfig.setLogExpire, 0)
    self.assertTrue(RDSConfig.setLogExpire(10))
    self.assertTrue(RDSConfig.logExpire == 10)

  def testSetLogFile(self):
    name = 'aaa'
    RDSConfig.setLogFile(name)
    self.assertTrue(RDSConfig.logFile == name)

  def testSetUnifyLog(self):
    name = 'bbb'
    RDSConfig.setUnifyLog(name)
    self.assertTrue(RDSConfig.unifyLog == name)

  def testSetMysqlUser(self):
    user='root'
    RDSConfig.setMysqlUser(user)
    self.assertTrue(RDSConfig.mysqlUser == user)

class RDSConfigManagerTest(unittest.TestCase):
  
  def setUp(self):
    pass

  def tearDown(self):
    pass

  def testInitByConfigParser(self):
    l=["[mysqld]","first=1"]
    fileName='testInitByConfigParser.cnf'
    f = open(fileName,'w+')
    f.write('\n'.join(l))
    f.close()
    cfgParser = RDSConfigParser(fileName)
    self.assertRaises(RDSException, RDSConfigManager.initByConfigParser, cfgParser)
    os.remove(fileName)

    l=["[agent]","aaaa=1"]
    fileName='testInitByConfigParser.cnf'
    f = open(fileName,'w+')
    f.write('\n'.join(l))
    f.close()
    cfgParser = RDSConfigParser(fileName)
    self.assertRaises(RDSException, RDSConfigManager.initByConfigParser, cfgParser)
    os.remove(fileName)

    l=["[agent]","manager_ip=192.168.0.1"]
    fileName='testInitByConfigParser.cnf'
    f = open(fileName,'w+')
    f.write('\n'.join(l))
    f.close()
    cfgParser = RDSConfigParser(fileName)
    RDSConfigManager.initByConfigParser(cfgParser)
    self.assertTrue(RDSConfig.managerIP == "192.168.0.1")
    os.remove(fileName)

if __name__ == "__main__":
  unittest.main()
