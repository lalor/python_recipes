#!/usr/bin/python
#-*- coding: UTF-8 -*-
"""
upgrade rds Agent
"""
import argparse
import re
import os
import paramiko
import time
import stat
import shutil
from functools import partial
from multiprocessing.dummy import Pool as ThreadPool
import pdb
#from multiprocessing import Pool as ThreadPool
from ConfigParser import SafeConfigParser
from collections import defaultdict


KEY = '/home/rds-user/upgradeAgent/.rds-agent-pri-yanlian'
GET_MD5SUM = 'md5sum /home/rds-user/log/heartBeat.log'


class BColors:
    """ colorful output"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'


def main():
    """main function"""
    # 解析命令行参数
    parser = _argparse()

    # 解析ip列表
    ips = get_ip_list(parser.iplist)

    # 解析升级工具的配置文件
    if parser.tools:
        tools = parse_tool_cnf(parser.tools)
    else:
        tools = defaultdict(dict)

    pool = ThreadPool(100)
    # upgrade agent in their own threads
    # and return the results
    results = pool.map(partial(upgrade, tools, parser.agent), ips)
    #close the pool and wait for the work to finish
    pool.close()
    pool.join()
    print_result(results)


def _argparse():
    """ parse command-line arguments """
    parser = argparse.ArgumentParser('Just for upgrading agent of RDS')
    parser.add_argument('-i', action='store', dest='iplist', help='ip list file')
    parser.add_argument('-r', action='store', dest='agent', help='rdsAgent file')
    parser.add_argument('-t', action='store', dest='tools', help='config file for upgrade tools')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    cmdline = parser.parse_args()
    if not cmdline.iplist:
        parser.error("ip list file not given")

    if not cmdline.agent:
        parser.error("rdsAgent file not given")

    return cmdline


def get_ip_list(iplist):
    """get ip list from file"""
    ips = []
    with (open(iplist)) as f_handle:
        for line in f_handle:
            line = line.strip()
            if is_a_ip(line):
                ips.append(line)
            else:
                print ("{0}{1} is not a ip address, skip it...{2}".format(
                    BColors.WARNING, line, BColors.ENDC))
    return ips


def is_a_ip(ipaddress):
    """ Is a ip is a real ip address"""
    if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", ipaddress):
        return True
    else:
        return False


def parse_tool_cnf(cnf_file):
    """ parse """
    try:
        # Is file exists
        os.path.getsize(cnf_file)
        parser = SafeConfigParser(allow_no_value=True)
        parser.read(cnf_file)
        tools = defaultdict(dict)
        for section_name in parser.sections():
            tools[section_name]['name'] = section_name
            tools[section_name]['source'] = parser.get(section_name, 'source')
            os.path.getsize(tools[section_name]['source'])
            tools[section_name]['target'] = parser.get(section_name, 'target')
        return tools
    except Exception, err:
        raise err


def upgrade(tools, agent, ipaddress):
    """ upgrade """
    try:
        print "{0}".format(ipaddress)
        with get_conn(ipaddress) as ssh:
            # 升级工具
            upgrade_tool(ssh, tools)
            # 升级Agent
            upgrade_agent(ssh, agent)
            # 检查心跳
            return check_heartbeat(ssh, ipaddress)
    except Exception, err:
        return ipaddress, False, str(err)


def get_conn(ipaddress, port=1046, username='rds-user'):
    """get connection"""
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ipaddress, port, username, key_filename=KEY)
    return ssh


def upgrade_tool(ssh, tools):
    """upgrade tools"""
    with ssh.open_sftp() as sftp:
        """
        {'name':mysqlbinlog, 'source':'/home/rds-user/test/lmx.txt',
        'target':'/usr/bin/local/lmx.txt'}
        """
        for item in tools.values():
            rpath, rfile = os.path.split(item['target'])
            # 上传文件
            sftp.put(item['source'], rfile)
            # 加上可执行权限
            ssh.exec_command("sudo chmod +x {0}".format(rfile))
            # 将文件移到相应目录
            ssh.exec_command("sudo mv {0} {1}".format(rfile, rpath))


def upgrade_agent(ssh, agent):
    """upgrade agent"""

    # 将文件打包
    target_alias = "NewAgent"
    with ssh.open_sftp() as sftp:
        # 上传Agent
        sftp.put(agent, agent)
        # 更新Agent
        cmds = ["tar -zxf {0}".format(agent),
                "rm {0}".format(agent),
                "[ -e AgentBackup ] && rm -rf AgentBackup",
                "mv  rdsAgent AgentBackup",
                "mv {0} rdsAgent".format(target_alias),
                "cp AgentBackup/config/RDSAgent.cnf rdsAgent/config/RDSAgent.cnf",
                "cat ~/log/agent.pid  | xargs kill -9",
                "~rds-user/rdsAgent/agent_safe"]

        # 远程执行命令
        for cmd in cmds:
            _, stdout, _ = ssh.exec_command(cmd)
            a = stdout.readlines()


#def check_agent(agent):
#    """ check agent """
#    agent_safe = "{0}/agent_safe".format(agent)
#    try:
#        if not  os.path.isdir(agent):
#            raise Exception("{0} is not a directory".format(agent))
#        if not os.path.isfile("{0}".format(agent_safe)):
#            raise Exception("{0} not found".format(agent_safe))
#        sat = os.stat(agent_safe)
#        os.chmod(agent_safe, sat.st_mode | stat.S_IEXEC)
#    except Exception, err:
#        raise err


def check_heartbeat(ssh, ipaddress):
    """ check heartbeat """
    try:
        first = get_md5sum(ssh)
        time.sleep(2)
        second = get_md5sum(ssh)

        if first != second:
            return ipaddress, True, "success"
        else:
            return ipaddress, False, "md5sum is equal"
    except Exception, err:
        return ipaddress, False, "check heartbeat error: {0}".format(err)


def get_md5sum(ssh):
    """ get md5sum for agent"""
    _, stdout, _ = ssh.exec_command(GET_MD5SUM)
    res = stdout.readlines()[0]
    return res.split()[0]


def print_result(results):
    """print result"""
    success = [item for item in results if item[1] is True]
    fail = [item for item in results if item[1] is False]

    print "*" * 80
    print BColors.OKGREEN + "\n".join([x[0] for x in success]) + BColors.ENDC

    print "*" * 80
    print BColors.FAIL + "\n".join(["{0:<15}:{1}".format(x[0], x[2]) for x in fail]) + BColors.ENDC

    print "*" * 80
    print (BColors.BOLD + BColors.HEADER + "total:{0} success:{1} fail:{2}".format(len(results),
            len(success), len(fail)) + BColors.ENDC)


if __name__ == '__main__':
    main()
