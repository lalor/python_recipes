"""
doc
"""
#!/usr/bin/python
#-*- coding: UTF-8 -*-

from __future__ import print_function


class ATest:
    """
    class defination
    """
    def __init__(self):
        self.name = 'laimigxing'

    def method(self):
        """ test method """
        print('print function : %s' % self.name)

    def test(self):
        """ test method"""
        print('print function : %s' % self.name)


def main():
    """ main function"""
    instance = ATest()

    func = getattr(instance, 'method', 'default')
    func()

if __name__ == '__main__':
    main()
