#!/usr/bin/python
#-*- coding: UTF-8 -*-
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--port', action="store", nargs='?', default=5678, type=int)
parser.add_argument('--host', action="store", nargs='?', default='localhost')
opts = parser.parse_args()
print opts.port
print opts.host
