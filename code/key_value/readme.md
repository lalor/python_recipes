# A key-value storage


服务端: kv_server --host [ADDRESS] --port [PORT]

1. 实现SET命令

    SET key value 将字符串值 value 关联到 key, 如果 key 已经持有其他值， SET
就覆写旧值

2. 实现GET命令

    GET key 返回 key 所关联的字符串值. 如果 key 不存在那么返回空。

3. 实现AUTH命令

    AUTH username password 服务器启动时读取当前目录下的配置文件auth.conf,
    里面记录了若干组用户和密码.  当用户密码匹配时, 返回0; 当用户密码不匹配时, 返回-1.

4. 实现URL命令

     URL name url 当AUTH通过后才能运行此命令, 否则返回空.
     name是一个key, 输入此命令后, 如果这个key已持有值, 则返回这两个值.
     如果没有, 则server去拿到URL的HTTP状态和文件大小. 将这两个值关联到 name 。

5. 监听地址由--host 和 --port 指定, 默认为localhost:5678

     客户端 kv_client CMD [PARAMETERS...]
     1. 运行后默认连接localhost:5678
     2. 运行示例 ./kv_client set foo bar ./kv_client get foo #显示bar
     3. 运行失败的返回值为1并在错误输出中显示原因，成功则为0
