#!/usr/bin/python
#-*- coding: UTF-8 -*-

from time import ctime
from SocketServer import TCPServer, StreamRequestHandler, ThreadingMixIn
import shelve

class MyServer(ThreadingMixIn, TCPServer):
    pass

class MyInstance():
    def __init__(self, filename):
        self.db = shelve.open(filename)
        self.authorize = False

    def __del__(self):
        self.db.close()

    def Auth(self, username, password):
        #read conf file and auth
        self.authorize = True
        return "login successful"

    def isAuth(self):
        return self.authorize

    def dumpData(key, val):
        self.db[key] = val

    def loadData(key):
        try:
            val = self.db[key]
        except KeyError:
            val = ''
        return val


class MyHandler(StreamRequestHandler):
    def handle(self):
        instance = MyInstance("data.dat")

        while not instance.isAuth():
            print '...Connected from :', self.client_address
            s = self.rfile.readline()
            l = s.split()
            if (len(l) == 3) and (l[0].upper() == 'AUTH'):
                try:
                    self.wfile.write('[%s] %s' %(ctime(), instance.Auth(l[1], l[2]) ))
                except Exception, e:
                    print ("Ignore %s.", str(e))
            else:
                try:
                    self.wfile.write('[%s] %s' %(ctime(), "please login first..."))
                except Exception, e:
                    print ("Ignore %s.", str(e))

        while True:
            #print '...Connected from :', self.client_address
            s = self.rfile.readline()
            self.wfile.write('[%s] %s' %(ctime(), s))

        #parseAndAnswer(s)
        #self.wfile.write('[%s] %s' %(ctime(), s))

        #DumpData('laimingxing', 'xmu')
        #DumpData('wangkai', 'hdu')
        #DumpData('tuple', (200, 4321))

        #print '*** display information'
        #print LoadData('laimingxing')
        #print LoadData('wangkai')
        #print LoadData('jy')
        #print LoadData('tuple')


def main():
    tcpServer = MyServer(('', 21567), MyHandler)
    tcpServer.serve_forever()

    """main function"""
if __name__ == '__main__':
    main()
