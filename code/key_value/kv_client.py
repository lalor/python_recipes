#!/usr/bin/python
#-*- coding: UTF-8 -*-

import socket
import sys


def main():
    port = 5678
    host = "localhost"
    tcpCliSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ADDR = (host, port)
    tcpCliSock.connect(ADDR)

    # command mode
    if len(sys.argv) > 1:
        data = ' '.join( [ str(n) for n in sys.argv[1:] ] )

        username = raw_input('username:')
        password = raw_input('password:')
        tcpCliSock.send( 'auth ' + username + ' ' + password)
        tcpCliSock.recv(1024)

        tcpCliSock.send(data)
        data = tcpCliSock.recv(1024)
        print data
        tcpCliSock.close()
        if 'None' in data or 'Illegal' in data:
           sys.exit(1)
        else:
           sys.exit(0)

    #interactive mode
    print """
    Welcome to Mingxing LAI's key-value database.
    Usage:
        SET key value for storing data
        GET key for loading data
        URL website's url for return code
        AUTH username password for logining
            eg: AUTH test test
        QUIT for exit.  """
    while True:
        data = raw_input('>')
        if not data:
            continue
        elif data.strip().upper().startswith('QUIT'):
            print "bye"
            break
        else:
            tcpCliSock.send(data)
            data = tcpCliSock.recv(1024)
            print data
    tcpCliSock.close()


if __name__ == '__main__':
    main()
