#!/usr/bin/python
#-*- coding: UTF-8 -*-
from time import ctime
from threading import Thread
import shelve
import socket
import urllib2
import argparse
import sys



class MyInstance():
    def __init__(self, filename):
        self.db = shelve.open(filename)
        self.authorize = False
        self.user = []
        try:
            f = open('auth.conf', 'r')
            for line in f:
                if len(line.strip().split(':')) != 2 :
                    continue
                person = ( line.strip().split(':')[0],line.strip().split(':')[1])
                self.user.append(person)
        except IOError, e:
            print str(e)
            sys.exit(1)

    def __del__(self):
        self.db.close()

    def Auth(self, username, password):
        person = (username, password)
        if person in self.user:
            self.authorize = True
            return "login successful"
        else:
            return "login failed"

    def isAuth(self):
        return self.authorize

    def dumpData(self, key, val):
        try:
            self.db[key] = val
            return "Successful"
        except Exception, e:
            return str(e)

    def loadData(self, key):
        try:
            val = self.db[key]
        except KeyError:
            val = ''
        return str(val)


def handlechild(clientsock):
    instance = MyInstance("data.dat")

    while not instance.isAuth():
        s = clientsock.recv(1024)
        l = s.split()

        try:
            if (len(l) == 3) and (l[0].upper() == 'AUTH'):
                clientsock.sendall('[%s] %s' %(ctime(), instance.Auth(l[1], l[2]) ))
            else:
                clientsock.sendall('[%s] %s' %(ctime(), "please login first..."))
        except Exception, e:
            #print ("Ignore %s.", str(e))
            break

    while True:
        data = clientsock.recv(1024)
        l = data.split()

        if (len(l) == 2) and (l[0].upper() == 'GET'):
            res = instance.loadData(l[1])

        elif (len(l) == 3) and (l[0].upper() == 'SET'):
            res = instance.dumpData(l[1], l[2])

        elif (len(l) == 3) and (l[0].upper() == 'AUTH'):
            res = "you have login."

        elif (len(l) == 3) and (l[0].upper() == 'URL'):
            res = instance.loadData(l[1])
            if not res:
                try:
                    urlObj = urllib2.urlopen(l[2])
                    val = (urlObj.code, len(urlObj.read()))
                    instance.dumpData( l[1], val)
                    res = instance.loadData(l[1])
                except ValueError, e:
                    res = str(e)

        else:
            res = 'unkown command or illegal arguments'

        if not res:
            res = "None"

        try:
            clientsock.sendall(res)
        except Exception, e:
            #print str(e)
            break

    clientsock.close()





def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', action="store", nargs='?', default=5678, type=int)
    parser.add_argument('--host', action="store", nargs='?', default='localhost')
    opts = parser.parse_args()
    host = opts.host
    port = opts.port
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((host, port))
    sock.listen(100)

    while True:
        try:
            clientsock, clientaddr = sock.accept()
        #except (KeyboardInterrupt, SystemExit):
        #    raise
        except:
            break

        t = Thread(target=handlechild, args=[clientsock])
        #t.setDaemon(1)
        t.start()


if __name__ == '__main__':
    main()
