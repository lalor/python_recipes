#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
The mere presence of the yield statement in a function turns it into a
generator, Unlick a normal function, a generator only runs in response to
iteration. 
"""


def frange(start, stop, increment):
    x = start
    while x < stop:
        yield x
        x += increment


print list(frange(0, 1, 0.125))
a = frange(0, 1, 0.125)

# generator
print type(a)
