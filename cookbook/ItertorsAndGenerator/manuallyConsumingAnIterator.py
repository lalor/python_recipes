#!/usr/bin/python
# -*- coding: UTF-8 -*-



with open('/etc/passwd') as f:
    try:
        while True:
            line = next(f)
            print(line, end='')
    except StopIteration:
        pass
