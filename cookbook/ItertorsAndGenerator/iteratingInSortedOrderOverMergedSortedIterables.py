#!/usr/bin/python
# -*- coding: UTF-8 -*-

import heapq

a = [i for i in range(1, 10, 2)]
b = [i for i in range(1, 10, 3)]

c = heapq.merge(a, b)

print list(c)
