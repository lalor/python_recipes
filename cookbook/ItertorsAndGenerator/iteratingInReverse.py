#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
you want to iterate in reverse over a sequence, implement a __reversed__()
special method
"""


class CountDown:
    def __init__(self, start):
        self._start = start

    def __iter__(self):
        """ forward iterator"""
        n = self._start
        while n > 0:
            yield n
            n -= 1

    def __reversed__(self):
        """ reverse iterator"""
        n = 1
        while n <= self._start:
            yield n
            n += 1
