# chapter 1

## 1.1 Unpacking Sequence into Separate variables

    p = (4, 5)
    x, y = p


## 1.2 Unpacking elements from iterables of Arbitrary Length

    x = [ i for i in range(5)]
    first, *middle, last = x # for python 3


## 1.3 Keeping the Last N Items

You want to keep a limited history of the last few items seen during iteration
of during some other kind of processing.

Keep a limited history is a perfect use for a collections.deque

    x = [ i for i in range(5)]
    q = deque(x, maxlen=3)

    In [9]: q
    Out[9]: deque([2, 3, 4], maxlen=3)

# 1.4 Findint the largest or smallest N items

The heapq module has two functions - nlagest() and nsmallest(), that do exactly
what you want.

    In [12]: x = [ random.randint(1, 100) for i in range(5) ]

    In [13]: x
    Out[13]: [72, 55, 69, 74, 91]

    In [14]: heapq.nlargest(3, x)
    Out[14]: [91, 74, 72]

# 1.5 Implementing a Priority Queue

the core of this recipe concerns the use of the heapq module.

    import heapq
    class PriorityQueue:
        def __init__(self):
            self._queue = []
            # The role of the index variable is to properly order items with the
            # same priority level.
            self._index = 0

        def push(self, item, priority):
            heapq.heappush(self._queue, (-priority, self._index), item)
            self._index +=1

        def pop(self):
            return heapq.heappop(self._queue)[-1]

# 1.6 Mapping Keys to Multiple Values in a Dictionary

    d = {}
    for key, value in pairs:
        if key not in d:
            d[key] = []
        d[key].append(value)

    d = defaultdict(list)
    for key, value in pairs:
        d[key].append(value)

# 1.7 Keeping Dictionaries in Order

To control the order of items in a dictionary, you can use an OrderedDict from
the collections module. It exactly preserves the original insertion order of
data when iterating.


Be aware that the size of an OrderedDict is more than twice as large as a
normal dictionary due to the extra linked list that's created.

# 1.8 Calculating with Dictionaries

Consider a dictionary that maps stock names to prices:

    prices = {
    'ACME': 45.23,
    'AAPL': 612.78,
    'IBM': 205.55,
    'HPQ': 37.20,
    'FB': 10.75
    }

In order to perform useful calculations on the dictionary contents, it is often useful to
invert the keys and values of the dictionary using zip(). For example, here is how to
find the minimum and maximum price and stock name:

    min_price = min(zip(prices.values(), prices.keys()))
    # min_price is (10.75, 'FB')

    max_price = max(zip(prices.values(), prices.keys()))
    # max_price is (612.78, 'AAPL')

Similarly, to rank the data, use zip() with sorted(), as in the following:

    prices_sorted = sorted(zip(prices.values(), prices.keys()))
    # prices_sorted is [(10.75, 'FB'), (37.2, 'HPQ'),

either:

    min(prices, key=lambda k: prices[k[)
    max(prices, key=lambda k: prices[k[)

# 1.9 Finding Commonalities in tow dictionaries

A little-known feature of keys views is that they also support common set operations such as unions, intersections, and differences.

    a.keys() & b.keys() # for python 3
    set(a.keys() & b.keys()) # for python 2.7

# 1.10 Removing Duplicates from a Sequence while Maintaining Order

if all you want to do is eliminate duplicates, it is ofter easy enough to make a set.

    a = [ random.randint(1, 10) for x in range(100) ]
    set(a)

You want to eliminate the duplicate values in a sequence, but preserve the order of the remaining items.

    def dedupe(items, key=None):
        seen = set()
        for item in items:
            val = item if key is None else key(item)
            if val not in seen:
                yield item
                seen.add(val)

# 1.11 naming a Slice

    SHARES = slice(20, 32)
    PRICE = slice(40, 48)
    cost = int(record[SHARES] * flat(record[PRICE])

# 1.12 Determining the Most Frequently Occurring items in a sequence

The collections.Counter class is designed for just such a problem. It even comes with a handy most_common() method that will give you the answer.

    words = [
    'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
    'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
    'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
    'my', 'eyes', "you're", 'under'
    ]
    from collections import Counter
    word_counts = Counter(words)
    top_three = word_counts.most_common(3)
    print(top_three)
    # Outputs [('eyes', 8), ('the', 5), ('look', 4)]

# 1.13 Sorting a List of Dictionaries by a common Key

It's fairly easy to output these rows ordered by andy of the fields common to all of the dictionaries.

    from operator import itemgetter
    rows_by_fname = sorted(rows, key=itemgetter('fname'))
    rows_by_uid = sorted(rows, key=itemgetter('uid'))

    print(rows_by_fname)
    pritn(rows_by_uid)

The itemgetter() function can also accept multiple keys.

# 1.14 Sorting Objects Without Native Comparison Support

* use lambda

        sorted(users, key=lambda u:u.user_id)

* attrgetter

        sorted(users, key=attrgetter('user_id'))

# 1.15 Grouping Records Together Based on a Field

the itertools.groupby() functions is particularyly userful for grouping data together like this.

    rows = [
    {'address': '5412 N CLARK', 'date': '07/01/2012'},
    {'address': '5148 N CLARK', 'date': '07/04/2012'},
    {'address': '5800 E 58TH', 'date': '07/02/2012'},
    {'address': '2122 N CLARK', 'date': '07/03/2012'},
    {'address': '5645 N RAVENSWOOD', 'date': '07/02/2012'},
    {'address': '1060 W ADDISON', 'date': '07/02/2012'},
    {'address': '4801 N BROADWAY', 'date': '07/01/2012'},
    {'address': '1039 W GRANVILLE', 'date': '07/04/2012'},
    ]
Now suppose you want to iterate over the data in chunks grouped by date. To do it, first
sort by the desired field (in this case, date) and then use itertools.groupby():

    from operator import itemgetter
    from itertools import groupby

    # Sort by the desired field first
    rows.sort(key=itemgetter('date'))

    # Iterate in groups
    for date, items in groupby(rows, key=itemgetter('date')):
    print(date)
    for i in items:
    print(' ', i)

This produces the following output:

        07/01/2012
        {'date': '07/01/2012', 'address': '5412 N CLARK'}
        {'date': '07/01/2012', 'address': '4801 N BROADWAY'}
        07/02/2012
        {'date': '07/02/2012', 'address': '5800 E 58TH'}
        {'date': '07/02/2012', 'address': '5645 N RAVENSWOOD'}
        {'date': '07/02/2012', 'address': '1060 W ADDISON'}
        07/03/2012
        {'date': '07/03/2012', 'address': '2122 N CLARK'}
        07/04/2012
        {'date': '07/04/2012', 'address': '5148 N CLARK'}
        {'date': '07/04/2012', 'address': '1039 W GRANVILLE'}

## 1.16 Filtering Sequence Elements

1. use a list comprehension
2. put the filtering code into its own function and use the built-in filter()

## 1.17 Extracting a subset of a dictionary

use a dictionary comprehension.

    p = { key:value for key, vlaue in prices.items() if value > 200 }

## 1.18 use Namedtuple

## 1.19 Transforming and reducing data at the same time

    nums = [1, 2, 3, 4, 5]
    s = sum(x * x for x in nums)

Determine if any .py files exist in a directory

    import os
    files = os.listdir('dirname')
    if any(name.endswith('.py') for name in files):
        print('There be python!')
    else:
        print('Sorry, no python.')

Output a tuple as CSV

    s = ('ACME', 50, 123.45)
    print(','.join(str(x) for x in s))

## 1.20 Combing Multiple Mappings into a Single Mapping

Suppose you want to perform lookups where you have to check both dictionaries. An easy way to do this is to use the ChainMap class from the collections module.

    from collections import ChainMap
    c = ChainMap(a, b)

# Chapter 2 Strings and Text

## 2.1 Spliting Strings on Any of Multiple Delimiters

You need to split a string into fields, but the delimiters aren't consistent throughout the string.

    line = 'asdf fjdk; afed, fjek,asdf, foo'
    import re
    re.split(r'[;,\s]\s*', line)
    ['asdf', 'fjdk', 'afed', 'fjek', 'asdf', 'foo']

## 2.2 use startwith() and endwith()

1. multiple choices
If you need to check against multiple choices, simply provide a tuple of possibilities to startwith() or endwith():

    [ name for name in filenames if name.endwith('.c', '.h')) ]

2. this is one part of python where a tuple is actually required as input

        choices = ['http:', 'ftp:']
        url.startwith(choices) # Exception
        url.startwith(tuple(choices)) # OK

## 2.3 Matching String Using Shell Wildcard Patterns

Introduction of fnmatch

* fnmatch
* fnmatchcase
* filter

        [ name for name in names if fnmatch(name, 'Dat*.csv')]

## 2.4 Matching and Searching for Text Patterns(re)

* re.match
* precompile

        datepat = re.compile(r'\d+/\d+/\d+')
        datepat.match(text1)

* findall

## 2.5 Searching and Replacing text (re.sub)

For simple literal patterns, use the str.replace() method, for more complicated patterns, use sub() functions in the re module.

    >>> text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
    >>> import re
    >>> re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2', text)
    'Today is 2012-11-27. PyCon starts 2013-3-13.'

## 2.6 Searching and replacing Case-Insensitive Text(re.IGNORECASE)

iTo perform case-insensitive text operations, you need to use the re module and supply
the re.IGNORECASE flag to various operations. For example:

    >>> text = 'UPPER PYTHON, lower python, Mixed Python'
    >>> re.findall('python', text, flags=re.IGNORECASE)
    ['PYTHON', 'python', 'Python']
    >>> re.sub('python', 'snake', text, flags=

## 2.7  Specifying a regular expression for the shortest match(?)

    text2 = 'Computer says "no." Phone says "yes."'
    str_pat.findall(text2)
    ['no." Phone says "yes.']

In this example, the pattern r'\"(.*)\"' is attempting to match text enclosed inside
quotes. However, the * operator in a regular expression is greedy, so matching is based
on finding the longest possible match.

To fix this, add the ? modifier after the * operator in the pattern, like this:

    str_pat = re.compile(r'\"(.*?)\"')
    str_pat.findall(text2)
    ['no.', 'yes.']

## 2.8 Writing a Regular Expression for Multiline patterns

Dot(.) to match any character but forget to account for the fact that it doesn't match newlines.

The re.compile() function accepts a flag, re.DOTALL, which is useful here. It makes the . in a regular expression match all characters, including newlines. For examples:

    comment = re.compiler(r'/\*(.*?)\*/', re.DOTALL)
    comment.findall(text2)

## 2.11 Stripping Unwanted Characters from Strings(lstrip(), rstrip())

* advanced

It is often the case that you want to combine string stripping operations with some other kind of iterative processing, such as readling lines of data from a file. If so, this is one area where a generator expression can be useful.

    with open(filename) as f:
        lines = (line.strip() for line in f)
        for line in lines:
        ...
## 2.12 Sanitizing and Cleaning up text

    >>> remap = {
    ... ord('\t') : ' ',
    ... ord('\f') : ' ',
    ... ord('\r') : None # Deleted
    ... }
    >>> a = s.translate(remap)
    >>> a
    'pýtĥöñ is awesome\n'


## 2.13 Aligning Text Strings(ljust, rjust, center, format)

## 2.14 Combing and Concatenating Strings( join, +, format)

## 2.15 interpolating variables in strings(format, vars())

## 2.16 reformatting text to a fixed number of columns(textwrap)

    pritn (textwrap.fill(text, 70, initial_indent='    '))


# Chapter 3 Numbers, Dates, and Times

## 3.1 Rounding numerical values

* round
* format

## 3.2 Performing Accurate Decimal Calculations

    a = 4.2
    b = 2.1
    (a + b) == 6.3 # False

* decimal

## 3.3 Formatting number for output(format)

## 3.4 working with binary, octal, and hexadecimal integer

* bin
* oct
* hex

## 3.7 working with infinity and nans

* float('inf')
* float('-inf')
* float('-nan')

## 3.10 performing matrix and linear algebra calculations(numpy)

## picking things at random

* random
* sample
* choice
* shuffle
* randint


# Chapter 4 Iterators and Generators

## 4.1 Manualy Consuming an Iterator

1. catch the StopIteration exception
2. line  = next(f, None)

## 4.2 Delegating Iteration

Typically, all you needd to to is define an `__iter__()` method that delegates iteration to the internally held container.

## 4.3 generator

The mere presence of the yield statement in a function truns it into a generator.

## 4.5 iterating in reverse


        x = [ i for i in range(5) ]
        list(reversed(x))


Many programmers don't realize that reversed iteration can be customized on user-defined classes if they implement the __reversed__() method.


## 4.4 Defining generator functions with extra state

you can easily implement it as a class, putting the generator function code in the __iter__() method.


## 4.5 taking a slice of an iterator(islice)


    def count(n):
        while True:
            yield n
            n += 1

    c = count(0)
    c[10:20] # error


    for x in itertools.islice(c, 10, 20):
        print(x)


## 4.6 Skipping the First part of an iterable(dropwhile)

    from itertools import dropwhile
    with open('/etc/passwd') as f
        for line in dropwhile(lambda line: line.startswith('#'), f):
            print line


## 4.7 iterating over all possible combination or permutations(permutations)

    items = ['a', 'b', 'c']
    from itertools import permutations
    for p in permutations(items):
        print(p)


## 4.10 iterating over the index-value pairs of a sequence(enumerate)


## 4.11 iterating over multiple sequences simultaneously(zip)

## 4.12 iterating on items in separate containers(chain)

## 4.14 flattening a nested sequence

This is easily solved by writing a recursive generator function involving a yield from statement.

## 4.15 iterating in Sorted Order Over merged sorted iterables

the heapq.merge() function does exactly what you want

    import heapq
    a = [1, 4, 7, 10]
    b = [2, 5, 6, 11]
    for c in heapq.merge(a, b):
        print(c)


#chapter 5 Files and IO

## 5.2 priting to a File(print function)

## 5.3 printing with a different separator or line ending(sep, end)

## 5.4 reading and writing binary data(rb, wb)

## 5.5 writing to a file that doesn't already exist(x mode)

## 5.6 Performing IO operations on a string(io.StringIO)

## 5.7 Reading and Writing Compressed Datafiles(gzip, bz2)

## 5.11 Manipulating pathnames(os.path.basename, join, expanduser)

## 5.12 Testing for the existence of a file(isfile, exists)

## 5.13 Getting a Directory Listing (listdir)

    import os
    names = os.listdir('somedir')

    # Get all regular files
    names = [name for name in os.listdir('somedir')
    if os.path.isfile(os.path.join('somedir'), name) ]

    # Get all dirs
    dirnames = [name for name in os.listdir('somedir')
    if os.path.isdir(os.path.join('somedir'), name) ]

    pyfiles = [name for name in os.listdir('somedir')
    if name.endswith('.py') ]

    import glob
    pyfiles = glob.glob('somedir/*.py')

    from fnmatch import fnmatch
    pyfiles = [name for name in os.listdir('somedir')
    if fnmatch(name, "*.py")]


## 5.19 Making Temporary Files and Directories(tempfile)

## 5.21 serializing python object(pickle)


# chapter 6 Data Encoding and Processing

## 6.1 Reading and Writing CSV Data(csv)

* csv.reader(f)
* csv.writer(f)

Since such indexing can often be confusing, this is one place where you might
want to consider the use of named tuples. for example:

    from collections import namedtuple
    with open('stock.csv') as f:
        f_csv = csv.header(f)
        headings = next(f_csv)
        Row = namedtuple('Row', headings)
        for r in f_csv:
            row = Row(*r)

## 6.2 Reading and Writing JSON Data

* dumps
* loads
* dump(file)
* load(file)

## 6.3 Parsing Simple XML Data(xml.etree.ElementTree)


    from urllib2 import urlopen
    from xml.etree.ElementTree import parse

    u = urlopen('http;//planet.python.org/rss20.xml')
    doc = parse(u)

    for item in doc.itemfind('channel/item'):
        title = item.findtext('title')
        date = item.findtext('pubDate')
        link = item.findtext('link')

## 6.5 Turning a Dictionary into XML

Although the `xml.etree.ElementTree` library is commonly used for parsing. it
can also be used to create XML documents.

## 6.6 Parsing, Modifying, and Rewriting XML(xml.etree.ElementTree)

## 6.8 Interacting with a Relational database

## 6.13 Summarizing Data and Performing Statistics(Pandas)


# chapter 10 Modules and Packages

## 10.1 making a Hierarchical package of modules

just create a empty `__init__` file in direcotry.

## 10.2 Controlling the import of everything

if `__all__` is defined, then only the names explicitly listed will be exported




# chapter 13

## 13.1 Accepting Script Input vis Redirection, Pipes, or Input Files(fileinput)

## 13.2 Terminating a program with an Error Message

    raise SystemExit("It failed")

    import sys
    sys.stderr.write('it failed!\n')
    raise SystemExit(1)

## 13.3 parsing command-line options(argparse)

## 13.4 prompting for a password at runtime(getpass)

## 13.5 Getting the erminal size(os.get_terminal_size(), python 3)

## 13.6 Executing an external command and getting its output

* check_output
* popen

If the executed command returns a nonzero exit code, an exception is raised. Here is
an example of catching errors and getting the output created along with the exit code:

    try:
        out_bytes = subprocess.check_output(['cmd','arg1','arg2'])
    except subprocess.CalledProcessError as e:
        out_bytes = e.output # Output generated before error
        code = e.returncode # Return code

By default, check_output() only returns output written to standard output. If you want
both standard output and error collected, use the stderr argument:

    out_bytes = subprocess.check_output(['cmd','arg1','arg2'], stderr=subprocess.STDOUT)

If you need to execute a command with a timeout, use the timeout argument:

    try:
        out_bytes = subprocess.check_output(['cmd','arg1','arg2'], timeout=5)
    except subprocess.TimeoutExpired as e:
        ...

## 13.7 Copy or moving files and directories(shutil)

## 13.8 creating and unpacking archives(shutil)

* make_archive
* unpack_archive
* get_archive_formats


        In [5]: shutil.get_archive_formats()
        Out[5]: [('bztar', "bzip2'ed tar-file"), ('gztar', "gzip'ed tar-file"), ('tar', 'uncompressed tar file'), ('zip', 'ZIP file')]

        In [8]: shutil.make_archive('test', 'gztar', '.')
        Out[8]: '/home/rds-user/.github/python_recipes/cookbook/chapter1/test.tar.gz'

## 13.10 reading configuration files(configparser)

## 13.11 Adding Logging to Simple Scripts (logging)

## 13.12 Adding Logging to libraries

    log = logging.getLogger(__name__)
    log.addHandler(logging.NullHandler())

## 13.13 Making a Stopwatch Timer(time)

    with Timer() as t2:
        countdown(100000)
    print(t2.elapsed)

## 13.14 Putting Limits on Memory and CPU Usage

The resource module can be used to perform both tasks.

## 13.15 Lanching a web browser

    import webbrowser
    webbrowser.open('http://www.python.org')
