#!/usr/bin/python
# -*- coding: UTF-8 -*-


def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)


def main():
    """main function"""
    a = [1, 5, 2, 1, 9, 1, 5, 10]
    print list(dedupe(a))


if __name__ == '__main__':
    main()
