#!/usr/bin/python3.2
#-*- coding: UTF-8 -*-

import sys


record = ('ACME', 50, 123.45, (12, 18, 2012))

name, *, (*, year) = record
