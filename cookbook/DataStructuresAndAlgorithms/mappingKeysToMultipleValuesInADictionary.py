#!/usr/bin/python
# -*- coding: UTF-8 -*-


from collections import defaultdict
import pprint


def main():
    """main function"""

    pairs = {'{0}'.format(i): i*i for i in range(5)}
    #d = {}
    #for key, value in pairs:
    #    if key not in d:
    #        d[key] = []
    #    d[key].append(value)

    #pprint.pprint(d)

    d = defaultdict(list)
    for key, value in pairs.iteritems():
        d[key].append(value)

    pprint.pprint(d)


if __name__ == '__main__':
    main()
