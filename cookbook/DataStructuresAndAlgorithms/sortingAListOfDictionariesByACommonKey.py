#!/usr/bin/python
# -*- coding: UTF-8 -*-

from operator import itemgetter
import pprint


rows = [
        {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
        {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
        {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
        {'fname': 'Big', 'lname': 'Jones', 'uid': 1004},
        ]

# It's fairly easy to output these rows ordered by any of the fields common to
# all of the dictionaries

rows_by_fname = sorted(rows, key=itemgetter('fname'))
rows_by_uid = sorted(rows, key=itemgetter('uid'))

# The functionality of itemgetter() is sometimes replaced by lambda
# expressions, for example:
#
rows_by_f_name = sorted(rows, key=lambda r: r['fname'])
rows_by_l_name = sorted(rows, key=lambda r: (r['lname'], r['fname']))

pprint.pprint(rows_by_f_name)
pprint.pprint(rows_by_l_name)
