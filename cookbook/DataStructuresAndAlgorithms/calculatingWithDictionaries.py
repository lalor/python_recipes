#!/usr/bin/python
# -*- coding: UTF-8 -*-

import pprint


prices = {
        'ACME': 45.23,
        'APPL': 612.78,
        'IBM': 205.55,
        'HPQ': 37.20,
        'FB': 10.75
        }


print 'min value:'
min_price = min(zip(prices.values(), prices.keys()))
print min_price

print 'max value:'
max_price = max(zip(prices.values(), prices.keys()))

print max_price

print 'sorted prices:'
prices_sorted = sorted(zip(prices.values(), prices.keys()))
pprint.pprint(prices_sorted)


print '*' * 80

print min(prices, key=lambda k: prices[k])
print max(prices, key=lambda k: prices[k])

