#!/usr/bin/python
# -*- coding: UTF-8 -*-

import heapq
import pprint


class PriorityQueue():
    def __init__(self):
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        return heapq.heappop(self._queue)[-1]

    def __len__(self):
        return len(self._queue)


class Item:
    def __init__(self, name):
        self._name = name

    def __repr__(self):
        return 'Item({!r})'.format(self._name)


def main():
    q = PriorityQueue()
    q.push(Item('foo'), 1)
    q.push(Item('bar'), 5)
    q.push(Item('spam'), 4)
    q.push(Item('grok'), 1)

    while q:
        pprint.pprint(q.pop())


if __name__ == '__main__':
    main()
