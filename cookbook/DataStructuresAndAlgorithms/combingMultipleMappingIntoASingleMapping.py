#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from collections import ChainMap

a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}


# just in python 3
c = ChainMap(a, b)
print(c['x'])  #Outputs 1
print(c['y'])  #Outputs 2
print(c['z'])  #Outputs 3
