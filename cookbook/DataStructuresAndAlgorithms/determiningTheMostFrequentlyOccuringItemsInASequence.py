#!/usr/bin/python
# -*- coding: UTF-8 -*-

import random
from collections import Counter


def main():
    a = []

    for i in range(100):
        a.append(str(random.randint(1, 35)))

    print a
    item_counts = Counter(a)
    top_three = item_counts.most_common(3)
    print type(top_three)
    print(top_three)


if __name__ == '__main__':
    main()
