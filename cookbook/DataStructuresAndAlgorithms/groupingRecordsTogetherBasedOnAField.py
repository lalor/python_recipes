#!/usr/bin/python
#-*- coding: UTF-8 -*-


from operator import itemgetter
from itertools import groupby

# sort by the desired field first
rows.sort(key=itemgetter('date'))

#iterate in groups
for date, items in groupby(rows, key=itemgetter('date')):
    print (date)
    for i in items:
        print('   ', i)
