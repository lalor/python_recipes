#!/usr/bin/python
# -*- coding: UTF-8 -*-

mylist = [1, 4, -5, 10, -7, 2, 3, -1]
[n for n in mylist if n > 0]

# One potential downside of using a list comprehansion is that it might product
# a large result if the original input is large. If this is a concern, you can
# use generator expressions to produce the filtered values iteratively.
#
pos = (n for n in mylist if n > 0)
print type(pos)


for x in pos:
    print x
