#!/usr/bin/python
# -*- coding: UTF-8 -*-


from collections import OrderedDict

d = OrderedDict()
d['foo'] = 1
d['bar'] = 2
d['spam'] = 3
d['grok'] = 4


for key in d.iterkeys():
    print(key, d[key])


import json
print json.dumps(d)
