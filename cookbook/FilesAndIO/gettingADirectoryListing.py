#!/usr/bin/python
#-*- coding: UTF-8 -*-

import os
import os.path

home_dir = os.path.expanduser('~')
names = os.listdir(os.path.expanduser('~'))


# get all regular files
print [ name for name in os.listdir(home_dir) if
        os.path.isfile(os.path.join(home_dir, name)) ]


# get all dirs
print [ name for name in os.listdir(home_dir) if
        os.path.isdir(os.path.join(home_dir, name)) ]

# shell files
print [ name for name in os.listdir(home_dir) if name.endswith('.sh') ]
