from __future__ import print_function
import tempfile

def main():
    tempfile.TemporaryFile

    with tempfile.TemporaryFile() as f:
        print("hello, world", file=f)



if __name__ == '__main__':
    main()
