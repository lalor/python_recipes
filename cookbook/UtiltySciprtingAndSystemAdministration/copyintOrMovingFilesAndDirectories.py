#!/usr/bin/python
#-*- coding: UTF-8 -*-

import shutil
import os
import tarfile
"""
演示了shutil的用户
"""


def main():
    """docstring for main"""

    print shutil.get_archive_formats()
    pardir = os.path.realpath(os.path.join(os.path.realpath(__file__), os.path.pardir))

    shutil.copytree(pardir, 'backup-test')
    shutil.make_archive('backup-test', 'gztar', 'backup-test')
    shutil.rmtree('backup-test')


    target = 'backup-test.tar.gz'
    if not os.path.exists(target):
        raise SystemExit('not found')

    t = tarfile.open(target, 'r')
    t.extractall('.')




if __name__ == '__main__':
    main()
