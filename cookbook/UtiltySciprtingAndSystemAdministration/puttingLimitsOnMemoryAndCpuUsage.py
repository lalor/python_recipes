#!/usr/bin/python
#-*- coding: UTF-8 -*-
import signal
import resource


def time_exceeded(signo, frame):
    """ handler"""
    print signo
    print frame
    raise SystemExit("Time's up")


def set_max_runtime(seconds):
    """docstring for set_max_runtime"""
    _, hard = resource.getrlimit(resource.RLIMIT_CPU)
    resource.setrlimit(resource.RLIMIT_CPU, (seconds, hard))
    signal.signal(signal.SIGXCPU, time_exceeded)

def main():
    """
    main function
    """
    set_max_runtime(5)
    while True:
        pass


if __name__ == '__main__':
    main()
