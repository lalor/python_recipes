#!/usr/bin/python
#-*- coding: UTF-8 -*-

import time


class Timer:

    def __init__(self, func=time.clock):
        self.elapsed = 0.0
        self._func = func
        self._start = None

    def start(self):
        if self._start is not None:
            raise RuntimeError('already started')
        self._start = self._func()

    def stop(self):
        if self._start is None:
            raise RuntimeError('not started')
        self.elapsed += self._func() - self._start
        self._start = None

    def reset(self):
        self.elapsed = 0.0

    @property
    def running(self):
        return self._start is not None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()


def countdown(n):
    while n > 0:
        n -= 1

def main():
    """docstring for main"""
    with Timer() as t:
        countdown(10000000)

    print t.elapsed

if __name__ == '__main__':
    main()
