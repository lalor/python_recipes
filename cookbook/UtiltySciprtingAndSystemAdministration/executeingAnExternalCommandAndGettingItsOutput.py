#!/usr/bin/python
#-*- coding: UTF-8 -*-

import subprocess

def exec_command(cmd):
    p = subprocess.Popen(cmd, shell=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    code = p.returncode

    print '*' * 80
    print code
    print '*' * 80
    print stdout
    print '*' * 80
    print stderr
    print '*' * 80
    


def main():
    """docstring for main"""
    cmd = "ls -al laimignxing"
    exec_command(cmd)

    cmd = "ls -al *"
    exec_command(cmd)



if __name__ == '__main__':
    main()
