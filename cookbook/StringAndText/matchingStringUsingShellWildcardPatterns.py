#!/usr/bin/python
# -*- coding: UTF-8 -*-
from fnmatch import fnmatch, fnmatchcase

print fnmatch('foo.txt', '*.txt')
print fnmatch('foo.txt', '?oo.txt')

print fnmatch('foo.txt', '*.TXT')
print fnmatchcase('foo.txt', '*.TXT')
