#!/usr/bin/python
# -*- coding: UTF-8 -*-

import re

# To perform case-insensitive text operations, you need to use the re
# module and supply thre re.IGNORE flag to various operations.
text = 'UPPER PYTHON, lower python , mixed Python'

print re.findall('python', text, flags=re.IGNORECASE)
print re.sub('python', 'java', text, flags=re.IGNORECASE)
