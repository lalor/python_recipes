#!/usr/bin/python
# -*- coding: UTF-8 -*-

import re

# This problem often arises in patterns that try to match text enclosed inside
# a pair of starting and ending delimiters.

# str_pat = re.compile(r'\"(.*)\"')
str_pat = re.compile(r'\"(.*?)\"')
text1 = 'Computer says "no."'
print str_pat.findall(text1)


text2 = 'Computer says "no.", phone says "yes."'
print str_pat.findall(text2)
