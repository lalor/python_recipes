#!/usr/bin/python
# -*- coding: UTF-8 -*-

import re


text = 'Today is 11/27/2012, PyCon starts 3/13/2013.'

ret = re.match(r'\d+/\d+/\d+', text)
print ret
print re.findall(r'\d+/\d+/\d+', text)

print '*' * 80


datepat = re.compile(r'\d+/\d+/\d+')
print datepat.match(text)
print datepat.findall(text)
