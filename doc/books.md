# books
1. learning python
2. programming python
3. python cookbook
4. core python programming
5. python pocket reference
5. python in a nutshell
6. python essential reference


# standard docs

1.  [Built-in Functions][1]
2.  python [module][2] [index][3]
3. [The Python Language References][4]




[1]:https://docs.python.org/3/library/functions.html
[2]:https://docs.python.org/3/library/index.html
[3]:https://docs.python.org/3/py-modindex.html
[4]:https://docs.python.org/3/reference/index.html#reference-index
