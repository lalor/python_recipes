### 正则表达式

Python 中有两种使用RE的方式：

1. 使用re 模块中的函数
2. 创建一个它编译的正则表达式对象`re.compile()`


**常用的正则表达式方法：**

* finditer()
* match()
* search()
* findall()

###进一步学习正则表达式
1. 《Python in a Nutshell》 charpter 9
2. 《DataCrunching》

# re

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    import re

    re_string = "{{(.*?)}}"
    some_string = "This is a string with {{words}} embedded in {{curl brackets}} to show an {{example}}"

    for match in re.findall(re_string, some_string):
        print "MATCH->", match

#re.compile

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    import re

    re_obj =re.compile("{{(.*?)}}")
    some_string = "This is a string with {{words}} embedded in {{curl brackets}} to show an {{example}}"

    for match in re_obj.findall(some_string):
        print "MATCH->", match
