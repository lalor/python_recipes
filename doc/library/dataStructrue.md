
# 常用的数据结构

1. collections
    * Counter
    * deque
    * defaultdict
    * namedtuple
    * OrderedDict
2. array
3. heapq (作用于list上)
4. bisect (作用于list上)
5. Queue
    * Queue
    * LifoQueue()
    * PriorityQueue()
