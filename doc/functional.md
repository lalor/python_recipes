# map

        >>> map(f, [1, 2, 3, 4, 5, 6, 7, 8, 9])
        [1, 4, 9, 16, 25, 36, 49, 64, 81]

# reduce

    reduce(f, [x1, x2, x3, x4]) = f(f(f(x1, x2), x3), x4)

# map & reduce

    #int(x)
    def str2int(s):
        def fn(x, y):
            return x * 10 + y
        def char2num(s):
             return {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}[s]
        return reduce(fn, map(char2num, s))


    #use labmda
    def char2num(s):
        return {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}[s]

    def str2int(s):
        return reduce(lambda x,y: x*10+y, map(char2num, s))

    #reverse compare
    def reversed_cmp(x, y):
        if x > y:
                return -1
            if x < y:
                return 1
            return 0
    def cmp_ignore_case(s1, s2):
        u1 = s1.upper()
        u2 = s2.upper()
        if u1 < u2:
            return -1
        if u1 > u2:
            return 1
        return 0

    >>> sorted([36, 5, 12, 9, 21], reversed_cmp)
    [36, 21, 12, 9, 5]
