#持久化存储

1. 提供一个类似字典和文件的对象，可以完成字符串的永久存储
	* anydbm
	* dbm
	* gdbm
	* dbhash
	* dumbdbm

2. 提供Python对象序列化或存储转换功能
	* marshal
	* pickle

3. 提供了python对象的序列化存储转换，以及类似字典和文件的对象，可以完成Python对象的永久性存储
	* shelve

# Pickle

如果希望透明地存储Python对象，而不丢失其身份和类型等信息，则需要某种形式的对象序列化：它是一个将任意复杂的对象转成对象的文本或二进制表示的过程。

pickle模块及其同类模块cPickle向Python提供了pickle支持。后者是用C编码的，它具有更好的性能。

	#!/usr/bin/python3
	#python provides a standard modules called pickle using which
	#you can store any python object in a file and then get it back later.

	import pickle

	def main():
		#the name of the file where we will store the object
		shoplistfile = 'shoplist.data'
		#the list of things to buy
		shoplist = ['apple', 'mango', 'carrot']
		people = ['lalor', 'mxl', 'zyl']

		#write to the file
		f = open(shoplistfile , 'wb')
		pickle.dump(shoplist, f) #dump the object to a file
		pickle.dump(people, f) #dump the object to a file
		f.close()

		del shoplist #destory the shoplist varibale
		#read back from the storage
		f = open(shoplistfile , 'rb')
		storedlist = pickle.load(f) #load the object from the file
		storedpeople = pickle.load(f)
		print(storedlist)
		print(storedpeople)


	if __name__ == '__main__':
		main()


# shelve
shelve 模块类似与Pickle ，但它将对象保存在一个类似字典的数据库中：

	import shelve

	obj = SomeObject()
	db = shelve.open("filename") #打开一个shelve
	db['key']= obj#将对象保存在shelve
	...
	obj = db['key']
	db.close()#关闭shelve

#marshal

类似于shelve与pickle模块，不过功能不强，我估计都没什么人用了。
