
### 常见的方法

* in
* not in
* index
    不存在返回`ValueError`错误
* find
    不存在返回-1
* startwith
* endwith
    可以使用切分技术实现这个功能
* lstrip
* rstrip
* strip
    删除空格，tab，回车，空格，换行，也可以带参数
* upper
* lower
* split
* join
* replace
    切分，strip，replace 都会创建新的字符串
* 所有适用于序列的切分操作，也都适用于string
