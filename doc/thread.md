# 相关模块

python提供了几个用于多线程编程的模块，包括thread、threading和Queue。

# 多线程编程

使用Thread类创建多线程程序的三种方法：
1. 创建一个Thread的实例，传给它一个函数
2. 创建一个Thread的实例，传给它一个可调用的类对象（__call__)
3. 从Thread派生出一个子类，创建这个子类的实例

# threading 模块

thread模块包含很对对象：
* Thread 表示一个线程的可执行对象
* Lock 锁原语
* RLock 可重入锁对象
* Condition 条件变量对象能让一个线程停下来
* Event 通用的条件变量
* Semaphore 信号量
* BoundedSemaphore 
* Timer

# Thread 类

* start() 开始线程
* run() 被重写子类覆盖，定义线程的功能函数
* join() 程序挂起，直到线程介绍
* getName() 返回线程的名字
* setName() 设置线程的名字
* isAlive() 
* isDaemon()
* setDaemon()

#  多线程编程示例
