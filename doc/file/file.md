# 文件的常用方法

通过内建函数open()或file()创建一个文件对象，执行失败，python会产生一个IOError异常，执行成功，就返回一个文件对象，所有对该文件的后续操作都将通过这个“句柄”进行。

* file.close() 关闭文件
* file.fileno() 返回一个整数，即文件描述符
* file.flush() 刷新文件的内部缓冲区
* file.isatty() 判断file是否是一个类tty设备
* file.next() 返回下一行，在没有其他行时，引发一个StopIteration异常
* file.read() 读取size个字节
* file.readline 读取一行
* file.readlines() 读取所有行
* file.seek() 移动文件指针 0代表文件开始，1代表当前位置，2，代表文件末尾
* file.tell() 返回当前文件位置
* file.truncate() 截取文件大小
* file.write() 写入字符串
* file.writelines() 向文件写入一个字符串序列

# 文件的内建属性 

* file.closed
* file.encoding
* file.mode
* file.name
* file.newlines
* file.softspace

<h1 align="center">写文件的几种方式</h1>

#one

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    l1=['hi', 'hello', 'welcome']
    f=open('f1.txt', 'w')
    for elem in l1:
        f.write(elem + '\n')
    f.close()

# two

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    l1=['hi', 'hello', 'welcome']
    f=open('f1.txt', 'w')
    s1='\n'.join(l1);
    f.write(s1);
    f.close()

# three

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    l1=['hi', 'hello', 'welcome']
    with open('f1.txt', 'w') as f:
        f.write('\n'.join(l1))

# four

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-

    l1=['hi', 'hello', 'welcome']
    f=open('f1.txt', 'w')
    l1=map(lambda x:x+'\n', l1)
    f.writelines(l1)
    f.close()
