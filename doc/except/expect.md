# Python异常

* try-expect
* try-finally

捕获异常：

	def safe_float(obj):
	    try:
	        retval = float(obj)
	    except ValueError:
	        retval = 'Could not convert non-number to float'
	    except TypeError:
	        retval = 'object type connot be converted to float'
	    return retval
	
	def safe_float(obj):
	    try:
	        retval = float(obj)
	    except (ValueError, TypeError):
	        return = 'argument must be a number or numeric string'
	    return retval


捕获所有异常：

	def safe_float(obj):
	    try:
	        retval = float(obj)
	    except Exception, e:
	        #error occurred, do something
	    return retval

# try-finally

try-finally不是用来捕捉异常的，作为替代，它常常用来维持一致的行为而无论异常是否发生。

# with

保证共享资源的唯一分配，并在任务结束时释放它

# 断言

	try:
		assert 1 == 0, 'one does not equal zero silly!'
	except AssertionError, args:
		print '%s : %s' % (args.__class__.__name__, args)