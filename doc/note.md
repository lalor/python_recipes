#
defaultDict
OrderedDict
zip
slice
Counter
Operator-itemgetter


# key syntx

1. list comprehension expression

        [ x * x for x in range(5) ]
        [ x * x for x in range(10) if x % 2 == 0 ]

2. dict comprehension expression

        { x : x*2 for x in range(10)}

    字典中读取不存在的值会出错，有三种解决办法：
    1. 用if语句进行测试
    2. 用try语句捕获错误并修复
    3. 用get方法为不存在的键提供一个默认值


3. map
4. filter

# built-in function
* pow
* abs
* round
* int
* hex
* bin
* is vs ==
