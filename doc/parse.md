# 命令行参数解析

* getopt
* optparse
* argparse

重典学习argparse 模块

#optparse

该模块已经被 Deprecated ，现在用[argparse](http://docs.python.org/3/library/optparse.html?highlight=optparse#optparse) 模块。

	#!/usr/bin/python
	#-*- coding: UTF-8 -*-
	import optparse
	p = optparse.OptionParser()

	p.add_option("-o", action="store", dest="outfile")
	p.add_option("--output", action="store", dest="outfile")


	p.add_option("-d", action="store_true", dest="debug")
	p.add_option("--debug", action="store_true", dest="debug")

	p.set_defaults(debug=False)

	opts, args = p.parse_args()
	outfile = opts.outfile

	debugmode = opts.debug



