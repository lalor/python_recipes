# 面向对象编程

1. 通过 class关键字定义类
2. 通过__init__函数初始化成员变量
3. 通过class NewObjectType(bases):实现继承
4. 查看类中的方法 dir, __dict__
5. 特殊的类属性
	* __name__ 类的名字
	* __doc__ 文档字符串
	* __bases__ 父类
	* __dict__ 类的属性
	* __module__ 类定义所在的模块
	* __class__ 示例对应的类
6. issubclass()
7. isinstance()
8. 静态成员方法@staticmethod
9. python 不支持重载
10. python 中私有成员变量或成员函数，用`__`开头
