# python

* enumerate()
* sys模块
* 实用的函数 cmp, expr, dir, help, int, len, open, str, type, id
* python语言中，对象是通过引用传递的
* python支持"多元赋值" x,y,z = 1, 2, 'a string'
* python对象
	python使用对象模型来存储数据。构造任何类型的值都是一个对象。所有的python对象都拥有三个特性(id, type, value)
* 测试两个变量是否指向同一个对象 a is b, a is not b, id(a) == id(b)
* 字符串，列表，元组统称为序列
	1. 成员关系操作符号 in, not in
	2. 连接操作符 +
	3. 重复操作符 *
	4. 切片操作符 [] [:] [::]
	5. 用步长索引来进行扩展的切片操作[::-1] [::2]
	6. python为序列类型提供了许多内建的函数
* 跟数字类型一样，字符串也是不可变的

		s = 'abcdef'
		s[2] = 'C' #将出错
		s = '%sC%s'%(s[0:2],s[3:])

* 字典
	1. 创建 {} dict(()) {}.fromkeys(())
	2. 访问 for key in dict.keys(); for key in dict
	3. 更新 dict['key'] = value
	4. 删除 del dict[]; dict.clear()
	5. 拷贝 dict1 = dict2.copy()
* 集合 set() 与 frozenset()
	与集合相关的操作有in, not in , ==, !=, <, >, |, update, add, remove, pop, clear

# 重点模块
	1. os sys
	2. 存储相关 pickle, marshal

# 函数式编程
1. lambda
    Usage: lambda 参数: expression
2. filter
    filter( lambda n: n%2, allNums)
    print [ n for n in allNums if n % 2 ]
3. map
	与filter相似，因为它也能通过函数来处理序列。然而，不像filter，map将函数调用“映射”到每个序列元素上，并返回一个含有所有返回值的列表。
	map( (lambda x : x + 2), [0,1,2,3,4,5])
	map( lambda x : x**2 , range(6))
4. reduce
	reduce (func, [1,2,3]) = func(func(1,2),3)
	reduce ((lambda x,y: x+y), range(5))
5. yield生成器
	从语法上讲，生成器是一个带yield语句的函数 

#面向对象编程

1. 通过class关键字定义类
2. 通过__init__函数初始化成员变量
3. 通过 class NewObjectType(bases)继承，并在子类中调用父类的__init__方法初始化父类的成员变量
4. 查看类的方法(1: dir 2: __dict__ )
5. 特殊的类属性
	* __name__
	* __doc__
	* __bases__
	* __dict__
	* __module__
	* __class__

6. issubclass
7. isinstance
8. 静态成员函数 @staticmethod
9. python私有成员
	Python中默认的成员函数，成员变量都是公开的(public),而且python中没有类似public,private等关键词来修饰成员函数，成员变量。在python中定义私有变量只需要在变量名或函数名前加上 ”__“两个下划线，那么这个函数或变量就会为私有的了。
