# 利用Python的SocketServer框架编写网络服务程序

# 1.前言：

虽说用Python编写简单的网络程序很方便，但复杂一点的网络程序还是用现成的框架比较好。这样就可以专心事务逻辑，而不是套接字的各种细节。SocketServer模块简化了编写网络服务程序的任务。同时SocketServer模块也是Python标准库中很多服务器框架的基础。

# 2.网络服务类：

SocketServer提供了4个基本的服务类：

* TCPServer针对TCP套接字流
* UDPServer针对UDP数据报套接字
* UnixStreamServer和UnixDatagramServer针对UNIX域套接字，不常用。

它们的继承关系如下：

	+------------+
	| BaseServer |
	+------------+
	      |
	      v
	+-----------+        +------------------+
	| TCPServer |------->| UnixStreamServer |
	+-----------+        +------------------+
	      |
	      v
	+-----------+        +--------------------+
	| UDPServer |------->| UnixDatagramServer |
	+-----------+        +--------------------+

# 2.1异步处理：


这个四个服务类都是同步处理请求的。一个请求没处理完不能处理下一个请求。要想支持异步模型，可以利用多继承让server类继承ForkingMixIn ThreadingMixIn mix-in classes
* ForkingMixIn利用多进程（分叉）实现异步
* ThreadingMixIn利用多线程实现异步。

# 3.请求处理类：

要实现一项服务，还必须派生一个handler class请求处理类，并重写父类的handle()方法。handle方法就是用来专门是处理请求的。该模块是通过服务类和请求处理类组合来处理请求的。

SocketServer模块提供的请求处理类有BaseRequestHandler，以及它的派生类StreamRequestHandler和DatagramRequestHandler。从名字看出可以
个处理流式套接字，一个处理数据报套接字。

# 4.总结用SocketServer创建一个服务的步骤：

1.创建一个request handler class（请求处理类），继承自BaseRequestHandler class并重写它的handle()方法，该方法将处理到的请求。
2.实例化一个server class对象，并将服务的地址和之前创建的request handler class传递给它。
3.调用server class对象的handle_request() 或 serve_forever()方法来开始处理请求。

一个基于SocketServer的服务器示例：

	from SocketServer import TCPServer,StreamRequestHandler
	#定义请求处理类
	class Handler(StreamRequestHandler):
	    def handle(self):
	        addr = self.request.getpeername()
	        print 'Got connection from ',addr
	        self.wfile.write('Thank you for connecting')
	server = TCPServer(('',1234), handler)  #实例化服务类对象
	server.server_forever() #开启服务
	from SocketServer import TCPServer,StreamRequestHandler
	#定义请求处理类
	class Handler(StreamRequestHandler):

		def handle(self):
			addr = self.request.getpeername()
			print 'Got connection from ',addr
			self.wfile.write('Thank you for connecting')

	server = TCPServer(('',1234), handler)	#实例化服务类对象
	server.server_forever()	#开启服务

# 4.2 socketServer 的客户端

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    from socket import *
    HOST='localhost'
    PORT=21567
    BUFSIZ=1024
    ADDR=(HOST, PORT)

    while True:
        tcpCliSock = socket(AF_INET, SOCK_STREAM)
        tcpCliSock.connect(ADDR)
        data = raw_input('>')
        if not data:
            break
        tcpCliSock.send('%s\r\n' % data)
        data = tcpCliSock.recv(BUFSIZ)
        if not data:
            break
        print data.strip()
        tcpCliSock.close()

SocketServer 的请求处理的默认行为是接受连接，得到请求，然后就关闭连接。这使得我们不能在程序运行时，一直保持连接状态，而是每次发送数据到服务器的时候都要创建一个新的套接字。

# 5.实现异步，支持多连接

前面介绍服务类时提到过，四个基本的服务类默认是同步模型的。要想支持异步可以利用多继承从ForkingMixIn 或ThreadingMixInmix-in classes和一个基本的服务类继承来定义一个支持异步的服务类。比如：

	class Server(ThreadingMixIn, TCPServer): pass

ForkingMixIn 要考虑进程间的通信。ThreadingMixIn要考虑线程访问同一变量时的同步和互斥。

一个使用了多线程处理的服务器示例：

	from SocketServer import TCPServer, ThreadingMixIn, StreamRequestHandler
	#定义支持多线程的服务类，注意是多继承
	class Server(ThreadingMixIn, TCPServer): pass
	#定义请求处理类
	class Handler(StreamRequestHandler)：
	    def handle(self):
	        addr = self.request.getpeername()
	        print 'Got connection from ',addr
	        self.wfile.write('Thank you for connection')
	server = Server(('', 1234), Handler)    #实例化服务类
	server.serve_forever()  #开启服务
	from SocketServer import TCPServer, ThreadingMixIn, StreamRequestHandler

	#定义支持多线程的服务类，注意是多继承
	class Server(ThreadingMixIn, TCPServer): pass

	#定义请求处理类
	class Handler(StreamRequestHandler)：

		def handle(self):
			addr = self.request.getpeername()
			print 'Got connection from ',addr
			self.wfile.write('Thank you for connection')

	server = Server(('', 1234), Handler)	#实例化服务类
	server.serve_forever()	#开启服务
