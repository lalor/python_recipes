# 网络编程相关模块

1. socket
2. httplib
3. ftplib FTP的所有功能
4. urllib, urllib2 位于标准库模板的最高层
5. paramkio SSH SFTP
6. twisted

# socket
1. 面向连接与面向无连接的网络编程 SOCK_STREAM, SOCK_DGRAM

# 套接字对象内建方法

1. 服务器端套接字函数

	* bind()
	* listen()
	* accept()

2. 客户端套接字函数

	* connect()
	* connect_ex()

3.公用的套接字函数

	* recv()
	* send()
	* sendall()
	* recvfrom()
	* sendto()
	* getpeername()
	* getsockname()
	* getsockopt()
	* setsockopt()
	* close()

4. 面向模块的套接字函数

	* s.setblocking()
	* s.settimeout()
	* s.gettimeout()

5.面向文件的套接字函数

	* s.fileno()
	* s.makefile()

# 服务器端示例

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    from socket import *
    from time import ctime

    HOST=''
    PORT=21567
    BUFSIZ=1024
    ADDR=(HOST, PORT)

    tcpSerSock = socket(AF_INET, SOCK_STREAM)
    tcpSerSock.bind(ADDR)
    tcpSerSock.listen(3)

    while True:
        print 'Wating for connection...'
        tcpCliSock, addr = tcpSerSock.accept()
        print '... connected from:', addr

        while True:
            data = tcpCliSock.recv(BUFSIZ)
            if not data:
                break
            tcpCliSock.send('[%s] %s' % (ctime(), data))

        tcpCliSock.close()

    tcpSerSock.close()

# 客户端示例

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    from socket import *
    HOST='localhost'
    PORT=21567
    BUFSIZ=1024
    ADDR=(HOST, PORT)

    tcpCliSock = socket(AF_INET, SOCK_STREAM)
    tcpCliSock.connect(ADDR)

    while True:
        data = raw_input('>')
        if not data:
            break
        tcpCliSock.send(data)
        data = tcpCliSock.recv(BUFSIZ)
        if not data:
            break
        print data

    tcpCliSock.close()


