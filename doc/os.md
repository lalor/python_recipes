# os 模块

对文件系统的访问大都是通过os模块来实现。os模块只是一个封装，而具体的“模块”依赖于具体的操作系统，不过我们不需要关心这些，只要导入os模块，python会为我们选择正确的模块。
另一个模块是os.path，它可以完成一些针对路径名的操作。它提供的函数可以完成管理和操作文件路径名中的各个部分，获取文件或子目录的信息。

# os
1. 文件处理
	* mkfifo()/mknod()
	* remove()/unlink()
	* rename()/renames()
	* stat()/lstat()/xstat()
	* symlink()
	* utime()
	* tmpfile()
	* walk()

2. 目录/文件夹
	* chdir()/fchdir()
	* chroot()
	* listdir()
	* getcwd()/getcwdu()
	* mkdir()/mkdirs()
	* rmdir()/removedirs()
    * os.pardir 父目录
    * os.curdir

3. 访问权限
	* access()
	* chmod()
	* chown()/lchown()
	* umask()
	* 4.文件描述符操作
	* open()
	* read()/write()
	* dup()/dup2()

4. 设备号
	* makedev()
	* major()/minor()

# os.path

1.分隔
	* basename()
	* dirname()
	* join()
	* split()
	* splitdrive()
	* splitext()

2.信息
	* getatime()
	* getctime()
	* getmtime()
	* getsize()

3.查询
	* ists()
	* abs()
	* dir()
	* file()
	* link()
	* mount()
	* mefile()


# for example

    #!/usr/bin/python
    #-*- coding: UTF-8 -*-
    import os
    for tmpdir in ('/tmp', r'c:\temp'):
        if os.path.isdir(tmpdir):
            break
        else:
            print "no temp directory available"
            tmpdir=''

    if tmpdir:
        os.chdir(tmpdir)
        print "*** current temporary directory"
        print os.getcwd()

        print '***creating example directory...'
        os.mkdir('example')
        os.chdir('example')
        cwd = os.getcwd()
        print '*** new working directory'
        print cwd

        print '*** original directory listing:'
        print os.listdir(cwd)

        print '*** creating test file...'
        fobj = open('test', 'w')
        fobj.writelines("foo")
        fobj.writelines("bar")
        fobj.close()
        print "*** updated directory listing:"
        print os.listdir(cwd)


        print "***renaming 'test' to 'filetest.txt'"
        os.rename('test', 'filetest.txt')
        print '*** updated direcotry listing:'
        print os.listdir(cwd)

        path = os.path.join(cwd, os.listdir(cwd)[0])
        print '*** full file pathname'
        print path

        print '*** (pathname, basename) =='
        print os.path.split(path)
        print '*** (filename, extension) =='
        print os.path.splitext(os.path.basename(path))

        print '*** displaying file content:'
        fobj = open(path)
        for eachline in fobj:
            print eachline
        fobj.close()

        print '*** deleting test file'
        os.remove(path)
        print '*** updated directory listing:'
        print os.listdir(cwd)
        os.chdir(os.pardir)
        print '*** deleting test directory'
        os.rmdir('example')
        print '*** DONE'
